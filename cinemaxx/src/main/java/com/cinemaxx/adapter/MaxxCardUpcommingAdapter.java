package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.adapter.FavCinemasAdapter.ViewHolder;
import com.cinemaxx.bean.MaxxCardUpCommingTo;
import com.cinemaxx.util.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MaxxCardUpcommingAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<MaxxCardUpCommingTo> dataList;
	MaxxCardUpCommingTo data;
	
	public MaxxCardUpcommingAdapter(Activity activity, ArrayList<MaxxCardUpCommingTo> dataList){
		this.activity = activity;
		this.dataList = dataList;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.maxx_card_upcoming_booking_list_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		data = dataList.get(position);
		
		holder.movieTitle = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_movie_title_tv);
		holder.cinemaTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_cinema_tv);
		holder.showTimeTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_show_time_tv);
		holder.formatTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_format_tv);
		holder.cinemaNoTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_cinema_no_tv);
		holder.seatSelectedTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_seat_selected_tv);
		holder.totalSeatTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_total_seat_tv);
		holder.pricePerTicketTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_price_per_ticket_tv);
		holder.ticketTotalTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_ticket_total_tv);
		holder.bookingFeeTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_booking_fee_tv);
		holder.discountTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_discount_tv);
		holder.totalPriceTv = (TextView) convertView.findViewById(R.id.maxx_card_upcomming_total_price_tv);
		
		if(data.getMovieNAme().trim().length() > 0){
			holder.movieTitle.setText(data.getMovieNAme().trim());
		}else{
			holder.movieTitle.setText("");
		}
		if(data.getCinemaName().trim().length() > 0){
			holder.cinemaTv.setText(data.getCinemaName().trim());
		}else{
			holder.cinemaTv.setText("");
		}
		if(data.getShowDateTime().trim().length() > 0){
			holder.showTimeTv.setText(Utils.getMaxxCardUpcomingDate(data.getShowDateTime().trim()));
		}else{
			holder.showTimeTv.setText("");
		}
		if(data.getSeatNo().trim().length() > 0){
			holder.seatSelectedTv.setText(data.getSeatNo().trim());
		}else{
			holder.seatSelectedTv.setText("");
		}
		if(data.getNoOfSeat().trim().length() > 0){
			holder.totalSeatTv.setText(data.getNoOfSeat().trim());
		}else{
			holder.totalSeatTv.setText("");
		}
		if(data.getTicketPrice().trim().length() > 0){
			holder.pricePerTicketTv.setText("Rp " + data.getTicketPrice().trim());
		}else{
			holder.pricePerTicketTv.setText("");
		}
		if(data.getTotalPrice().trim().length() > 0){
			holder.ticketTotalTv.setText("Rp " + data.getTotalPrice().trim());
		}else{
			holder.ticketTotalTv.setText("");
		}
		if(data.getBookingFee().trim().length() > 0){
			holder.bookingFeeTv.setText("Rp " + data.getBookingFee().trim());
		}else{
			holder.bookingFeeTv.setText("");
		}
		if(data.getDiscount().trim().length() > 0){
			holder.discountTv.setText("Rp " + data.getDiscount().trim());
		}else{
			holder.discountTv.setText("");
		}
		if(data.getTotalAmount().trim().length() > 0){
			holder.totalPriceTv.setText("Rp " + data.getTotalAmount().trim());
		}else{
			holder.totalPriceTv.setText("");
		}
		
		
		return convertView;
	}
	
    public class ViewHolder{
		
		TextView movieTitle, cinemaTv, showTimeTv, formatTv, cinemaNoTv, seatSelectedTv, totalSeatTv, pricePerTicketTv, 
		ticketTotalTv, bookingFeeTv, discountTv, totalPriceTv;
		
	}

}
