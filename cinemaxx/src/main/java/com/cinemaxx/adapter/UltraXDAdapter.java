package com.cinemaxx.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.cinemaxx.activity.BuyTicketsFragment;
import com.cinemaxx.activity.CinemaxxGoldFragment;
import com.cinemaxx.activity.MovieInfoFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.activity.UltraXDFragment;
import com.cinemaxx.adapter.GoldCinemasAdapter.ViewHolder;
import com.cinemaxx.bean.GoldCinemaClassTo;
import com.cinemaxx.bean.GoldCinemaTo;
import com.cinemaxx.bean.GoldMovieTo;
import com.cinemaxx.bean.GoldShowTimeTo;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;

public class UltraXDAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<GoldCinemaTo> dataList;
	UltraXDFragment fragment;
	GoldCinemaTo data;
	CinemaxxPref pref;
	
	public UltraXDAdapter(Activity activity, ArrayList<GoldCinemaTo> todayShowDetls, UltraXDFragment fragment){
		this.activity = activity;
		this.dataList = todayShowDetls;
		this.fragment = fragment;
		pref = new CinemaxxPref(activity);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		System.out.println("position/////////" + position);
		System.out.println("GoldAdapter...........................");
		ViewHolder holder;
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.inflater_gold_cinema, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
		
		data = dataList.get(position);
		
		holder.detailsListView = (ListView) convertView.findViewById(R.id.cinemaxx_gold_lv);
		holder.txtHeader = (TextView) convertView.findViewById(R.id.cinemaxx_gold_txt_header);
		
		holder.txtHeader.setText(dataList.get(position).getCinemaName());
		
		UltraXDMovieAdapter adapterg = new UltraXDMovieAdapter(activity, dataList.get(position).getMovieList(), dataList.get(position).getCinemaName(), dataList.get(position).getCityName(), fragment);
		holder.detailsListView.setAdapter(adapterg);
		holder.detailsListView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
		
		
		return convertView;
	}
	
	
	public class ViewHolder{
		TextView txtHeader;
		ListView detailsListView;
	}
	
	
}
