package com.cinemaxx.adapter;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cinemaxx.activity.BuyTicketsFragment;
import com.cinemaxx.activity.CinemasFragment;
import com.cinemaxx.activity.CinemaxxGoldFragment;
import com.cinemaxx.activity.MovieInfoFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.activity.ShowVideoActivity;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.bean.GoldCinemaClassTo;
import com.cinemaxx.bean.GoldCinemaTo;
import com.cinemaxx.bean.GoldMovieTo;
import com.cinemaxx.bean.GoldShowTimeTo;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.ImageLoader;

public class GoldMovieAdapter extends BaseAdapter {

	Activity activity;
	ArrayList<GoldMovieTo> dataList;
	CinemaxxGoldFragment fragment;
	GoldMovieTo data;
	CinemaxxPref pref;
	String cinemaName = "", cityName = "";

	public GoldMovieAdapter(Activity activity, ArrayList<GoldMovieTo> todayShowDetls, String cinemaName, String cityName, CinemaxxGoldFragment fragment) {
		this.activity = activity;
		this.dataList = todayShowDetls;
		this.fragment = fragment;
		this.cinemaName = cinemaName;
		this.cityName = cityName;
		pref = new CinemaxxPref(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		System.out.println("GoldMovieAdapter...........................");
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) activity
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.cinemaxx_gold_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		data = dataList.get(position);

		holder.cinemaNameTv = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_item_txt);
		holder.lnrTime = (LinearLayout) convertView
				.findViewById(R.id.cinemaxx_gold_lnr_time);
		holder.showTimeTv1 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_1);
		holder.showTimeTv2 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_2);
		holder.showTimeTv3 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_3);
		holder.showTimeTv4 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_4);
		holder.showTimeTv5 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_5);
		holder.showTimeTv6 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_6);
		holder.showTimeTv7 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_7);
		holder.showTimeTv8 = (TextView) convertView
				.findViewById(R.id.cinemaxx_gold_tv_8);
		holder.layout1 = (LinearLayout) convertView.findViewById(R.id.gold_layout_1);
		holder.layout2 = (LinearLayout) convertView.findViewById(R.id.gold_layout_2);
		holder.layout3 = (LinearLayout) convertView.findViewById(R.id.gold_layout_3);
		holder.layout4 = (LinearLayout) convertView.findViewById(R.id.gold_layout_4);
		holder.mainLay = (LinearLayout) convertView.findViewById(R.id.cinemaxx_gold_item_main_lay);
		holder.mainLay.setVisibility(View.GONE);


//		String showTime = "";
		ArrayList<GoldCinemaClassTo> classList = dataList.get(position)
				.getCinemaClassList();
		
		if(classList.size() > 0 && classList.get(0).getShowTimeList().size() > 0){
			holder.cinemaNameTv.setText(dataList.get(position).getMovieName());
			holder.mainLay.setVisibility(View.VISIBLE);
		}else{
			holder.mainLay.setVisibility(View.GONE);
		}
		
		for (int k = 0; k < classList.size(); k++) {
			ArrayList<GoldShowTimeTo> showList = classList.get(k)
					.getShowTimeList();
			for (int j = 0; j < showList.size(); j++) {
				
				/*showTime = showTime + showList.get(j).getShowTime();

				if (j + 1 < showList.size()) {
					showTime = showTime + "   ";
				}*/
				String showTime = "";
				showTime = showList.get(j).getShowTime();
				
				if(j == 0){
					holder.layout1.setVisibility(View.VISIBLE);
					holder.showTimeTv1.setText(showTime);
					holder.showTimeTv1.setVisibility(View.VISIBLE);
				}
				if(j == 1){
					holder.layout1.setVisibility(View.VISIBLE);
					holder.showTimeTv2.setText(showTime);
					holder.showTimeTv2.setVisibility(View.VISIBLE);
				}
				if(j == 2){
					holder.layout2.setVisibility(View.VISIBLE);
					holder.showTimeTv3.setText(showTime);
					holder.showTimeTv3.setVisibility(View.VISIBLE);
				}
				if(j == 3){
					holder.layout2.setVisibility(View.VISIBLE);
					holder.showTimeTv4.setText(showTime);
					holder.showTimeTv4.setVisibility(View.VISIBLE);
				}
				if(j == 4){
					holder.layout3.setVisibility(View.VISIBLE);
					holder.showTimeTv5.setText(showTime);
					holder.showTimeTv5.setVisibility(View.VISIBLE);
				}
				if(j == 5){
					holder.layout3.setVisibility(View.VISIBLE);
					holder.showTimeTv6.setText(showTime);
					holder.showTimeTv6.setVisibility(View.VISIBLE);
				}
				if(j == 6){
					holder.layout4.setVisibility(View.VISIBLE);
					holder.showTimeTv7.setText(showTime);
					holder.showTimeTv7.setVisibility(View.VISIBLE);
				}
				if(j == 7){
					holder.layout4.setVisibility(View.VISIBLE);
					holder.showTimeTv8.setText(showTime);
					holder.showTimeTv8.setVisibility(View.VISIBLE);
				}
			}

		}

		final int pos = position;

		holder.cinemaNameTv.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Toast.makeText(activity, "Clicked on position: " + pos, Toast.LENGTH_LONG).show();
				CinemaxxPref pref = new CinemaxxPref(activity);
				pref.setMovieId(dataList.get(pos).getMovieId());
				System.out.println("Movieid________________" + dataList.get(pos).getMovieId());

				String com = "now";
				if (dataList.get(pos).getIsComingSoon().equals("False")) {
					com = "now";
					pref.setMovieStatus("0");
				} else if (dataList.get(pos).getIsComingSoon().equals("Talse")) {
					com = "coming";
					pref.setMovieStatus("1");
				}
				int fvr = 1;
				ArrayList<String> array = new ArrayList<String>();
				Fragment fragmentCC = new MovieInfoFragment(com, array, fvr);
				FragmentContainer.getInstance().setFragment(
						CurrentFragmentSingletone.getInstance().getFragment());
				if (fragmentCC != null) {
					FragmentTransaction ft = fragment.getFragmentManager()
							.beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(
							fragmentCC);
					ft.setCustomAnimations(R.anim.enter_from_right,
							R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragmentCC, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}

			}
		});

		holder.showTimeTv1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv1.getText().toString().trim());
			}
		});
		holder.showTimeTv2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv2.getText().toString().trim());
			}
		});
		holder.showTimeTv3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv3.getText().toString().trim());
			}
		});
		holder.showTimeTv4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv4.getText().toString().trim());
			}
		});
		holder.showTimeTv5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv5.getText().toString().trim());
			}
		});
		holder.showTimeTv6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv6.getText().toString().trim());
			}
		});
		holder.showTimeTv7.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv7.getText().toString().trim());
			}
		});
		holder.showTimeTv8.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, holder.showTimeTv8.getText().toString().trim());
			}
		});

		return convertView;
	}

	public class ViewHolder {
		LinearLayout lnrTime;
		TextView cinemaNameTv, showTimeTv1, showTimeTv2, showTimeTv3, showTimeTv4, showTimeTv5, showTimeTv6, showTimeTv7, showTimeTv8;
		LinearLayout layout1, layout2, layout3, layout4, mainLay;
	}
	
	@SuppressWarnings("static-access")
	public void navigate(int posi, String time){
		String[] Time = time.split("\n");
		Fragment frag = new BuyTicketsFragment(dataList.get(posi).getMovieName().trim(), dataList.get(posi).getMovieId().trim(), cityName, cinemaName, dataList.get(posi).getCinemaClassList().get(0).getCinemaClassName().trim(), pref.getGoldDate(), "movie", Time[0]);
		FragmentContainer.getInstance().setFragment(
				CurrentFragmentSingletone.getInstance().getFragment());
		if (frag != null) {
			FragmentTransaction ft = fragment.getFragmentManager()
					.beginTransaction();
			CurrentFragmentSingletone.getInstance().setFragment(frag);
			ft.setCustomAnimations(R.anim.enter_from_right,
					R.anim.exit_to_left);
			ft.replace(R.id.frame_container, frag, "fragment");
			// Start the animated transition.
			ft.commit();

			// update selected item and title, then close the drawer

		}
//		Toast.makeText(activity, time, Toast.LENGTH_LONG).show();
	}

}
