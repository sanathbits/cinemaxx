package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.adapter.MaxxCardUpcommingAdapter.ViewHolder;
import com.cinemaxx.bean.MaxxCardUpCommingTo;
import com.cinemaxx.util.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MaxxCardHistoryAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<MaxxCardUpCommingTo> dataList;
	MaxxCardUpCommingTo data;
	
	public MaxxCardHistoryAdapter(Activity activity, ArrayList<MaxxCardUpCommingTo> dataList){
		this.activity = activity;
		this.dataList = dataList;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.maxx_card_recent_history_list_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		data = dataList.get(position);
		
		holder.dateTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_date_tv);
		holder.cinemaTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_cinema_tv);
		holder.movieTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_movie_tv);
		holder.spendTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_spend_tv);
		holder.pointUserTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_point_user_tv);
		holder.pointEarnedTv = (TextView) convertView.findViewById(R.id.maxx_card_recent_history_points_earn_tv);
		
		if(!data.getTransDateTime().equals("null") && data.getTransDateTime().trim().length() > 0){
			holder.dateTv.setText("Date: "+Utils.getMaxxCardExpiryDate(data.getTransDateTime().trim()));
		}else{
			holder.dateTv.setText("Date: ");
		}
		
		if(!data.getCinemaName().trim().equals("null") && data.getCinemaName().trim().length() > 0){
			holder.cinemaTv.setText(data.getCinemaName().trim());
		}else{
			holder.cinemaTv.setText("");
		}
		
		if(!data.getMovieNAme().trim().equals("null") && data.getMovieNAme().trim().length() > 0){
			holder.movieTv.setText(data.getMovieNAme().trim());
		}else{
			holder.movieTv.setText("");
		}
		
		if(data.getTotalAmount().trim().length() > 0){
			holder.spendTv.setText("Rp " + data.getTotalAmount().trim());
		}else{
			holder.spendTv.setText("");
		}
		
		if(data.getPointsEarned().trim().length() > 0){
			holder.pointUserTv.setText(data.getPointsRedeem().trim());
		}else{
			holder.pointUserTv.setText("");
		}
		
		if(data.getPointsEarned().trim().length() > 0){
			holder.pointEarnedTv.setText(data.getPointsEarned().trim());
		}else{
			holder.pointEarnedTv.setText("");
		}
		
		return convertView;
	}
	
    public class ViewHolder{
		
		TextView dateTv, cinemaTv, movieTv, spendTv, pointUserTv, pointEarnedTv;
		
	}

}
