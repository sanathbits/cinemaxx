package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.FavPromoFragment;
import com.cinemaxx.activity.PromoDetailsFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.PromoTo;
import com.cinemaxx.dialog.YesNoDialog;
import com.cinemaxx.listener.YesNoDialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FavPromoAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<PromoTo> dataList;
	FavPromoFragment fragment;
	PromoTo data;
	ImageLoader imageLoader;
	
	public FavPromoAdapter(Activity activity, ArrayList<PromoTo> dataList, FavPromoFragment fragment){
		
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		imageLoader = new ImageLoader(activity);
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.fav_movies_grid_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
			
		}else{
			
			holder = (ViewHolder) convertView.getTag();
			
		}
		
		data = dataList.get(position);
		final int pos = position;
		
		holder.imageView = (ImageView) convertView.findViewById(R.id.now_playing_grid_iv);
		holder.movieNameTv1 = (TextView) convertView.findViewById(R.id.now_playing_tv1);
		holder.movieNameTv2 = (TextView) convertView.findViewById(R.id.now_playing_tv2);
		holder.crossBtn = (Button) convertView.findViewById(R.id.now_playing_cross_btn);
		
		if(holder.imageView != null && data.getPromoImage().trim().length() > 0){
			imageLoader.DisplayImage(data.getPromoImage().replace(" ", "%20").trim(), holder.imageView);
		}else{
			
		}
		
		if(holder.movieNameTv1 != null && data.getPromoTitle().trim().length() > 0){
			holder.movieNameTv1.setText(data.getPromoTitle());
		}else{
			
		}
		
		holder.crossBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Toast.makeText(activity, "Clicked on position: " +pos, Toast.LENGTH_LONG).show();
                YesNoDialog dialog = new YesNoDialog(activity, "Do you want to remove from \nyour favourite list", new YesNoDialogListener() {
					
					@Override
					public void onYesClick() {
						// TODO Auto-generated method stub
						fragment.callRemoveFavorite(dataList.get(pos).getPromoId(), pos);
					}
					
					@Override
					public void onNoClick() {
						// TODO Auto-generated method stub
						
					}
				});
				dialog.show();
				
			}
		});
		
		holder.imageView.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Toast.makeText(activity, "Clicked on position: " +pos, Toast.LENGTH_LONG).show();
				CinemaxxPref pref = new CinemaxxPref(activity);
                pref.setPromoId(dataList.get(pos).getPromoId());
				
				Fragment fragmentCC = new PromoDetailsFragment();
				FragmentContainer.getInstance().setFragment(
						CurrentFragmentSingletone.getInstance().getFragment());
				if (fragmentCC != null) {
					FragmentTransaction ft = fragment.getFragmentManager()
							.beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(
							fragmentCC);
					ft.setCustomAnimations(R.anim.enter_from_right,
							R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragmentCC, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
				
			}
		});
		
		
		return convertView;
	}
	
	public class ViewHolder{
		ImageView imageView;
		TextView movieNameTv1, movieNameTv2;
		Button crossBtn;
	}

}
