package com.cinemaxx.adapter;

import java.util.ArrayList;
import java.util.Locale;
import com.cinemaxx.activity.CinemasFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.util.ImageLoader;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CinemasAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<CinemaTo> dataList;
	CinemasFragment fragment;
	ImageLoader imageLoader;
	CinemaTo data;
	private ArrayList<CinemaTo> arraylist;
	
	public CinemasAdapter(Activity activity, ArrayList<CinemaTo> dataLiat, CinemasFragment fragment){
		this.activity = activity;
		this.dataList = dataLiat;
		this.fragment = fragment;
		imageLoader = new ImageLoader(activity);
		
		this.arraylist = new ArrayList<CinemaTo>();
		this.arraylist.addAll(dataList);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.cinemas_cinema_item, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
		
		data = dataList.get(position);
		
		holder.imageIV = (ImageView) convertView.findViewById(R.id.cinemas_item_iv);
		holder.cinemaNameTv = (TextView) convertView.findViewById(R.id.cinemas_item_cinema_name_tv);
		holder.cityTv = (TextView) convertView.findViewById(R.id.cinemas_item_location_tv);
		holder.statusTv1 = (TextView) convertView.findViewById(R.id.cinemas_item_status_tv1);
		holder.statusTv2 = (TextView) convertView.findViewById(R.id.cinemas_item_status_tv2);
		holder.statusTv3 = (TextView) convertView.findViewById(R.id.cinemas_item_status_tv3);
		holder.statusTv4 = (TextView)convertView.findViewById(R.id.cinemas_item_status_tv4);
        holder.statusTv5 = (TextView)convertView.findViewById(R.id.cinemas_item_status_tv5);

		
		if(holder.imageIV != null && data.getImage().trim().length() > 0){
			imageLoader.DisplayImage(data.getImage().replace(" ", "%20").trim(), holder.imageIV);
		}else{
			
		}
		if(holder.cinemaNameTv != null && data.getCinemaName().trim().length() > 0){
			holder.cinemaNameTv.setText(data.getCinemaName().trim());
		}else{
			
		}
		if(holder.cityTv != null && data.getAddress().trim().length() > 0){
			holder.cityTv.setText(data.getAddress());
		}else{
			
		}
		
		ArrayList<CinemaClassTo> classList = data.getClassList();
		
		if(classList.size() > 0){
			if(classList.get(0).getClassName().trim().length() > 0){
				holder.statusTv1.setVisibility(View.VISIBLE);
				if(classList.get(0).getClassId().equals("1")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("2")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("3")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("4")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("5")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			holder.statusTv1.setVisibility(View.INVISIBLE);
		}
		if(classList.size() > 1){
			if(classList.get(1).getClassName().trim().length() > 0){
				holder.statusTv2.setVisibility(View.VISIBLE);
				if(classList.get(1).getClassId().equals("1")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("2")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("3")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
				else if(classList.get(1).getClassId().equals("4")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("5")){
                    holder.statusTv2.setText(classList.get(1).getClassName().trim());
                    holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
                }
			}
		}else{
			holder.statusTv2.setVisibility(View.INVISIBLE);
		}
		if(classList.size() > 2){
			if(classList.get(2).getClassName().trim().length() > 0){
				holder.statusTv3.setVisibility(View.VISIBLE);
				if(classList.get(2).getClassId().equals("1")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("2")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("3")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
				else if(classList.get(2).getClassId().equals("4")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("5")){
                    holder.statusTv3.setText(classList.get(2).getClassName().trim());
                    holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
                }
			}
		}else{
			holder.statusTv3.setVisibility(View.INVISIBLE);
		}
		if(classList.size() > 3){
			if(classList.get(3).getClassName().trim().length() > 0){
				holder.statusTv4.setVisibility(View.VISIBLE);
				if(classList.get(3).getClassId().equals("1")){
					holder.statusTv4.setText(classList.get(3).getClassName().trim());
					holder.statusTv4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("2")){
					holder.statusTv4.setText(classList.get(3).getClassName().trim());
					holder.statusTv4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("3")){
					holder.statusTv4.setText(classList.get(3).getClassName().trim());
					holder.statusTv4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
				else if(classList.get(3).getClassId().equals("4")){
					holder.statusTv4.setText(classList.get(3).getClassName().trim());
					holder.statusTv4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("5")){
                    holder.statusTv4.setText(classList.get(3).getClassName().trim());
                    holder.statusTv4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
                }
			}
		}else{
			holder.statusTv4.setVisibility(View.INVISIBLE);
		}

        if(classList.size() > 4){
            if(classList.get(4).getClassName().trim().length() > 0){
                holder.statusTv5.setVisibility(View.VISIBLE);
                if(classList.get(4).getClassId().equals("1")){
                    holder.statusTv5.setText(classList.get(4).getClassName().trim());
                    holder.statusTv5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
                }else if(classList.get(4).getClassId().equals("2")){
                    holder.statusTv5.setText(classList.get(4).getClassName().trim());
                    holder.statusTv5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
                }else if(classList.get(4).getClassId().equals("3")){
                    holder.statusTv5.setText(classList.get(4).getClassName().trim());
                    holder.statusTv5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
                }
                else if(classList.get(4).getClassId().equals("4")){
                    holder.statusTv5.setText(classList.get(4).getClassName().trim());
                    holder.statusTv5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
                }else if(classList.get(4).getClassId().equals("5")){
                    holder.statusTv5.setText(classList.get(4).getClassName().trim());
                    holder.statusTv5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
                }
            }
        }else{
            holder.statusTv5.setVisibility(View.INVISIBLE);
        }
		
		
		return convertView;
	}
	
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		dataList.clear();
		if (charText.length() == 0) {
			dataList.addAll(arraylist);
		} else {
			for (CinemaTo wp : arraylist) {
				if (wp.getCityName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					dataList.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}
	
	public class ViewHolder{
		
		ImageView imageIV;
		TextView cinemaNameTv, cityTv, statusTv1, statusTv2, statusTv3,statusTv4, statusTv5;
	}
	
	
}
