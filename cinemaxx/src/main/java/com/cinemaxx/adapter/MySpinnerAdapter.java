package com.cinemaxx.adapter;

import java.util.List;

import com.cinemaxx.activity.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MySpinnerAdapter extends ArrayAdapter<String> {
    // Initialise custom font, for example:
	Context context;
    Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                        "Gotham-Thin.ttf");
    Typeface fontLight = Typeface.createFromAsset(getContext().getAssets(),
            "Gotham-Light.ttf");

    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public MySpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.context = context;
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextSize(context.getResources().getDimension(R.dimen.buy_ticket_fragment_Spinner_textsize));
        view.setTypeface(font);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTextSize(context.getResources().getDimension(R.dimen.buy_ticket_fragment_Spinner_textsize));
        view.setTypeface(fontLight);
        return view;
    }
}
