package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.bean.SeatLayoutClassListTo;
import com.cinemaxx.util.SharedPreference;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ClassListAdapter extends BaseAdapter{
	
	Activity activity;
	private ArrayList<SeatLayoutClassListTo> classList;
	private SharedPreference shpref;
	private  Dialog classDialog;
	
	public ClassListAdapter(Activity activity, ArrayList<SeatLayoutClassListTo> classList, Dialog classDialog){
		this.activity = activity;
		this.classList = classList;
		this.classDialog =classDialog;
		shpref = new SharedPreference();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return classList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater mInflater = (LayoutInflater)
                activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		
        convertView = mInflater.inflate(R.layout.class_list_item, null);
        
        TextView txtPackageName = (TextView) convertView.findViewById(R.id.txtPackageName);
        TextView txtMinNoOfSeat = (TextView) convertView.findViewById(R.id.txtMinNoOfSeat);
        txtPackageName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				shpref.set_Loyalty(activity.getApplicationContext(), "true");
				classDialog.dismiss();
			}
		});
        
        txtMinNoOfSeat.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				shpref.set_Loyalty(activity.getApplicationContext(), "true");
				classDialog.dismiss();

			}
		});
        
        txtPackageName.setText(classList.get(0).getPackageName().trim());
        txtMinNoOfSeat.setText("Minimum number of seats is " + classList.get(0).getMinNoOfSeats());
        
        return convertView;
	}

}
