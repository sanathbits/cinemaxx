package com.cinemaxx.adapter;

import java.util.ArrayList;
import com.cinemaxx.activity.PromoFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.PromoTo;
import com.cinemaxx.util.ImageLoader;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PromoAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<PromoTo> dataList;
	PromoTo data;
	PromoFragment fragment;	
	ImageLoader imageLoader;
	
	public PromoAdapter(Activity activity, ArrayList<PromoTo> dataList, PromoFragment fragment){
		
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		
		imageLoader = new ImageLoader(activity);
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.now_playing_grid_item, null);
			
			viewHolder = new ViewHolder();
			convertView.setTag(viewHolder);		
		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		data = dataList.get(position);
		
		viewHolder.imageView = (ImageView) convertView.findViewById(R.id.now_playing_grid_iv);
		viewHolder.movieNameTv1 = (TextView) convertView.findViewById(R.id.now_playing_tv1);
		viewHolder.movieNameTv2 = (TextView) convertView.findViewById(R.id.now_playing_tv2);
		
		if(viewHolder.imageView != null && data.getPromoImage().trim().length() > 0){
			imageLoader.DisplayImage(data.getPromoImage().replace(" ", "%20").trim(), viewHolder.imageView);
		}else{
			
		}
		if(viewHolder.movieNameTv1 != null && data.getPromoTitle().trim().length() > 0){
			viewHolder.movieNameTv1.setText(data.getPromoTitle().trim());
		}else{
			
		}
		
		
		return convertView;
	}
	
	
	public class ViewHolder{
		
		ImageView imageView;
		TextView movieNameTv1, movieNameTv2;
		
	}

}
