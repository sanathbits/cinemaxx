package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.CinemaInfoFragment;
import com.cinemaxx.activity.FavCinemasFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaInfoTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.dialog.YesNoDialog;
import com.cinemaxx.listener.YesNoDialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FavCinemasAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<CinemaTo> dataList;
	FavCinemasFragment fragment;
	ImageLoader imageLoader;
	CinemaTo data;
	
	public FavCinemasAdapter(Activity activity, ArrayList<CinemaTo> dataList, FavCinemasFragment fragment){
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		imageLoader = new ImageLoader(activity);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.fav_cinema_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		data = dataList.get(position);
		holder.imageView = (ImageView) convertView.findViewById(R.id.fav_cinema_item_iv);
		holder.cinemaNameTv = (TextView) convertView.findViewById(R.id.fav_cinema_item_cinema_name_tv);
		holder.statusTv1 = (TextView) convertView.findViewById(R.id.fav_cinema_item_status_tv1);
		holder.statusTv2 = (TextView) convertView.findViewById(R.id.fav_cinema_item_status_tv2);
		holder.statusTv3 = (TextView) convertView.findViewById(R.id.fav_cinema_item_status_tv3);
		holder.crossBtn = (Button) convertView.findViewById(R.id.fav_cinema_item_cross_btn);
		holder.containerLay = (LinearLayout) convertView.findViewById(R.id.fav_cinema_container_lay);
		
		if(holder.imageView != null && data.getImage().trim().length() > 0){
			imageLoader.DisplayImageCinema(data.getImage().replace(" ", "%20"), holder.imageView);
		}else{
			
		}
		if(holder.cinemaNameTv != null && data.getCinemaName().trim().length() > 0){
			holder.cinemaNameTv.setText(data.getCinemaName().trim());
		}else{
			
		}
		
		ArrayList<CinemaClassTo> classList = data.getClassList();
		
		if(classList.size() > 0){
			if(classList.get(0).getClassName().trim().length() > 0){
				holder.statusTv1.setVisibility(View.VISIBLE);
				if(classList.get(0).getClassId().equals("1")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("2")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("3")){
					holder.statusTv1.setText(classList.get(0).getClassName().trim());
					holder.statusTv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
			}
		}
		if(classList.size() > 1){
			if(classList.get(1).getClassName().trim().length() > 0){
				holder.statusTv2.setVisibility(View.VISIBLE);
				if(classList.get(1).getClassId().equals("1")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("2")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("3")){
					holder.statusTv2.setText(classList.get(1).getClassName().trim());
					holder.statusTv2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
			}
		}
		if(classList.size() > 2){
			if(classList.get(2).getClassName().trim().length() > 0){
				holder.statusTv3.setVisibility(View.VISIBLE);
				if(classList.get(2).getClassId().equals("1")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("2")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("3")){
					holder.statusTv3.setText(classList.get(2).getClassName().trim());
					holder.statusTv3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}
			}
		}
		
		final int pos = position;
		
		holder.crossBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				YesNoDialog dialog = new YesNoDialog(activity, "Do you want to remove from \nyour favourite list", new YesNoDialogListener() {
					
					@Override
					public void onYesClick() {
						// TODO Auto-generated method stub
						fragment.callRemoveFavorite(dataList.get(pos).getCinemaId(), pos);
					}
					
					@Override
					public void onNoClick() {
						// TODO Auto-generated method stub
						
					}
				});
				dialog.show();
				
			}
		});
		
		holder.containerLay.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CinemaTo cinemaTo = dataList.get(pos);
				Fragment fragmentIn = new CinemaInfoFragment(cinemaTo);
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragmentIn != null) {
					FragmentTransaction ft = FavCinemasAdapter.this.fragment.getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragmentIn);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragmentIn, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
				
			}
		});
		
		/*holder.imageView.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Fragment fragmentC = new CinemaInfoFragment(dataList.get(pos));
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragmentC != null) {
					FragmentTransaction ft = fragment.getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragmentC);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragmentC, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});*/
		
		
		
		return convertView;
	}
	
	public class ViewHolder{
		
		ImageView imageView;
		TextView cinemaNameTv, statusTv1, statusTv2, statusTv3;
		Button crossBtn;
		LinearLayout containerLay;
		
	}

}
