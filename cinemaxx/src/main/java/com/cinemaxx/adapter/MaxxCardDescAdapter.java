package com.cinemaxx.adapter;

import java.util.List;

import com.cinemaxx.activity.R;
import com.cinemaxx.bean.MaxxCardDescTo;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MaxxCardDescAdapter extends BaseAdapter {

	Activity activity;
	List<MaxxCardManuDetailsTo> specialOfferList;

	public MaxxCardDescAdapter(Activity activity,
			List<MaxxCardManuDetailsTo> specialOfferList) {
		this.activity = activity;
		this.specialOfferList = specialOfferList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return specialOfferList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater mInflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		convertView = mInflater.inflate(R.layout.maxx_card_desc_item, null);
		TextView txtPublishDate = (TextView) convertView
				.findViewById(R.id.txtPublishDate);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
		WebView webViewDes = (WebView) convertView
				.findViewById(R.id.webViewDes);

		txtPublishDate.setText(specialOfferList.get(position)
				.getMaxxCardNewPublishDate());
		txtTitle.setText(specialOfferList.get(position).getMaxxCardNewsTitle());
		webViewDes
				.loadData(specialOfferList.get(position)
						.getMaxxCardNewsDescription(),
						"text/html; charset=UTF-8", null);
		/*webViewDes.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}

		});*/
		return convertView;
	}

}
