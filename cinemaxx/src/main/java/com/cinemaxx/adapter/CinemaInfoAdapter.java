package com.cinemaxx.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.cinemaxx.activity.BuyTicketsFragment;
import com.cinemaxx.activity.CinemaInfoFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.CinemaDetailsClassTo;
import com.cinemaxx.bean.CinemaInfoTo;
import com.cinemaxx.bean.CinemaMovieTo;
import com.cinemaxx.bean.CinemaShowDateTo;
import com.cinemaxx.bean.CinemaShowTimeTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CinemaInfoAdapter extends ArrayAdapter<CinemaMovieTo> {

	Activity activity;
	ArrayList<CinemaMovieTo> dataList;
	CinemaInfoFragment fragment;
	CinemaMovieTo data;
	ImageLoader imageLoader;
	CinemaTo cinemaTo;
	String showDateDesc;
	int POSITION;

	public CinemaInfoAdapter(Activity activity, ArrayList<CinemaMovieTo> dataList, CinemaInfoFragment fragment, CinemaTo cinemaTo, String showDateDesc) {
		super(activity, R.layout.cinema_info_item, dataList);
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		this.cinemaTo = cinemaTo;
		this.showDateDesc = showDateDesc;
		imageLoader = new ImageLoader(activity);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	/*
	 * @Override public Object getItem(int position) { // TODO Auto-generated
	 * method stub return dataList.get(position); }
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.cinema_info_item, null);
			holder = new ViewHolder();

			holder.imageIV = (ImageView) convertView.findViewById(R.id.cinema_info_item_iv);
			holder.movieNameTv = (TextView) convertView.findViewById(R.id.cinema_info_item_movie_name_tv);

			holder.movieStatusTv1 = (TextView) convertView.findViewById(R.id.cinema_info_item_status_tv1);

			holder.movieTimeTv11 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv11);
			holder.movieTimeTv12 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv12);
			holder.movieTimeTv13 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv13);
			holder.movieTimeTv14 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv14);
			holder.movieTimeTv15 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv15);
			holder.movieTimeTv16 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv16);
			holder.movieTimeTv17 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv17);
			holder.movieTimeTv18 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv18);
            holder.movieTimeTv19 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv19);
            holder.movieTimeTv20 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv20);

			holder.movieStatusTv2 = (TextView) convertView.findViewById(R.id.cinema_info_item_status_tv2);

			holder.movieTimeTv21 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv21);
			holder.movieTimeTv22 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv22);
			holder.movieTimeTv23 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv23);
			holder.movieTimeTv24 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv24);
			holder.movieTimeTv25 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv25);
			holder.movieTimeTv26 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv26);
			holder.movieTimeTv27 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv27);
			holder.movieTimeTv28 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv28);
            holder.movieTimeTv29 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv29);
            holder.movieTimeTv30 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv30);

			holder.movieStatusTv3 = (TextView) convertView.findViewById(R.id.cinema_info_item_status_tv3);

			holder.movieTimeTv31 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv31);
			holder.movieTimeTv32 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv32);
			holder.movieTimeTv33 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv33);
			holder.movieTimeTv34 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv34);
			holder.movieTimeTv35 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv35);
			holder.movieTimeTv36 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv36);
			holder.movieTimeTv37 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv37);
			holder.movieTimeTv38 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv38);
            holder.movieTimeTv39 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv39);
            holder.movieTimeTv40 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv40);

			holder.movieStatusTv4 = (TextView) convertView.findViewById(R.id.cinema_info_item_status_tv4);

			holder.movieTimeTv41 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv41);
			holder.movieTimeTv42 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv42);
			holder.movieTimeTv43 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv43);
			holder.movieTimeTv44 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv44);
			holder.movieTimeTv45 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv45);
			holder.movieTimeTv46 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv46);
			holder.movieTimeTv47 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv47);
			holder.movieTimeTv48 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv48);
            holder.movieTimeTv49 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv49);
            holder.movieTimeTv50 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv50);


			holder.movieStatusTv5 = (TextView) convertView.findViewById(R.id.cinema_info_item_status_tv5);

			holder.movieTimeTv51 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv51);
			holder.movieTimeTv52 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv52);
			holder.movieTimeTv53 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv53);
			holder.movieTimeTv54 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv54);
			holder.movieTimeTv55 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv55);
			holder.movieTimeTv56 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv56);
			holder.movieTimeTv57 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv57);
			holder.movieTimeTv58 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv58);
			holder.movieTimeTv59 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv59);
			holder.movieTimeTv60 = (TextView) convertView.findViewById(R.id.cinema_info_item_time_tv60);


			holder.layout1 = (LinearLayout) convertView.findViewById(R.id.cinema_info_item_layout1);
			holder.layout2 = (LinearLayout) convertView.findViewById(R.id.cinema_info_item_layout2);
			holder.layout3 = (LinearLayout) convertView.findViewById(R.id.cinema_info_item_layout3);
			holder.layout4 = (LinearLayout) convertView.findViewById(R.id.cinema_info_item_layout4);
			holder.layout5 = (LinearLayout) convertView.findViewById(R.id.cinema_info_item_layout5);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		data = dataList.get(position);

		// holder.buyNowBtn = (Button)
		// convertView.findViewById(R.id.cinema_info_item_buy_now_btn);

		if (holder.imageIV != null && data.getThumbImage().trim().length() > 0) {
			imageLoader.DisplayImage(data.getThumbImage().trim().replace(" ", "%20"), holder.imageIV);
		} else {

		}
		if (holder.movieNameTv != null && data.getMovieName().trim().length() > 0) {
			holder.movieNameTv.setText(data.getMovieName());
		} else {

		}

		final int pos = position;
		int type = getItemViewType(position);
		System.out.println("Position: " + type + "Main: " + position);
		final ArrayList<CinemaDetailsClassTo> classList = dataList.get(type).getClassList();
		int listSize = 0;
		holder.layout1.setVisibility(View.GONE);
		holder.layout2.setVisibility(View.GONE);
		holder.layout3.setVisibility(View.GONE);
		holder.layout4.setVisibility(View.GONE);
		holder.layout5.setVisibility(View.GONE);

        holder.movieTimeTv11.setVisibility(View.GONE);
		holder.movieTimeTv12.setVisibility(View.GONE);
		holder.movieTimeTv13.setVisibility(View.GONE);
		holder.movieTimeTv14.setVisibility(View.GONE);
		holder.movieTimeTv15.setVisibility(View.GONE);
		holder.movieTimeTv16.setVisibility(View.GONE);
		holder.movieTimeTv17.setVisibility(View.GONE);
		holder.movieTimeTv18.setVisibility(View.GONE);
        holder.movieTimeTv19.setVisibility(View.GONE);
        holder.movieTimeTv20.setVisibility(View.GONE);

		holder.movieTimeTv21.setVisibility(View.GONE);
		holder.movieTimeTv22.setVisibility(View.GONE);
		holder.movieTimeTv23.setVisibility(View.GONE);
		holder.movieTimeTv24.setVisibility(View.GONE);
		holder.movieTimeTv25.setVisibility(View.GONE);
		holder.movieTimeTv26.setVisibility(View.GONE);
		holder.movieTimeTv27.setVisibility(View.GONE);
		holder.movieTimeTv28.setVisibility(View.GONE);
        holder.movieTimeTv29.setVisibility(View.GONE);
        holder.movieTimeTv30.setVisibility(View.GONE);

		holder.movieTimeTv31.setVisibility(View.GONE);
		holder.movieTimeTv32.setVisibility(View.GONE);
		holder.movieTimeTv33.setVisibility(View.GONE);
		holder.movieTimeTv34.setVisibility(View.GONE);
		holder.movieTimeTv35.setVisibility(View.GONE);
		holder.movieTimeTv36.setVisibility(View.GONE);
		holder.movieTimeTv37.setVisibility(View.GONE);
		holder.movieTimeTv38.setVisibility(View.GONE);
        holder.movieTimeTv39.setVisibility(View.GONE);
        holder.movieTimeTv40.setVisibility(View.GONE);

		holder.movieTimeTv41.setVisibility(View.GONE);
		holder.movieTimeTv42.setVisibility(View.GONE);
		holder.movieTimeTv43.setVisibility(View.GONE);
		holder.movieTimeTv44.setVisibility(View.GONE);
		holder.movieTimeTv45.setVisibility(View.GONE);
		holder.movieTimeTv46.setVisibility(View.GONE);
		holder.movieTimeTv47.setVisibility(View.GONE);
		holder.movieTimeTv48.setVisibility(View.GONE);
        holder.movieTimeTv49.setVisibility(View.GONE);
        holder.movieTimeTv50.setVisibility(View.GONE);


		holder.movieTimeTv51.setVisibility(View.GONE);
		holder.movieTimeTv52.setVisibility(View.GONE);
		holder.movieTimeTv53.setVisibility(View.GONE);
		holder.movieTimeTv54.setVisibility(View.GONE);
		holder.movieTimeTv55.setVisibility(View.GONE);
		holder.movieTimeTv56.setVisibility(View.GONE);
		holder.movieTimeTv57.setVisibility(View.GONE);
		holder.movieTimeTv58.setVisibility(View.GONE);
		holder.movieTimeTv59.setVisibility(View.GONE);
		holder.movieTimeTv60.setVisibility(View.GONE);

		for (int i = 0; i < classList.size(); i++) {
			final CinemaDetailsClassTo to = classList.get(i);

			if (to.getClassId().trim().equals("1")) {
				listSize = listSize + 1;
				holder.layout1.setVisibility(View.VISIBLE);
				if (holder.movieStatusTv1 != null && to.getClassName().trim().length() > 0) {
					holder.movieStatusTv1.setText(to.getClassName().trim());
				}
				if (to.getShowTimeList().size() > 0) {
					// holder.movieTimeTv1.setText(getTime(data.getClassList().get(0).getShowTimeList()));
					for (int j = 0; j < to.getShowTimeList().size(); j++) {

						/*
						 * showTime = showTime + showList.get(j).getShowTime();
						 * 
						 * if (j + 1 < showList.size()) { showTime = showTime +
						 * "   "; }
						 */
						String showTime = "";
						showTime = to.getShowTimeList().get(j).getScheduleTime();

						if (j == 0) {
							holder.movieTimeTv11.setText(showTime);
							holder.movieTimeTv11.setVisibility(View.VISIBLE);
						}
						if (j == 1) {
							holder.movieTimeTv12.setText(showTime);
							holder.movieTimeTv12.setVisibility(View.VISIBLE);
						}
						if (j == 2) {
							holder.movieTimeTv13.setText(showTime);
							holder.movieTimeTv13.setVisibility(View.VISIBLE);
						}
						if (j == 3) {
							holder.movieTimeTv14.setText(showTime);
							holder.movieTimeTv14.setVisibility(View.VISIBLE);
						}
						if (j == 4) {
							holder.movieTimeTv15.setText(showTime);
							holder.movieTimeTv15.setVisibility(View.VISIBLE);
						}
						if (j == 5) {
							holder.movieTimeTv16.setText(showTime);
							holder.movieTimeTv16.setVisibility(View.VISIBLE);
						}
						if (j == 6) {
							holder.movieTimeTv17.setText(showTime);
							holder.movieTimeTv17.setVisibility(View.VISIBLE);
						}
						if (j == 7) {
							holder.movieTimeTv18.setText(showTime);
							holder.movieTimeTv18.setVisibility(View.VISIBLE);
						}
                        if (j == 8) {
                            holder.movieTimeTv19.setText(showTime);
                            holder.movieTimeTv19.setVisibility(View.VISIBLE);
                        }
                        if (j == 9) {
                            holder.movieTimeTv20.setText(showTime);
                            holder.movieTimeTv20.setVisibility(View.VISIBLE);
                        }
					}
				}
			}
			if (to.getClassId().trim().equals("2")) {
				listSize = listSize + 1;
				holder.layout2.setVisibility(View.VISIBLE);
				if (holder.movieStatusTv2 != null && to.getClassName().trim().length() > 0) {
					holder.movieStatusTv2.setText(to.getClassName().trim());
				}
				if (to.getShowTimeList().size() > 0) {
					for (int j = 0; j < to.getShowTimeList().size(); j++) {

						/*
						 * showTime = showTime + showList.get(j).getShowTime();
						 * 
						 * if (j + 1 < showList.size()) { showTime = showTime +
						 * "   "; }
						 */
						String showTime = "";
						showTime = to.getShowTimeList().get(j).getScheduleTime();

						if (j == 0) {
							holder.movieTimeTv21.setText(showTime);
							holder.movieTimeTv21.setVisibility(View.VISIBLE);
						}
						if (j == 1) {
							holder.movieTimeTv22.setText(showTime);
							holder.movieTimeTv22.setVisibility(View.VISIBLE);
						}
						if (j == 2) {
							holder.movieTimeTv23.setText(showTime);
							holder.movieTimeTv23.setVisibility(View.VISIBLE);
						}
						if (j == 3) {
							holder.movieTimeTv24.setText(showTime);
							holder.movieTimeTv24.setVisibility(View.VISIBLE);
						}
						if (j == 4) {
							holder.movieTimeTv25.setText(showTime);
							holder.movieTimeTv25.setVisibility(View.VISIBLE);
						}
						if (j == 5) {
							holder.movieTimeTv26.setText(showTime);
							holder.movieTimeTv26.setVisibility(View.VISIBLE);
						}
						if (j == 6) {
							holder.movieTimeTv27.setText(showTime);
							holder.movieTimeTv27.setVisibility(View.VISIBLE);
						}
						if (j == 7) {
							holder.movieTimeTv28.setText(showTime);
							holder.movieTimeTv28.setVisibility(View.VISIBLE);
						}
						if (j == 8) {
                            holder.movieTimeTv29.setText(showTime);
                            holder.movieTimeTv29.setVisibility(View.VISIBLE);
                        }
                        if (j == 9) {
                            holder.movieTimeTv30.setText(showTime);
                            holder.movieTimeTv30.setVisibility(View.VISIBLE);
                        }

					}
				}
			}
			if (to.getClassId().trim().equals("3")) {
				listSize = listSize + 1;
				holder.layout3.setVisibility(View.VISIBLE);
				if (holder.movieStatusTv3 != null && to.getClassName().trim().length() > 0) {
					holder.movieStatusTv3.setText(to.getClassName().trim());
				}
				if (to.getShowTimeList().size() > 0) {
					for (int j = 0; j < to.getShowTimeList().size(); j++) {

						/*
						 * showTime = showTime + showList.get(j).getShowTime();
						 * 
						 * if (j + 1 < showList.size()) { showTime = showTime +
						 * "   "; }
						 */
						String showTime = "";
						showTime = to.getShowTimeList().get(j).getScheduleTime();

						if (j == 0) {
							holder.movieTimeTv31.setText(showTime);
							holder.movieTimeTv31.setVisibility(View.VISIBLE);
						}
						if (j == 1) {
							holder.movieTimeTv32.setText(showTime);
							holder.movieTimeTv32.setVisibility(View.VISIBLE);
						}
						if (j == 2) {
							holder.movieTimeTv33.setText(showTime);
							holder.movieTimeTv33.setVisibility(View.VISIBLE);
						}
						if (j == 3) {
							holder.movieTimeTv34.setText(showTime);
							holder.movieTimeTv34.setVisibility(View.VISIBLE);
						}
						if (j == 4) {
							holder.movieTimeTv35.setText(showTime);
							holder.movieTimeTv35.setVisibility(View.VISIBLE);
						}
						if (j == 5) {
							holder.movieTimeTv36.setText(showTime);
							holder.movieTimeTv36.setVisibility(View.VISIBLE);
						}
						if (j == 6) {
							holder.movieTimeTv37.setText(showTime);
							holder.movieTimeTv37.setVisibility(View.VISIBLE);
						}
						if (j == 7) {
							holder.movieTimeTv38.setText(showTime);
							holder.movieTimeTv38.setVisibility(View.VISIBLE);
						}
                        if (j == 8) {
                            holder.movieTimeTv39.setText(showTime);
                            holder.movieTimeTv39.setVisibility(View.VISIBLE);
                        }
                        if (j == 9) {
                            holder.movieTimeTv40.setText(showTime);
                            holder.movieTimeTv40.setVisibility(View.VISIBLE);
                        }

					}
				}
			}
			if (to.getClassId().trim().equals("4")) {
				listSize = listSize + 1;
				holder.layout4.setVisibility(View.VISIBLE);
				if (holder.movieStatusTv4 != null && to.getClassName().trim().length() > 0) {
					holder.movieStatusTv4.setText(to.getClassName().trim());
				}
				if (to.getShowTimeList().size() > 0) {
					for (int j = 0; j < to.getShowTimeList().size(); j++) {
						
						

						/*
						 * showTime = showTime + showList.get(j).getShowTime();
						 * 
						 * if (j + 1 < showList.size()) { showTime = showTime +
						 * "   "; }
						 */
						String showTime = "";
						showTime = to.getShowTimeList().get(j).getScheduleTime();

						if (j == 0) {
							holder.movieTimeTv41.setText(showTime);
							holder.movieTimeTv41.setVisibility(View.VISIBLE);
						}
						if (j == 1) {
							holder.movieTimeTv42.setText(showTime);
							holder.movieTimeTv42.setVisibility(View.VISIBLE);
						}
						if (j == 2) {
							holder.movieTimeTv43.setText(showTime);
							holder.movieTimeTv43.setVisibility(View.VISIBLE);
						}
						if (j == 3) {
							holder.movieTimeTv44.setText(showTime);
							holder.movieTimeTv44.setVisibility(View.VISIBLE);
						}
						if (j == 4) {
							holder.movieTimeTv45.setText(showTime);
							holder.movieTimeTv45.setVisibility(View.VISIBLE);
						}
						if (j == 5) {
							holder.movieTimeTv46.setText(showTime);
							holder.movieTimeTv46.setVisibility(View.VISIBLE);
						}
						if (j == 6) {
							holder.movieTimeTv47.setText(showTime);
							holder.movieTimeTv47.setVisibility(View.VISIBLE);
						}
						if (j == 7) {
							holder.movieTimeTv48.setText(showTime);
							holder.movieTimeTv48.setVisibility(View.VISIBLE);
						}
                        if (j == 8) {
                            holder.movieTimeTv49.setText(showTime);
                            holder.movieTimeTv49.setVisibility(View.VISIBLE);
                        }
                        if (j == 9) {
                            holder.movieTimeTv50.setText(showTime);
                            holder.movieTimeTv50.setVisibility(View.VISIBLE);
                        }
					}
				}
			}

			if (to.getClassId().trim().equals("5")) {
				listSize = listSize + 1;
				holder.layout5.setVisibility(View.VISIBLE);
				if (holder.movieStatusTv5 != null && to.getClassName().trim().length() > 0) {
					holder.movieStatusTv5.setText(to.getClassName().trim());
				}
				if (to.getShowTimeList().size() > 0) {
					for (int j = 0; j < to.getShowTimeList().size(); j++) {


						/*
						 * showTime = showTime + showList.get(j).getShowTime();
						 *
						 * if (j + 1 < showList.size()) { showTime = showTime +
						 * "   "; }
						 */
						String showTime = "";
						showTime = to.getShowTimeList().get(j).getScheduleTime();

						if (j == 0) {
							holder.movieTimeTv51.setText(showTime);
							holder.movieTimeTv51.setVisibility(View.VISIBLE);
						}
						if (j == 1) {
							holder.movieTimeTv52.setText(showTime);
							holder.movieTimeTv52.setVisibility(View.VISIBLE);
						}
						if (j == 2) {
							holder.movieTimeTv53.setText(showTime);
							holder.movieTimeTv53.setVisibility(View.VISIBLE);
						}
						if (j == 3) {
							holder.movieTimeTv54.setText(showTime);
							holder.movieTimeTv54.setVisibility(View.VISIBLE);
						}
						if (j == 4) {
							holder.movieTimeTv55.setText(showTime);
							holder.movieTimeTv55.setVisibility(View.VISIBLE);
						}
						if (j == 5) {
							holder.movieTimeTv56.setText(showTime);
							holder.movieTimeTv56.setVisibility(View.VISIBLE);
						}
						if (j == 6) {
							holder.movieTimeTv57.setText(showTime);
							holder.movieTimeTv57.setVisibility(View.VISIBLE);
						}
						if (j == 7) {
							holder.movieTimeTv58.setText(showTime);
							holder.movieTimeTv58.setVisibility(View.VISIBLE);
						}
						if (j == 8) {
							holder.movieTimeTv59.setText(showTime);
							holder.movieTimeTv59.setVisibility(View.VISIBLE);
						}
						if (j == 9) {
							holder.movieTimeTv60.setText(showTime);
							holder.movieTimeTv60.setVisibility(View.VISIBLE);
						}
					}
				}
			}

		}

		holder.movieTimeTv11.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv11.getText().toString().trim());
			}
		});
		holder.movieTimeTv12.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv12.getText().toString().trim());
			}
		});
		holder.movieTimeTv13.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv13.getText().toString().trim());
			}
		});
		holder.movieTimeTv14.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv14.getText().toString().trim());
			}
		});
		holder.movieTimeTv15.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv15.getText().toString().trim());
			}
		});
		holder.movieTimeTv16.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv16.getText().toString().trim());
			}
		});
		holder.movieTimeTv17.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv17.getText().toString().trim());
			}
		});
		holder.movieTimeTv18.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv18.getText().toString().trim());
			}
		});
        holder.movieTimeTv19.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv19.getText().toString().trim());
            }
        });
        holder.movieTimeTv20.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Regular", showDateDesc, holder.movieTimeTv20.getText().toString().trim());
            }
        });


		holder.movieTimeTv21.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv21.getText().toString().trim());
			}
		});
		holder.movieTimeTv22.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv22.getText().toString().trim());
			}
		});
		holder.movieTimeTv23.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv23.getText().toString().trim());
			}
		});
		holder.movieTimeTv24.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv24.getText().toString().trim());
			}
		});
		holder.movieTimeTv25.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv25.getText().toString().trim());
			}
		});
		holder.movieTimeTv26.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv26.getText().toString().trim());
			}
		});
		holder.movieTimeTv27.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv27.getText().toString().trim());
			}
		});
		holder.movieTimeTv28.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv28.getText().toString().trim());
			}
		});
        holder.movieTimeTv29.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv29.getText().toString().trim());
            }
        });
        holder.movieTimeTv30.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Ultra XD", showDateDesc, holder.movieTimeTv30.getText().toString().trim());
            }
        });



		holder.movieTimeTv31.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv31.getText().toString().trim());
			}
		});
		holder.movieTimeTv32.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv32.getText().toString().trim());
			}
		});
		holder.movieTimeTv33.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv33.getText().toString().trim());
			}
		});
		holder.movieTimeTv34.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv34.getText().toString().trim());
			}
		});
		holder.movieTimeTv35.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv35.getText().toString().trim());
			}
		});
		holder.movieTimeTv36.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv36.getText().toString().trim());
			}
		});
		holder.movieTimeTv37.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv37.getText().toString().trim());
			}
		});
		holder.movieTimeTv38.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv38.getText().toString().trim());
			}
		});
        holder.movieTimeTv39.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv39.getText().toString().trim());
            }
        });
        holder.movieTimeTv40.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Gold", showDateDesc, holder.movieTimeTv40.getText().toString().trim());
            }
        });


		holder.movieTimeTv41.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv41.getText().toString().trim());
			}
		});
		holder.movieTimeTv42.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv42.getText().toString().trim());
			}
		});
		holder.movieTimeTv43.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv43.getText().toString().trim());
			}
		});
		holder.movieTimeTv44.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv44.getText().toString().trim());
			}
		});
		holder.movieTimeTv45.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv45.getText().toString().trim());
			}
		});
		holder.movieTimeTv46.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv46.getText().toString().trim());
			}
		});
		holder.movieTimeTv47.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv47.getText().toString().trim());
			}
		});
		holder.movieTimeTv48.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv48.getText().toString().trim());
			}
		});
        holder.movieTimeTv49.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv49.getText().toString().trim());
            }
        });
        holder.movieTimeTv50.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("static-access")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Junior", showDateDesc, holder.movieTimeTv50.getText().toString().trim());
            }
        });



		holder.movieTimeTv51.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv11.getText().toString().trim());
			}
		});
		holder.movieTimeTv52.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv12.getText().toString().trim());
			}
		});
		holder.movieTimeTv53.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv13.getText().toString().trim());
			}
		});
		holder.movieTimeTv54.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv14.getText().toString().trim());
			}
		});
		holder.movieTimeTv55.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv15.getText().toString().trim());
			}
		});
		holder.movieTimeTv56.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv16.getText().toString().trim());
			}
		});
		holder.movieTimeTv57.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv17.getText().toString().trim());
			}
		});
		holder.movieTimeTv58.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv18.getText().toString().trim());
			}
		});
		holder.movieTimeTv59.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv19.getText().toString().trim());
			}
		});
		holder.movieTimeTv60.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				navigate(pos, dataList.get(pos).getMovieId(), "Cinemaxx Deluxe", showDateDesc, holder.movieTimeTv20.getText().toString().trim());
			}
		});

		return convertView;
	}

	@SuppressWarnings("static-access")
	public void navigate(int pos, String movieID, String cinemaType, String showDate, String time) {
		String[] Time = time.split("\n");
		Fragment frag = new BuyTicketsFragment(dataList.get(pos).getMovieName().trim(), movieID, cinemaTo.getCityName(), cinemaTo.getCinemaName(), cinemaType, showDate, "movie", Time[0]);
		FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
		if (frag != null) {
			FragmentTransaction ft = fragment.getFragmentManager().beginTransaction();
			CurrentFragmentSingletone.getInstance().setFragment(frag);
			ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
			ft.replace(R.id.frame_container, frag, "fragment");
			// Start the animated transition.
			ft.commit();

			// update selected item and title, then close the drawer
			fragment.setIndex(0);
		}
	}

	public static class ViewHolder {

		ImageView imageIV;
		TextView movieNameTv, movieStatusTv1, movieStatusTv2, movieStatusTv3, movieStatusTv4, movieStatusTv5;
		TextView movieTimeTv11, movieTimeTv12, movieTimeTv13, movieTimeTv14, movieTimeTv15, movieTimeTv16, movieTimeTv17, movieTimeTv18, movieTimeTv19, movieTimeTv20,
				movieTimeTv21, movieTimeTv22, movieTimeTv23, movieTimeTv24, movieTimeTv25, movieTimeTv26, movieTimeTv27, movieTimeTv28, movieTimeTv29, movieTimeTv30,
				movieTimeTv31, movieTimeTv32, movieTimeTv33, movieTimeTv34, movieTimeTv35, movieTimeTv36, movieTimeTv37, movieTimeTv38, movieTimeTv39, movieTimeTv40,
				movieTimeTv41, movieTimeTv42, movieTimeTv43, movieTimeTv44, movieTimeTv45, movieTimeTv46, movieTimeTv47, movieTimeTv48, movieTimeTv49, movieTimeTv50,
				movieTimeTv51, movieTimeTv52, movieTimeTv53, movieTimeTv54, movieTimeTv55, movieTimeTv56, movieTimeTv57, movieTimeTv58, movieTimeTv59, movieTimeTv60;
		// Button buyNowBtn;
		LinearLayout layout1, layout2, layout3, layout4, layout5;

	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		// POSITION = dataList.get(position).getClassList().size();
		POSITION = position;
		return POSITION;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	/*
	 * public String getTime(ArrayList<CinemaShowTimeTo> list){
	 * 
	 * String returnVal = "";
	 * 
	 * for(int i = 0; i < list.size(); i++){
	 * 
	 * CinemaShowTimeTo timeTo = list.get(i); returnVal = returnVal
	 * +" "+timeTo.getScheduleTime().trim();
	 * 
	 * }
	 * 
	 * return returnVal; }
	 */

}
