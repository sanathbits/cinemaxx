package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.bean.HistoryTo;

import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HistoryAdapter extends BaseAdapter{

	Activity activity;
	Fragment fragment;
	ArrayList<HistoryTo> dataLIst;
	HistoryTo data;
	
	public HistoryAdapter(Activity activity, Fragment fragment, ArrayList<HistoryTo> dataLIst){
		
		this.activity = activity;
		this.fragment = fragment;
		this.dataLIst = dataLIst;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataLIst.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataLIst.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.history_list_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		data = dataLIst.get(position);
		
		holder.moviewNameTv = (TextView) convertView.findViewById(R.id.history_movie_name_tv);
		holder.bookingIdTv = (TextView) convertView.findViewById(R.id.history_booking_id_tv);
		holder.showTimeTv = (TextView) convertView.findViewById(R.id.history_show_time_tv);
		
		if(holder.moviewNameTv != null && data.getMovieName().trim().length() > 0){
			holder.moviewNameTv.setText(data.getMovieName().trim());
		}else{
			holder.moviewNameTv.setText("");
		}
		if (holder.bookingIdTv != null
				&& data.getBookingId().trim().length() > 0) {
			holder.bookingIdTv.setText("Ref No: "+data.getBookingId().trim());
		} else {
			holder.bookingIdTv.setText("");
		}
		if (holder.showTimeTv != null
				&& data.getShowTime().trim().length() > 0) {
			holder.showTimeTv.setText(data.getShowTime().trim());
		} else {
			holder.showTimeTv.setText("");
		}
		
		
		return convertView;
	}
	
	public class ViewHolder{
		
		TextView moviewNameTv, bookingIdTv, showTimeTv;
		
	}

}
