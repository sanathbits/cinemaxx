package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.activity.UltraXDFragment;
import com.cinemaxx.bean.CinemaTo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UltraXDCinemaListAdapter extends BaseAdapter {

	Activity activity;
	ArrayList<CinemaTo> dataList;
	UltraXDFragment fragment;
	CinemaTo data;
	
	public UltraXDCinemaListAdapter(Activity activity, ArrayList<CinemaTo> todayShowDetls, UltraXDFragment fragment){
		this.activity = activity;
		this.dataList = todayShowDetls;
		this.fragment = fragment;
		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.gold_cinema_item, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
		
		data = dataList.get(position);
		
		holder.cinemaNameTv = (TextView) convertView.findViewById(R.id.cinema_list_name_tv);
//		holder.lnrTime = (LinearLayout) convertView.findViewById(R.id.cinemaxx_gold_lnr_time);
//		holder.showTimeTv = (TextView) convertView.findViewById(R.id.cinemaxx_gold_tv);
//		if(holder.cinemaNameTv != null && data.getCinemaName().trim().length() > 0){
//			holder.cinemaNameTv.setText(data.getCinemaName().trim());
//		}
		holder.cinemaNameTv.setText(dataList.get(position).getCinemaName());
//		String showTime = "";
//		ArrayList<GoldCinemaClassTo> classList = data.getCinemaClassList();
//		for(int i = 0; i < classList.size(); i++){
//			
//			ArrayList<GoldShowTimeTo> showList = classList.get(i).getShowTimeList();
//			for(int j = 0; j < showList.size(); j++){
//				showTime = showTime + showList.get(j).getShowTime();
//				
//				if(j+1 < showList.size()){
//					showTime = showTime + "   ";
//				}
//			}
//			
//			
//		}
//		holder.showTimeTv.setText(showTime);
		
		final int pos = position;
		
		
		
		return convertView;
	}
	
	
	public class ViewHolder{
		LinearLayout lnrTime;
		TextView cinemaNameTv, showTimeTv;
	}
	
	
}
