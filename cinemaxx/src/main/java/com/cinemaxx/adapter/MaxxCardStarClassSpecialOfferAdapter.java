package com.cinemaxx.adapter;

import java.util.List;

import com.cinemaxx.activity.R;
import com.cinemaxx.bean.MaxxCardDescTo;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MaxxCardStarClassSpecialOfferAdapter extends BaseAdapter {

	Activity activity;
	List<MaxxCardManuDetailsTo> starClassSpecialOfferList;
	ImageLoader imageLoader;

	public MaxxCardStarClassSpecialOfferAdapter(Activity activity,
			List<MaxxCardManuDetailsTo> starClassSpecialOfferList) {
		this.activity = activity;
		this.starClassSpecialOfferList = starClassSpecialOfferList;
		imageLoader = new ImageLoader(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return starClassSpecialOfferList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater mInflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		convertView = mInflater.inflate(
				R.layout.maxcard_starclassspecialoffer_item, null);
		ImageView imageView = (ImageView) convertView.findViewById(R.id.imageV);
		TextView txtHeader = (TextView) convertView.findViewById(R.id.txttitle);
		WebView offerview = (WebView) convertView.findViewById(R.id.webView2);

		// TextView txtDesc = (TextView)
		// convertView.findViewById(R.id.txtDescription);
		if (starClassSpecialOfferList.get(position).getStarClassSpecialOfferImageURL() != null
				&& !starClassSpecialOfferList.get(position)
						.getStarClassSpecialOfferImageURL().equals("")) {
			imageLoader.DisplayImage(starClassSpecialOfferList.get(position)
					.getStarClassSpecialOfferImageURL(), imageView);

		}
		txtHeader.setText(starClassSpecialOfferList.get(position)
				.getStarClassSpecialOfferTitle());

		offerview.loadData(starClassSpecialOfferList.get(position)
				.getStarClassSpecialOfferDescription(),
				"text/html; charset=UTF-8", null);
		/*offerview.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});*/
		return convertView;
	}

}
