package com.cinemaxx.adapter;

import java.util.List;

import com.cinemaxx.activity.R;
import com.cinemaxx.bean.MaxxCardDescTo;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;
import com.cinemaxx.constant.MaxxCardManuConstant;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MaxxCardSpecialOfferAdapter extends BaseAdapter {

	Activity activity;
	List<MaxxCardManuDetailsTo> specialOfferList;
	ImageLoader imageLoader;

	public MaxxCardSpecialOfferAdapter(Activity activity,
			List<MaxxCardManuDetailsTo> specialOfferList) {
		this.activity = activity;
		this.specialOfferList = specialOfferList;
		imageLoader = new ImageLoader(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return specialOfferList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater mInflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		convertView = mInflater.inflate(R.layout.max_card_specialoffer_item,
				null);
		ImageView imageView = (ImageView) convertView.findViewById(R.id.imageV);
		TextView txtHeader = (TextView) convertView.findViewById(R.id.txttitle);
		WebView offerview = (WebView) convertView.findViewById(R.id.webView1);
		System.out
				.println("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm:::::::::::::"
						+ specialOfferList.get(position)
								.getSpecialOfferImageURL());
		if (specialOfferList.get(position).getSpecialOfferImageURL() != null
				&& !specialOfferList.get(position).getSpecialOfferImageURL()
						.equals("")) {

			imageLoader.DisplayImage(specialOfferList.get(position)
					.getSpecialOfferImageURL(), imageView);
		}
		txtHeader
				.setText(specialOfferList.get(position).getSpecialOfferTitle());

		offerview
				.loadData(specialOfferList.get(position)
						.getSpecialOfferDescription(),
						"text/html; charset=UTF-8", null);
		/*offerview.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});*/
		return convertView;
	}

}
