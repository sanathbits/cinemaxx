package com.cinemaxx.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.cinemaxx.activity.ComingSoonFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.ComingSoonTo;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ComingSoonAdapter extends BaseAdapter{

	
	Activity activity;
	ArrayList<ComingSoonTo> dataList;
	ComingSoonFragment fragment;
	ComingSoonTo data;
	ImageLoader imageLoader;
	
	public ComingSoonAdapter(Activity activity, ArrayList<ComingSoonTo> dataList, ComingSoonFragment fragment){
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		imageLoader = new ImageLoader(activity);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.now_playing_grid_item, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
        	holder = (ViewHolder) convertView.getTag();
        }
		
		data = dataList.get(position);
		
		
		holder.imageIV = (ImageView) convertView.findViewById(R.id.now_playing_grid_iv);
		holder.nameTv1 = (TextView) convertView.findViewById(R.id.now_playing_tv1);
		holder.nameTv2 = (TextView) convertView.findViewById(R.id.now_playing_tv2);
		
		if(holder.imageIV != null && data.getMovieImage().trim().length() > 0){
			imageLoader.DisplayImage(data.getMovieImage().replace(" ", "%20").trim(), holder.imageIV);
		}else{
			
		}
		if(holder.nameTv1 != null && data.getMovieName().trim().length() > 0){
			holder.nameTv1.setText(data.getMovieName().trim());
		}else{
			
		}
		if(holder.nameTv2 != null && data.getMovieRating().trim().length() > 0){
			if(data.getMovieName().length() > 15){
				holder.nameTv2.setText("..("+ data.getMovieRating()+")");
			}else{
				holder.nameTv2.setText("("+ data.getMovieRating()+")");
			}
		}else{
			
		}
		
		
		return convertView;
	}
	
	public class ViewHolder{
		
		ImageView imageIV;
		TextView nameTv1, nameTv2;
	}

}
