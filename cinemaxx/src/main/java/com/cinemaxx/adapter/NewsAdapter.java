package com.cinemaxx.adapter;

import java.util.ArrayList;

import com.cinemaxx.activity.NewsFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.bean.NewsTo;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<NewsTo> dataList;
	NewsFragment fragment;
	NewsTo data;
	ImageLoader imageLoader;
	
	public NewsAdapter(Activity activity, ArrayList<NewsTo> dataList, NewsFragment fragment){
		this.activity = activity;
		this.dataList = dataList;
		this.fragment = fragment;
		
		imageLoader = new ImageLoader(activity);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		if(convertView == null){
			LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.news_list_item, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		data = dataList.get(position);
		
		holder.imageIv = (ImageView) convertView.findViewById(R.id.news_activity_news_img);
		holder.descriptionTv = (TextView) convertView.findViewById(R.id.new_activity_description_txt);
		holder.timeTv = (TextView) convertView.findViewById(R.id.news_activity_datetime_txt);
		holder.titleTv = (TextView) convertView.findViewById(R.id.new_activity_news_title_txt);
		
		if(holder.imageIv != null && data.getNewsImage().trim().length() > 0){
			imageLoader.DisplayImage(data.getNewsImage().replace(" ", "%20").trim(), holder.imageIv);
		}else{
			
		}
		if(holder.titleTv != null && data.getNewsTitle().trim().length() > 0){
			holder.titleTv.setText(data.getNewsTitle().trim());
		}else{
			
		}
		
		return convertView;
	}
	
	public class ViewHolder{
		
		ImageView imageIv;
		TextView descriptionTv, titleTv, timeTv;
		
	}
	

}
