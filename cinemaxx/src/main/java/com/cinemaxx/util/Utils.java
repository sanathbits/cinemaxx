package com.cinemaxx.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.GetChars;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static int getWidth(String layout) {

        int width = 0;

        int ctn = 0;
        String[] myWords = layout.split("#");
        if (myWords.length > 1) {
            String fStr = myWords[1];
            int length = fStr.length();
            String[] tilesArr = fStr.split("~");
            for (int i = 0; i < tilesArr.length; i++) {
                if (tilesArr[i].toString().trim().length() > 1) {
                    ctn++;
                }
            }
        }
        width = ctn;

        return width;

    }

    public static String getHtmlCode(String layout, String maxNumber) {


        String styleString = "<style> " +
                "td{ padding: 5px;\n" +
                "    position: relative;\n" +
                "    text-align: center;\n" +
                "    width: 15px;\n" +
                "    cursor: pointer; } " +
                "span{ position: absolute;\n" +
                "    top: 6px;\n" +
                "    left: 6px;\n" +
                "    font-weight: 700;\n" +
                "    font-size: 10px;\n" +
                "    width: 15px;\n" +
                "    text-align: center; }" +
                "    #menu {list-style: none;} +\n" +
                "    #menu li{display: inline;} +" +
                " </style>";


        String __layout = getLayout(layout);

        String jsString = "<script type=\"text/javascript\" language=\"javascript\"> var DoctorVisitString=\"\"; var status = 0; var __url = \"\"; function ToggleSelection(td) { var check = td.childNodes[0]; var img = td.childNodes[1]; var selChkId = check.id; var chk = document.getElementsByName(\"chk\"); var chkcnt = 0; var maxNumber = "
                + maxNumber
                + "; var selSeats = \"\"; " +
                " var __parts = img.id.split('|'); " +
                " var __name = __parts[__parts.length - 1];" +
                " if (check.checked) { check.checked = false; " +
                " __url = 'http://www.cinemaxxtheater.com/images/Seats/' + __name + '-avail-class.gif';"+
                "img.src = __url; " +
                "status--; alert(\"dis:\"+selChkId) } else if(status < maxNumber){ for (var i = 0; i < chk.length; i++) " +
                "{ if (chk[i].checked) { chkcnt++; } } alert(selChkId); " +
                " __url = 'http://www.cinemaxxtheater.com/images/Seats/' + __name + '-select-class.gif';"+
                "check.checked = true; img.src = __url; " +
                "status++; }else{alert(\"10\");} var chk = document.getElementsByName(\"chk\"); " +
                "var chkcnt = 0; for (var i = 0; i < chk.length; i++) " +
                "{ if (chk[i].checked) { chkcnt++; } } var selSeats = \"\"; " +
                "for (var i = 0; i < chk.length; i++) { if (chk[i].checked) " +
                "{ console.log(chk[i]); console.log(chk[i].id.split('_')[1]); " +
                "var hdn = document.getElementById(\"hdn_\" + chk[i].id.split('_')[1]); " +
                "console.log(hdn); if (selSeats == \"\") " +
                "{ selSeats += hdn.value; console.log(\"A\"+selSeats); } " +
                "else { selSeats += '~' + hdn.value; console.log(\"B\"+selSeats); } } } " +
                "console.log(\"C\"+selSeats); DoctorVisitString = selSeats; window.cpjs.sendToAndroid(DoctorVisitString); } " +
                "function getText() { return DoctorVisitString; } </script><style> " +
                "#menu ul{list-style: none;} #menu li{display: inline;}</style>";


        String statusString = "<ul id=\"menu\"><li><img src=\"http://www.cinemaxxtheater.com/images/seat-select-class.gif\" " +
                "width=\"17\" height=\"12\" alt=\"\">Selected</li><li>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/seat-avail-class.gif\" " +
                "width=\"17\" height=\"12\" alt=\"\">Available</li><li>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/seat-book-class.gif\" width=\"17\" height=\"12\" alt=\"\">Taken</li></ul>";


        /*String statusString = "<ul id=\"menu\"><li><span><img src=\"http://www.cinemaxxtheater.com/images/seat-select-class.gif\" " +
                "width=\"17\" height=\"12\" alt=\"\"></span>Selected</li><li><span>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/Seats/GOLD-avail-class.gif\" width=\"17\" " +
                "height=\"12\" alt=\"\"></span>Available</li><li><span>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/seat-book-class.gif\" width=\"17\" height=\"12\" alt=\"\"></span>Taken</li></ul>";*/


        String __ = "<html><head>" + styleString + jsString
                + "</head><body><div align=\"center\">" + statusString
                + __layout + "</div></body></html>";


        return __;


    }

    public static String getHtmlCodetest(String layout, int maxNumber) {


        //In asset folder custom.js

        String styleString = "<style> " +
                "td{ padding: 5px;\n" +
                "    position: relative;\n" +
                "    text-align: center;\n" +
                "    width: 15px;\n" +
                "    cursor: pointer; } " +
                "span{ position: absolute;\n" +
                "    top: 6px;\n" +
                "    left: 6px;\n" +
                "    font-weight: 700;\n" +
                "    font-size: 10px;\n" +
                "    width: 15px;\n" +
                "    text-align: center; }" +
                "    #menu {list-style: none;} +\n" +
                "    #menu li{display: inline;} +" +
                " </style>";


        /*	String jsString = "<script type=\"text/javascript\" language=\"javascript\"> var DoctorVisitString=\"\"; " +
                "var status = 0; function ToggleSelection(td) { var check = td.childNodes[0]; var img = td.childNodes[1]; " +
                "var selChkId = check.id; var chk = document.getElementsByName(\"chk\"); var chkcnt = 0; var maxNumber = "
				+ maxNumber
				+ "; var selSeats = \"\"; if (check.checked) { check.checked = false; " +
                "img.src = \"http://www.cinemaxxtheater.com/images/seat-avail-class.gif\"; " +
                "status--; alert(\"dis:\"+selChkId) } else if(status < maxNumber){ for (var i = 0; i < chk.length; i++) " +
                "{ if (chk[i].checked) { chkcnt++; } } alert(selChkId); check.checked = true; " +
                "img.src = \"http://www.cinemaxxtheater.com/images/seat-select-class.gif\"; status++; }else{alert(\"10\");} " +
                "var chk = document.getElementsByName(\"chk\"); var chkcnt = 0; for (var i = 0; i < chk.length; i++) " +
                "{ if (chk[i].checked) { chkcnt++; } } var selSeats = \"\"; for (var i = 0; i < chk.length; i++) " +
                "{ if (chk[i].checked) { console.log(chk[i]); console.log(chk[i].id.split('_')[1]); " +
                "var hdn = document.getElementById(\"hdn_\" + chk[i].id.split('_')[1]); console.log(hdn); if (selSeats == \"\") " +
                "{ selSeats += hdn.value; console.log(\"A\"+selSeats); } " +
                "else { selSeats += '~' + hdn.value; console.log(\"B\"+selSeats); } } } " +
                "console.log(\"C\"+selSeats); DoctorVisitString = selSeats; window.cpjs.sendToAndroid(DoctorVisitString); } " +
                "function getText() { return DoctorVisitString; } </script><style> #menu ul{list-style: none;} #menu li{display: inline;}</style>";
                */

        String __layout = getLayouttest(layout);

        String javascriptString = "<script type=\"text/javascript\" language=\"javascript\"> " +
                "function ToggleSelection(e){if(e.childNodes.length>1)" +
                "{var t=e.childNodes,a=t[0],i=t[1],s='SPAN'==e.lastChild.nodeName?parseInt(e.lastChild.innerHTML):'',l=e.parentNode,r=l.cells;" +
                "if('INPUT'==a.nodeName&&'IMG'==i.nodeName&&a.hasAttribute('type')&&'checkbox'==a.type&&a.hasAttribute('alt')&&a.alt.length>0)" +
                "{var n=a.id,d=a.alt,c=d.toUpperCase(),o=i;if(a.checked)if('sofabed'==d)if(s%2==0){for(var h=null,_=0;_<r.length;_++)" +
                "if('SPAN'==r[_].lastChild.nodeName&&!isNaN(parseInt(r[_].lastChild.innerHTML))&&parseInt(r[_].lastChild.innerHTML)==s-1)" +
                "{h=r[_];break}if(null!=h&&h.childNodes.length>1){var u=h.childNodes,N=u[0],m=u[1];" +
                "'INPUT'==N.nodeName&&'IMG'==m.nodeName&&N.hasAttribute('type')&&'checkbox'==N.type&&(a.checked=!1,N.checked=!1,o.src=base_url+c+avail_img_str,m.src=base_url+c+" +
                "avail_img_str,n+=','+N.id,seat_count=parseInt(seat_count)-2,'android'==" +
                "device&&alert('dis:'+n))}}else{for(var g=null,_=0;_<r.length;_++)" +
                "if('SPAN'==r[_].lastChild.nodeName&&!isNaN(parseInt(r[_].lastChild.innerHTML))" +
                "&&parseInt(r[_].lastChild.innerHTML)==s+1){g=r[_];break}if(null!=g&&g.childNodes.length>1)" +
                "{var v=g.childNodes,p=v[0],f=v[1];'INPUT'==p.nodeName&&'IMG'==f.nodeName&&p.hasAttribute('type')&&'checkbox'==p.type&&(a.checked=!1,p.checked=!1," +
                "o.src=base_url+c+avail_img_str,f.src=base_url+c+avail_img_str,n+=','+p.id,seat_count=parseInt(seat_count)-2," +
                "'android'==device&&alert('dis:'+n))}}else a.checked=!1,o.src=base_url+c+avail_img_str,seat_count=parseInt(seat_count)-1," +
                "'android'==device&&alert('dis:'+n);else if(max_seat_limit>seat_count)if('sofabed'==d)if(max_seat_limit-seat_count>=2)" +
                "if(s%2==0){for(var h=null,_=0;_<r.length;_++)if('SPAN'==r[_].lastChild.nodeName&&!isNaN(parseInt(r[_].lastChild.innerHTML))" +
                "&&parseInt(r[_].lastChild.innerHTML)==s-1){h=r[_];break}if(null!=h&&h.childNodes.length>1){var u=h.childNodes,N=u[0],m=u[1];" +
                "'INPUT'==N.nodeName&&'IMG'==m.nodeName&&N.hasAttribute('type')&&'checkbox'==N.type&&(a.checked=!0,N.checked=!0," +
                "o.src=base_url+c+sel_img_str,m.src=base_url+c+sel_img_str," +
                "n+=','+N.id,seat_count=parseInt(seat_count)+2,'android'==device&&alert(n))}}" +
                "else{for(var g=null,_=0;_<r.length;_++)if('SPAN'==r[_].lastChild.nodeName&&!isNaN(parseInt(r[_].lastChild.innerHTML))&&" +
                "parseInt(r[_].lastChild.innerHTML)==s+1){g=r[_];break}if(null!=g&&g.childNodes.length>1){var v=g.childNodes,p=v[0],f=v[1];" +
                "'INPUT'==p.nodeName&&'IMG'==f.nodeName&&p.hasAttribute('type')&&'checkbox'==p.type&&(a.checked=!0,p.checked=!0," +
                "o.src=base_url+c+sel_img_str,f.src=base_url+c+sel_img_str,n+=','+p.id,seat_count=parseInt(seat_count)+2,'android'==device&&alert(n))}}" +
                "else'android'==device&&alert(max_seat_limit);else a.checked=!0,o.src=base_url+c+sel_img_str,seat_count=parseInt(seat_count)+1," +
                "'android'==device&&alert(n);else'android'==device&&alert(max_seat_limit)}}for(var b=document.getElementsByName('chk'),I=0,_=0;_<b.length;_++)" +
                "b[_].checked&&I++;for(var k='',_=0;_<b.length;_++)if(b[_].checked){var T=document.getElementById('hdn_'+b[_].id.split('_')[1]);" +
                "k+=''==k?T.value:'~'+T.value}if(DoctorVisitString=k,'android'==device)window.cpjs.sendToAndroid(DoctorVisitString);" +
                "else{var C=k.split('~');seatCount=C.length,window.location='ios:webToNativeCall'}}function getText()" +
                "{return DoctorVisitString}var device='android',DoctorVisitString='',seat_count=0,max_seat_limit=" + maxNumber + "," +
                "base_url='http://cinemaxxtheater.com/images/Seats/',avail_img_str='-avail-class.gif'," +
                "sel_img_str='-select-class.gif'</script>";

        String statusString = "<ul id=\"menu\"><li><img src=\"http://www.cinemaxxtheater.com/images/seat-select-class.gif\" " +
                "width=\"17\" height=\"12\" alt=\"\">Selected</li><li>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/seat-avail-class.gif\" " +
                "width=\"17\" height=\"12\" alt=\"\">Available</li><li>" +
                "<img src=\"http://www.cinemaxxtheater.com/images/seat-book-class.gif\" width=\"17\" height=\"12\" alt=\"\">Taken</li></ul>";


        String __ = "<html><head>" + styleString + javascriptString
                + "</head><body><div align=\"center\">" + statusString
                + __layout + "</div></body></html>";


        return __;

    }


    //String s=	"SOFABED,0000000005#0~G~A!1!0000000005|4|0|0$G^0000000005|4|0|0^0000000005|4|0|1~A!2!0000000005|4|0|1$G^0000000005|4|0|0^0000000005|4|0|1~A!3!0000000005|4|0|2$G^0000000005|4|0|2^0000000005|4|0|3~A!4!0000000005|4|0|3$G^0000000005|4|0|2^0000000005|4|0|3~A!5!0000000005|4|0|4$G^0000000005|4|0|4^0000000005|4|0|5~A!6!0000000005|4|0|5$G^0000000005|4|0|4^0000000005|4|0|5~A!7!0000000005|4|0|6$G^0000000005|4|0|6^0000000005|4|0|7~A!8!0000000005|4|0|7$G^0000000005|4|0|6^0000000005|4|0|7~A!9!0000000005|4|0|8$G^0000000005|4|0|8^0000000005|4|0|9~A!10!0000000005|4|0|9$G^0000000005|4|0|8^0000000005|4|0|9~A!11!0000000005|4|0|10$G^0000000005|4|0|10^0000000005|4|0|11~A!12!0000000005|4|0|11$G^0000000005|4|0|10^0000000005|4|0|11~A!13!0000000005|4|0|12$G^0000000005|4|0|12^0000000005|4|0|13~A!14!0000000005|4|0|13$G^0000000005|4|0|12^0000000005|4|0|13~A!15!0000000005|4|0|14$G^0000000005|4|0|14^0000000005|4|0|15~A!16!0000000005|4|0|15$G^0000000005|4|0|14^0000000005|4|0|15@CINEMASEAT,0000000006#0~F~A!1!0000000006|3|0|0~A!2!0000000006|3|0|1~A!3!0000000006|3|0|2~A!4!0000000006|3|0|3~A!5!0000000006|3|0|4~A!6!0000000006|3|0|5~A!7!0000000006|3|0|6~A!8!0000000006|3|0|7~A!9!0000000006|3|0|8~A!10!0000000006|3|0|9~A!11!0000000006|3|0|10~A!12!0000000006|3|0|11~A!13!0000000006|3|0|12~A!14!0000000006|3|0|13~A!15!0000000006|3|0|14~A!16!0000000006|3|0|15~A!17!0000000006|3|0|16~A!18!0000000006|3|0|17#1~E~A!1!0000000006|3|1|0~A!2!0000000006|3|1|1~A!3!0000000006|3|1|2~A!4!0000000006|3|1|3~A!5!0000000006|3|1|4~A!6!0000000006|3|1|5~A!7!0000000006|3|1|6~A!8!0000000006|3|1|7~A!9!0000000006|3|1|8~A!10!0000000006|3|1|9~A!11!0000000006|3|1|10~A!12!0000000006|3|1|11~A!13!0000000006|3|1|12~A!14!0000000006|3|1|13~A!15!0000000006|3|1|14~A!16!0000000006|3|1|15~A!17!0000000006|3|1|16~A!18!0000000006|3|1|17#2~D~A!1!0000000006|3|2|0~A!2!0000000006|3|2|1~A!3!0000000006|3|2|2~A!4!0000000006|3|2|3~A!5!0000000006|3|2|4~A!6!0000000006|3|2|5~A!7!0000000006|3|2|6~A!8!0000000006|3|2|7~A!9!0000000006|3|2|8~A!10!0000000006|3|2|9~A!11!0000000006|3|2|10~A!12!0000000006|3|2|11~A!13!0000000006|3|2|12~A!14!0000000006|3|2|13~A!15!0000000006|3|2|14~A!16!0000000006|3|2|15~A!17!0000000006|3|2|16~A!18!0000000006|3|2|17@LOUNGER,0000000007#0~C~A!1!0000000007|2|0|0~A!2!0000000007|2|0|1~A!3!0000000007|2|0|2~A!4!0000000007|2|0|3~A!5!0000000007|2|0|4~A!6!0000000007|2|0|5~A!7!0000000007|2|0|6~A!8!0000000007|2|0|7~A!9!0000000007|2|0|8~A!10!0000000007|2|0|9~A!11!0000000007|2|0|10~A!12!0000000007|2|0|11~A!13!0000000007|2|0|12~A!14!0000000007|2|0|13~A!15!0000000007|2|0|14~A!16!0000000007|2|0|15@BEANBAG,0000000008#1!0000~B~A!0000008|1|0|0~A!2!0000000008|1|0|1~A!3!0000000008|1|0|2~A!4!0000000008|1|0|3~A!5!0000000008|1|0|4~A!6!0000000008|1|0|5~A!7!0000000008|1|0|6~A!8!0000000008|1|0|7~A!9!0000000008|1|0|8~A!10!0000000008|1|0|9~A!11!0000000008|1|0|10~A!12!0000000008|1|0|11#1~A~A!1!0000000008|1|1|0~A!2!0000000008|1|1|1~A!3!0000000008|1|1|2~A!4!0000000008|1|1|3~A!5!0000000008|1|1|4~A!6!0000000008|1|1|5~A!7!0000000008|1|1|6~A!8!0000000008|1|1|7~A!9!0000000008|1|1|8~A!10!0000000008|1|1|9~A!11!0000000008|1|1|10~A!12!0000000008|1|1|11@S";


    public static String getLayout(String layout) {
        int ctn = 0;
        String[] myWords = layout.split("#");
        if (myWords.length > 1) {
            String fStr = myWords[1];
            int length = fStr.length();
            String[] tilesArr = fStr.split("~");
            for (int i = 0; i < tilesArr.length; i++) {
                if (tilesArr[i].toString().trim().length() > 1) {
                    ctn++;
                }
            }
        }

        int widthVal = 0;
        if (ctn * 30 > 320) {
            widthVal = ctn * 30;
        } else {
            widthVal = 320;
        }

        StringBuilder strBSeatlayout = new StringBuilder("");
        String[] arrArea = layout.split("@");
        String[] areaDet;
        String[] areaDesc;
        String[] rowDet;
        String[] seatDet;
        String[] areaDescSeat;

        strBSeatlayout
                .append("<table width="
                        + widthVal
                        + "; id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
        strBSeatlayout.append("<thead></thead>");
        // strBSeatlayout.append("<tr bgcolor=\"#867F7F\"><th colspan=\"80\"><font color=\"#FFFFFF\">STANDARD</font></th></tr>");

        for (int k = 0; k < arrArea.length; k++) {
            if (arrArea[k] == "S") {
                strBSeatlayout.append("<tr><td colspan=" + ctn + 1
                        + ">SCREEN</td></tr>");
                strBSeatlayout
                        .append("<tr><td colspan="
                                + ctn
                                + 1
                                + " align=\"center\"><img src=\"http://www.cinemaxxtheater.com/images/Seats/screen.gif\" style=\"min-width:250px\" width=\"120%\" height=\"25\"/></td></tr>");
            } else {
                areaDet = arrArea[k].split("#");
                areaDesc = areaDet[0].split("\\|");
                areaDescSeat = new String[areaDesc.length];
                //strBSeatlayout.append("<tr><th style=\"font-family:'Gotham-Light';font-size:14px;\" colspan=" + ctn + 1 + ">");
                //strBSeatlayout.append(areaDesc[0].split(",")[0]);
                //strBSeatlayout.append("</th></tr>");


                for (int i = 1; i < areaDet.length; i++) {
                    rowDet = areaDet[i].split("~");
                    if (rowDet[0].trim() != "NA") {
                        strBSeatlayout.append("<tr><td><span>");
                        strBSeatlayout.append(rowDet[1]);
                        strBSeatlayout.append("</span></td>");
                        for (int j = 2; j < rowDet.length; j++) {
                            seatDet = rowDet[j].split("!");
                            StringBuilder __builder = new StringBuilder();
                            for (String s : areaDesc) {
                                __builder.append(s);
                            }
                            areaDescSeat[0] = __builder.toString().trim();

                            if (seatDet.length > 2) {
                                if (seatDet[2].split("\\|")[0].equals("0000000009")) {
                                    areaDescSeat[0] = "D-BOX," + "0000000009";
                                }
                            }

                            switch (seatDet[0].trim()) {
                                case "A":
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' style='display:none;'/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-avail-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "C":
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' style='display:none;'/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-select-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "O":
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-other-class.gif' />");
                                    strBSeatlayout.append("</td>");
                                    break;
                                case "B":
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-book-class.gif' />");
                                    strBSeatlayout.append("</td>");
                                    break;
                                default:
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/seat_blank.gif' /></td>");
                                    break;
                            }
                        }
                        //strBSeatlayout.append("</td>");
                    } else {
                        strBSeatlayout.append("<tr><td colspan='" + ctn + 1 + "'>&nbsp;</td><tr>");
                    }
                }
            }
        }

        strBSeatlayout.append("<tr><td colspan='100'>&nbsp;");
        strBSeatlayout.append("<tr><td colspan=\"100\" align=\"center\">SCREEN</td></tr>" +
                "<tr><td colspan=\"100\" align=\"center\"><img src=\"http://www.cinemaxxtheater.com/images/Seats/screen.gif\" style=\"min-width: 250px\"width=\"100%\" height=\"15\"></td></tr><tr><td colspan=\"100\">&nbsp;</td></tr>");
        strBSeatlayout.append("</td><tr></table>");
        return strBSeatlayout.toString();

    }


    private static String getLayouttest(String layout) {

        int ctn = 0;
        String[] myWords = layout.split("#");
        if (myWords.length > 1) {
            String fStr = myWords[1];
            int length = fStr.length();
            String[] tilesArr = fStr.split("~");
            for (int i = 0; i < tilesArr.length; i++) {
                if (tilesArr[i].toString().trim().length() > 1) {
                    ctn++;
                }
            }
        }

        int widthVal = 0;
        if (ctn * 30 > 320) {
            widthVal = ctn * 30;
        } else {
            widthVal = 320;
        }

        StringBuilder strBSeatlayout = new StringBuilder("");
        String[] arrArea = layout.split("@");
        String[] areaDet;
        String[] areaDesc;
        String[] rowDet;
        String[] seatDet;
        String[] seattype;

        strBSeatlayout
                .append("<table width="
                        + widthVal
                        + "; id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
        strBSeatlayout.append("<thead></thead>");

        for (int k = 0; k < arrArea.length - 1; k++) {
            if (arrArea[k] == "S") {
                strBSeatlayout.append("<tr><td colspan=" + ctn + 1
                        + ">SCREEN</td></tr>");
                strBSeatlayout
                        .append("<tr><td colspan="
                                + ctn
                                + 1
                                + " align=\"center\"><img src=\"http://www.cinemaxxtheater.com/images/Seats/screen.gif\" style=\"min-width:250px\" width=\"120%\" height=\"25\"/></td></tr>");
            } else {
                areaDet = arrArea[k].split("#");
                seattype = areaDet[0].split(",");
                Log.d("seat", "seat" + seattype[0]);

                areaDesc = areaDet[0].split("\\|");
                strBSeatlayout
                        .append("<tr><th style=\"font-family:'Gotham-Light';font-size:14px;padding-top:24px;\" colspan="
                                + ctn + 1 + ">");
                strBSeatlayout.append(seattype[0]);
                strBSeatlayout.append("</th></tr>");


                for (int i = 1; i < areaDet.length; i++) {
                    rowDet = areaDet[i].split("~");
                    if (rowDet[0].trim() != "NA") {
                        strBSeatlayout.append("<tr><td><span>");
                        strBSeatlayout.append(rowDet[1]);
                        strBSeatlayout.append("</span></td>");
                        for (int j = 2; j < rowDet.length; j++) {
                            seatDet = rowDet[j].split("!");


                            switch (seatDet[0].trim()) {
                                case "A":


                                    /********************************Old code start*****************************/
                                /*strBSeatlayout
								.append("<td onclick='javascript:ToggleSelection(this);'>");
								strBSeatlayout
								.append("<input type='checkbox' name='chk' id='chk_");*/

                                    /********************************Old code end*****************************/

                                    /********************************************New code start*******************************************************/


                                    strBSeatlayout.append("<td id='" + seattype[0].toLowerCase() + seatDet[1] + "' onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input alt='" + seattype[0].toLowerCase() + "' type='checkbox' name='chk' id='chk_");


                                    /***************************************************New code end************************************************/

                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout
                                            .append("' style='display:none;'/>");
                                    strBSeatlayout
                                            .append("<img src='http://www.cinemaxxtheater.com/images/Seats/" + seattype[0] + "-avail-class.gif' />");
                                    strBSeatlayout
                                            .append("<input type='hidden' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "C":
                                    strBSeatlayout
                                            .append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout
                                            .append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout
                                            .append("' style='display:none;' checked = \"checked\"/>");
                                    strBSeatlayout
                                            .append("<img src='http://www.cinemaxxtheater.com/images/Seats/" + seattype[0] + "-select-class.gif' />");
                                    strBSeatlayout
                                            .append("<input type='hidden' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "O":
                                    strBSeatlayout
                                            .append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/seat-other-class.gif' />");
                                    // strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</td>");
                                    break;
                                case "B":
                                    strBSeatlayout
                                            .append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/" + seattype[0] + "-book-class.gif' />");
                                    // strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</td>");
                                    break;
                                default:
                                    strBSeatlayout
                                            .append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/seat_blank.gif' /></td>");
                                    break;
                            }
                        }
                        strBSeatlayout.append("</td>");
                    } else {
                        strBSeatlayout.append("<tr><td colspan='" + ctn + 1
                                + "'>&nbsp;</td><tr>");
                    }
                }
            }
        }

        strBSeatlayout.append("<tr><td colspan='100'>&nbsp;");
        strBSeatlayout
                .append("<tr><td colspan=\"100\">SCREEN</td></tr><tr><td colspan=\"100\" align=\"center\"><img src=\"http://www.cinemaxxtheater.com/images/Seats/screen.gif\" style=\"min-width: 250px\"width=\"100%\" height=\"25\"></td></tr><tr><td colspan=\"100\">&nbsp;</td></tr>");
        strBSeatlayout.append("</td><tr></table>");
        return strBSeatlayout.toString();

    }


    private static String getLayoutWithDBox(String layout) {

        int ctn = 0;
        String[] myWords = layout.split("#");
        if (myWords.length > 1) {
            String fStr = myWords[1];
            int length = fStr.length();
            String[] tilesArr = fStr.split("~");
            for (int i = 0; i < tilesArr.length; i++) {
                if (tilesArr[i].toString().trim().length() > 1) {
                    ctn++;
                }
            }
        }

        int widthVal = 0;
        if (ctn * 30 > 320) {
            widthVal = ctn * 30;
        } else {
            widthVal = 320;
        }

        StringBuilder strBSeatlayout = new StringBuilder("");
        String[] arrArea = layout.split("@");
        String[] areaDet;
        String[] areaDesc;
        String[] areaDescSeat;
        String[] rowDet;
        String[] seatDet;
        String[] valueDet;


        for (int k = 0; k < arrArea.length; k++) {
            if (arrArea[k] == "S") {
                strBSeatlayout.append("<table id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
                strBSeatlayout.append("<thead></thead>");
                strBSeatlayout.append("<tr><td colspan=\"100\">SCREEN</td></tr>");
                strBSeatlayout.append("<tr><td colspan=\"100\" align=\"center\"><img src=\"http://www.cinemaxxtheater.com/images/screen.gif\" style=\"min-width:250px\" width=\"100%\" height=\"15\"/></td></tr>");
            } else {
                areaDet = arrArea[k].split("#");
                areaDesc = areaDet[0].split("\\|");
                areaDescSeat = new String[areaDesc.length];
                strBSeatlayout.append("<table id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
                strBSeatlayout.append("<thead></thead>");

                strBSeatlayout.append("<tr><th colspan=\"100\"><strong>");
                strBSeatlayout.append(areaDesc[0].split(",")[0]);
                strBSeatlayout.append("</strong></th></tr>");


                for (int i = 1; i < areaDet.length; i++) {
                    rowDet = areaDet[i].split("~");
                    if (rowDet[0].trim() != "NA") {
                        strBSeatlayout.append("<tr><td>");
                        strBSeatlayout.append(rowDet[1]);
                        strBSeatlayout.append("</td>");
                        for (int j = 2; j < rowDet.length; j++) {
                            seatDet = rowDet[j].split("!");

                            StringBuilder __builder = new StringBuilder();
                            for (String s : areaDesc) {
                                __builder.append(s);
                            }
                            areaDescSeat[0] = __builder.toString().trim();

                            if (seatDet.length > 2) {
                                if (seatDet[2].split("\\|")[0].equals("0000000009")) {
                                    areaDescSeat[0] = "D-BOX," + "0000000009";
                                }
                            }
                            switch (seatDet[0].trim()) {
                                case "A":
                                    valueDet = seatDet[2].split("^");
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' style='display:none;'/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-avail-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' name='hdn' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "C":
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' style='display:none;' checked = \"checked\"/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-select-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' name='hdn' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;

                                case "O":
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-other-class.gif' />");
                                    strBSeatlayout.append("</td>");
                                    break;
                                case "B":
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(",")[0]);
                                    strBSeatlayout.append("-book-class.gif' />");
                                    strBSeatlayout.append("</td>");
                                    break;
                                default:
                                    strBSeatlayout.append("<td><img src='http://www.cinemaxxtheater.com/images/Seats/seat_blank.gif' /></td>");
                                    break;
                            }
                        }
                        strBSeatlayout.append("</td>");
                    } else {
                        strBSeatlayout.append("<tr><td colspan='100'>&nbsp;</td><tr>");
                    }
                }
            }
            strBSeatlayout.append("<tr><td colspan='100'>&nbsp;");
            strBSeatlayout.append("</td><tr></table>");
        }
        return strBSeatlayout.toString();

		/*
		//LayOut -  AreaDesc,AreaCode#RowId~RowName~Seat~NA~Seat~..........#RowId~RowName~Seat.......@
        //          AreaDesc,AreaCode#RowId~RowName~Seat~NA~Seat~..........#RowId~RowName~Seat.......@
        //          AreaDesc,AreaCode#RowId~RowName~Seat~NA~Seat~..........#NA#RowId~RowName~Seat.......@
        //Row  - Format    - RowId~RowName~Seat~NA~Seat~..........
        //     - No Row    - NA
        //Seat - Format - Status!SeatNo!Value
        //     - Available - A!SeatNo!Value
        //     - Current   - C!SeatNo!Value
        //     - Booked    - B!SeatNo
        //     - Blocked   - O!SeatNo
        //     - No Seat   - NA

        StringBuilder strBSeatlayout = new StringBuilder("");
        string[] arrArea = layout.split('@');
        string[] areaDet;
        string[] areaDesc;
        string[] areaDescSeat;
        string[] rowDet;
        string[] seatDet;
        string[] valueDet;
        bool isSeatGroup = false;


        //        strBSeatlayout.append(@"<p style='margin: 0; font-size: .9em; padding: 5px;'>
        //                Note: Selected seats will remain blocked for 15 mins of every transaction.</p>");
        //        strBSeatlayout.append("<div class='seatLegend'>");
        //        strBSeatlayout.append("<ul>");
        //        strBSeatlayout.append("<li><span><img src='images/Seats/STANDARD-select-class.gif' width='17' height='12' alt=''/></span>Selected</li>");
        //        strBSeatlayout.append("<li><span><img src='images/Seats/STANDARD-avail-class.gif' width='17' height='12' alt=''/></span>Available</li>");
        //        strBSeatlayout.append("<li><span><img src='images/Seats/STANDARD-book-class.gif' width='17' height='12' alt=''/></span>Taken</li>");
        //        strBSeatlayout.append("</ul>");
        //        strBSeatlayout.append("</div>");
        //strBSeatlayout.append("<div class='seatTab'>");
        //strBSeatlayout.append("<table id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
        //strBSeatlayout.append("<thead></thead>");
        for (int k = 0; k < arrArea.Length; k++)
        {
            isSeatGroup = false;
            if (arrArea[k] == "S")
            {
                strBSeatlayout.append("<table id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
                strBSeatlayout.append("<thead></thead>");
                strBSeatlayout.append("<tr><td colspan=\"100\">SCREEN</td></tr>");
                strBSeatlayout.append("<tr><td colspan=\"100\" align=\"center\"><img src=\"./images/Seats/screen.gif\" style=\"min-width:250px\" width=\"100%\" height=\"15\"/></td></tr>");
            }
            else
            {
                areaDet = arrArea[k].split('#');
                areaDesc = areaDet[0].split('|');
                areaDescSeat = new string[areaDesc.Length];
                strBSeatlayout.append("<table id='tblLayout' border=\"0\" class=\"seatStbl\" cellspacing=\"0\" cellpadding=\"0\">");
                strBSeatlayout.append("<thead></thead>");
                if (k == 0)
                {
                    strBSeatlayout.append("<tr><th colspan=\"100\">");
                    strBSeatlayout.append("<div class='seatLegend'>");
                    strBSeatlayout.append("<ul>");
                    strBSeatlayout.append("<li><span><img src='images/Seats/");
                    strBSeatlayout.append(areaDesc[0].split(',')[0]);
                    strBSeatlayout.append("-select-class.gif' width='17' height='12' alt=''/></span>Selected</li>");
                    strBSeatlayout.append("<li><span><img src='images/Seats/");
                    strBSeatlayout.append(areaDesc[0].split(',')[0]);
                    strBSeatlayout.append("-avail-class.gif' width='17' height='12' alt=''/></span>Available</li>");
                    strBSeatlayout.append("<li><span><img src='images/Seats/");
                    strBSeatlayout.append(areaDesc[0].split(',')[0]);
                    strBSeatlayout.append("-book-class.gif' width='17' height='12' alt=''/></span>Taken</li>");
                    strBSeatlayout.append("</ul>");
                    strBSeatlayout.append("</div>");
                    strBSeatlayout.append("</th></tr>");
                }
                strBSeatlayout.append("<tr><th colspan=\"100\"><strong>");
                strBSeatlayout.append(areaDesc[0].split(',')[0]);
                strBSeatlayout.append("</strong></th></tr>");


                for (int i = 1; i < areaDet.Length; i++)
                {
                    rowDet = areaDet[i].split('~');
                    if (rowDet[0].Trim() != "NA")
                    {
                        strBSeatlayout.append("<tr><td>");
                        strBSeatlayout.append(rowDet[1]);
                        strBSeatlayout.append("</td>");
                        for (int j = 2; j < rowDet.Length; j++)
                        {
                            seatDet = rowDet[j].split('!');
                            areaDesc.CopyTo(areaDescSeat, 0);
                            if (seatDet.Length > 2 && seatDet[2].split('|')[0].Trim() == WebSettings.DBOXAreaCode)
                            {
                                areaDescSeat[0] = "D-BOX," + WebSettings.DBOXAreaCode;
                            }
                            switch (seatDet[0].Trim())
                            {
                                case "A":
                                    valueDet = seatDet[2].split('^');
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' style='display:none;'/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' src='./images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("-avail-class.gif' />");
                                    //strBSeatlayout.append("<img src='./images/Seats/");
                                    //strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    //strBSeatlayout.append("-avail-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' name='hdn' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "C":
                                    strBSeatlayout.append("<td onclick='javascript:ToggleSelection(this);'>");
                                    strBSeatlayout.append("<input type='checkbox' name='chk' id='chk_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' style='display:none;' checked = \"checked\"/>");
                                    strBSeatlayout.append("<img id='img_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' src='./images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("-select-class.gif' />");
                                    //strBSeatlayout.append("<img src='./images/Seats/");
                                    //strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    //strBSeatlayout.append("-select-class.gif' />");
                                    strBSeatlayout.append("<input type='hidden' name='hdn' id='hdn_");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("|");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("' value='");
                                    strBSeatlayout.append(rowDet[1]);
                                    strBSeatlayout.append("-");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(seatDet[2]);
                                    strBSeatlayout.append("$");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("'/><span>");
                                    strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</span></td>");
                                    break;
                                case "O":
                                    strBSeatlayout.append("<td><img src='./images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("-other-class.gif' />");
                                    //strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</td>");
                                    break;
                                case "B":
                                    strBSeatlayout.append("<td><img src='./images/Seats/");
                                    strBSeatlayout.append(areaDescSeat[0].split(',')[0]);
                                    strBSeatlayout.append("-book-class.gif' />");
                                    //strBSeatlayout.append(seatDet[1]);
                                    strBSeatlayout.append("</td>");
                                    break;
                                default:
                                    strBSeatlayout.append("<td><img src='./images/Seats/seat_blank.gif' /></td>");
                                    break;
                            }
                        }
                        strBSeatlayout.append("</td>");
                    }
                    else
                    {
                        strBSeatlayout.append("<tr><td colspan='100'>&nbsp;</td><tr>");
                    }
                }
            }
            strBSeatlayout.append("<tr><td colspan='100'>&nbsp;");
            strBSeatlayout.append("</td><tr></table>");
        }
        //strBSeatlayout.append("<tr><td colspan='100'>&nbsp;");
        //strBSeatlayout.append("</td><tr></table>");
        //strBSeatlayout.append("<div class='clr'></div>");
        //strBSeatlayout.append("</div>");
        return strBSeatlayout.ToString();
		 */
    }


    @SuppressLint("SimpleDateFormat")
    @SuppressWarnings("deprecation")
    public static boolean compareDate(String value) {

        String[] valueTime = value.split(" ");
        String[] valueA = valueTime[0].split("/");

        int month = 0, day = 0, year = 0;
        int hour = 0, minute = 0;
        try {
            month = Integer.parseInt(valueA[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            day = Integer.parseInt(valueA[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            year = Integer.parseInt(valueA[2]);
            year = year + 2000;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (valueTime.length > 1) {
            String[] timeAr = valueTime[1].split(":");
            try {
                hour = Integer.parseInt(timeAr[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                minute = Integer.parseInt(timeAr[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (valueTime[2].trim().equals("PM") && hour < 12) {
                    hour = hour + 12;
                }
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        Date mainDate = new Date();
        mainDate.setDate(day);
        mainDate.setMonth(month - 1);
        mainDate.setYear(year);
        // mainDate.setHours(hour); // for minute checking
        // mainDate.setMinutes(minute);

        Date currentDate = new Date();
        // DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss"); // for
        // minute checking
        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        System.out.println("Current: " + df.format(currentDate));
        System.out.println("Given: " + df.format(mainDate));

		/*
		 * if (df.format(currentDate).compareTo(df.format(mainDate)) == 0) {
		 * return true; }else if
		 * (df.format(currentDate).compareTo(df.format(mainDate)) > 0) { return
		 * false; } else if
		 * (df.format(currentDate).compareTo(df.format(mainDate)) < 0) { return
		 * true; }
		 */
        if (currentDate.equals(mainDate)) {
            return true;
        } else if (currentDate.after(mainDate)) {
            return false;
        } else if (currentDate.before(mainDate)) {
            return true;
        }

        return false;
    }

    public static String getDay(String value) {
        String day = "";

        String[] valueArr = value.split("/");
        if (valueArr.length > 0) {
            day = valueArr[1];
        }

        return day;
    }

    @SuppressWarnings("deprecation")
    public static String currentDate() {

        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

        return String.valueOf(dayOfMonth);
    }

    @SuppressWarnings("deprecation")
    public static String tomorrowDate() {

        int dateV;
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 1);
        Date date = c.getTime();
        dateV = c.get(Calendar.DAY_OF_MONTH);
        Log.d("to", "to" + String.valueOf(dateV));

        return String.valueOf(dateV);
    }

    public static String getShowDate(String val) {

        String cDate = "";

        if (val.trim().equals("Today")) {
            cDate = Utils.currentDate();
        } else if (val.trim().equals("Tomorrow")) {
            cDate = Utils.tomorrowDate();
        } else {
            String[] timeArr = val.trim().split(" ");
            if (timeArr.length > 0) {
                cDate = timeArr[0].trim();
            }
        }
        if (cDate.length() < 2) {
            cDate = "0" + cDate;

        }

        return cDate;
    }

    public static String getDateFromFormatedString(String format) {

        String returnVal = "";
        String[] valueTime = format.split(" ");
        String[] valueA = valueTime[0].split("/");

        int month = 0, day = 0;
        try {
            month = Integer.parseInt(valueA[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            day = Integer.parseInt(valueA[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (month == 1) {
            returnVal = String.valueOf(day) + " " + "Jan";
        } else if (month == 2) {
            returnVal = String.valueOf(day) + " " + "Feb";
        } else if (month == 3) {
            returnVal = String.valueOf(day) + " " + "Mar";
        } else if (month == 4) {
            returnVal = String.valueOf(day) + " " + "Apr";
        } else if (month == 5) {
            returnVal = String.valueOf(day) + " " + "May";
        } else if (month == 6) {
            returnVal = String.valueOf(day) + " " + "Jun";
        } else if (month == 7) {
            returnVal = String.valueOf(day) + " " + "Jul";
        } else if (month == 8) {
            returnVal = String.valueOf(day) + " " + "Aug";
        } else if (month == 9) {
            returnVal = String.valueOf(day) + " " + "Sep";
        } else if (month == 10) {
            returnVal = String.valueOf(day) + " " + "Oct";
        } else if (month == 11) {
            returnVal = String.valueOf(day) + " " + "Nov";
        } else if (month == 12) {
            returnVal = String.valueOf(day) + " " + "Dec";
        }

        if (currentDate().equals(String.valueOf(day))) {
            returnVal = "Today";
        } else if (tomorrowDate().equals(String.valueOf(day))) {
            returnVal = "Tomorrow";
        }
        System.out.println(currentDate()
                + "::::::::::::::::::::::::::::::::::::" + tomorrowDate());

        return returnVal;
    }

    public static void getDeviceId(Context context) {

        CinemaxxPref pref = new CinemaxxPref(context);
        // TelephonyManager mngr =
        // (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);

        String deviceId = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
        // String deviceId = mngr.getDeviceId();
        // Toast.makeText(context, deviceId, Toast.LENGTH_SHORT).show();
        pref.setDeviceId(deviceId);

    }

    public static boolean isNetworkAvailable(Activity activity) {

        ConnectivityManager cm = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                if (activeNetwork.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {

                    return true;

                }
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (activeNetwork.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {

                    return true;

                }
            }
        }
        return false;
    }

    public static boolean isValidMail(String email2) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();

		/*
		 * if (!check) { txtEmail.setError("Not Valid Email"); }
		 */
        return check;
    }

    public static Bitmap getBitmap(String encodedImage) {

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                decodedString.length);

        return decodedByte;
    }

    public static String getTncWebview(String body) {
        return "<html><head></head><body>" + body + "</body></html>";
    }

    @SuppressWarnings("deprecation")
    public static String getMaxxCardExpiryDate(String dateValue) {
        String returnValue = "";

        try {

            Date dateObj = new Date(Long.parseLong(dateValue.replaceAll("\\D+",
                    "")));

            Calendar calender = Calendar.getInstance();
            calender.setTime(dateObj);

            returnValue = getWeekDayValue(dateObj.getDay()) + ", "
                    + dateObj.getDate() + " "
                    + getMonthValue(dateObj.getMonth()) + " "
                    + calender.get(Calendar.YEAR);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    @SuppressLint("SimpleDateFormat")
    @SuppressWarnings("deprecation")
    public static String getMaxxCardUpcomingDate(String dateValue) {
        String returnValue = "";

        try {
            long longValue = Long.parseLong(dateValue.replaceAll("\\D+", ""));
            if (String.valueOf(longValue).trim().length() > 1) {
                Date dateObj = new Date(longValue);

                Calendar calender = Calendar.getInstance();
                calender.setTime(dateObj);

                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                String dateforrow = dateFormat.format(calender.getTime());

                returnValue = dateforrow + " | "
                        + getWeekDayValue(dateObj.getDay()) + ", "
                        + dateObj.getDate() + " "
                        + getMonthValue(dateObj.getMonth()) + " "
                        + calender.get(Calendar.YEAR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    private static String getWeekDayValue(int value) {
        String returnVal = "";

        if (value == 0) {
            returnVal = "Sunday";
        } else if (value == 1) {
            returnVal = "Monday";
        } else if (value == 2) {
            returnVal = "Tuesday";
        } else if (value == 3) {
            returnVal = "Wednesday";
        } else if (value == 4) {
            returnVal = "Thursday";
        } else if (value == 5) {
            returnVal = "Friday";
        } else if (value == 6) {
            returnVal = "Saturday";
        }

        return returnVal;
    }

    private static String getMonthValue(int value) {
        String returnVal = "";

        if (value == 0) {
            returnVal = "January";
        } else if (value == 1) {
            returnVal = "February";
        } else if (value == 2) {
            returnVal = "March";
        } else if (value == 3) {
            returnVal = "April";
        } else if (value == 4) {
            returnVal = "May";
        } else if (value == 5) {
            returnVal = "June";
        } else if (value == 6) {
            returnVal = "July";
        } else if (value == 7) {
            returnVal = "August";
        } else if (value == 8) {
            returnVal = "September";
        } else if (value == 9) {
            returnVal = "October";
        } else if (value == 10) {
            returnVal = "November";
        } else if (value == 11) {
            returnVal = "December";
        }

        return returnVal;
    }

    public static String CompareWithcurrentMonth(String dateValue) {

        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        int cMonth = cal.get(Calendar.MONTH);
        int cYear = cal.get(Calendar.YEAR);

        String returnValue = "1";

        try {

            Date dateObj = new Date(Long.parseLong(dateValue.replaceAll("\\D+",
                    "")));

            Calendar calender = Calendar.getInstance();
            calender.setTime(dateObj);

            int eMonth = dateObj.getMonth();
            int eYear = calender.get(Calendar.YEAR);

            if (eYear > cYear) {
                returnValue = "0";
            } else if (eMonth > cMonth) {
                returnValue = "0";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValue;
    }

}
