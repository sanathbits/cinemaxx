package com.cinemaxx.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.internal.widget.ActivityChooserModel;


public class SharedPreference {

    SharedPreferences shPref;

    public static final String LOYALTY_CHECk = "loyalty_check";
    public static final String LOYALTY_CHECk_KEY = "loyalty_check_key";

    public static final String MAXCARD_MEMBERID = "maxcard_memberid";
    public static final String MAXCARD_MEMBERID_KEY = "maxcard_memberid_key";


    public static final String LOYALTY_REQUEST_ID = "loyalty_request_id";
    public static final String LOYALTY_REQUEST_ID_KEY = "loyalty_request_id_key";

    public static final String CINEMAXXCARD_IMAGE = "cinemaxxcard_image";
    public static final String CINEMAXXCARD_IMAGE_KEY = "cinemaxxcard_image_key";


    public void set_Loyalty(Context appContext, String text) {
        Editor editor;
        shPref = appContext.getSharedPreferences(LOYALTY_CHECk,
                Context.MODE_PRIVATE);
        editor = shPref.edit();
        editor.putString(LOYALTY_CHECk_KEY, text);
        editor.commit();
    }

    public String get_Loyalt(Context appContext) {
        String text;
        shPref = appContext.getSharedPreferences(LOYALTY_CHECk,
                Context.MODE_PRIVATE);
        text = shPref.getString(LOYALTY_CHECk_KEY, "false");
        return text;
    }


    public void set_MaxcardMemberid(Context appContext, String text) {
        Editor editor;
        shPref = appContext.getSharedPreferences(MAXCARD_MEMBERID,
                Context.MODE_PRIVATE);
        editor = shPref.edit();
        editor.putString(MAXCARD_MEMBERID_KEY, text);
        editor.commit();
    }

    public String get_MaxcardMemberid(Context appContext) {
        String text;
        shPref = appContext.getSharedPreferences(MAXCARD_MEMBERID,
                Context.MODE_PRIVATE);
        text = shPref.getString(MAXCARD_MEMBERID_KEY, "");
        return text;
    }


    public void set_MaxxcardImage(Context appContext, String text) {
        Editor editor;
        shPref = appContext.getSharedPreferences(CINEMAXXCARD_IMAGE,
                Context.MODE_PRIVATE);
        editor = shPref.edit();
        editor.putString(CINEMAXXCARD_IMAGE_KEY, text);
        editor.commit();
    }

    public String get_MaxxcardImage(Context appContext) {
        String text;
        shPref = appContext.getSharedPreferences(CINEMAXXCARD_IMAGE,
                Context.MODE_PRIVATE);
        text = shPref.getString(CINEMAXXCARD_IMAGE_KEY, "");
        return text;
    }


    public void set_LoyaltyRequestid(Context appContext, String text) {
        Editor editor;
        shPref = appContext.getSharedPreferences(LOYALTY_REQUEST_ID,
                Context.MODE_PRIVATE);
        editor = shPref.edit();
        editor.putString(LOYALTY_REQUEST_ID_KEY, text);
        editor.commit();
    }

    public String get_LoyaltyRequestid(Context appContext) {
        String text;
        shPref = appContext.getSharedPreferences(LOYALTY_REQUEST_ID,
                Context.MODE_PRIVATE);
        text = shPref.getString(LOYALTY_REQUEST_ID_KEY, "");
        return text;
    }
}
