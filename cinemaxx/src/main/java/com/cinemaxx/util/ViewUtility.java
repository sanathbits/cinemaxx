package com.cinemaxx.util;

import com.cinemaxx.adapter.CinemaInfoAdapter;
import com.cinemaxx.adapter.CinemasAdapter;
import com.cinemaxx.adapter.ComingSoonAdapter;
import com.cinemaxx.adapter.FavCinemasAdapter;
import com.cinemaxx.adapter.FavMoviesAdapter;
import com.cinemaxx.adapter.FavPromoAdapter;
import com.cinemaxx.adapter.NewsAdapter;
import com.cinemaxx.adapter.NowPlayingAdapter;
import com.cinemaxx.adapter.PromoAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ViewUtility {

	
	
	
	public static void setListViewHeightBasedOnNowPlayingFragment(GridView listView, Activity storeActivity) {
		NowPlayingAdapter listAdapter = (NowPlayingAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnFavMovieFragment(GridView listView, Activity storeActivity) {
		FavMoviesAdapter listAdapter = (FavMoviesAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnFavPromoFragment(GridView listView, Activity storeActivity) {
		FavPromoAdapter listAdapter = (FavPromoAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnComingSoonFragment(GridView listView, Activity storeActivity) {
		ComingSoonAdapter listAdapter = (ComingSoonAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnPromoFragment(GridView listView, Activity storeActivity) {
		PromoAdapter listAdapter = (PromoAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnCinemasFragment(ListView listView, Activity storeActivity) {
		CinemasAdapter listAdapter = (CinemasAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        /*if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }*/
	        totalHeight += view.getMeasuredHeight();

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnNewsFragment(ListView listView, Activity storeActivity) {
		NewsAdapter listAdapter = (NewsAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        /*if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }*/
	        totalHeight += view.getMeasuredHeight();

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnFavCinemaFragment(ListView listView, Activity storeActivity) {
		FavCinemasAdapter listAdapter = (FavCinemasAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        /*if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }*/
	        totalHeight += view.getMeasuredHeight();

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	public static void setListViewHeightBasedOnCinemasInfoFragment(ListView listView, Activity storeActivity) {
		CinemaInfoAdapter listAdapter = (CinemaInfoAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        /*if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }*/
	        totalHeight += view.getMeasuredHeight();

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	
	/*	public static void setListViewHeightBasedOnChildrenWishList(GridView listView, Activity storeActivity) {
		WishlistAdapter listAdapter = (WishlistAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}
	public static void setListViewHeightBasedOnChildrenFollowed(GridView listView, Activity storeActivity) {
		FollowedStoreAdapter listAdapter = (FollowedStoreAdapter) listView.getAdapter();
	    if (listAdapter == null)
	        return;

	    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
	    int totalHeight = 0;
	    View view = null;
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	        view = listAdapter.getView(i, view, listView);
	        if (i == 0)
	            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

	        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
	        if(ViewUtility.getScreenOrientation(storeActivity) == 1 || ViewUtility.getScreenOrientation(storeActivity) == 9){
		        if((i+1) % 2 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }else{
		        if((i+1) % 3 == 1){
			        totalHeight += view.getMeasuredHeight();
		        }
	        }

	    }
	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getHeight() * (listAdapter.getCount() - 1));
	    listView.setLayoutParams(params);
	    listView.requestLayout();
	}*/
	
	public static int getScreenOrientation(Activity context) {
	    int rotation = context.getWindowManager().getDefaultDisplay().getRotation();
	    DisplayMetrics dm = new DisplayMetrics();
	    context.getWindowManager().getDefaultDisplay().getMetrics(dm);
	    int width = dm.widthPixels;
	    int height = dm.heightPixels;
	    int orientation;
	    // if the device's natural orientation is portrait:
	    if ((rotation == Surface.ROTATION_0
	            || rotation == Surface.ROTATION_180) && height > width ||
	        (rotation == Surface.ROTATION_90
	            || rotation == Surface.ROTATION_270) && width > height) {
	        switch(rotation) {
	            case Surface.ROTATION_0:
	                orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	                break;
	            case Surface.ROTATION_90:
	                orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
	                break;
	            case Surface.ROTATION_180:
	                orientation =
	                    ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
	                break;
	            case Surface.ROTATION_270:
	                orientation =
	                    ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
	                break;
	            default:

	                orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	                break;              
	        }
	    }
	    // if the device's natural orientation is landscape or if the device
	    // is square:
	    else {
	        switch(rotation) {
	            case Surface.ROTATION_0:
	                orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
	                break;
	            case Surface.ROTATION_90:
	                orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	                break;
	            case Surface.ROTATION_180:
	                orientation =
	                    ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
	                break;
	            case Surface.ROTATION_270:
	                orientation =
	                    ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
	                break;
	            default:

	                orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
	                break;              
	        }
	    }

	    return orientation;
	}
	
}
