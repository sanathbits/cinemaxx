package com.cinemaxx.util;

import com.cinemaxx.activity.R;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class CustomProgressDialog {

	public Dialog mydialog;
	private Activity activity;

	public CustomProgressDialog(Activity activity) {
		this.activity = activity;
	}

	public void showDialog() {
		mydialog = new Dialog(activity, android.R.style.Theme_Translucent);
		mydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mydialog.setContentView(R.layout.custom_progress_dialog);
		mydialog.setCancelable(false);
		mydialog.show();
		Animation animation1 = AnimationUtils.loadAnimation(activity,
				R.anim.clockwise);
		ImageView imageView1 = (ImageView) mydialog
				.findViewById(R.id.imageView1);
		imageView1.startAnimation(animation1);

	}

	public void dismissDialog() {
		mydialog.dismiss();
	}

}
