package com.cinemaxx.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

@SuppressLint("CommitPrefEdits")
public class CinemaxxPref {
	
	SharedPreferences preference;
	private String PREFERENCE_NAME = "cinemaxx_pref";
	private int PRIVATE_MODE = 0;
	Editor editor;
	Context context;
	
	private static final String DEVICE_ID = "device_id";
	private static final String CITY_NAME = "city_name";
	private static final String CITY_CODE = "city_code";
	private static final String NEWS_ID = "news_id";
	private static final String MOVIE_ID = "movie_id";
	private static final String MOVIE_STATUS = "movie_status";
	private static final String PROMO_ID = "promo_id";
	private static final String TOKEN_ID = "token_id";
	private static final String TIMER_TIME = "timer_time";
	private static final String USER_NAME = "user_name";
	private static final String USER_EMAIL = "user_email";
	private static final String USER_PHONE = "user_phone";
	private static final String GOLD_ULTRA_CITY = "city";
	private static final String CINEMA_NAME = "cinema_name";
	private static final String CLASS_NAME = "class_name";
	private static final String GOLD_DATE = "gold_date";
	private static final String ULTRA_DATE = "ultra_date";
	private static final String MAXX_CARD_EMAIL_ID = "maxx_card_email_id";
	private static final String MAXX_CARD_EMAIL_PASSWORD = "maxx_card_password";
	private static final String MAXX_CARD_LOGIN_RESPONSE = "maxx_card_login_response";
	private static final String MANU_ABOUT_MAXX_CARD = "menu_about_maxx_card";
	private static final String MANU_PRIVILEGES_AND_BENEFITS = "menu_privileges_and_benefits";
	private static final String MANU_TERMS_AND_CONDITIONS = "menu_terms_and_conditions";
	private static final String MANU_FAQ = "menu_faq";
	private static final String MANU_STAR_CLASS_PRIVILEGES = "menu_star_class_privileges";
	private static final String MANU_MAXX_CARD_NEWS = "menu_maxx_card_news";
	private static final String MANU_SPCIAL_OFFER = "menu_spcial_offer";
	private static final String STAR_CLASS_SPCIAL_OFFER = "star_class_spcial_offer";
	private static final String CLICK_MANU_ABOUT_MAXX_CARD = "click_menu_about_maxx_card";
	
	
	
	public CinemaxxPref(Context context){
		this.context = context;
		preference = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
		editor = preference.edit();
	}
	
	public void setMaxxCardLoginResponse(String response){
		editor.putString(MAXX_CARD_LOGIN_RESPONSE, response);
		editor.commit();
	}
	public String getMaxxCardLoginResponse(){
		return preference.getString(MAXX_CARD_LOGIN_RESPONSE, "");
	}
	
	public void setMaxxCardEmail(String email){
		editor.putString(MAXX_CARD_EMAIL_ID, email);
		editor.commit();
	}
	public String getMaxxCardEmail(){
		return preference.getString(MAXX_CARD_EMAIL_ID, "");
	}
	
	public void setMaxxCardPassword(String pass){
		editor.putString(MAXX_CARD_EMAIL_PASSWORD, pass);
		editor.commit();
	}
	public String getMaxxCardPassword(){
		return preference.getString(MAXX_CARD_EMAIL_PASSWORD, "");
	}
	
	public void setUserName(String tokenId){
		editor.putString(USER_NAME, tokenId);
		editor.commit();
	}
	public String getUserName(){
		return preference.getString(USER_NAME, "");
	}
	
	public void setUserEmail(String tokenId){
		editor.putString(USER_EMAIL, tokenId);
		editor.commit();
	}
	public String getUserEmaile(){
		return preference.getString(USER_EMAIL, "");
	}
	
	public void setUserPhone(String tokenId){
		editor.putString(USER_PHONE, tokenId);
		editor.commit();
	}
	public String getUserPhone(){
		return preference.getString(USER_PHONE, "");
	}
	
	
	public void setTimerTime(String tokenId){
		editor.putString(TIMER_TIME, tokenId);
		editor.commit();
	}
	public String getTimerTime(){
		return preference.getString(TIMER_TIME, "");
	}
	public void setTokenId(String tokenId){
		editor.putString(TOKEN_ID, tokenId);
		editor.commit();
	}
	public String getTokenId(){
		return preference.getString(TOKEN_ID, "");
	}
	
	public void setPromoId(String promoId){
		editor.putString(PROMO_ID, promoId);
		editor.commit();
	}
	public String getPromoId(){
		return preference.getString(PROMO_ID, "");
	}
	
	public void setDeviceId(String deviceId){
		editor.putString(DEVICE_ID, deviceId);
		editor.commit();
	}
	public String getDeviceId(){
		return preference.getString(DEVICE_ID, "");
	}
	
	public void setCityName(String city){
		editor.putString(CITY_NAME, city);
		editor.commit();
	}
	public String getCityName(){
		return preference.getString(CITY_NAME, "");
	}
	
	public void setCityCode(String cityCode){
		editor.putString(CITY_CODE, cityCode);
		editor.commit();
	}
	public String getCityCode(){
		return preference.getString(CITY_CODE, "");
	}
	
	public void setNewsId(String newsId){
		editor.putString(NEWS_ID, newsId);
		editor.commit();
	}
	public String getNewsId(){
		return preference.getString(NEWS_ID, "");
	}
	
	public void setMovieId(String movieId){
		editor.putString(MOVIE_ID, movieId);
		editor.commit();
	}
	public String getMovieId(){
		return preference.getString(MOVIE_ID, "");
	}
	
	public void setMovieStatus(String status){
		editor.putString(MOVIE_STATUS, status);
		editor.commit();
	}
	public String getMovieStatus(){
		return preference.getString(MOVIE_STATUS, "");
	}
	
	public void setCinemaName(String cinema){
		editor.putString(CINEMA_NAME, cinema);
		editor.commit();
	}
	public String getCinemaName(){
		return preference.getString(CINEMA_NAME, "");
	}
	
	public void setGoldUltraCity(String city){
		editor.putString(GOLD_ULTRA_CITY, city);
		editor.commit();
	}
	public String getGoldUltraCity(){
		return preference.getString(GOLD_ULTRA_CITY, "");
	}
	public void setClassName(String claas){
		editor.putString(CLASS_NAME, claas);
		editor.commit();
	}
	public String getClassName(){
		return preference.getString(CLASS_NAME, "");
	}
	
	public void setGoldDate(String gold_date){
		editor.putString(GOLD_DATE, gold_date);
		editor.commit();
	}
	public String getGoldDate(){
		return preference.getString(GOLD_DATE, "");
	}
	
	public void setUltraDate(String ultra_date){
		editor.putString(ULTRA_DATE, ultra_date);
		editor.commit();
	}
	public String getUltraDate(){
		return preference.getString(ULTRA_DATE, "");
	}
	
	public void setManuPrivilegesAndBenefits(String response){
		editor.putString(MANU_PRIVILEGES_AND_BENEFITS, response);
		editor.commit();
	}
	public String getManuPrivilegesAndBenefits(){
		return preference.getString(MANU_PRIVILEGES_AND_BENEFITS, "");
	}
	
	
	public void setManuAboutMaxxCard(String response){
		editor.putString(MANU_ABOUT_MAXX_CARD, response);
		editor.commit();
	}
	public String getManuAboutMaxxCard(){
		return preference.getString(MANU_ABOUT_MAXX_CARD, "");
	}
	
	public void setManuTermsAndConditions(String response){
		editor.putString(MANU_TERMS_AND_CONDITIONS, response);
		editor.commit();
	}
	public String getManuTermsAndConditions(){
		return preference.getString(MANU_TERMS_AND_CONDITIONS, "");
	}
	
	public void setManuFaq(String response){
		editor.putString(MANU_FAQ, response);
		editor.commit();
	}
	public String getManuFaq(){
		return preference.getString(MANU_FAQ, "");
	}
	
	public void setManuStarClassPrivileges(String response){
		editor.putString(MANU_STAR_CLASS_PRIVILEGES, response);
		editor.commit();
	}
	public String getManuStarClassPrivileges(){
		return preference.getString(MANU_STAR_CLASS_PRIVILEGES, "");
	}
	
	public void setManuMaxxCardNews(String response){
		editor.putString(MANU_MAXX_CARD_NEWS, response);
		editor.commit();
	}
	public String getManuMaxxCardNews(){
		return preference.getString(MANU_MAXX_CARD_NEWS, "");
	}
	
	public void setManuSpcialOffer(String response){
		editor.putString(MANU_SPCIAL_OFFER, response);
		editor.commit();
	}
	public String getManuSpcialOffer(){
		return preference.getString(MANU_SPCIAL_OFFER, "");
	}
	
	public void setStarClassSpcialOffer(String response){
		editor.putString(STAR_CLASS_SPCIAL_OFFER, response);
		editor.commit();
	}
	public String getStarClassSpcialOffer(){
		return preference.getString(STAR_CLASS_SPCIAL_OFFER, "");
	}
	
	
	
	public void setClickManuAboutMaxxCard(String response){
		editor.putString(CLICK_MANU_ABOUT_MAXX_CARD, response);
		editor.commit();
	}
	public String getClickManuAboutMaxxCard(){
		return preference.getString(CLICK_MANU_ABOUT_MAXX_CARD, "");
	}
	

}
