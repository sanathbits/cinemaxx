package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NewsTo implements Serializable{
	
	private String newsId;
	private String newsTitle;
	private String newsImage;
	
	
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public String getNewsImage() {
		return newsImage;
	}
	public void setNewsImage(String newsImage) {
		this.newsImage = newsImage;
	}
	
	

}
