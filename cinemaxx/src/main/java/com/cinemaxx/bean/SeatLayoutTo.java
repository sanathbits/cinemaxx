package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SeatLayoutTo implements Serializable{

	private String requistId;
	private String cinemaClassId;
	private String cinemaClassName;
	private String cinemaName;
	private String bookingFees;
	private String price; 
	private String maxSeat;
	private String movieFormat;
	private String movieName;
	private String movieId;
	private String movieRating;
	private String showDate;
	private String showTime;
	private String format;
	private String screenName;
	private String seatSelected;
	private String noOfSeat;
	private String cinemaNo;
	
	private ArrayList<SeatLayoutClassListTo> classList;
	
	
	
	public String getCinemaNo() {
		return cinemaNo;
	}
	public void setCinemaNo(String cinemaNo) {
		this.cinemaNo = cinemaNo;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getSeatSelected() {
		return seatSelected;
	}
	public void setSeatSelected(String seatSelected) {
		this.seatSelected = seatSelected;
	}
	public String getNoOfSeat() {
		return noOfSeat;
	}
	public void setNoOfSeat(String noOfSeat) {
		this.noOfSeat = noOfSeat;
	}
	public String getRequistId() {
		return requistId;
	}
	public void setRequistId(String requistId) {
		this.requistId = requistId;
	}
	public String getCinemaClassId() {
		return cinemaClassId;
	}
	public void setCinemaClassId(String cinemaClassId) {
		this.cinemaClassId = cinemaClassId;
	}
	public String getCinemaClassName() {
		return cinemaClassName;
	}
	public void setCinemaClassName(String cinemaClassName) {
		this.cinemaClassName = cinemaClassName;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getBookingFees() {
		return bookingFees;
	}
	public void setBookingFees(String bookingFees) {
		this.bookingFees = bookingFees;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getMaxSeat() {
		return maxSeat;
	}
	public void setMaxSeat(String maxSeat) {
		this.maxSeat = maxSeat;
	}
	public String getMovieFormat() {
		return movieFormat;
	}
	public void setMovieFormat(String movieFormat) {
		this.movieFormat = movieFormat;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieRating() {
		return movieRating;
	}
	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}
	public String getShowDate() {
		return showDate;
	}
	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public ArrayList<SeatLayoutClassListTo> getClassList() {
		return classList;
	}
	public void setClassList(ArrayList<SeatLayoutClassListTo> classList) {
		this.classList = classList;
	}

}
