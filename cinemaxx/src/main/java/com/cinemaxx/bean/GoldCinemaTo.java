package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GoldCinemaTo implements Serializable{
	
	private String cinemaId;
	private String cinemaName;
	private String cityName;
	private ArrayList<GoldMovieTo> movieList;
	
	public String getCinemaId() {
		return cinemaId;
	}
	public void setCinemaId(String cinemaId) {
		this.cinemaId = cinemaId;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public ArrayList<GoldMovieTo> getMovieList() {
		return movieList;
	}
	public void setMovieList(ArrayList<GoldMovieTo> movieList) {
		this.movieList = movieList;
	}
	
	

}
