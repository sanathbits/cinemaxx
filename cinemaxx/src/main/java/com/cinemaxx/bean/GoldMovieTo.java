package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GoldMovieTo implements Serializable{
	
	private String movieId;
	private String movieName;
	private String thumbnailImage;
	private String isComingSoon;
	private ArrayList<GoldCinemaClassTo> cinemaClassList;
	
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public ArrayList<GoldCinemaClassTo> getCinemaClassList() {
		return cinemaClassList;
	}
	public void setCinemaClassList(ArrayList<GoldCinemaClassTo> cinemaClassList) {
		this.cinemaClassList = cinemaClassList;
	}
	public String getThumbnailImage() {
		return thumbnailImage;
	}
	public void setThumbnailImage(String thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}
	/**
	 * @return the isComingSoon
	 */
	public String getIsComingSoon() {
		return isComingSoon;
	}
	/**
	 * @param isComingSoon the isComingSoon to set
	 */
	public void setIsComingSoon(String isComingSoon) {
		this.isComingSoon = isComingSoon;
	}
	
	

}
