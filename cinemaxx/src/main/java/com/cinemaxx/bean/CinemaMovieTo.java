package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CinemaMovieTo implements Serializable{
	
	private String movieId;
	private String movieName;
	private String movieFormat;
	private String thumbImage;
	private String showDate;
	private ArrayList<CinemaDetailsClassTo> classList;
	
	
	
	public String getShowDate() {
		return showDate;
	}
	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieFormat() {
		return movieFormat;
	}
	public void setMovieFormat(String movieFormat) {
		this.movieFormat = movieFormat;
	}
	public String getThumbImage() {
		return thumbImage;
	}
	public void setThumbImage(String thumbImage) {
		this.thumbImage = thumbImage;
	}
	public ArrayList<CinemaDetailsClassTo> getClassList() {
		return classList;
	}
	public void setClassList(ArrayList<CinemaDetailsClassTo> classList) {
		this.classList = classList;
	}
	
	

}
