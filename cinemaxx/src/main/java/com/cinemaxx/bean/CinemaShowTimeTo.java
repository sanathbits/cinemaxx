package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CinemaShowTimeTo implements Serializable{
	
	private String scheduleId;
	private String scheduleTime;
	private String price;
	
	public String getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}
	public String getScheduleTime() {
		return scheduleTime;
	}
	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	

}
