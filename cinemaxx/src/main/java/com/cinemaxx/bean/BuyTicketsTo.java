package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class BuyTicketsTo implements Serializable{

	private String movieName;
	private String cityName;
	private String cinemaName;
	private String cinemaTypeName;
	private String date;
	private String time;
	private ArrayList<String> seatList;
	
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getCinemaTypeName() {
		return cinemaTypeName;
	}
	public void setCinemaTypeName(String cinemaTypeName) {
		this.cinemaTypeName = cinemaTypeName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public ArrayList<String> getSeatList() {
		return seatList;
	}
	public void setSeatList(ArrayList<String> seatList) {
		this.seatList = seatList;
	}
	
	
}
