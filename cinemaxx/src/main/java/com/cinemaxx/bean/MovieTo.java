package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MovieTo implements Serializable{
	
	private String movieId;
	private String movieName;
	private String movieRating;
	private String movieImage;
	private String movieIsComing;
	
	
	public String getMovieIsComing() {
		return movieIsComing;
	}
	public void setMovieIsComing(String movieIsComing) {
		this.movieIsComing = movieIsComing;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieRating() {
		return movieRating;
	}
	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}
	public String getMovieImage() {
		return movieImage;
	}
	public void setMovieImage(String movieImage) {
		this.movieImage = movieImage;
	}
	
	

}
