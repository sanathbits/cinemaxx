package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CinemaDetailsClassTo implements Serializable{
	
	private String classId;
	private String className;
	private ArrayList<CinemaShowTimeTo> showTimeList;
	
	
	public ArrayList<CinemaShowTimeTo> getShowTimeList() {
		return showTimeList;
	}
	public void setShowTimeList(ArrayList<CinemaShowTimeTo> showTimeList) {
		this.showTimeList = showTimeList;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	
	

}
