package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class HistoryDescTo implements Serializable{
	
	private String BookingId;
	private String BookingNumber;
	private String OrderId;
	private String MovieName;
	private String CinemaName;
	private String ShowTime;
	private String Format;
	private String CinemaNo;
	private String Email;
	private String PaymentMode;
	private String SeatNumber;
	private String NoofSeats;
	private String PricePerTicket;
	private String TotalTicket;
	private String ConvenieceFee;
	private String DiscountAmount;
	private String LoyaltyPoints;
	private String LoyaltyAmount;
	private String TotalPrice;
	private String QrCode;
	private String OrderStatus;
	
	
	public String getBookingId() {
		return BookingId;
	}
	public void setBookingId(String bookingId) {
		BookingId = bookingId;
	}
	public String getBookingNumber() {
		return BookingNumber;
	}
	public void setBookingNumber(String bookingNumber) {
		BookingNumber = bookingNumber;
	}
	public String getOrderId() {
		return OrderId;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public String getMovieName() {
		return MovieName;
	}
	public void setMovieName(String movieName) {
		MovieName = movieName;
	}
	public String getCinemaName() {
		return CinemaName;
	}
	public void setCinemaName(String cinemaName) {
		CinemaName = cinemaName;
	}
	public String getShowTime() {
		return ShowTime;
	}
	public void setShowTime(String showTime) {
		ShowTime = showTime;
	}
	public String getFormat() {
		return Format;
	}
	public void setFormat(String format) {
		Format = format;
	}
	public String getCinemaNo() {
		return CinemaNo;
	}
	public void setCinemaNo(String cinemaNo) {
		CinemaNo = cinemaNo;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPaymentMode() {
		return PaymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		PaymentMode = paymentMode;
	}
	public String getSeatNumber() {
		return SeatNumber;
	}
	public void setSeatNumber(String seatNumber) {
		SeatNumber = seatNumber;
	}
	public String getNoofSeats() {
		return NoofSeats;
	}
	public void setNoofSeats(String noofSeats) {
		NoofSeats = noofSeats;
	}
	public String getPricePerTicket() {
		return PricePerTicket;
	}
	public void setPricePerTicket(String pricePerTicket) {
		PricePerTicket = pricePerTicket;
	}
	public String getTotalTicket() {
		return TotalTicket;
	}
	public void setTotalTicket(String totalTicket) {
		TotalTicket = totalTicket;
	}
	public String getConvenieceFee() {
		return ConvenieceFee;
	}
	public void setConvenieceFee(String convenieceFee) {
		ConvenieceFee = convenieceFee;
	}
	public String getDiscountAmount() {
		return DiscountAmount;
	}
	public void setDiscountAmount(String discountAmount) {
		DiscountAmount = discountAmount;
	}
	public String getLoyaltyPoints() {
		return LoyaltyPoints;
	}
	public void setLoyaltyPoints(String loyaltyPoints) {
		LoyaltyPoints = loyaltyPoints;
	}
	public String getLoyaltyAmount() {
		return LoyaltyAmount;
	}
	public void setLoyaltyAmount(String loyaltyAmount) {
		LoyaltyAmount = loyaltyAmount;
	}
	public String getTotalPrice() {
		return TotalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		TotalPrice = totalPrice;
	}
	public String getQrCode() {
		return QrCode;
	}
	public void setQrCode(String qrCode) {
		QrCode = qrCode;
	}
	public String getOrderStatus() {
		return OrderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	
	

}
