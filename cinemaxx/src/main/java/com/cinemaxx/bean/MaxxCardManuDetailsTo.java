package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MaxxCardManuDetailsTo implements Serializable {

	public Long MaxxCardNewsId;
	public String MaxxCardNewsTitle;
	public String MaxxCardNewsMedia;
	public String MaxxCardNewsDescription;
	public String MaxxCardNewPublishDate;
	public String MaxxCardNewsImageURL;
	public String MaxxCardNewsValidFrom;
	public String MaxxCardNewsValidTo;
	public String MaxxCardNewsStatus;
	public Long SpecialOfferId;
	public String SpecialOfferTitle;
	public String SpecialOfferDescription;
	public String SpecialOfferImageURL;
	public Long StarClassSpecialOfferId;
	
	
	public Long getStarClassSpecialOfferId() {
		return StarClassSpecialOfferId;
	}

	public void setStarClassSpecialOfferId(Long starClassSpecialOfferId) {
		StarClassSpecialOfferId = starClassSpecialOfferId;
	}

	public String getStarClassSpecialOfferTitle() {
		return StarClassSpecialOfferTitle;
	}

	public void setStarClassSpecialOfferTitle(String starClassSpecialOfferTitle) {
		StarClassSpecialOfferTitle = starClassSpecialOfferTitle;
	}

	public String getStarClassSpecialOfferDescription() {
		return StarClassSpecialOfferDescription;
	}

	public void setStarClassSpecialOfferDescription(
			String starClassSpecialOfferDescription) {
		StarClassSpecialOfferDescription = starClassSpecialOfferDescription;
	}

	public String getStarClassSpecialOfferImageURL() {
		return StarClassSpecialOfferImageURL;
	}

	public void setStarClassSpecialOfferImageURL(
			String starClassSpecialOfferImageURL) {
		StarClassSpecialOfferImageURL = starClassSpecialOfferImageURL;
	}

	public String StarClassSpecialOfferTitle;
	public String StarClassSpecialOfferDescription;
	public String StarClassSpecialOfferImageURL;

	public Long getSpecialOfferId() {
		return SpecialOfferId;
	}

	public void setSpecialOfferId(Long specialOfferId) {
		SpecialOfferId = specialOfferId;
	}

	public String getSpecialOfferTitle() {
		return SpecialOfferTitle;
	}

	public void setSpecialOfferTitle(String specialOfferTitle) {
		SpecialOfferTitle = specialOfferTitle;
	}

	public String getSpecialOfferDescription() {
		return SpecialOfferDescription;
	}

	public void setSpecialOfferDescription(String specialOfferDescription) {
		SpecialOfferDescription = specialOfferDescription;
	}

	public String getSpecialOfferImageURL() {
		return SpecialOfferImageURL;
	}

	public void setSpecialOfferImageURL(String specialOfferImageURL) {
		SpecialOfferImageURL = specialOfferImageURL;
	}

	

	public Long getMaxxCardNewsId() {
		return MaxxCardNewsId;
	}

	public void setMaxxCardNewsId(Long maxxCardNewsId) {
		MaxxCardNewsId = maxxCardNewsId;
	}

	public String getMaxxCardNewsTitle() {
		return MaxxCardNewsTitle;
	}

	public void setMaxxCardNewsTitle(String maxxCardNewsTitle) {
		MaxxCardNewsTitle = maxxCardNewsTitle;
	}

	public String getMaxxCardNewsMedia() {
		return MaxxCardNewsMedia;
	}

	public void setMaxxCardNewsMedia(String maxxCardNewsMedia) {
		MaxxCardNewsMedia = maxxCardNewsMedia;
	}

	public String getMaxxCardNewsDescription() {
		return MaxxCardNewsDescription;
	}

	public void setMaxxCardNewsDescription(String maxxCardNewsDescription) {
		MaxxCardNewsDescription = maxxCardNewsDescription;
	}

	public String getMaxxCardNewPublishDate() {
		return MaxxCardNewPublishDate;
	}

	public void setMaxxCardNewPublishDate(String maxxCardNewPublishDate) {
		MaxxCardNewPublishDate = maxxCardNewPublishDate;
	}

	public String getMaxxCardNewsImageURL() {
		return MaxxCardNewsImageURL;
	}

	public void setMaxxCardNewsImageURL(String maxxCardNewsImageURL) {
		MaxxCardNewsImageURL = maxxCardNewsImageURL;
	}

	public String getMaxxCardNewsValidFrom() {
		return MaxxCardNewsValidFrom;
	}

	public void setMaxxCardNewsValidFrom(String maxxCardNewsValidFrom) {
		MaxxCardNewsValidFrom = maxxCardNewsValidFrom;
	}

	public String getMaxxCardNewsValidTo() {
		return MaxxCardNewsValidTo;
	}

	public void setMaxxCardNewsValidTo(String maxxCardNewsValidTo) {
		MaxxCardNewsValidTo = maxxCardNewsValidTo;
	}

	public String getMaxxCardNewsStatus() {
		return MaxxCardNewsStatus;
	}

	public void setMaxxCardNewsStatus(String maxxCardNewsStatus) {
		MaxxCardNewsStatus = maxxCardNewsStatus;
	}

}
