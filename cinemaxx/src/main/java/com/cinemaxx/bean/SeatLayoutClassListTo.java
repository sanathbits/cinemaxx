package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SeatLayoutClassListTo implements Serializable{
	
	private String AreaCode;
	private String AvailableSeats;
	private String BookingFee;
	private String Price;
	private String Qty;
	private String ScheduleClassId; 
	private String SeatClass;
	private boolean IsLoyalty;
	private boolean IsPackage;
	private String maxQty;
	private String minNoOfSeats;
	private String minQty;
	private String PackageName;
	private String classIdDP;
	private int noOfSeats;

	public String getClassIdDP() {
		return classIdDP;
	}
	public void setClassIdDP(String classIdDP) {
		this.classIdDP = classIdDP;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	
	public String getAreaCode() {
		return AreaCode;
	}
	public String getAvailableSeats() {
		return AvailableSeats;
	}
	public String getBookingFee() {
		return BookingFee;
	}
	public String getPrice() {
		return Price;
	}
	public String getQty() {
		return Qty;
	}
	public String getScheduleClassId() {
		return ScheduleClassId;
	}
	public String getSeatClass() {
		return SeatClass;
	}
	public boolean isIsLoyalty() {
		return IsLoyalty;
	}
	public boolean isIsPackage() {
		return IsPackage;
	}
	public String getMaxQty() {
		return maxQty;
	}
	public String getMinNoOfSeats() {
		return minNoOfSeats;
	}
	public String getMinQty() {
		return minQty;
	}
	public String getPackageName() {
		return PackageName;
	}
	public void setAreaCode(String areaCode) {
		AreaCode = areaCode;
	}
	public void setAvailableSeats(String availableSeats) {
		AvailableSeats = availableSeats;
	}
	public void setBookingFee(String bookingFee) {
		BookingFee = bookingFee;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public void setScheduleClassId(String scheduleClassId) {
		ScheduleClassId = scheduleClassId;
	}
	public void setSeatClass(String seatClass) {
		SeatClass = seatClass;
	}
	public void setIsLoyalty(boolean isLoyalty) {
		IsLoyalty = isLoyalty;
	}
	public void setIsPackage(boolean isPackage) {
		IsPackage = isPackage;
	}
	public void setMaxQty(String maxQty) {
		this.maxQty = maxQty;
	}
	public void setMinNoOfSeats(String minNoOfSeats) {
		this.minNoOfSeats = minNoOfSeats;
	}
	public void setMinQty(String minQty) {
		this.minQty = minQty;
	}
	public void setPackageName(String packageName) {
		PackageName = packageName;
	}
	
	
	

}
