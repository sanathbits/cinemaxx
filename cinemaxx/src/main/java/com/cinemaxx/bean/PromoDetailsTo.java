package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PromoDetailsTo implements Serializable{
	
	private String promoId;
	private String promoTitle;
	private String promoDes;
	private String promoImage;
	private String validFrom;
	private String validTo;
	
	public String getPromoId() {
		return promoId;
	}
	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}
	public String getPromoTitle() {
		return promoTitle;
	}
	public void setPromoTitle(String promoTitle) {
		this.promoTitle = promoTitle;
	}
	public String getPromoDes() {
		return promoDes;
	}
	public void setPromoDes(String promoDes) {
		this.promoDes = promoDes;
	}
	public String getPromoImage() {
		return promoImage;
	}
	public void setPromoImage(String promoImage) {
		this.promoImage = promoImage;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	
	

}
