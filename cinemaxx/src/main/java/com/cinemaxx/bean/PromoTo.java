package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PromoTo implements Serializable{
	
	private String promoId;
	private String promoTitle;
	private String promoImage;
	
	
	public String getPromoId() {
		return promoId;
	}
	public void setPromoId(String promoId) {
		this.promoId = promoId;
	}
	public String getPromoTitle() {
		return promoTitle;
	}
	public void setPromoTitle(String promoTitle) {
		this.promoTitle = promoTitle;
	}
	public String getPromoImage() {
		return promoImage;
	}
	public void setPromoImage(String promoImage) {
		this.promoImage = promoImage;
	}
	
	

}
