package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MaxxCardUpCommingTo implements Serializable{

	private String id;
	private String vistaTransId;
	private String transDateTime;
	private String cinemaName;
	private String movieNAme;
	private String movieCode;
	private String showDateTime;
	private String screen;
	private String seatNo;
	private String noOfSeat;
	private String ticketPrice;
	private String totalPrice;
	private String bookingFee;
	private String discount;
	private String totalAmount;
	private String pointsRedeem;
	private String pointsEarned;
	private String isUpcoming;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVistaTransId() {
		return vistaTransId;
	}
	public void setVistaTransId(String vistaTransId) {
		this.vistaTransId = vistaTransId;
	}
	public String getTransDateTime() {
		return transDateTime;
	}
	public void setTransDateTime(String transDateTime) {
		this.transDateTime = transDateTime;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getMovieNAme() {
		return movieNAme;
	}
	public void setMovieNAme(String movieNAme) {
		this.movieNAme = movieNAme;
	}
	public String getMovieCode() {
		return movieCode;
	}
	public void setMovieCode(String movieCode) {
		this.movieCode = movieCode;
	}
	public String getShowDateTime() {
		return showDateTime;
	}
	public void setShowDateTime(String showDateTime) {
		this.showDateTime = showDateTime;
	}
	public String getScreen() {
		return screen;
	}
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public String getNoOfSeat() {
		return noOfSeat;
	}
	public void setNoOfSeat(String noOfSeat) {
		this.noOfSeat = noOfSeat;
	}
	public String getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getPointsRedeem() {
		return pointsRedeem;
	}
	public void setPointsRedeem(String pointsRedeem) {
		this.pointsRedeem = pointsRedeem;
	}
	public String getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(String pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public String getIsUpcoming() {
		return isUpcoming;
	}
	public void setIsUpcoming(String isUpcoming) {
		this.isUpcoming = isUpcoming;
	}
	
	
	
	
}
