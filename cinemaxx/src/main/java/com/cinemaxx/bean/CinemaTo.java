package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CinemaTo implements Serializable{
	
	private ArrayList<CinemaClassTo> classList;
	private String cinemaId;
	private String cinemaName;
	private String multiplexId;
	private String address;
	private String cityId;
	private String location;
	private String latitude;
	private String longitude;
	private String facs;
	private String image;
	private String cityName;
	private String phone;
	private String email;
	private String distance;
	
	
	public ArrayList<CinemaClassTo> getClassList() {
		return classList;
	}
	public void setClassList(ArrayList<CinemaClassTo> classList) {
		this.classList = classList;
	}
	public String getCinemaId() {
		return cinemaId;
	}
	public void setCinemaId(String cinemaId) {
		this.cinemaId = cinemaId;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getMultiplexId() {
		return multiplexId;
	}
	public void setMultiplexId(String multiplexId) {
		this.multiplexId = multiplexId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getFacs() {
		return facs;
	}
	public void setFacs(String facs) {
		this.facs = facs;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	

}
