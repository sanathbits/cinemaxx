package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MaxxCardDetailsTo implements Serializable{
	
	private String memberId;
	private String userName;
	private String password;
	private String clubId;
	private String clubName;
	private String levelId;
	private String levelName;
	private String cardNumber = "";
	private String firstName;
	private String lastName;
	private String gender;
	private String meritalStatus;
	private String dob;
	private String email;
	private String homePhone;
	private String mobilePhone;
	private String address;
	private String inHouseId;
	private String houseHoldIncome;
	private String wishToReceiveSMS;
	private String sendNewsLetter;
	private String mailingFrequency;
	private String expiryDate;
	private String status;
	private String membershipActivated;
	private String balanceTypeId;
	private String balanceName;
	private String balanceMessage;
	private String balanceDisplay;
	private String balancePointRemaining;
	private boolean balanceIsDefault;
	private String isProfileComplete;
	private String loyaltyRequestId;
	private String loyaltyRedeemType;
	private String orderId;
	private String pointsExpiring;
	private String expireOn;
	private String balanceTypeIdExp;
	private String starClassCriteria;
	private String maxCardImage;
	
	
	
	
	
	public String getMaxCardImage() {
		return maxCardImage;
	}
	public void setMaxCardImage(String maxCardImage) {
		this.maxCardImage = maxCardImage;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getClubId() {
		return clubId;
	}
	public void setClubId(String clubId) {
		this.clubId = clubId;
	}
	public String getClubName() {
		return clubName;
	}
	/**
	 * @return the levelName
	 */
	public String getLevelName() {
		return levelName;
	}
	/**
	 * @param levelName the levelName to set
	 */
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	/**
	 * @return the levelId
	 */
	public String getLevelId() {
		return levelId;
	}
	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMeritalStatus() {
		return meritalStatus;
	}
	public void setMeritalStatus(String meritalStatus) {
		this.meritalStatus = meritalStatus;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getInHouseId() {
		return inHouseId;
	}
	public void setInHouseId(String inHouseId) {
		this.inHouseId = inHouseId;
	}
	public String getHouseHoldIncome() {
		return houseHoldIncome;
	}
	public void setHouseHoldIncome(String houseHoldIncome) {
		this.houseHoldIncome = houseHoldIncome;
	}
	public String getWishToReceiveSMS() {
		return wishToReceiveSMS;
	}
	public void setWishToReceiveSMS(String wishToReceiveSMS) {
		this.wishToReceiveSMS = wishToReceiveSMS;
	}
	public String getSendNewsLetter() {
		return sendNewsLetter;
	}
	public void setSendNewsLetter(String sendNewsLetter) {
		this.sendNewsLetter = sendNewsLetter;
	}
	public String getMailingFrequency() {
		return mailingFrequency;
	}
	public void setMailingFrequency(String mailingFrequency) {
		this.mailingFrequency = mailingFrequency;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMembershipActivated() {
		return membershipActivated;
	}
	public void setMembershipActivated(String membershipActivated) {
		this.membershipActivated = membershipActivated;
	}
	public String getBalanceTypeId() {
		return balanceTypeId;
	}
	public void setBalanceTypeId(String balanceTypeId) {
		this.balanceTypeId = balanceTypeId;
	}
	public String getBalanceName() {
		return balanceName;
	}
	public void setBalanceName(String balanceName) {
		this.balanceName = balanceName;
	}
	public String getBalanceMessage() {
		return balanceMessage;
	}
	public void setBalanceMessage(String balanceMessage) {
		this.balanceMessage = balanceMessage;
	}
	public String getBalanceDisplay() {
		return balanceDisplay;
	}
	public void setBalanceDisplay(String balanceDisplay) {
		this.balanceDisplay = balanceDisplay;
	}
	public String getBalancePointRemaining() {
		return balancePointRemaining;
	}
	public void setBalancePointRemaining(String balancePointRemaining) {
		this.balancePointRemaining = balancePointRemaining;
	}
	public boolean isBalanceIsDefault() {
		return balanceIsDefault;
	}
	public void setBalanceIsDefault(boolean balanceIsDefault) {
		this.balanceIsDefault = balanceIsDefault;
	}
	
	public String getLoyaltyRequestId() {
		return loyaltyRequestId;
	}
	public void setLoyaltyRequestId(String loyaltyRequestId) {
		this.loyaltyRequestId = loyaltyRequestId;
	}
	public String getIsProfileComplete() {
		return isProfileComplete;
	}
	public void setIsProfileComplete(String isProfileComplete) {
		this.isProfileComplete = isProfileComplete;
	}
	public String getLoyaltyRedeemType() {
		return loyaltyRedeemType;
	}
	public void setLoyaltyRedeemType(String loyaltyRedeemType) {
		this.loyaltyRedeemType = loyaltyRedeemType;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPointsExpiring() {
		return pointsExpiring;
	}
	public void setPointsExpiring(String pointsExpiring) {
		this.pointsExpiring = pointsExpiring;
	}
	public String getExpireOn() {
		return expireOn;
	}
	public void setExpireOn(String expireOn) {
		this.expireOn = expireOn;
	}
	public String getBalanceTypeIdExp() {
		return balanceTypeIdExp;
	}
	public void setBalanceTypeIdExp(String balanceTypeIdExp) {
		this.balanceTypeIdExp = balanceTypeIdExp;
	}
	public String getStarClassCriteria() {
		return starClassCriteria;
	}
	public void setStarClassCriteria(String starClassCriteria) {
		this.starClassCriteria = starClassCriteria;
	}
	
	
	
	
	
	

}
