package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CinemaShowDateTo implements Serializable{
	
	private String showDate;
	private String showDateDesc;
	private ArrayList<CinemaMovieTo> movieList;
	
	public String getShowDate() {
		return showDate;
	}
	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}
	public ArrayList<CinemaMovieTo> getMovieList() {
		return movieList;
	}
	public void setMovieList(ArrayList<CinemaMovieTo> movieList) {
		this.movieList = movieList;
	}
	public String getShowDateDesc() {
		return showDateDesc;
	}
	public void setShowDateDesc(String showDateDesc) {
		this.showDateDesc = showDateDesc;
	}
	
	

}
