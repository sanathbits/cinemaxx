package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class CinemaInfoTo implements Serializable{
	
	private String cinemaId;
	private String cinemaName;
	private String cinemaAddress;
	private String cinemaLocation;
	private String cityCode;
	private String cityName;
	private ArrayList<CinemaShowDateTo> showDateList;
	
	public String getCinemaId() {
		return cinemaId;
	}
	public void setCinemaId(String cinemaId) {
		this.cinemaId = cinemaId;
	}
	public String getCinemaName() {
		return cinemaName;
	}
	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}
	public String getCinemaAddress() {
		return cinemaAddress;
	}
	public void setCinemaAddress(String cinemaAddress) {
		this.cinemaAddress = cinemaAddress;
	}
	public String getCinemaLocation() {
		return cinemaLocation;
	}
	public void setCinemaLocation(String cinemaLocation) {
		this.cinemaLocation = cinemaLocation;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public ArrayList<CinemaShowDateTo> getShowDateList() {
		return showDateList;
	}
	public void setShowDateList(ArrayList<CinemaShowDateTo> showDateList) {
		this.showDateList = showDateList;
	}
	
	

}
