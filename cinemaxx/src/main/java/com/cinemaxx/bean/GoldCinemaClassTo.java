package com.cinemaxx.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class GoldCinemaClassTo implements Serializable{
	
	private String cinemaClassId;
	private String cinemaClassName;
	private ArrayList<GoldShowTimeTo> showTimeList;
	
	public String getCinemaClassId() {
		return cinemaClassId;
	}
	public void setCinemaClassId(String cinemaClassId) {
		this.cinemaClassId = cinemaClassId;
	}
	public String getCinemaClassName() {
		return cinemaClassName;
	}
	public void setCinemaClassName(String cinemaClassName) {
		this.cinemaClassName = cinemaClassName;
	}
	public ArrayList<GoldShowTimeTo> getShowTimeList() {
		return showTimeList;
	}
	public void setShowTimeList(ArrayList<GoldShowTimeTo> showTimeList) {
		this.showTimeList = showTimeList;
	}
	
	

}
