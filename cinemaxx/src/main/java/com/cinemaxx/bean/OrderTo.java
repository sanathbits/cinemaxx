package com.cinemaxx.bean;

public class OrderTo {

	private String orderDate;
	private String orderId;
	private String charges;
	private String orderAmount;
	private String discountAmount;
	private String totalPrice;
	private String quentity;
	private String noOfSeat;
	private String selectedSeat;
	private String bookingFee;
	
	
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	public String getSelectedSeat() {
		return selectedSeat;
	}
	public void setSelectedSeat(String selectedSeat) {
		this.selectedSeat = selectedSeat;
	}
	public String getNoOfSeat() {
		return noOfSeat;
	}
	public void setNoOfSeat(String noOfSeat) {
		this.noOfSeat = noOfSeat;
	}
	public String getQuentity() {
		return quentity;
	}
	public void setQuentity(String quentity) {
		this.quentity = quentity;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
	
	
}
