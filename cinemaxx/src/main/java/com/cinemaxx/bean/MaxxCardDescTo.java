package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MaxxCardDescTo implements Serializable{

	public int imageLogo;
	public String header, desc;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public int getImageLogo() {
		return imageLogo;
	}

	public String getDesc() {
		return desc;
	}

	public void setImageLogo(int demo1) {
		this.imageLogo = demo1;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
