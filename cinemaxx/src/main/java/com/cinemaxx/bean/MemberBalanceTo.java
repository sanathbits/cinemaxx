package com.cinemaxx.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MemberBalanceTo implements Serializable{
	
	private String balanceTypeId;
	private String recognitionID;
	private String name;
	private String description;
	private String messageText;
	private String display;
	private String isManuallySelected;
	private String qtyAvailable;
	
	
	public String getBalanceTypeId() {
		return balanceTypeId;
	}
	public void setBalanceTypeId(String balanceTypeId) {
		this.balanceTypeId = balanceTypeId;
	}
	public String getRecognitionID() {
		return recognitionID;
	}
	public void setRecognitionID(String recognitionID) {
		this.recognitionID = recognitionID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getIsManuallySelected() {
		return isManuallySelected;
	}
	public void setIsManuallySelected(String isManuallySelected) {
		this.isManuallySelected = isManuallySelected;
	}
	public String getQtyAvailable() {
		return qtyAvailable;
	}
	public void setQtyAvailable(String qtyAvailable) {
		this.qtyAvailable = qtyAvailable;
	}
	public String getQtyEarn() {
		return qtyEarn;
	}
	public void setQtyEarn(String qtyEarn) {
		this.qtyEarn = qtyEarn;
	}
	public String getVistaId() {
		return vistaId;
	}
	public void setVistaId(String vistaId) {
		this.vistaId = vistaId;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	private String qtyEarn;
	private String vistaId;
	private String expiryDate;
	

}
