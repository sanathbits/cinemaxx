package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.CinemaInfoAdapter;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaDetailsClassTo;
import com.cinemaxx.bean.CinemaInfoTo;
import com.cinemaxx.bean.CinemaMovieTo;
import com.cinemaxx.bean.CinemaShowDateTo;
import com.cinemaxx.bean.CinemaShowTimeTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.constant.CinemaDetailsConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class CinemaInfoFragment extends Fragment{

	ListView listView;
	ImageView cinemaImageIv;
	TextView cinemaNameTv, cinemaAddressTv, cinemaTypeTv, phoneTv;
	TextView statusBtn1, statusBtn2, statusBtn3,statusBtn4, statusBtn5;
	LinearLayout containerLayout1, containerLayout2;
	Button addToFavoriteBtn;
	Button nextBtn, prevBytn, timingBtn1, timingBtn2, timingBtn3, timingBtn4, timingBtn5, timingBtn6;
	LinearLayout dateLayout;
	CinemaInfoAdapter adapter;
	TextView locationNameTv;
	CinemaxxPref pref;
	CinemaTo cinemaTo;
	ImageLoader imageLoader;
	ArrayList<CinemaInfoTo> dataList;
	ArrayList<CinemaMovieTo> movieMainList;
	ArrayList<CinemaMovieTo> movieMainFilterList;
	ArrayList<String> showingDateList, showDateDescList;
	String showDateDescValue = "";
    int startedFrom = 0;
    //int[] startingIndexes = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int showDateIndex = 0;
	MainActivity activity;
	String isFavourite = "";
	
	public CinemaInfoFragment(CinemaTo cinemaTo){
		this.cinemaTo = cinemaTo;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_cinemas_info, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();

		
		locationNameTv.setText("Location: "+pref.getCityName());
		initListener();
		
		callService(cinemaTo.getCinemaId());
	}

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		imageLoader = new ImageLoader(getActivity());
		dataList = new ArrayList<CinemaInfoTo>();
		movieMainList = new ArrayList<CinemaMovieTo>();
		showingDateList = new ArrayList<String>();
		showDateDescList = new ArrayList<String>();
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		locationNameTv.setText(pref.getCityName());
		locationNameTv.setTypeface(faceThin);
		listView = (ListView) getView().findViewById(R.id.cinemas_info_lv);
		cinemaImageIv = (ImageView) getView().findViewById(R.id.cinema_info_iv);
		imageLoader.DisplayImageCinema(cinemaTo.getImage().replace(" ", "%20"), cinemaImageIv);
		cinemaNameTv = (TextView) getView().findViewById(R.id.cinema_info_cinema_name_tv);
		cinemaNameTv.setText(cinemaTo.getCinemaName());
		cinemaAddressTv  = (TextView) getView().findViewById(R.id.cinema_info_cast_tv);
		cinemaAddressTv.setText(cinemaTo.getCityName());
		phoneTv  = (TextView) getView().findViewById(R.id.cinema_info_phone_tv);
		phoneTv.setText(cinemaTo.getPhone());
		cinemaTypeTv = (TextView) getView().findViewById(R.id.cinema_info_status_tv1);
		addToFavoriteBtn = (Button) getView().findViewById(R.id.cinema_info_add_to_favorites_btn);
		addToFavoriteBtn.setVisibility(View.INVISIBLE);
		dateLayout = (LinearLayout) getView().findViewById(R.id.cinema_info_date_layout);
		
		nextBtn = (Button) getView().findViewById(R.id.cinema_info_timing_btn_next);
		prevBytn = (Button) getView().findViewById(R.id.cinema_info_timing_btn_prev);
		timingBtn1 = (Button) getView().findViewById(R.id.cinema_info_timing_btn1);
		timingBtn2 = (Button) getView().findViewById(R.id.cinema_info_timing_btn2);
		timingBtn3 = (Button) getView().findViewById(R.id.cinema_info_timing_btn3);
		timingBtn4 = (Button) getView().findViewById(R.id.cinema_info_timing_btn4);
		timingBtn5 = (Button) getView().findViewById(R.id.cinema_info_timing_btn5);
		timingBtn6 = (Button) getView().findViewById(R.id.cinema_info_timing_btn6);
		
		containerLayout1 = (LinearLayout) getView().findViewById(R.id.cinema_info_timing_container1);
		containerLayout2 = (LinearLayout) getView().findViewById(R.id.cinema_info_timing_container2);
		
		statusBtn1 = (TextView) getView().findViewById(R.id.cinema_info_status_tv1);
		statusBtn2 = (TextView) getView().findViewById(R.id.cinema_info_status_tv2);
		statusBtn3 = (TextView) getView().findViewById(R.id.cinema_info_status_tv3);
		statusBtn4= (TextView)getView().findViewById(R.id.cinema_info_status_tv4);
		statusBtn5 = (TextView)getView().findViewById(R.id.cinema_info_status_tv5);
        ArrayList<CinemaClassTo> classList = cinemaTo.getClassList();
		if(classList.size() > 0){
			if(classList.get(0).getClassName().trim().length() > 0){
				statusBtn1.setVisibility(View.VISIBLE);
				if(classList.get(0).getClassId().equals("1")){
					statusBtn1.setText(classList.get(0).getClassName().trim());
					statusBtn1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("2")){
					statusBtn1.setText(classList.get(0).getClassName().trim());
					statusBtn1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("3")){
					statusBtn1.setText(classList.get(0).getClassName().trim());
					statusBtn1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("4")){
					statusBtn1.setText(classList.get(0).getClassName().trim());
					statusBtn1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(0).getClassId().equals("5")){
					statusBtn1.setText(classList.get(0).getClassName().trim());
					statusBtn1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			statusBtn1.setVisibility(View.INVISIBLE);
		}
		if(classList.size() > 1){
			if(classList.get(1).getClassName().trim().length() > 0){
				statusBtn2.setVisibility(View.VISIBLE);
				if(classList.get(1).getClassId().equals("1")){
					statusBtn2.setText(classList.get(1).getClassName().trim());
					statusBtn2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("2")){
					statusBtn2.setText(classList.get(1).getClassName().trim());
					statusBtn2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("3")){
					statusBtn2.setText(classList.get(1).getClassName().trim());
					statusBtn2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("4")){
					statusBtn2.setText(classList.get(1).getClassName().trim());
					statusBtn2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(1).getClassId().equals("5")){
					statusBtn2.setText(classList.get(1).getClassName().trim());
					statusBtn2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			statusBtn2.setVisibility(View.INVISIBLE);
		}
		if(classList.size() > 2){
			if(classList.get(2).getClassName().trim().length() > 0){
				statusBtn3.setVisibility(View.VISIBLE);
				if(classList.get(2).getClassId().equals("1")){
					statusBtn3.setText(classList.get(2).getClassName().trim());
					statusBtn3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("2")){
					statusBtn3.setText(classList.get(2).getClassName().trim());
					statusBtn3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("3")){
					statusBtn3.setText(classList.get(2).getClassName().trim());
					statusBtn3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("4")){
					statusBtn3.setText(classList.get(2).getClassName().trim());
					statusBtn3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(2).getClassId().equals("5")){
					statusBtn3.setText(classList.get(2).getClassName().trim());
					statusBtn3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			statusBtn3.setVisibility(View.INVISIBLE);
		}if(classList.size() > 3){
			if(classList.get(3).getClassName().trim().length() > 0){
				statusBtn4.setVisibility(View.VISIBLE);
				if(classList.get(3).getClassId().equals("1")){
					statusBtn4.setText(classList.get(3).getClassName().trim());
					statusBtn4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("2")){
					statusBtn4.setText(classList.get(3).getClassName().trim());
					statusBtn4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("3")){
					statusBtn4.setText(classList.get(3).getClassName().trim());
					statusBtn4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("4")){
					statusBtn4.setText(classList.get(3).getClassName().trim());
					statusBtn4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(3).getClassId().equals("5")){
					statusBtn4.setText(classList.get(3).getClassName().trim());
					statusBtn4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			statusBtn4.setVisibility(View.INVISIBLE);
		}if(classList.size() > 4){
			if(classList.get(4).getClassName().trim().length() > 0){
				statusBtn5.setVisibility(View.VISIBLE);
				if(classList.get(4).getClassId().equals("1")){
					statusBtn5.setText(classList.get(4).getClassName().trim());
					statusBtn5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}else if(classList.get(4).getClassId().equals("2")){
					statusBtn5.setText(classList.get(4).getClassName().trim());
					statusBtn5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_xd, 0, 0, 0);
				}else if(classList.get(4).getClassId().equals("3")){
					statusBtn5.setText(classList.get(4).getClassName().trim());
					statusBtn5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_gold, 0, 0, 0);
				}else if(classList.get(4).getClassId().equals("4")){
					statusBtn5.setText(classList.get(4).getClassName().trim());
					statusBtn5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hello, 0, 0, 0);
				}else if(classList.get(4).getClassId().equals("5")){
					statusBtn5.setText(classList.get(4).getClassName().trim());
					statusBtn5.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cin_reg, 0, 0, 0);
				}
			}
		}else{
			statusBtn5.setVisibility(View.INVISIBLE);
		}
		
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnCinema();
		
	}
	
	
	public void setIndex(int index){
		showDateIndex = index;
	}
	
	public void initListener(){
		
		addToFavoriteBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFavourite.equals("") && isFavourite.equals("False")){
					callAddToFavorite(cinemaTo.getCinemaId());
				}else{
					callRemovedFavorite(cinemaTo.getCinemaId());
					
				}
			}
		});
		nextBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(showingDateList.size() > showDateIndex + 1){
					showDateIndex = showDateIndex + 1;
                    startedFrom = showDateIndex;
                    timingBtn4.setText(showingDateList.get(showDateIndex));
					showDateDescValue = showDateDescList.get(showDateIndex);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					
					if(showingDateList.size() > showDateIndex + 1){
						showDateIndex = showDateIndex + 1;
						timingBtn5.setText(showingDateList.get(showDateIndex));
						timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
						if(showingDateList.size() > showDateIndex + 1){
							showDateIndex = showDateIndex + 1;
							timingBtn6.setText(showingDateList.get(showDateIndex));
							timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
						}else{
							timingBtn6.setVisibility(View.GONE);
						}
					}else{
						timingBtn5.setVisibility(View.GONE);
						timingBtn6.setVisibility(View.GONE);
					}


//					timingBtn3.setText(showingDateList.get(showDateIndex + 2));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
        prevBytn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(showDateIndex > 0){
					if(showDateIndex == 4 || showDateIndex == 3 || showDateIndex == 2){
						containerLayout1.setVisibility(View.VISIBLE);
						containerLayout2.setVisibility(View.GONE);
                        showDateIndex = 0;

                        ///started from
                        startedFrom = showDateIndex;
					}else{
						if(timingBtn6.getVisibility() == View.VISIBLE){
							showDateIndex = showDateIndex - 3;
                            startedFrom = startedFrom - 3;
						}else if(timingBtn5.getVisibility() == View.VISIBLE){
							showDateIndex = showDateIndex - 2;
                            startedFrom = startedFrom - 2;
						}else{
							showDateIndex = showDateIndex - 1;
                            startedFrom = startedFrom - 3;
						}
						timingBtn4.setText(showingDateList.get(showDateIndex - 2));

                        //started from
                        //startedFrom = showDateIndex;
                        //currentHead = showDateIndex - startedFrom + 1;

                        showDateDescValue = showDateDescList.get(showDateIndex);

                        if(startedFrom < 0){
                            startedFrom = 0;
                        }

						timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
						timingBtn4.setVisibility(View.VISIBLE);
						
						if(showingDateList.size() > showDateIndex - 2){
							timingBtn5.setText(showingDateList.get(showDateIndex - 1));
							timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
							timingBtn5.setVisibility(View.VISIBLE);
							if(showingDateList.size() > showDateIndex - 1){
								timingBtn6.setText(showingDateList.get(showDateIndex));
								timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
								timingBtn6.setVisibility(View.VISIBLE);
							}
						}
					}

				}
			}
		});
        timingBtn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(showingDateList.size() > showDateIndex + 2){
					containerLayout1.setVisibility(View.GONE);
					containerLayout2.setVisibility(View.VISIBLE);
					showDateIndex = showDateIndex + 2;
                    startedFrom = showDateIndex;

                    timingBtn4.setText(showingDateList.get(showDateIndex));
					
					if(showingDateList.size() > showDateIndex + 1){
						showDateIndex = showDateIndex + 1;
						if(showingDateList.get(showDateIndex).trim().length() > 0){
							timingBtn5.setText(showingDateList.get(showDateIndex));
							timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
						}else{
							timingBtn5.setVisibility(View.GONE);
						}
						
						if(showingDateList.size() > showDateIndex + 1){
							showDateIndex = showDateIndex + 1;
							if(showingDateList.get(showDateIndex).trim().length() > 0){
								timingBtn6.setText(showingDateList.get(showDateIndex));
								timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
							}else{
								timingBtn6.setVisibility(View.GONE);
							}
						}else{
							timingBtn6.setVisibility(View.GONE);
						}
					}else{
						timingBtn5.setVisibility(View.GONE);
						timingBtn6.setVisibility(View.GONE);
					}
//					timingBtn5.setText(showingDateList.get(showDateIndex + 1));
//					timingBtn3.setText(showingDateList.get(showDateIndex + 2));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
        timingBtn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
				timingBtn2.setBackgroundResource(R.drawable.city_list_item_back);
				timingBtn4.setBackgroundResource(R.drawable.city_list_item_back);*/
				
				String cDate = Utils.getShowDate(timingBtn1.getText().toString().trim());
				System.out.println("Date is : " +cDate);
				
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for(int i = 0; i < movieMainList.size(); i++){	
					System.out.println(cDate+" : "+movieMainList.get(i).getShowDate().trim());
					if(cDate.trim().equals(movieMainList.get(i).getShowDate().trim())){
						movieMainFilterList.add(movieMainList.get(i));
					}
				}
				if(movieMainFilterList.size() > 0){
					timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					showDateDescValue = showDateDescList.get(0);
					initListView(movieMainFilterList, 0);
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
        timingBtn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				timingBtn2.setBackgroundResource(R.drawable.buy_tickets_btn_back);
//				timingBtn1.setBackgroundResource(R.drawable.city_list_item_back);
//				timingBtn4.setBackgroundResource(R.drawable.city_list_item_back);
				
                String cDate = Utils.getShowDate(timingBtn2.getText().toString().trim());
                System.out.println("Date is : " +cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for(int i = 0; i < movieMainList.size(); i++){	
					System.out.println(cDate+" : "+movieMainList.get(i).getShowDate().trim());
					if(cDate.trim().equals(movieMainList.get(i).getShowDate().trim())){
						movieMainFilterList.add(movieMainList.get(i));
					}
				}
				if(movieMainFilterList.size() > 0){
					timingBtn2.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					showDateDescValue = showDateDescList.get(1);
					initListView(movieMainFilterList, 0);
				}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								
							}
						});
						dialog.show();
				}
				
			}
		});
        /*timingBtn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                String cDate = Utils.getShowDate(timingBtn3.getText().toString().trim());
                System.out.println("Date is : " +cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for(int i = 0; i < movieMainList.size(); i++){	
					System.out.println(cDate+" : "+movieMainList.get(i).getShowDate().trim());
					if(cDate.trim().equals(movieMainList.get(i).getShowDate().trim())){
						
						movieMainFilterList.add(movieMainList.get(i));
						
					}
				}
				initListView(movieMainFilterList, 0);
			}
		});*/
		timingBtn4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timingBtn4.setBackgroundResource(R.drawable.buy_tickets_btn_back);
				timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
//				if(showDateDescList.size() > showDateIndex - 2){

                    showDateDescValue = showDateDescList.get(startedFrom);

					/*if(showDateDescList.size() == showDateIndex + 1){
						showDateDescValue = showDateDescList.get(showDateIndex - 1);
					}else if(showDateDescList.size() == showDateIndex){
						showDateDescValue = showDateDescList.get(showDateIndex - 2);
					}else{
						showDateDescValue = showDateDescList.get(showDateIndex - 3);
					}	*/
//				}
				
				String cDate = Utils.getShowDate(timingBtn4.getText()
						.toString().trim());
				System.out.println("Date is : " + cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for (int i = 0; i < movieMainList.size(); i++) {
					System.out.println(cDate + " : "
							+ movieMainList.get(i).getShowDate().trim());
					if (cDate.trim().equals(
							movieMainList.get(i).getShowDate().trim())) {

						movieMainFilterList.add(movieMainList.get(i));

					}
				}
				initListView(movieMainFilterList, 0);
				if(movieMainFilterList.size() == 0){
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
		
		timingBtn5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timingBtn5.setBackgroundResource(R.drawable.buy_tickets_btn_back);
				timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
				
//				if(showDateDescList.size() > showDateIndex - 1){
//					showDateDescValue = showDateDescList.get(showDateIndex - 1);
//				}

                showDateDescValue = showDateDescList.get(startedFrom + 1);

				/*if(showDateDescList.size() == showDateIndex + 1){
					showDateDescValue = showDateDescList.get(showDateIndex);
				}else{
					showDateDescValue = showDateDescList.get(showDateIndex - 1);
				}*/
				
				String cDate = Utils.getShowDate(timingBtn5.getText()
						.toString().trim());
				System.out.println("Date is : " + cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for (int i = 0; i < movieMainList.size(); i++) {
					System.out.println(cDate + " : "
							+ movieMainList.get(i).getShowDate().trim());
					if (cDate.trim().equals(
							movieMainList.get(i).getShowDate().trim())) {

						movieMainFilterList.add(movieMainList.get(i));

					}
				}
				initListView(movieMainFilterList, 0);
				if(movieMainFilterList.size() == 0){
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
		
		timingBtn6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timingBtn6.setBackgroundResource(R.drawable.buy_tickets_btn_back);
				timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
				timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);


                showDateDescValue = showDateDescList.get(startedFrom + 2);

				/*if(showDateDescList.size() > showDateIndex){
					showDateDescValue = showDateDescList.get(showDateIndex);
				}*/
				
				String cDate = Utils.getShowDate(timingBtn6.getText()
						.toString().trim());
				System.out.println("Date is : " + cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for (int i = 0; i < movieMainList.size(); i++) {
					System.out.println(cDate + " : "
							+ movieMainList.get(i).getShowDate().trim());
					if (cDate.trim().equals(
							movieMainList.get(i).getShowDate().trim())) {

						movieMainFilterList.add(movieMainList.get(i));

					}
				}
				initListView(movieMainFilterList, 0);
				if(movieMainFilterList.size() == 0){
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
					dialog.show();
				}
			}
		});
		/*timingBtn5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timingBtn5.setBackgroundResource(R.drawable.buy_tickets_btn_back);
				timingBtn4.setBackgroundResource(R.drawable.city_list_item_back);
				
				String cDate = Utils.getShowDate(timingBtn5.getText()
						.toString().trim());
				System.out.println("Date is : " + cDate);
				movieMainFilterList = new ArrayList<CinemaMovieTo>();
				for (int i = 0; i < movieMainList.size(); i++) {
					System.out.println(cDate + " : "
							+ movieMainList.get(i).getShowDate().trim());
					if (cDate.trim().equals(
							movieMainList.get(i).getShowDate().trim())) {

						movieMainFilterList.add(movieMainList.get(i));

					}
				}
				initListView(movieMainFilterList, 0);
			}
		});*/
		
	}
	
	
	public void callService(final String cinemaId){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				
				System.out.println("Response:   "+Data.toString());
				try{
					
					System.out.println("Cinema Id:"+cinemaId);
					Log.d("cinemaid", "cinemaid" +cinemaId);
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaDetailsConstant.ERROR_CODE).equals("0")){
						JSONArray cinemaMainArray = mainObj.optJSONArray(CinemaDetailsConstant.CINEMA_MAIN_ARRAY);
						for(int i = 0; i < cinemaMainArray.length(); i++){
							
							JSONObject cinemaInfoObj = cinemaMainArray.optJSONObject(i);
							CinemaInfoTo cinemaInfoTo = new CinemaInfoTo();
							
							cinemaInfoTo.setCinemaId(cinemaInfoObj.optString(CinemaDetailsConstant.CINEMA_ID));
							cinemaInfoTo.setCinemaName(cinemaInfoObj.optString(CinemaDetailsConstant.CINEMA_NAME));
							cinemaInfoTo.setCinemaAddress(cinemaInfoObj.optString(CinemaDetailsConstant.CINEMA_ADDRESS));
							cinemaInfoTo.setCinemaLocation(cinemaInfoObj.optString(CinemaDetailsConstant.CINEMA_LOCATION));
							cinemaInfoTo.setCityCode(cinemaInfoObj.optString(CinemaDetailsConstant.CITY_CODE));
							cinemaInfoTo.setCityName(cinemaInfoObj.optString(CinemaDetailsConstant.CITY_NAME));
							isFavourite = cinemaInfoObj.optString(CinemaDetailsConstant.IS_FAVOURITE);
							System.out.println("Is favourite::::::::::::::::::"+isFavourite);
							if(!isFavourite.equals("") && isFavourite.equals("False")){
								addToFavoriteBtn.setVisibility(View.VISIBLE);
								addToFavoriteBtn.setText("Add To Favorites");
							}else{
								addToFavoriteBtn.setVisibility(View.VISIBLE);
								addToFavoriteBtn.setText("Remove From Favorites");
							}
							JSONObject showDateObj = cinemaInfoObj.optJSONObject(CinemaDetailsConstant.CINEMA_SHOW_DATE_OBJ);
							System.out.println("::::://////////////-------- " + showDateObj.toString());
							JSONArray showDateArray = showDateObj.optJSONArray(CinemaDetailsConstant.CINEMA_SHOW_DATE_ARRAY);
							ArrayList<CinemaShowDateTo> showDateList = new ArrayList<CinemaShowDateTo>();
							for(int j = 0; j < showDateArray.length(); j ++){
								
								JSONObject showDateJsonObj = showDateArray.optJSONObject(j);
								String sTime = showDateJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_LIST_SHOW_DATE);
								
								if(!sTime.trim().equals("")){
									System.out.println("STime::::::::::::::::::::::::::::::::"+Utils.getDateFromFormatedString(sTime));
									if(Utils.compareDate(sTime)){
										
										CinemaShowDateTo showDateTo = new CinemaShowDateTo();
										
										if(Utils.getDateFromFormatedString(sTime).trim().length() > 0){
											showingDateList.add(Utils.getDateFromFormatedString(sTime));
										}
										showDateTo.setShowDate(showDateJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_LIST_SHOW_DATE));
										showDateTo.setShowDateDesc(showDateJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_LIST_SHOW_DATE_DISP));
										showDateDescList.add(showDateJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_LIST_SHOW_DATE_DISP));
										JSONObject cinemaMovieListObj = showDateJsonObj.optJSONObject(CinemaDetailsConstant.CINEMA_MOVIE_LIST_OBJ);
										
										JSONArray cinemaMovieListArray = cinemaMovieListObj.optJSONArray(CinemaDetailsConstant.CINEMA_MOVIE_LIST_ARRAY);
										ArrayList<CinemaMovieTo> cinemaMovieListList = new ArrayList<CinemaMovieTo>();
										for(int k = 0; k < cinemaMovieListArray.length(); k ++){
											
											JSONObject cinemaMovieListJsonObj = cinemaMovieListArray.optJSONObject(k);
											CinemaMovieTo movieTo = new CinemaMovieTo();
											
											movieTo.setShowDate(Utils.getDay(sTime));
											movieTo.setMovieId(cinemaMovieListJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_ID));
											movieTo.setMovieName(cinemaMovieListJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_NAME));
											movieTo.setMovieFormat(cinemaMovieListJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_FORMAT));
											movieTo.setThumbImage(cinemaMovieListJsonObj.optString(CinemaDetailsConstant.CINEMA_MOVIE_THUMBNAIL));
											JSONObject cinemaDetailsClassObj = cinemaMovieListJsonObj.optJSONObject(CinemaDetailsConstant.CINEMA_DETAILS_CLASS_OBJ);
											
											JSONArray cinemaDetailsClassArray = cinemaDetailsClassObj.optJSONArray(CinemaDetailsConstant.CINEMA_DETAILS_CLASS_ARRAY);
											ArrayList<CinemaDetailsClassTo> cinemaDetailsClassList = new ArrayList<CinemaDetailsClassTo>();
											for(int m = 0; m < cinemaDetailsClassArray.length(); m++){
												
												JSONObject cinemaDetailsClassJsonObj = cinemaDetailsClassArray.optJSONObject(m);
												CinemaDetailsClassTo cinemaDetailslassTo = new CinemaDetailsClassTo();
												
												cinemaDetailslassTo.setClassId(cinemaDetailsClassJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_CLASS_ID));
												cinemaDetailslassTo.setClassName(cinemaDetailsClassJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_CLASS_NAME));
												JSONObject showTimeObj = cinemaDetailsClassJsonObj.optJSONObject(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_OBJ);
												
												JSONArray showTimeArray = showTimeObj.optJSONArray(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_ARRAY);
												ArrayList<CinemaShowTimeTo> showTimeList = new ArrayList<CinemaShowTimeTo>();
												for(int n = 0; n < showTimeArray.length(); n++){
													
													
													
													JSONObject showTimeJsonObj = showTimeArray.optJSONObject(n);
													CinemaShowTimeTo showTo = new CinemaShowTimeTo();													
													showTo.setScheduleId(showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_ID));
//													showTo.setScheduleTime(showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_TIME)+"("+showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_FORMAT)+")");
													showTo.setScheduleTime(showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_TIME)+" ("+showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_FORMAT)+")" + "\n" + showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_PRICE));
//													showTo.setPrice("\nRp." +showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_PRICE));
													System.out
															.println("ShowTime: "+showTimeJsonObj.optString(CinemaDetailsConstant.CINEMA_DETAILS_SHOW_TIME_SCHEDULE_TIME)+"Movie: "+movieTo.getMovieName());
													showTimeList.add(showTo);
												}
												cinemaDetailslassTo.setShowTimeList(showTimeList);
												
												cinemaDetailsClassList.add(cinemaDetailslassTo);
											}
											movieTo.setClassList(cinemaDetailsClassList);
											
											cinemaMovieListList.add(movieTo);
											
											movieMainList.add(movieTo);
										}
										showDateTo.setMovieList(cinemaMovieListList);
										
										showDateList.add(showDateTo);
																		}
								}
								
							}
							cinemaInfoTo.setShowDateList(showDateList);
							
							dataList.add(cinemaInfoTo);
							
						}
						
						
						dateLayout.setVisibility(View.VISIBLE);
//						initListView(movieMainList, 1);				
/*						timingBtn1.setText(showingDateList.get(0));
						timingBtn2.setText(showingDateList.get(1));
						timingBtn3.setText(showingDateList.get(2));*/
						for(int i = 0; i < showingDateList.size(); i++){
							System.out.println("Date___________________________________________ :" +showingDateList.get(i));
						}
						if(showingDateList.get(0).trim().equals("Today") || showingDateList.get(0).trim().equals("Tomorrow")){
							containerLayout1.setVisibility(View.VISIBLE);
							containerLayout2.setVisibility(View.GONE);
							timingBtn1.setText("Today");
							timingBtn2.setText("Tomorrow");
							timingBtn3.setText("Next Days");
							timingBtn1.setSelected(true);
							
							ArrayList<CinemaMovieTo> mList = new ArrayList<CinemaMovieTo>();
							/*for(int g = 0; g < movieMainList.size(); g++){
								if(Utils.getDateFromFormatedString(movieMainList.get(g).getShowDate()).equals("Today")){
									CinemaMovieTo mTo = movieMainList.get(g);
									mList.add(mTo);
								}
							}
							if(mList.size() > 0){
								timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
							}
							
							initListView(mList, 1);*/
							
							String cDate = Utils.getShowDate("Today");
							System.out.println("Date is : " +cDate);
							
							movieMainFilterList = new ArrayList<CinemaMovieTo>();
							for(int i = 0; i < movieMainList.size(); i++){	
								System.out.println(cDate+" : "+movieMainList.get(i).getShowDate().trim());
//								if(cDate.trim().equals(movieMainList.get(i).getShowDate().trim())){
								if(Integer.parseInt(cDate.trim()) == Integer.parseInt(movieMainList.get(i).getShowDate().trim())){
									movieMainFilterList.add(movieMainList.get(i));
									
								}
							}
							if(movieMainFilterList.size() > 0){
								timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
								timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
								timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
								showDateDescValue = showDateDescList.get(0);
								initListView(movieMainFilterList, 0);
							}
							
						}else{
							containerLayout1.setVisibility(View.GONE);
							containerLayout2.setVisibility(View.VISIBLE);
							timingBtn4.setText(showingDateList.get(0));
							timingBtn5.setText(showingDateList.get(1));
						}
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
                        AlertDialog dialog = new AlertDialog(getActivity(), "Cinema details are not available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}

					
				
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetCinemaDetailAsync(pref.getDeviceId(), cinemaId, getActivity());
			Log.d("cinemaid", "cinemaid" +cinemaId);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initListView(ArrayList<CinemaMovieTo> list, int status){
		
//		listView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0));
		adapter = new CinemaInfoAdapter(getActivity(), list, CinemaInfoFragment.this, cinemaTo, showDateDescValue);
		listView.setAdapter(adapter);
		/*if(status == 1){
			ViewUtility.setListViewHeightBasedOnCinemasInfoFragment(listView, getActivity());
			listView.setOnTouchListener(new OnTouchListener() {
			    // Setting on Touch Listener for handling the touch inside ScrollView
			    @Override
			    public boolean onTouch(View v, MotionEvent event) {
			    // Disallow the touch request for parent scroll on touch of child view
//			    v.getParent().requestDisallowInterceptTouchEvent(true);
			    return false;
			    }

			});
		}*/	
	}
	
	
	public void callAddToFavorite(String cinemaId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " + Data.toString());
				if (Data.toString().trim().equals("Success")) {
//					Toast.makeText(getActivity(),
//							"Successfully added to favorite list.",
//							Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully added to \nfavorite list.");*/
                    AlertDialog dialog = new AlertDialog(getActivity(), "Successfully added to your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "True";
					addToFavoriteBtn.setText("Remove From Favorites");
				} else {
//					Toast.makeText(getActivity(),
//							"Not added to favorite list.", Toast.LENGTH_LONG)
//							.show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not added to favorite list.");*/
					AlertDialog dialog = new AlertDialog(getActivity(),
							"Not added to your favorite", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									// activity.callBackPress();
								}
							});
					dialog.show();
				}
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try {
			schedule.AddToFavoritesAsync(pref.getDeviceId(), cinemaId, "2", getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void callRemovedFavorite(String cinemaId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " + Data.toString());
				if (Data.toString().trim().equals("Success")) {
//					Toast.makeText(getActivity(),
//							"Successfully added to favorite list.",
//							Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully added to \nfavorite list.");*/
                    AlertDialog dialog = new AlertDialog(getActivity(), "Successfully removed from your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "False";
					addToFavoriteBtn.setText("Add To Favorites");
				} else {
//					Toast.makeText(getActivity(),
//							"Not added to favorite list.", Toast.LENGTH_LONG)
//							.show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not added to favorite list.");*/
					AlertDialog dialog = new AlertDialog(getActivity(),
							"Not removed from your favorite", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									// activity.callBackPress();
								}
							});
					dialog.show();
				}
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try {
			schedule.RemoveFromFavoritesAsync(pref.getDeviceId(), cinemaId, "2", getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
}
