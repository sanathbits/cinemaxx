package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.NowPlayingAdapter;
import com.cinemaxx.bean.NowPlayingTo;
import com.cinemaxx.constant.CityConstant;
import com.cinemaxx.constant.NowPlayingConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class NowPlayingFragment extends Fragment{

	GridView gridView;
	TextView movieNameTv;
	NowPlayingAdapter nowPlayingAdapter;
	CinemaxxPref pref;
	ArrayList<NowPlayingTo> movieList;
	MainActivity activity;
	ArrayList<String> movieIdList;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_now_playing, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		initClickListener();
		
		
		
		movieNameTv.setText("Location: "+pref.getCityName());
		
		activity = (MainActivity) getActivity();
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		gridView = (GridView) getView().findViewById(R.id.now_playing_gv);
		movieNameTv = (TextView) getView().findViewById(R.id.now_playing_location_tv);
		movieNameTv.setTypeface(faceThin);
		TextView textTv = (TextView) getView().findViewById(R.id.now_playing_text_tv); 
		textTv.setTypeface(faceThin);
		pref = new CinemaxxPref(getActivity());
		
		movieList = new ArrayList<NowPlayingTo>();
		/*
		HashMap<String, String> map1 = new HashMap<String, String>();
		map1.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/hobbit_the_battle_of_the_five_armies%20%28431x640%29.jpg");
		map1.put("name", "Hobbit");
		list.add(map1);
		HashMap<String, String> map2 = new HashMap<String, String>();
		map2.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Supernova%20%28434x640%29.jpg");
		map2.put("name", "Supernova");
		list.add(map2);
		HashMap<String, String> map3 = new HashMap<String, String>();
		map3.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Doraemon%20640.jpg");
		map3.put("name", "Stand By");
		list.add(map3);
		HashMap<String, String> map4 = new HashMap<String, String>();
		map4.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Pendekar%20Tongkat%20Emas%20%28448x640%29.jpg");
		map4.put("name", "Pendekar");
		list.add(map4);
		HashMap<String, String> map5 = new HashMap<String, String>();
		map5.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/hobbit_the_battle_of_the_five_armies%20%28431x640%29.jpg");
		map5.put("name", "Hobbit");
		list.add(map5);
		HashMap<String, String> map6 = new HashMap<String, String>();
		map6.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Supernova%20%28434x640%29.jpg");
		map6.put("name", "Supernova");
		list.add(map6);
		HashMap<String, String> map7 = new HashMap<String, String>();
		map7.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Movies/Thumbnail/Doraemon%20640.jpg");
		map7.put("name", "Stand By");
		list.add(map7);
		*/

		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("MOVIES");
	}
	
	
	public void initClickListener(){
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				NowPlayingTo nowTo = (NowPlayingTo) parent.getItemAtPosition(position);
				pref.setMovieId(nowTo.getMovieId());
				pref.setMovieStatus("0");
				
				Fragment fragment = new MovieInfoFragment("now", movieIdList, 0);
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
	}
	
	
	public void callService(){
		final MainActivity activity = (MainActivity) getActivity();
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
//				System.out.println("Now ___Playing Movie________ : " +Data);
				CustomProgressDialog pDialog;

				pDialog = new CustomProgressDialog(getActivity());
				pDialog.showDialog();
				movieList = new ArrayList<NowPlayingTo>();
				
				try{
					
					if(Data.toString().trim().length() > 1){
						JSONObject mainObj = new JSONObject(Data.toString());
						if(mainObj.optString(NowPlayingConstant.ERROR_DESC).equals("0")){
							JSONArray movieArray = mainObj.optJSONArray(NowPlayingConstant.MOVIE_ARRAY);
							movieIdList = new ArrayList<String>();
							for(int i = 0; i < movieArray.length(); i++){
								
								JSONObject movieObj = movieArray.optJSONObject(i);
								NowPlayingTo movieTo = new NowPlayingTo();
								movieTo.setMovieId(movieObj.optString(NowPlayingConstant.MOVIE_ID));
								movieTo.setMovieName(movieObj.optString(NowPlayingConstant.MOVIE_NAME));
								movieTo.setMovieRating(movieObj.optString(NowPlayingConstant.MOVIE_RATING));
								movieTo.setMovieImage(movieObj.optString(NowPlayingConstant.MOVIE_IMAGE));
								
//								movieTo.getMovieImage().replaceAll(" ", "%20");

								movieList.add(movieTo);
								movieIdList.add(movieObj.optString(NowPlayingConstant.MOVIE_ID));
								System.out.println("movieIdList:::::" + movieIdList);
							}
							
							initList();
						}else{
//							MainActivity activity = (MainActivity) getActivity();
//							activity.showCustomAlert("Server is not working.");
	                        AlertDialog dialog = new AlertDialog(getActivity(), "No movies are available", new DialogListener() {
								
								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
							dialog.show();
						}
					}else{
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
                        AlertDialog dialog = new AlertDialog(getActivity(), "No movies are available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
					if(pDialog != null){
						pDialog.dismissDialog();
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			
			schedule.GetNowPlayingMovieListAsync(pref.getDeviceId(), pref.getCityCode(), getActivity());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public void initList(){
		
		
		nowPlayingAdapter = new NowPlayingAdapter(getActivity(), movieList, NowPlayingFragment.this);
		gridView.setAdapter(nowPlayingAdapter);
		/*ViewUtility.setListViewHeightBasedOnNowPlayingFragment(gridView, getActivity());
		gridView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	
	
}
