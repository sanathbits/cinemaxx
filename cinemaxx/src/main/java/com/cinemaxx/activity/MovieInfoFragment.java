package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.constant.MovieInfoConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.R.integer;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MovieInfoFragment extends Fragment{

	TextView bahasaContentTv, locationNameTv, movieNameTv, openingDateTv, castTv, directorTv, languageTv, genreTv, runtimeTv, ratingTv;
	Button addToFavoriteBtn, trailerBtn, buyTicketsBtn;
	LinearLayout contentLayout;
	ImageView movieImageIv;
	CinemaxxPref pref;
	ImageLoader imageLoader;
	String movieId;
//	CustomProgressDialog pDialog;
	String url = "";
	MainActivity activity;
	String status;
	String comingStatus = "";
	String isScheduleAvailable = "", isFavourite = "";
	Button btnLeft, btnRight;
	int indexMovieId;
	ArrayList<String> movieIdList;
	int fvr = 0;
	
	public MovieInfoFragment(String status, ArrayList<String> movieidlist, int fvr){
		this.status = status;
		this.movieIdList = movieidlist;
		this.fvr = fvr;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_movie_info, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		locationNameTv.setText("Location: "+pref.getCityName());
		
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void initView(){
		
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf"); 
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
//		pDialog = new CustomProgressDialog(getActivity());
//		pDialog.showDialog();
		pref = new CinemaxxPref(getActivity());
		imageLoader = new ImageLoader(getActivity());
		bahasaContentTv = (TextView) getView().findViewById(R.id.movie_info_bahasa_tv);
		bahasaContentTv.setTypeface(face);
//		bahasaContentWebView = (WebView) getView().findViewById(R.id.movie_info_bahasa_wv);
//		englistContentTv = (TextView) getView().findViewById(R.id.movie_info_english_tv);
//		englistContentTv.setTypeface(face);
		locationNameTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		locationNameTv.setTypeface(faceThin);
		movieNameTv = (TextView) getView().findViewById(R.id.movie_info_movie_name_tv);
//		movieNameTv.setTypeface(faceGBold);
		openingDateTv = (TextView) getView().findViewById(R.id.movie_info_opening_date_tv);
		TextView openingDateTvN = (TextView) getView().findViewById(R.id.movie_info_opening_date_tv_n);
		openingDateTv.setTypeface(face);
		openingDateTvN.setTypeface(face);
		castTv = (TextView) getView().findViewById(R.id.movie_info_cast_tv);
		TextView castTvN = (TextView) getView().findViewById(R.id.movie_info_cast_tv_n);
		castTv.setTypeface(face);
		castTvN.setTypeface(face);
		directorTv = (TextView) getView().findViewById(R.id.movie_info_director_tv);
		TextView directorTvN = (TextView) getView().findViewById(R.id.movie_info_director_tv_n);
		directorTv.setTypeface(face);
		directorTvN.setTypeface(face);
		languageTv = (TextView) getView().findViewById(R.id.movie_info_language_tv);
		TextView languageTvN = (TextView) getView().findViewById(R.id.movie_info_language_tv_n);
		languageTv.setTypeface(face);
		languageTvN.setTypeface(face);
		genreTv = (TextView) getView().findViewById(R.id.movie_info_genre_tv);
		TextView genreTvN = (TextView) getView().findViewById(R.id.movie_info_genre_tv_n);
		genreTv.setTypeface(face);
		genreTvN.setTypeface(face);
		runtimeTv = (TextView) getView().findViewById(R.id.movie_info_runtime_tv);
		TextView runtimeTvN = (TextView) getView().findViewById(R.id.movie_info_runtime_tv_n);
		runtimeTv.setTypeface(face);
		runtimeTvN.setTypeface(face);
		ratingTv = (TextView) getView().findViewById(R.id.movie_info_rating_tv);
		TextView ratingTvN = (TextView) getView().findViewById(R.id.movie_info_rating_tv_n);
		ratingTv.setTypeface(face);
		ratingTvN.setTypeface(face);
		
		contentLayout = (LinearLayout) getView().findViewById(R.id.fragment_movie_content_layout);
		
		movieImageIv = (ImageView) getView().findViewById(R.id.movie_info_movie_iv);
		
		addToFavoriteBtn = (Button) getView().findViewById(R.id.movie_info_add_to_favourite_btn);
		addToFavoriteBtn.setTypeface(faceThin);
		trailerBtn = (Button) getView().findViewById(R.id.movie_info_trailer_btn);
		trailerBtn.setTypeface(faceThin);
		buyTicketsBtn = (Button) getView().findViewById(R.id.movie_info_buy_tickets_btn);
		buyTicketsBtn.setTypeface(faceThin);
		
		btnLeft = (Button) getView().findViewById(R.id.btnLeft);
		btnRight = (Button) getView().findViewById(R.id.btnRight);
		
		if(fvr == 1){
			btnLeft.setVisibility(View.INVISIBLE);
			btnRight.setVisibility(View.INVISIBLE);
		}
		
		
		if(status.equals("coming")){
			buyTicketsBtn.setVisibility(View.INVISIBLE);
			comingStatus = "1";
		}else{
			comingStatus = "0";
		}
		
//		String bahasa = "<h2>Bahasa Indonesia - </h2>";
//		bahasaContentTv.setText(Html.fromHtml(bahasa) +" Tim arkeolog Amerika menemukan piramida yang hilang di gurun Mesir. Ketika mereka mencari dan mengungkap rahasia mengerikan yang terkubur, para arkeolog tersesat dalam kegelapan lorong-lorongnya yang tak berujung. Mereka putus asa mencari jalan keluar dan melihat sinar matahari lagi. Para arkeolog itu menyadari mereka tidak hanya terperangkap, tapi juga diburu.");
//		String english = "<h2>English - </h2> Tim arkeolog Amerika menemukan piramida yang hilang di gurun Mesir. Ketika mereka mencari dan mengungkap rahasia mengerikan yang terkubur, para arkeolog tersesat dalam kegelapan lorong-lorongnya yang tak berujung. Mereka putus asa mencari jalan keluar dan melihat sinar matahari lagi. Para arkeolog itu menyadari mereka tidak hanya terperangkap, tapi juga diburu.";
//		englistContentTv.setText(Html.fromHtml(english));
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("MOVIES");
	}
	
	public void initListener() {

		addToFavoriteBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFavourite.equals("") && isFavourite.equals("False")){
					callAddFavorite(movieId);
					
					
				}else{
					callRemoveFromFavorite(movieId);
					
				}
			}
		});
		trailerBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

//				url = "https://www.youtube.com/embed/t-8YsulfxVI";
				String []videoUrl = url.split("/");
				if(videoUrl.length > 1){
					Intent videoIntent = new Intent(getActivity(), ShowVideoActivity.class);
					videoIntent.putExtra("url", videoUrl[videoUrl.length - 1]);
					getActivity().startActivity(videoIntent);
				}else{
//					Toast.makeText(getActivity(), "No Trailer available.", Toast.LENGTH_LONG).show();
					MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("No Trailer available.");
				}
				
			}
		});
		buyTicketsBtn.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new BuyTicketsFragment(movieNameTv.getText().toString().trim(), movieId, pref.getCityName(), "", "", "","movie", "");
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
		btnLeft.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(indexMovieId == 0){
					indexMovieId = movieIdList.size() - 1;
					pref.setMovieId(movieIdList.get(indexMovieId));
				}else{
					indexMovieId = indexMovieId - 1;
					pref.setMovieId(movieIdList.get(indexMovieId));
				}
				callService();
			}
		});
		
		btnRight.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(indexMovieId == movieIdList.size() - 1){
					indexMovieId = 0;
					pref.setMovieId(movieIdList.get(indexMovieId));
				}else{
					indexMovieId = indexMovieId + 1;
					pref.setMovieId(movieIdList.get(indexMovieId));
				}
				callService();
			}
		});

	}
	
	public void callRemoveFromFavorite(String movieId){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
//					Toast.makeText(getActivity(), "Successfully added to favorite list.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully added to \nfavorite list.");*/
					AlertDialog dialog = new AlertDialog(getActivity(), "Successfully removed from your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "False";
					addToFavoriteBtn.setText("Add To Favorites");
				}else{
//					Toast.makeText(getActivity(), "Not added to favorite list.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not added to favorite list.");*/
                    AlertDialog dialog = new AlertDialog(getActivity(), "Not removed from your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.MoviesRemoveFromFavoritesAsync(pref.getDeviceId(), movieId, "1", comingStatus, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void callAddFavorite(String movieId){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
//					Toast.makeText(getActivity(), "Successfully added to favorite list.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully added to \nfavorite list.");*/
					AlertDialog dialog = new AlertDialog(getActivity(), "Successfully added to your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "True";
					addToFavoriteBtn.setText("Remove From Favorites");
				}else{
//					Toast.makeText(getActivity(), "Not added to favorite list.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not added to favorite list.");*/
                    AlertDialog dialog = new AlertDialog(getActivity(), "Not added to your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.MoviesAddToFavoritesAsync(pref.getDeviceId(), movieId, "1", comingStatus, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				try{
					
					contentLayout.setVisibility(View.VISIBLE);
//					if(pDialog != null){
//						pDialog.dismissDialog();
//						pDialog = null;
//					}
					System.out.println("Movie Info: " +Data.toString());
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(MovieInfoConstant.ERROR_DESC).equals("0")){
						JSONArray movieArray = mainObj.optJSONArray(MovieInfoConstant.MOVIE_ARRAY);
						for(int i = 0; i < movieArray.length(); i++){
							
							JSONObject movieObj = movieArray.optJSONObject(i);
							
							movieId = movieObj.optString(MovieInfoConstant.MOVIE_ID);
							movieNameTv.setText(movieObj.optString(MovieInfoConstant.MOVIE_NAME));
							openingDateTv.setText(movieObj.optString(MovieInfoConstant.RELEASE_DATE));
							castTv.setText(movieObj.optString(MovieInfoConstant.CAST_N_CREW));
							directorTv.setText(movieObj.optString(MovieInfoConstant.DIRECTOR));
							languageTv.setText(movieObj.optString(MovieInfoConstant.LANGUAGE));
							genreTv.setText(movieObj.optString(MovieInfoConstant.GENER));
							runtimeTv.setText(movieObj.optString(MovieInfoConstant.MOVIE_LENGTH));
							ratingTv.setText(movieObj.optString(MovieInfoConstant.MOVIE_RATING));
							bahasaContentTv.setText(Html.fromHtml(movieObj.optString(MovieInfoConstant.SYNOPSYS)));
							isScheduleAvailable = movieObj.optString(MovieInfoConstant.IS_SCHEDULE_AVAILABLE);
							isFavourite = movieObj.optString(MovieInfoConstant.IS_FAVOURITE);
//							bahasaContentWebView.loadData(movieObj.optString(MovieInfoConstant.SYNOPSYS),  "text/html; charset=UTF-8", null);
							url = movieObj.optString(MovieInfoConstant.TRAILER);
							
							imageLoader.DisplayImage(movieObj.optString(MovieInfoConstant.MOVIE_IMAGE).replace(" ", "%20").trim(), movieImageIv);
							
							if(!isFavourite.equals("") && isFavourite.equals("False")){
								addToFavoriteBtn.setText("Add To Favorites");
							}else{
								addToFavoriteBtn.setText("Remove From Favorites");
							}
						}
						System.out.println("isScheduleAvailable/////:::" + isScheduleAvailable);
						if(isScheduleAvailable.equals("1")){
							buyTicketsBtn.setVisibility(View.VISIBLE);
						}else if (status.equals("coming") && isScheduleAvailable.equals("0")) {
							buyTicketsBtn.setVisibility(View.INVISIBLE);
						}
						
						
						
						System.out.println("Moviidlist:::::::://////" + movieIdList);
						if(movieIdList.size() > 0 && movieIdList.contains(movieId)){
							indexMovieId = movieIdList.indexOf(movieId);
						}
						
						
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
                        AlertDialog dialog = new AlertDialog(getActivity(), "Movie details are not available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			schedule.GetMovieDetailsAsync(pref.getDeviceId(), pref.getMovieId(), pref.getMovieStatus(), getActivity());
			System.out.println("Movie ID:::" +  pref.getMovieId() + "Status:::" + pref.getMovieStatus());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	

}
