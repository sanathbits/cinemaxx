package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.FavPromoAdapter;
import com.cinemaxx.bean.PromoTo;
import com.cinemaxx.constant.PromoConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FavPromoFragment extends Fragment{

	TextView locationNameTv;
	GridView gridView;
	ArrayList<PromoTo> dataList;
	FavPromoAdapter adapter;
	CinemaxxPref pref;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_fav_promo, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
        initView();
        activity = (MainActivity) getActivity();
		
		locationNameTv.setText("Location: "+pref.getCityName());
		
		initListener();
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("PROMOS");
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceLight = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.fav_promo_location_tv);
		locationNameTv.setTypeface(faceThin);
		TextView textTv = (TextView) getView().findViewById(R.id.fav_promo_text_tv); 
		textTv.setTypeface(faceLight);
		gridView = (GridView) getView().findViewById(R.id.fav_promo_gv);
		dataList = new ArrayList<PromoTo>();
		pref = new CinemaxxPref(getActivity());
		
	}
	
	public void initListener(){
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				
				System.out.println("Fav Promo: " +Data.toString());
				dataList = new ArrayList<PromoTo>();
                if(Data.toString().trim().length() > 1){
                	try{
    					
    					JSONObject mainObj = new JSONObject(Data.toString());
    					if(mainObj.optString(PromoConstant.ERROR_DESC).equals("0")){
    						JSONArray movieArray = mainObj.optJSONArray(PromoConstant.PROMO_ARRAY);
    						for(int i = 0; i < movieArray.length(); i++){
    							
    							PromoTo promoTo = new PromoTo();
    							JSONObject movieObj = movieArray.optJSONObject(i);
    							
    							promoTo.setPromoId(movieObj.optString(PromoConstant.PROMO_ID));
    							promoTo.setPromoImage(movieObj.optString(PromoConstant.PROMO_IMAGE));
    							promoTo.setPromoTitle(movieObj.optString(PromoConstant.PROMO_TITLE));
    							
    							dataList.add(promoTo);
    						}
    						initGrid();
    					}else{
    						/*MainActivity activity = (MainActivity) getActivity();
    						activity.showCustomAlert("Server is not working.");*/
                            AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any Promos yet.", new DialogListener() {
    							
    							@Override
    							public void onOkClick() {
    								// TODO Auto-generated method stub
    								activity.callBackPress();
    							}
    						});
                            dialog.setCancelable(false);
    						dialog.show();
    					}
    					
    				}catch(Exception e){
    					e.printStackTrace();
    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
				
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetFavouriteListAsync(pref.getDeviceId(), "3", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void callRemoveFavorite(String movieId, final int position){
		
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response for removed: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
					dataList.remove(position);
					initGrid();
//					Toast.makeText(getActivity(), "Successfully removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully removed from \nfavoritelist.");*/
				}else{
//					Toast.makeText(getActivity(), "Not removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*Toast.makeText(getActivity(), "Not removed from \nfavoritelist..", Toast.LENGTH_LONG).show();
					MainActivity activity = (MainActivity) getActivity();*/
					AlertDialog dialog = new AlertDialog(getActivity(),
							"Not removed from from \nyour favoritelist", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
//									activity.callBackPress();
								}
							});
					dialog.show();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.RemoveFromFavoritesAsync(pref.getDeviceId(), movieId, "3", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initGrid(){
		
		adapter = new FavPromoAdapter(getActivity(), dataList, FavPromoFragment.this);
		gridView.setAdapter(adapter);
		
		if(dataList.size() == 0){
			AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any Promos yet.", new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}

		/*ViewUtility.setListViewHeightBasedOnFavPromoFragment(gridView, getActivity());
		gridView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}

}
