package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.HashMap;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.BuyTicketAdapter;
import com.cinemaxx.adapter.CinemasAdapter;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.constant.CinemaConstant;
import com.cinemaxx.constant.CityConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.GPSUtil;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class CinemasFragment extends Fragment implements OnWheelChangedListener{

	TextView locationTv;
	TextView citySpinner;
	Button nearMeBtn;
	ListView listView;
	CinemaxxPref pref;
	ArrayList<CinemaTo> list;
	ArrayList<HashMap<String, String>> cityList;
	String []cityNameArray;
	CinemasAdapter adapter;
	boolean cityStatus = false;
	MainActivity activity;
	LinearLayout lnrwheelCity;
	BuyTicketAdapter buyTicketAdapter;
	private WheelView wvBuyTicketCity;
	boolean flagt = false, flagtt = false;
	String [] citySList;
	int mvalue = 0, cvalue = 0;
	int cityIndex = 0;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_cinemas, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		initListener();
		activity = (MainActivity) getActivity();

		
		locationTv.setText("Location: "+pref.getCityName());
		
		
		if(Utils.isNetworkAvailable(getActivity())){
			callCityService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		locationTv.setText("Location: "+pref.getCityName());
		locationTv.setTypeface(faceThin);
		citySpinner = (TextView) getView().findViewById(R.id.cinemas_city_picker);
		nearMeBtn = (Button) getView().findViewById(R.id.cinemas_near_me_btn);
		listView = (ListView) getView().findViewById(R.id.cinemas_lv);
		lnrwheelCity = (LinearLayout) getView().findViewById(R.id.lnrwheelCinema);
		wvBuyTicketCity = (WheelView) getView().findViewById(R.id.wvBuyTicketMovie);
		
		cityList = new ArrayList<HashMap<String, String>>();
		
		list = new ArrayList<CinemaTo>();
		
/*		HashMap<String, String> map1 = new HashMap<String, String>();
		map1.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/cin-01.jpg");
		map1.put("name", "Cinemaxx Semanggi");
		map1.put("city", "Jakarta");
		list.add(map1);
		HashMap<String, String> map2 = new HashMap<String, String>();
		map2.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/FX_Lobby.jpg");
		map2.put("name", "Cinemaxx FX Sudirman");
		map2.put("city", "Manado");
		list.add(map2);
		HashMap<String, String> map3 = new HashMap<String, String>();
		map3.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/Palembang.jpg");
		map3.put("name", "Cinemaxx Palembang Icon");
		map3.put("city", "Jakarta");
		list.add(map3);
		HashMap<String, String> map4 = new HashMap<String, String>();
		map4.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/cin-01.jpg");
		map4.put("name", "Cinemaxx FX Sudirman");
		map4.put("city", "Jakarta");
		list.add(map4);
		HashMap<String, String> map5 = new HashMap<String, String>();
		map5.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/Palembang.jpg");
		map5.put("name", "Cinemaxx Palembang Icon");
		map5.put("city", "Manado");
		list.add(map5);
		HashMap<String, String> map6 = new HashMap<String, String>();
		map6.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/FX_Lobby.jpg");
		map6.put("name", "Cinemaxx FX Sudirman");
		map6.put("city", "Jakarta");
		list.add(map6);
		HashMap<String, String> map7 = new HashMap<String, String>();
		map7.put("image", "http://cinemaxx.cinemaxxtheater.com/Gallery/Cinema/Thumbnail/Palembang.jpg");
		map7.put("name", "Cinemaxx Palembang Icon");
		map7.put("city", "Jakarta");
		list.add(map7);*/
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnCinema();
	}
	
	
	public void initListener(){
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CinemaTo cinemaTo = (CinemaTo) parent.getItemAtPosition(position);
				Fragment fragment = new CinemaInfoFragment(cinemaTo);
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
		citySpinner.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(citySList != null && citySList.length > 0){
					flagt = false;
					flagtt = false;
					lnrwheelCity.setVisibility(View.VISIBLE);
					citySpinner.setEnabled(false);
					initCinemaCityList();
				}
			}
		});
		
		/*citySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				nearMeBtn.setBackgroundResource(R.drawable.near_me);
				if(cityStatus){
					String cityCode = cityList.get(position).get("id");
					if(cityCode.equals("aaa")){
						callServiceAllCity(cityCode);
					}else{
						callService(cityCode);
					}
					if(position == 0){
						callServiceAllCity(cityCode);
					}else{
						callService(cityCode);
					}
				}
				
//				String item = parent.getItemAtPosition(position).toString();
//				if(!item.trim().equals("All cities")){
//					
//					adapter.filter(item);
//					listView.setAdapter(adapter);
//					
//				}else{
//					
//					adapter = new CinemasAdapter(getActivity(), list, CinemasFragment.this);
//					listView.setAdapter(adapter);
//
//				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
		nearMeBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GPSUtil gUtils = new GPSUtil(getActivity());
				if(gUtils.getLatitude() != 0.0 && gUtils.getLongitude() != 0.0){
					
					nearMeBtn.setBackgroundResource(R.drawable.near_me_hover);
					callServiceNearMe(pref.getCityCode(), String.valueOf(gUtils.getLatitude()), String.valueOf(gUtils.getLongitude()));
					
				}
			}
		});
		
	}
	
	protected void initCinemaCityList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), citySList, 0);
		
		wvBuyTicketCity.setViewAdapter(buyTicketAdapter);
//		if(citytv.getText().toString().contains("Select")){
//			cityIndex = 0;
//		}else{
//			wvBuyTicketCity.setCurrentItem(cityIndex);
//		}
			if(cityIndex != 0){
				wvBuyTicketCity.setCurrentItem(cityIndex);
			}else{
				wvBuyTicketCity.setCurrentItem(0);
			}
		
		wvBuyTicketCity.addChangingListener(this);
		wvBuyTicketCity.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketCity.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if(flagtt == false){
									flagtt = true;
									System.out.println("addClickingListener::::::::::::::::::::::::::::::;"+ citySList[wvBuyTicketCity.getCurrentItem()]);
									cityIndex = wvBuyTicketCity.getCurrentItem();
									citySpinner.setText(citySList[wvBuyTicketCity.getCurrentItem()]);
									
									String cityCode = cityList.get(wvBuyTicketCity.getCurrentItem()).get("id");
									/*if(cityCode.equals("aaa")){
										callServiceAllCity(cityCode);
									}else{
										callService(cityCode);
									}*/
									System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
									if(wvBuyTicketCity.getCurrentItem() == 0){
										callServiceAllCity(cityCode);
									}else{
										callService(cityCode);
									}
									
									citySpinner.setEnabled(true);
									lnrwheelCity.setVisibility(View.GONE);
									flagt = false;
									cvalue = 0;
									wvBuyTicketCity.setViewAdapter(null);
								}
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCity.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){
							
							String cityCode = cityList.get(wvBuyTicketCity.getCurrentItem()).get("id");
							flagt = true;
							System.out.println("setOnTouchListener///////////////////////////////////////"+ citySList[wvBuyTicketCity.getCurrentItem()]);
							cityIndex = wvBuyTicketCity.getCurrentItem();
							citySpinner.setText(citySList[wvBuyTicketCity.getCurrentItem()]);
//							citySelectName = cityNameList[wvBuyTicketCity.getCurrentItem()];
//							citySelectId = cityIdList.get(wvBuyTicketCity.getCurrentItem());
							System.out.println("_____________________________________________________________________________________________");
							if(wvBuyTicketCity.getCurrentItem() == 0){
								callServiceAllCity(cityCode);
							}else{
								callService(cityCode);
							}
							
							citySpinner.setEnabled(true);
							lnrwheelCity.setVisibility(View.GONE);
							cvalue = 0;
							wvBuyTicketCity.setViewAdapter(null);
							
						}
						
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCity.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);
				
			}
		});
		
	}

//	public void initSpinner(int posi){
//		
//		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, cityNameArray);
//		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		citySpinner.setAdapter(spinnerAdapter);
//		citySpinner.setSelection(posi);
//		callServiceAllCity("aaa");
//		
//	}
	
	public void callCityService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("City List:___________________" +Data.toString());
				cityStatus = false;
				
				cityList = new ArrayList<HashMap<String, String>>();
				int pos = 0;
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CityConstant.ERROR_DESC).equals("0")){
						 JSONArray cityArray = mainObj.optJSONArray(CityConstant.CITY_ARRAY);
		                	cityNameArray = new String[cityArray.length() + 1];

		                    HashMap<String, String> cityS = new HashMap<String, String>();
		                    cityS.put("name", "All Cities");
		                    cityS.put("id", "aaa");
		                    cityList.add(cityS);
		                    cityNameArray[0] = "All Cities";
		                    citySList = new String[cityArray.length() +1];
		                    citySList[0] = "All Cities";
		                    for(int i = 0; i < cityArray.length(); i++){
		                    	
		                    	JSONObject cityObj = cityArray.optJSONObject(i);
		                    	HashMap<String, String> city = new HashMap<String, String>();
		                    	
//		                    	if(pref.getCityCode().equals(cityObj.optString(CityConstant.CITY_ID))){
//		                    		pos = i + 1;
//		                    	}
		                    	city.put("name", cityObj.optString(CityConstant.CITY_NAME));
		                    	city.put("id", cityObj.optString(CityConstant.CITY_ID));
		                    	cityNameArray[i + 1] = cityObj.optString(CityConstant.CITY_NAME);
		                    	
		                    	cityList.add(city);
		                    	citySList[i + 1] = cityObj.optString(CityConstant.CITY_NAME);
		                    }
		                    
//		    				initSpinner(0);
		                    citySpinner.setText(citySList[0]);
		                    callServiceAllCity("aaa");
					}else{
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
	                    	AlertDialog dialog = new AlertDialog(getActivity(), "Cities are not available", new DialogListener() {
								
								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
							dialog.show();
					}
                   

				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			
			schedule.GetCityListAsync(pref.getDeviceId(), getActivity());

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void callService(String cityCode){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cityStatus = true;

				System.out.println("Cinema Response: " +Data.toString());
				list = new ArrayList<CinemaTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaConstant.ERROR_DESC).equals("0")){
						JSONArray mainArray = mainObj.optJSONArray(CinemaConstant.CINEMA_ARRAY);
						for(int i = 0; i < mainArray.length(); i++){
							
							JSONObject cinemaObj = mainArray.optJSONObject(i);
							CinemaTo cinemaTo = new CinemaTo();
							cinemaTo.setCinemaId(cinemaObj.optString(CinemaConstant.CINEMA_ID));
							cinemaTo.setCinemaName(cinemaObj.optString(CinemaConstant.CINEMA_NAME));
							cinemaTo.setMultiplexId(cinemaObj.optString(CinemaConstant.MULTIPLEX_ID));
							cinemaTo.setAddress(cinemaObj.optString(CinemaConstant.ADDRESS));
							cinemaTo.setCityId(cinemaObj.optString(CinemaConstant.CITY_ID));
							cinemaTo.setLocation(cinemaObj.optString(CinemaConstant.LOCATION));
							cinemaTo.setLatitude(cinemaObj.optString(CinemaConstant.LATITUDE));
							cinemaTo.setLongitude(cinemaObj.optString(CinemaConstant.LONGITUDE));
							cinemaTo.setFacs(cinemaObj.optString(CinemaConstant.FACS));
							cinemaTo.setImage(cinemaObj.optString(CinemaConstant.THUMBNAIL));
							cinemaTo.setCityName(cinemaObj.optString(CinemaConstant.CITY_NAME));
							cinemaTo.setPhone(cinemaObj.optString(CinemaConstant.PHONE));
							cinemaTo.setEmail(cinemaObj.optString(CinemaConstant.EMAIL));
							cinemaTo.setDistance(cinemaObj.optString(CinemaConstant.DISTANCE));
							
							JSONObject classObj = cinemaObj.optJSONObject(CinemaConstant.CINEMA_CLASS_DATA);
							JSONArray cinemaClassArray = classObj.optJSONArray(CinemaConstant.CINEMA_CLASS_ARRAY);
							
							ArrayList<CinemaClassTo> classList = new ArrayList<CinemaClassTo>();
							for(int j = 0; j < cinemaClassArray.length(); j++){
							
								JSONObject classValObj = cinemaClassArray.optJSONObject(j);
								CinemaClassTo classTo = new CinemaClassTo();
								
								classTo.setClassId(classValObj.optString(CinemaConstant.CINEMA_CLASS_ID));
								classTo.setClassName(classValObj.optString(CinemaConstant.CINEMA_CLASS_NAME));
								
								classList.add(classTo);
							}
							cinemaTo.setClassList(classList);
							
							list.add(cinemaTo);

						}
//						shortList(pref.getCityName());
						initListview();
					}else{
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
						 AlertDialog dialog = new AlertDialog(getActivity(), "No cinemas are available", new DialogListener() {
								
								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
							dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
//			schedule.GetCinemaListAsync(pref.getDeviceId(), getActivity());
			schedule.GetCinemaListByCityAsync(pref.getDeviceId(), cityCode, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
    public void callServiceAllCity(String cityCode){
		
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cityStatus = true;

				System.out.println("Cinema Response city: " +Data.toString());
				list = new ArrayList<CinemaTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaConstant.ERROR_DESC).equals("0")){
						JSONArray mainArray = mainObj.optJSONArray(CinemaConstant.CINEMA_ARRAY);
						for(int i = 0; i < mainArray.length(); i++){
							
							JSONObject cinemaObj = mainArray.optJSONObject(i);
							CinemaTo cinemaTo = new CinemaTo();
							cinemaTo.setCinemaId(cinemaObj.optString(CinemaConstant.CINEMA_ID));
							cinemaTo.setCinemaName(cinemaObj.optString(CinemaConstant.CINEMA_NAME));
							cinemaTo.setMultiplexId(cinemaObj.optString(CinemaConstant.MULTIPLEX_ID));
							cinemaTo.setAddress(cinemaObj.optString(CinemaConstant.ADDRESS));
							cinemaTo.setCityId(cinemaObj.optString(CinemaConstant.CITY_ID));
							cinemaTo.setLocation(cinemaObj.optString(CinemaConstant.LOCATION));
							cinemaTo.setLatitude(cinemaObj.optString(CinemaConstant.LATITUDE));
							cinemaTo.setLongitude(cinemaObj.optString(CinemaConstant.LONGITUDE));
							cinemaTo.setFacs(cinemaObj.optString(CinemaConstant.FACS));
							cinemaTo.setImage(cinemaObj.optString(CinemaConstant.THUMBNAIL));
							cinemaTo.setCityName(cinemaObj.optString(CinemaConstant.CITY_NAME));
							cinemaTo.setPhone(cinemaObj.optString(CinemaConstant.PHONE));
							cinemaTo.setEmail(cinemaObj.optString(CinemaConstant.EMAIL));
							cinemaTo.setDistance(cinemaObj.optString(CinemaConstant.DISTANCE));
							
							JSONObject classObj = cinemaObj.optJSONObject(CinemaConstant.CINEMA_CLASS_DATA);
							JSONArray cinemaClassArray = classObj.optJSONArray(CinemaConstant.CINEMA_CLASS_ARRAY);
							
							ArrayList<CinemaClassTo> classList = new ArrayList<CinemaClassTo>();
							for(int j = 0; j < cinemaClassArray.length(); j++){
							
								JSONObject classValObj = cinemaClassArray.optJSONObject(j);
								CinemaClassTo classTo = new CinemaClassTo();
								
								classTo.setClassId(classValObj.optString(CinemaConstant.CINEMA_CLASS_ID));
								classTo.setClassName(classValObj.optString(CinemaConstant.CINEMA_CLASS_NAME));
								
								classList.add(classTo);
							}
							cinemaTo.setClassList(classList);
							
							list.add(cinemaTo);

						}
						shortList(pref.getCityName());
						initListview();
					}else{
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
						AlertDialog dialog = new AlertDialog(getActivity(), "No cinemas are available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetCinemaListAsync(pref.getDeviceId(), getActivity());
//			schedule.GetCinemaListByCityAsync(pref.getDeviceId(), cityCode, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
    
    public void callServiceNearMe(String cityCode, String latitude, String longitude){
		
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cityStatus = true;

				System.out.println("Cinema Response: " +Data.toString());
				list = new ArrayList<CinemaTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaConstant.ERROR_DESC).equals("0")){
						JSONArray mainArray = mainObj.optJSONArray(CinemaConstant.CINEMA_ARRAY);
						for(int i = 0; i < mainArray.length(); i++){
							
							JSONObject cinemaObj = mainArray.optJSONObject(i);
							CinemaTo cinemaTo = new CinemaTo();
							cinemaTo.setCinemaId(cinemaObj.optString(CinemaConstant.CINEMA_ID));
							cinemaTo.setCinemaName(cinemaObj.optString(CinemaConstant.CINEMA_NAME));
							cinemaTo.setMultiplexId(cinemaObj.optString(CinemaConstant.MULTIPLEX_ID));
							cinemaTo.setAddress(cinemaObj.optString(CinemaConstant.ADDRESS));
							cinemaTo.setCityId(cinemaObj.optString(CinemaConstant.CITY_ID));
							cinemaTo.setLocation(cinemaObj.optString(CinemaConstant.LOCATION));
							cinemaTo.setLatitude(cinemaObj.optString(CinemaConstant.LATITUDE));
							cinemaTo.setLongitude(cinemaObj.optString(CinemaConstant.LONGITUDE));
							cinemaTo.setFacs(cinemaObj.optString(CinemaConstant.FACS));
							cinemaTo.setImage(cinemaObj.optString(CinemaConstant.THUMBNAIL));
							cinemaTo.setCityName(cinemaObj.optString(CinemaConstant.CITY_NAME));
							cinemaTo.setPhone(cinemaObj.optString(CinemaConstant.PHONE));
							cinemaTo.setEmail(cinemaObj.optString(CinemaConstant.EMAIL));
							cinemaTo.setDistance(cinemaObj.optString(CinemaConstant.DISTANCE));
							
							JSONObject classObj = cinemaObj.optJSONObject(CinemaConstant.CINEMA_CLASS_DATA);
							JSONArray cinemaClassArray = classObj.optJSONArray(CinemaConstant.CINEMA_CLASS_ARRAY);
							
							ArrayList<CinemaClassTo> classList = new ArrayList<CinemaClassTo>();
							for(int j = 0; j < cinemaClassArray.length(); j++){
							
								JSONObject classValObj = cinemaClassArray.optJSONObject(j);
								CinemaClassTo classTo = new CinemaClassTo();
								
								classTo.setClassId(classValObj.optString(CinemaConstant.CINEMA_CLASS_ID));
								classTo.setClassName(classValObj.optString(CinemaConstant.CINEMA_CLASS_NAME));
								
								classList.add(classTo);
							}
							cinemaTo.setClassList(classList);
							
							list.add(cinemaTo);

						}
						
						initListview();
					}else{
                        AlertDialog dialog = new AlertDialog(getActivity(), "No cinemas are available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetNearMeCinemaListAsync("", latitude, longitude, getActivity());
//			schedule.GetCinemaListByCityAsync(pref.getDeviceId(), cityCode, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initListview(){
		
		
		adapter = new CinemasAdapter(getActivity(), list, CinemasFragment.this);
		listView.setAdapter(adapter);
		/*ViewUtility.setListViewHeightBasedOnCinemasFragment(listView, getActivity());
		listView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	
	
	public void shortList(String cityName){
		
		String statusVal = "";
		CinemaTo cinTo;
		int counter = 0;
		
		for(int i = 0; i < list.size(); i++){
			
			CinemaTo cinemaTo = list.get(i);
			if(cinemaTo.getCityName().trim().equals(cityName.trim())){
				
//				statusVal = list.get(counter).getCityName();
//				list.get(counter).setCityName(cinemaTo.getCityName());
//				list.get(i).setCityName(statusVal);
				
				cinTo = list.get(counter);
				list.set(counter, cinemaTo);
				list.set(i, cinTo);
								
				counter++;
			}
			
		}
		
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		
	}

	
}
