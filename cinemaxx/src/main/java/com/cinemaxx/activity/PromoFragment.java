package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.PromoAdapter;
import com.cinemaxx.bean.PromoTo;
import com.cinemaxx.constant.PromoConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class PromoFragment extends Fragment{

	TextView locationNameTv;
	GridView gridView;
	CinemaxxPref pref;
	ArrayList<PromoTo> list;
	PromoAdapter adapter;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_promo, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		initListener();
		activity = (MainActivity) getActivity();
		
        locationNameTv.setText("Location: "+pref.getCityName());
		
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.promo_location_tv);
		locationNameTv.setTypeface(faceThin);
		gridView = (GridView) getView().findViewById(R.id.promo_gv);
		pref = new CinemaxxPref(getActivity());
		list = new ArrayList<PromoTo>();
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnPromo();
	}
	
	public void initListener(){
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				PromoTo promoTo = (PromoTo) parent.getItemAtPosition(position);
				pref.setPromoId(promoTo.getPromoId());
				
				Fragment fragment = new PromoDetailsFragment();
				FragmentContainer.getInstance().setFragment(
						CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(
							fragment);
					ft.setCustomAnimations(R.anim.enter_from_right,
							R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
				
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " +Data.toString());
				list = new ArrayList<PromoTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(PromoConstant.ERROR_DESC).equals("0")){
						JSONArray promoArray = mainObj.optJSONArray(PromoConstant.PROMO_ARRAY);
						for(int i = 0; i < promoArray.length(); i ++){
							
							JSONObject promoObj = promoArray.optJSONObject(i);
							PromoTo promoTo = new PromoTo();
							
							promoTo.setPromoId(promoObj.optString(PromoConstant.PROMO_ID));
							promoTo.setPromoTitle(promoObj.optString(PromoConstant.PROMO_TITLE));
							promoTo.setPromoImage(promoObj.optString(PromoConstant.PROMO_IMAGE));
							
							list.add(promoTo);
						}
						
						initGrid();
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
                        AlertDialog dialog = new AlertDialog(getActivity(), "No promos are available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			
			schedule.GetPromoAsync(pref.getDeviceId(), getActivity());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initGrid(){
		
		adapter = new PromoAdapter(getActivity(), list, PromoFragment.this);
		gridView.setAdapter(adapter);
		/*ViewUtility.setListViewHeightBasedOnPromoFragment(gridView, getActivity());
		gridView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	
	

}
