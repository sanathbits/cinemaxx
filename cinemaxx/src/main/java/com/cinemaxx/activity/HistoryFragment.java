package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.HistoryAdapter;
import com.cinemaxx.bean.HistoryTo;
import com.cinemaxx.constant.CinemaConstant;
import com.cinemaxx.constant.HistoryConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class HistoryFragment extends Fragment{

	TextView locationTv;
	ListView listView;
	ArrayList<HistoryTo> dataList;
	HistoryAdapter adapter;
	MainActivity activity;
	CinemaxxPref pref;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_history, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
		initListener();
		
		activity = (MainActivity) getActivity();
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv = (TextView) getView().findViewById(R.id.history_location_tv);
		locationTv.setTypeface(faceThin);
		locationTv.setText("Location: "+pref.getCityName());
		listView = (ListView) getView().findViewById(R.id.history_lv);

		MainActivity act = (MainActivity) getActivity();
		act.clickOnHistory();
	}
	
	public void initListener(){
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				HistoryTo hisTo = (HistoryTo) parent.getItemAtPosition(position);
				
				Fragment frag = new HistoryDetailsFragment(hisTo.getOrderId());
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (frag != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(frag);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, frag, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				dataList = new ArrayList<HistoryTo>();
				
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(HistoryConstant.ERROR_DESC).equals("0")){
						JSONArray mainArray = mainObj.optJSONArray(HistoryConstant.BOOKING_HISTORY_ARRAY);
						for(int i = 0; i < mainArray.length(); i++){
							JSONObject bookingObj = mainArray.optJSONObject(i);
							HistoryTo hisTo = new HistoryTo();
							
							hisTo.setBookingId(bookingObj.optString(HistoryConstant.BOOKING_ID));
							hisTo.setBookingNumber(bookingObj.optString(HistoryConstant.BOOKING_NUMBER));
							hisTo.setMovieName(bookingObj.optString(HistoryConstant.MOVIE_NAME));
							hisTo.setOrderId(bookingObj.optString(HistoryConstant.ORDER_ID));
							hisTo.setShowTime(bookingObj.optString(HistoryConstant.SHOW_TIME));
							hisTo.setTotalPrice(bookingObj.optString(HistoryConstant.TOTAL_PRICE));
							
							dataList.add(hisTo);
						}
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
						AlertDialog dialog = new AlertDialog(getActivity(), "No histories are available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
				initListView();
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetHistoryAsync(pref.getDeviceId(), getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initListView(){
		
		adapter = new HistoryAdapter(getActivity(), HistoryFragment.this, dataList);
		listView.setAdapter(adapter);
		
	}

}
