package com.cinemaxx.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.constant.NewsDescriptionConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsDetailsFragment extends Fragment{

	TextView locationNameTv, newsTitleTv, newsDateTv, newsContentTv;
	ImageView newsImageIv;
	ImageLoader imageLoader;
	CinemaxxPref pref;
	MainActivity activity;
	WebView webView;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_news_details, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	
	public void initView(){
		
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf"); 
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		pref = new CinemaxxPref(getActivity());
		locationNameTv = (TextView) getView().findViewById(R.id.news_details_setlocation_txt);
		locationNameTv.setText("Location: "+pref.getCityName());
		locationNameTv.setTypeface(faceThin);
		newsTitleTv = (TextView) getView().findViewById(R.id.news_details_header_txt);
		newsDateTv = (TextView) getView().findViewById(R.id.news_details_datetime_txt);
//		newsContentTv = (TextView) getView().findViewById(R.id.news_details_description_txt);
//		newsContentTv.setTypeface(face);
		webView = (WebView) getView().findViewById(R.id.news_details_webview);
		newsImageIv = (ImageView) getView().findViewById(R.id.news_details_front_img);
		imageLoader = new ImageLoader(getActivity());
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("NEWS");
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(NewsDescriptionConstant.ERROR_DESC).equals("0")){
						JSONArray newsArray = mainObj.optJSONArray(NewsDescriptionConstant.LATEST_NEWS_ARRAY);
						for(int i = 0; i < newsArray.length(); i++){
							
							JSONObject newsObj = newsArray.optJSONObject(i);
							
							imageLoader.DisplayImage(newsObj.optString(NewsDescriptionConstant.NEWS_IMAGE).replace(" ", "%20").trim(), newsImageIv);
							newsTitleTv.setText(newsObj.optString(NewsDescriptionConstant.NEWS_TITLE));
							newsDateTv.setText(newsObj.optString(NewsDescriptionConstant.PUBLISHED_DATE));
//							newsContentTv.setText(Html.fromHtml(newsObj.optString(NewsDescriptionConstant.NEWS_DESCRIPTION)));
							webView.loadData(Utils.getTncWebview(newsObj.optString(NewsDescriptionConstant.NEWS_DESCRIPTION).trim()), "text/html; charset=UTF-8", null);
							
						}
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(),
								"News details are not available", new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
						dialog.show();
					
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			schedule.GetNewsDetailAsync(pref.getNewsId(), getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
