package com.cinemaxx.activity;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.bean.HistoryDescTo;
import com.cinemaxx.bean.OrderTo;
import com.cinemaxx.bean.SeatLayoutTo;
import com.cinemaxx.constant.HistoryConstant;
import com.cinemaxx.constant.PaymentConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract.Events;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class HistoryDetailsFragment extends Fragment{

	TextView bookingIdTv, bookingNumberTv, movieTitleTv, cinemaTv, showTimeTv, formatTv, cinemaNoTv, emailTv, paymentModeTv;
	TextView bookingIdTvCont, bookingNumberTvCont, movieTitleTvCont, cinemaTvCont, showTimeTvCont, formatTvCont, cinemaNoTvCont, emailTvCont, paymentModeTvCont;
	TextView seatSelectedTv, totalSeatTv, pricePerTicketTv, ticketTotalTv, bookingFeeTv, discountTv, totalPriceTv;
	TextView seatSelectedTvCont, totalSeatTvCont, pricePerTicketTvCont, ticketTotalTvCont, bookingFeeTvCont, discountTvCont, totalPriceTvCont;
	TextView containerTv, containerTv1, containerTv2, containerTv3;
	TextView locationTv;
	ImageView imageView;
	LinearLayout containerLayout, savetoHistorylnr;
	Button savetoHistoryBtn;
	CinemaxxPref pref;
	MainActivity activity;
	String orderId;
	ScrollView scrollView;
	
	
	public HistoryDetailsFragment(String orderId){
		this.orderId = orderId;
//		System.out.println(orderId);
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_history_details, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
		
		activity = (MainActivity) getActivity();
        if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		containerLayout = (LinearLayout) getView().findViewById(R.id.history_detals_container_layout);
		containerLayout.setVisibility(View.INVISIBLE);
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		savetoHistorylnr = (LinearLayout) getView().findViewById(R.id.history_save_to_history_lnr);
		savetoHistoryBtn = (Button) getView().findViewById(R.id.history_save_to_history_btn);
		savetoHistorylnr.setVisibility(View.GONE);
        locationTv = (TextView) getView().findViewById(R.id.history_details_location_tv);
        locationTv.setTypeface(faceThin);
        locationTv.setText("Location: "+pref.getCityName());
		
		containerTv = (TextView) getView().findViewById(R.id.history_details_content_tv);
//		containerTv.setTypeface(faceMedium);
		containerTv1 = (TextView) getView().findViewById(R.id.history_details_content_tv1);
//		containerTv1.setTypeface(faceMedium);
		containerTv2 = (TextView) getView().findViewById(R.id.history_details_content_tv2);
//		containerTv2.setTypeface(faceMedium);
		containerTv3 = (TextView) getView().findViewById(R.id.history_details_content_tv3);
//		containerTv3.setTypeface(faceMedium);
		imageView = (ImageView) getView().findViewById(R.id.history_iv);
		
		bookingIdTv = (TextView) getView().findViewById(R.id.history_booked_id);
//		bookingIdTv.setTypeface(faceMedium);
		bookingNumberTv = (TextView) getView().findViewById(R.id.history_booking_no);
//		bookingNumberTv.setTypeface(faceMedium);	
        movieTitleTv = (TextView) getView().findViewById(R.id.history_movie_title);
//        movieTitleTv.setTypeface(faceMedium);
        cinemaTv = (TextView) getView().findViewById(R.id.history_cinema);
//        cinemaTv.setTypeface(faceMedium);
        showTimeTv = (TextView) getView().findViewById(R.id.history_showtime);
//        showTimeTv.setTypeface(faceMedium);
        formatTv = (TextView) getView().findViewById(R.id.history_format);
//        formatTv.setTypeface(faceMedium);
        cinemaNoTv = (TextView) getView().findViewById(R.id.history_cinema_no);
//        cinemaNoTv.setTypeface(faceMedium);
        emailTv = (TextView) getView().findViewById(R.id.history_email);
//        emailTv.setTypeface(faceMedium);
        paymentModeTv = (TextView) getView().findViewById(R.id.history_payment_mode);
//        paymentModeTv.setTypeface(faceMedium);
        
        bookingIdTvCont = (TextView) getView().findViewById(R.id.textView1);
//        bookingIdTvCont.setTypeface(faceMedium);
		bookingNumberTvCont = (TextView) getView().findViewById(R.id.textView2);
//		bookingNumberTvCont.setTypeface(faceMedium);
        movieTitleTvCont = (TextView) getView().findViewById(R.id.textView3);
//        movieTitleTvCont.setTypeface(faceMedium);
        cinemaTvCont = (TextView) getView().findViewById(R.id.textView4);
//        cinemaTvCont.setTypeface(faceMedium);
        showTimeTvCont = (TextView) getView().findViewById(R.id.textView5);
//        showTimeTvCont.setTypeface(faceMedium);
        formatTvCont = (TextView) getView().findViewById(R.id.textView6);
//        formatTvCont.setTypeface(faceMedium);
        cinemaNoTvCont = (TextView) getView().findViewById(R.id.textView7);
//        cinemaNoTvCont.setTypeface(faceMedium);
        emailTvCont = (TextView) getView().findViewById(R.id.textView8);
//        emailTvCont.setTypeface(faceMedium);
        paymentModeTvCont = (TextView) getView().findViewById(R.id.textView9);
//        paymentModeTvCont.setTypeface(faceMedium);
        
        //////////////////////
        seatSelectedTv = (TextView) getView().findViewById(R.id.history_selected);
//        seatSelectedTv.setTypeface(faceMedium);
        totalSeatTv = (TextView) getView().findViewById(R.id.history_total_seat);
//        totalSeatTv.setTypeface(faceMedium);
        pricePerTicketTv = (TextView) getView().findViewById(R.id.history_price_perticket);
//        pricePerTicketTv.setTypeface(faceMedium);
        ticketTotalTv = (TextView) getView().findViewById(R.id.history_ticket_total);
//        ticketTotalTv.setTypeface(faceMedium);
        bookingFeeTv = (TextView) getView().findViewById(R.id.history_booking_fee);
//        bookingFeeTv.setTypeface(faceMedium);
        discountTv = (TextView) getView().findViewById(R.id.history_discount);
//        discountTv.setTypeface(faceMedium);
        totalPriceTv = (TextView) getView().findViewById(R.id.history_total_price);
//        totalPriceTv.setTypeface(faceMedium);
        
        seatSelectedTvCont = (TextView) getView().findViewById(R.id.textView11);
//        seatSelectedTvCont.setTypeface(faceMedium);
        totalSeatTvCont = (TextView) getView().findViewById(R.id.textView12);
//        totalSeatTvCont.setTypeface(faceMedium);
        pricePerTicketTvCont = (TextView) getView().findViewById(R.id.textView13);
//        pricePerTicketTvCont.setTypeface(faceMedium);
        ticketTotalTvCont = (TextView) getView().findViewById(R.id.textView14);
//        ticketTotalTvCont.setTypeface(faceMedium);
        bookingFeeTvCont = (TextView) getView().findViewById(R.id.textView16);
//        bookingFeeTvCont.setTypeface(faceMedium);
        discountTvCont = (TextView) getView().findViewById(R.id.textView17);
//        discountTvCont.setTypeface(faceMedium);
        totalPriceTvCont = (TextView) getView().findViewById(R.id.textView18);
//        totalPriceTvCont.setTypeface(faceMedium);
        scrollView = (ScrollView) getView().findViewById(R.id.scrollView);
	}
	
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
//				containerTv.setText(Data.toString());
				HistoryDescTo historyDescTo = null;
				containerLayout.setVisibility(View.VISIBLE);
//				System.out.println("Last Response: "+Data.toString());
				try{
					JSONObject mainJson = new JSONObject(Data.toString());
//					System.out.println("mainJson:::" + mainJson);
					if(mainJson.optString(PaymentConstant.ERROR_DESC).equals("0")){
						JSONArray mainArray = mainJson.optJSONArray("BookingHistory");
						for(int i = 0; i < mainArray.length(); i++){
							
							JSONObject hisObj = mainArray.optJSONObject(i);
							
							historyDescTo = new HistoryDescTo();
							
							historyDescTo.setBookingId(hisObj.optString(PaymentConstant.BOOKING_ID));
							historyDescTo.setBookingNumber(hisObj.optString(PaymentConstant.BOOKING_NUMBER));
							historyDescTo.setOrderId(hisObj.optString(PaymentConstant.ORDER_ID));
							historyDescTo.setMovieName(hisObj.optString(PaymentConstant.MOVIE_NAME));
							historyDescTo.setCinemaName(hisObj.optString(PaymentConstant.CINEMA_NAME));
							historyDescTo.setShowTime(hisObj.optString(PaymentConstant.SHOW_TIME));
							historyDescTo.setFormat(hisObj.optString(PaymentConstant.FORMAT));
							historyDescTo.setCinemaNo(hisObj.optString(PaymentConstant.CINEMA_NUMBER));
							historyDescTo.setEmail(hisObj.optString(PaymentConstant.EMAIL));
							historyDescTo.setPaymentMode(hisObj.optString(PaymentConstant.PAYMENT_MODE));
							historyDescTo.setSeatNumber(hisObj.optString(PaymentConstant.SEAT_NUMBER));
							historyDescTo.setNoofSeats(hisObj.optString(PaymentConstant.NO_OF_SEAT));
							historyDescTo.setPricePerTicket(hisObj.optString(PaymentConstant.PRICE_PER_TICKET));
							historyDescTo.setTotalTicket(hisObj.optString(PaymentConstant.TOTAL_TICKET));
							historyDescTo.setConvenieceFee(hisObj.optString(PaymentConstant.CONVINENCE_FEE));
							historyDescTo.setDiscountAmount(hisObj.optString(PaymentConstant.DISCOUNT_AMOUNT));
							historyDescTo.setTotalPrice(hisObj.optString(PaymentConstant.TOTAL_PRICE));
							historyDescTo.setQrCode(hisObj.optString(PaymentConstant.QR_CODE).replace("data:image/gif;base64,", ""));
							historyDescTo.setOrderStatus(hisObj.optString(PaymentConstant.STATUS_CODE));
							
							imageView.setImageBitmap(Utils.getBitmap(historyDescTo.getQrCode()));
							bookingIdTv.setText(historyDescTo.getBookingId());
							bookingNumberTv.setText(historyDescTo.getBookingNumber());
					        movieTitleTv.setText(historyDescTo.getMovieName());
					        cinemaTv.setText(historyDescTo.getCinemaName());
					        showTimeTv.setText(historyDescTo.getShowTime());
					        formatTv.setText(historyDescTo.getFormat());
					        cinemaNoTv.setText(historyDescTo.getCinemaNo());
					        emailTv.setText(historyDescTo.getEmail());
					        paymentModeTv.setText(historyDescTo.getPaymentMode());

					        seatSelectedTv.setText(historyDescTo.getSeatNumber());
					        totalSeatTv.setText(historyDescTo.getNoofSeats());
					        pricePerTicketTv.setText(historyDescTo.getPricePerTicket());
					        ticketTotalTv.setText(historyDescTo.getTotalTicket());
					        bookingFeeTv.setText(historyDescTo.getConvenieceFee());
					        discountTv.setText(historyDescTo.getDiscountAmount());
					        totalPriceTv.setText(historyDescTo.getTotalPrice());
					        
						}
						scrollView.setVisibility(View.VISIBLE);
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
						AlertDialog dialog = new AlertDialog(getActivity(), mainJson.optString(PaymentConstant.ERROR_DESC), new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					


				}catch(Exception e){
					e.printStackTrace();
				}
				
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
//			schedule.GetOrderSummaryAsync(seatTo.getRequistId(), orderTo.getOrderId(), "1", "cc", traId, getActivity());
			schedule.GetHistoryDetailAsync(pref.getDeviceId(), orderId, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}
