package com.cinemaxx.activity;

import com.cinemaxx.util.CinemaxxPref;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutMaxxCardFragment extends Fragment{

	TextView locationTv;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_about_maxx_card, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
	}
	
	public void initView(){
		activity = (MainActivity) getActivity();
		locationTv = (TextView) getView().findViewById(R.id.about_maxx_card_location_tv);
		CinemaxxPref pref = new CinemaxxPref(getActivity());
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv.setTypeface(faceThin);
		locationTv.setText("Location: "+pref.getCityName());
		
		activity.setHeaderText("MAXX CARD");
	}

}
