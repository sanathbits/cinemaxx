package com.cinemaxx.activity;

import com.cinemaxx.util.CinemaxxPref;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ContactFragment extends Fragment{

	TextView emailTv, websiteTv, textView1, textView2, textView3;
	MainActivity activity;
	TextView locationTv;
	CinemaxxPref pref;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
		initListener();
		
		
	}

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		activity = (MainActivity) getActivity();
		activity.setHeaderText("CONTACT");

		locationTv = (TextView) getView().findViewById(R.id.contact_setlocation_txt);
		locationTv.setText("Location: "+pref.getCityName());
		locationTv.setTypeface(faceThin);
		emailTv = (TextView) getView().findViewById(R.id.contact_email_tv);
		emailTv.setTypeface(faceMedium);
		emailTv.setText(Html.fromHtml("<u>feedback@cinemaxxtheater.com</u>"));
		websiteTv = (TextView) getView().findViewById(R.id.contact_website_tv);
		websiteTv.setTypeface(faceMedium);
		websiteTv.setText(Html.fromHtml("<u>http://www.cinemaxxtheater.com/ContactUs.aspx</u>"));
		
		textView1 = (TextView) getView().findViewById(R.id.contact_tv1);
		textView1.setTypeface(faceMedium);
		textView2 = (TextView) getView().findViewById(R.id.contact_tv2);
		textView2.setTypeface(faceMedium);
		textView3 = (TextView) getView().findViewById(R.id.contact_tv3);
		textView3.setTypeface(faceMedium);
		
	}
	
	public void initListener() {
		
		emailTv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("plain/text");
				intent.putExtra(Intent.EXTRA_EMAIL,
						new String[] { "feedback@cinemaxxtheater.com" });
				intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
				// intent.putExtra(Intent.EXTRA_TEXT, "mail body");
				startActivity(Intent.createChooser(intent, "Send from my Android"));
			}
		});
		
		websiteTv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.cinemaxxtheater.com/ContactUs.aspx"));
				startActivity(webIntent);
			}
		});
		
	}
	
}
