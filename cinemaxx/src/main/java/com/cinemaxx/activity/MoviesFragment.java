package com.cinemaxx.activity;

import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MoviesFragment extends Fragment{

	Button nowPlayingBtn, commingSoonBtn, favoritesBtn, buyTicketsBtn;
	TextView locationNameTv;
	CinemaxxPref pref;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_movies, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initView();
		initListener();

		
		locationNameTv.setText("Location: "+pref.getCityName());
	}
	
	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		locationNameTv.setTypeface(faceThin);
		locationNameTv.setText("Location: "+pref.getCityName());
		nowPlayingBtn = (Button) getView().findViewById(R.id.movies_now_playing_btn);
		commingSoonBtn = (Button) getView().findViewById(R.id.movies_comming_soon_btn);
		favoritesBtn = (Button) getView().findViewById(R.id.movies_your_favourites_btn);
		buyTicketsBtn = (Button) getView().findViewById(R.id.movies_buy_tickest_btn);
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnMovie();
	}
	
	public void initListener(){
		
		nowPlayingBtn.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new NowPlayingFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
		commingSoonBtn.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new ComingSoonFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
		favoritesBtn.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new FavoritesFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
		buyTicketsBtn.setOnClickListener(new OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new BuyTicketsFragment("", "", "","","","","", "");
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
	}
	
	

}
