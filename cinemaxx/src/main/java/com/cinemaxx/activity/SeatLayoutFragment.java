package com.cinemaxx.activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.bean.BuyTicketsTo;
import com.cinemaxx.bean.MaxxCardDetailsTo;
import com.cinemaxx.bean.OrderTo;
import com.cinemaxx.bean.SeatLayoutClassListJunior;
import com.cinemaxx.bean.SeatLayoutClassListTo;
import com.cinemaxx.bean.SeatLayoutTo;
import com.cinemaxx.constant.MaxxCardDetailsConstant;
import com.cinemaxx.constant.OrderConstant;
import com.cinemaxx.constant.SeatLayoutConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.dialog.ClassListDialog;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.listener.NumberofseatInterface;
import com.cinemaxx.loyaltyservice.Loyalty;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.SharedPreference;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import application.MyApplicationClass;

public class SeatLayoutFragment extends Fragment {

	WebView webView;
	Button plusBtn, continueBtn;
	TextView movieTv, cinemaTv, showTimeTv, formatTv, cinemaNoTv, seatSelectedTv, totalSeatTv, pricePerTicketTv, ticketTotalTv, bookingFeeTv, totalPriceTv, timerTv;
	TextView locationTv;
	LinearLayout infoLayout;
	boolean infoStatus = false;
	String scheduleID;
	BuyTicketsTo buyTicketsTo;
	ArrayList<String> seatList;
	ArrayList<Integer> priceList;
    ArrayList<Integer> bookingList;
	String layout;
	int maxNumber;
	SeatLayoutTo seatTo;
	String selectedSeat = "";
	CinemaxxPref pref;
	String movieId;
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	MyApplicationClass myApplicationClass;
	int totalPriceValue;
	Timer localTimer;
	CountDownTimer cTimer;
	RelativeLayout timerLayout;
	MainActivity activity;
	String numberofseats="";
	String seatdetails="";
    SeatLayoutClassListTo _package = null;


	private LinearLayout sofabed,cinemaseat,lounger,beanbag,totalprice;
	private String setat="";int count=0,cinemastcount=0,loungercount=0,bebangcount=0;
	private ScrollView scrollvie;
	private TextView sofabed_pricepertickit,sofabedsets,sofabedty,sofabedtickittotal,sofabedbookingfees;
	private TextView cinemast_pricepertickit,cinemastsets,cinemasttickittotal,cinemastbookingfees;
	private TextView loungere_pricepertickit,loungeresets,loungeretickittotal,loungerebookingfees;
	private TextView beabang_pricepertickit,beabangsets,beabangtickittotal,beabangbookingfees,totalpricevalue;
	private int price=0;
	//private LinearLayout pricepertickitLayout;
    private LinearLayout bookingfeesLayout,TicketTotalLayout;
	//private TextView pricepertickit;
	private TextView bookingsfees,ticketTotal;
	private ArrayList<SeatLayoutClassListTo> classListeJunior;
	private SharedPreference shPreference;

	public SeatLayoutFragment(String scheduleID, BuyTicketsTo buyTo, String movieId) {
		this.scheduleID = scheduleID;
		System.out.println("Schedule Id : " + scheduleID);
		buyTicketsTo = buyTo;
		this.movieId = movieId;


	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_seat_layout, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initView();
		initListener();
		shPreference = new SharedPreference();
		activity = (MainActivity) getActivity();

		if (Utils.isNetworkAvailable(getActivity())) {
			callService(scheduleID);
			
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}

		activity.clickOnSeatSelection();

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if (cTimer != null) {
			cTimer.cancel();
		}
	}

	@SuppressWarnings("deprecation")
	public void initView() {

		seatList = new ArrayList<String>();
		priceList = new ArrayList<Integer>();
        bookingList = new ArrayList<Integer>();
		pref = new CinemaxxPref(getActivity());

		timerLayout = (RelativeLayout) getView().findViewById(R.id.seat_timer_layout);
		timerLayout.setVisibility(View.GONE);
		webView = (WebView) getView().findViewById(R.id.seat_layout_webview);
		plusBtn = (Button) getView().findViewById(R.id.seat_layout_cinemaInfo_btn);


		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");

		locationTv = (TextView) getView().findViewById(R.id.news_details_setlocation_txt);
		locationTv.setText("Location: " + pref.getCityName());
		locationTv.setTypeface(faceThin);

		movieTv = (TextView) getView().findViewById(R.id.seat_layout_movie_tv);
		movieTv.setTypeface(faceMedium);
		TextView movieTvN = (TextView) getView().findViewById(R.id.seat_layout_movie_tv_n);
		movieTvN.setTypeface(faceMedium);

		timerTv = (TextView) getView().findViewById(R.id.seat_layout_timer_tv);
		timerTv.setTypeface(faceMedium);
		timerTv.setVisibility(View.INVISIBLE);

		cinemaTv = (TextView) getView().findViewById(R.id.seat_layout_cinema_tv);
		cinemaTv.setTypeface(faceMedium);
		cinemaTv.setText(buyTicketsTo.getCinemaName());
		TextView cinemaTvN = (TextView) getView().findViewById(R.id.seat_layout_cinema_tv_n);
		cinemaTvN.setTypeface(faceMedium);

		showTimeTv = (TextView) getView().findViewById(R.id.seat_layout_showtime_tv);
		showTimeTv.setTypeface(faceMedium);
		showTimeTv.setText(buyTicketsTo.getTime().replace("(2D)", "").replace("(3D)", "") + " | " + buyTicketsTo.getDate());
		Log.d("time", "time" +buyTicketsTo.getTime());
		Log.d("", "");
		TextView showTimeTvN = (TextView) getView().findViewById(R.id.seat_layout_showtime_tv_n);
		showTimeTvN.setTypeface(faceMedium);

		formatTv = (TextView) getView().findViewById(R.id.seat_layout_format_tv);
		formatTv.setTypeface(faceMedium);
		formatTv.setText(buyTicketsTo.getCinemaTypeName());
		TextView formatTvN = (TextView) getView().findViewById(R.id.seat_layout_format_tv_n);
		formatTvN.setTypeface(faceMedium);

		cinemaNoTv = (TextView) getView().findViewById(R.id.seat_layout_cinema_no_tv);
		cinemaNoTv.setTypeface(faceMedium);
		TextView cinemaNoTvN = (TextView) getView().findViewById(R.id.seat_layout_cinema_no_tv_n);
		cinemaNoTvN.setTypeface(faceMedium);

		seatSelectedTv = (TextView) getView().findViewById(R.id.seat_layout_seat_selected_tv);
		seatSelectedTv.setTypeface(faceMedium);
		TextView seatSelectedTvN = (TextView) getView().findViewById(R.id.seat_layout_seat_selected_tv_n);
		seatSelectedTvN.setTypeface(faceMedium);

		totalSeatTv = (TextView) getView().findViewById(R.id.seat_layout_number_of_seats_tv);
		totalSeatTv.setTypeface(faceMedium);
		TextView totalSeatTvN = (TextView) getView().findViewById(R.id.seat_layout_number_of_seats_tv_n);
		totalSeatTvN.setTypeface(faceMedium);

		pricePerTicketTv = (TextView) getView().findViewById(R.id.seat_layout_number_of_price_pre_ticket_tv);
		pricePerTicketTv.setTypeface(faceMedium);
		TextView pricePerTicketTvN = (TextView) getView().findViewById(R.id.seat_layout_number_of_price_pre_ticket_tv_n);
		pricePerTicketTvN.setTypeface(faceMedium);

		ticketTotalTv = (TextView) getView().findViewById(R.id.seat_layout_number_of_ticket_total_tv);
		ticketTotalTv.setTypeface(faceMedium);
		TextView ticketTotalTvN = (TextView) getView().findViewById(R.id.seat_layout_number_of_ticket_total_tv_n);
		ticketTotalTvN.setTypeface(faceMedium);

		bookingFeeTv = (TextView) getView().findViewById(R.id.seat_layout_number_of_booking_fee_tv);
		bookingFeeTv.setTypeface(faceMedium);
		TextView bookingFeeTvN = (TextView) getView().findViewById(R.id.seat_layout_number_of_booking_fee_tv_n);
		bookingFeeTvN.setTypeface(faceMedium);

		totalPriceTv = (TextView) getView().findViewById(R.id.seat_layout_number_of_total_price_tv);
		totalPriceTv.setTypeface(faceMedium);
		TextView totalPriceTvN = (TextView) getView().findViewById(R.id.seat_layout_number_of_total_price_tv_n);
		totalPriceTvN.setTypeface(faceMedium);

		infoLayout = (LinearLayout) getView().findViewById(R.id.seat_layout_cinemainfo_layout);
		infoLayout.setVisibility(View.GONE);

		continueBtn = (Button) getView().findViewById(R.id.seat_layout_continue_btn);
		
		sofabed=(LinearLayout)getView().findViewById(R.id.sofabed);
		cinemaseat=(LinearLayout)getView().findViewById(R.id.cinemast);
		lounger=(LinearLayout)getView().findViewById(R.id.lounger);		
		beanbag=(LinearLayout)getView().findViewById(R.id.beebang);
		scrollvie=(ScrollView)getView().findViewById(R.id.scrollid);
		
		sofabed_pricepertickit=(TextView)getView().findViewById(R.id.sofabed_priceper_tickit_value);
		sofabedsets=(TextView)getView().findViewById(R.id.sofabed_seats_value);
		sofabedty=(TextView)getView().findViewById(R.id.sofabed_priceper_tickit_value_ty);
		sofabedtickittotal=(TextView)getView().findViewById(R.id.sofabed_ticket_total_value);
		sofabedbookingfees=(TextView)getView().findViewById(R.id.sofabed_bookingfee_value);
		
		cinemast_pricepertickit=(TextView)getView().findViewById(R.id.cinemast_priceper_tickit_value);
		cinemastsets=(TextView)getView().findViewById(R.id.cinemast_seats_value);
		cinemasttickittotal=(TextView)getView().findViewById(R.id.cinemast_ticket_total_value);
		cinemastbookingfees=(TextView)getView().findViewById(R.id.cinemast_bookingfee_value);
		
		 loungere_pricepertickit=(TextView)getView().findViewById(R.id.lounger_priceper_tickit_value);
		 loungeresets=(TextView)getView().findViewById(R.id.lounger_seats_value);
		 loungeretickittotal=(TextView)getView().findViewById(R.id.lounger_ticket_total_value);		 
		 loungerebookingfees=(TextView)getView().findViewById(R.id.lounger_bookingfee_value);
		
		beabang_pricepertickit=(TextView)getView().findViewById(R.id.beebang_priceper_tickit_value);
		beabangsets=(TextView)getView().findViewById(R.id.beebang_seats_value);
		beabangtickittotal=(TextView)getView().findViewById(R.id.beebang_ticket_total_value);
		beabangbookingfees=(TextView)getView().findViewById(R.id.beebang_bookingfee_value);
		//pricepertickitLayout=(LinearLayout)getView().findViewById(R.id.seat_layout_number_of_seats_tv_pricepertickit_vie);
		TicketTotalLayout=(LinearLayout)getView().findViewById(R.id.seat_layout_number_of_seats_tv_tickettotal_vie);
		bookingfeesLayout=(LinearLayout)getView().findViewById(R.id.seat_layout_number_of_seats_tv_bookingfees);
		
		//pricepertickit=(TextView)getView().findViewById(R.id.seat_layout_number_of_seats_tv_pricepertickit);
		bookingsfees=(TextView)getView().findViewById(R.id.seat_layout_number_of_seats_tv_bookingfee);
		ticketTotal=(TextView)getView().findViewById(R.id.seat_layout_number_of_seats_tv_tickettotal);

        //android.webkit.webView.
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(false);
		webSettings.setAllowFileAccess(true);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setSavePassword(false);
		webSettings.setSaveFormData(false);
	}

	public void callService(final String scheduleId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				try {
					System.out.println("Response : " + Data.toString());
					JSONObject mainJson = new JSONObject(Data.toString());
					System.out.println("Schedule Id: " + scheduleId);
					System.out.println("Seat Layout: " + Data.toString());

					if (mainJson.optString(SeatLayoutConstant.ERROR_DESC).equals("0")) {
						seatTo = new SeatLayoutTo();
						layout = mainJson.optString(SeatLayoutConstant.LAYOUT);
						maxNumber = Integer.parseInt(mainJson.optString(SeatLayoutConstant.MAX_SEAT));

						seatTo.setRequistId(mainJson.optString(SeatLayoutConstant.REQUEST_ID));
						seatTo.setCinemaClassId(mainJson.optString(SeatLayoutConstant.CINEMA_CLASS_ID));
						seatTo.setCinemaClassName(mainJson.optString(SeatLayoutConstant.CINEMA_CLASS_NAME));
						seatTo.setCinemaName(mainJson.optString(SeatLayoutConstant.CINEMA_NAME));
						seatTo.setCinemaNo(mainJson.optString(SeatLayoutConstant.CINEMA_NO));
						cinemaNoTv.setText(mainJson.optString(SeatLayoutConstant.CINEMA_NO));

						JSONArray classArray = mainJson.optJSONArray(SeatLayoutConstant.CLASS_LIST);
						ArrayList<SeatLayoutClassListTo> classList = new ArrayList<SeatLayoutClassListTo>();
					    classListeJunior = new ArrayList<SeatLayoutClassListTo>();
						
						for (int i = 0; i < classArray.length(); i++) {
							SeatLayoutClassListTo classListTo = new SeatLayoutClassListTo();
							JSONObject classObj;
							
								classObj = classArray.optJSONObject(i);
								seatTo.setBookingFees(classObj.optString(SeatLayoutConstant.BOOKING_FEE));
								seatTo.setPrice(classObj.optString(SeatLayoutConstant.PRICE));

								classListTo.setAreaCode(classObj.optString(SeatLayoutConstant.AREA_CODE));
								classListTo.setAvailableSeats(classObj.optString(SeatLayoutConstant.AVAILABLE_SEATS));
								classListTo.setBookingFee(classObj.optString(SeatLayoutConstant.BOOKING_FEE));
								classListTo.setClassIdDP(classObj.optString("ClassIdDP"));
								classListTo.setIsLoyalty(classObj.optBoolean(SeatLayoutConstant.IS_LOYALTY));
								classListTo.setIsPackage(classObj.optBoolean(SeatLayoutConstant.IS_PACKAGE));
								classListTo.setMaxQty(classObj.optString(SeatLayoutConstant.MAXQTY));
								classListTo.setMinNoOfSeats(classObj.optString(SeatLayoutConstant.MIN_NO_OF_SEATS));
								classListTo.setMinQty(classObj.optString(SeatLayoutConstant.MINQTY));
								classListTo.setPackageName(classObj.optString(SeatLayoutConstant.PACKAGE_NAME));
								classListTo.setPrice(classObj.optString(SeatLayoutConstant.PRICE));
								classListTo.setQty(classObj.optString(SeatLayoutConstant.QTY));
								classListTo.setScheduleClassId(classObj.optString(SeatLayoutConstant.SCHEDULE_CLASS_ID));
								classListTo.setSeatClass(classObj.optString(SeatLayoutConstant.SEAT_CLASS));
							
							
							classListeJunior.add(classListTo);

							if(classObj.optBoolean(SeatLayoutConstant.IS_LOYALTY)){
								classList.add(classListTo);
							}
						}
						
						
						seatTo.setClassList(classList);
						/*if (seatTo.getCinemaNo().equals("JUNIOR")) {											

							seatTo.setClassList(classListeJunior);

						}*/

						seatTo.setMaxSeat(mainJson.optString(SeatLayoutConstant.MAX_SEAT));
						seatTo.setMovieFormat(mainJson.optString(SeatLayoutConstant.MOVIE_FORMAT));
						seatTo.setMovieId(movieId);
						seatTo.setMovieName(mainJson.optString(SeatLayoutConstant.MOVIE_NAME));
						movieTv.setText(mainJson.optString(SeatLayoutConstant.MOVIE_NAME));
						seatTo.setMovieRating(mainJson.optString(SeatLayoutConstant.MOVIE_RATING));
						seatTo.setShowDate(mainJson.optString(SeatLayoutConstant.SHOW_DATE));
						seatTo.setShowTime(mainJson.optString(SeatLayoutConstant.SHOW_TIME));
						seatTo.setFormat(mainJson.optString(SeatLayoutConstant.FORMAT));
						formatTv.setText(mainJson.optString(SeatLayoutConstant.FORMAT));
						seatTo.setScreenName(mainJson.optString(SeatLayoutConstant.SCREEN));
						seatTo.setSeatSelected(seatSelectedTv.getText().toString().trim());
						seatTo.setNoOfSeat(String.valueOf(seatList.size()));

						if (seatTo.getClassList().size() != 0 ) {
							new ClassListDialog(activity, seatTo.getClassList(),numberinterface);
						}

						if (seatTo.getCinemaNo().equals("JUNIOR")) {
							//pricepertickitLayout.setVisibility(View.GONE);
							TicketTotalLayout.setVisibility(View.GONE);
							bookingfeesLayout.setVisibility(View.GONE);
							seatTo.setClassList(classListeJunior);

							webView.addJavascriptInterface(new IJavascriptHandler(), "cpjs");	
							webView.loadData(Utils.getHtmlCodetest(layout, maxNumber), "text/html", "UTF-8");
							webView.setVisibility(View.INVISIBLE);
							timerLayout.setVisibility(View.VISIBLE);

						} else {	
							//pricepertickitLayout.setVisibility(View.VISIBLE);
							TicketTotalLayout.setVisibility(View.VISIBLE);

							bookingfeesLayout.setVisibility(View.VISIBLE);
							webView.addJavascriptInterface(new IJavascriptHandler(), "cpjs");
                            seatTo.setClassList(classListeJunior);
                            //actual was getHtmlCode
							webView.loadData(Utils.getHtmlCode(layout, String.valueOf(maxNumber)), "text/html", "UTF-8");
							webView.setVisibility(View.INVISIBLE);
							timerLayout.setVisibility(View.VISIBLE);
							setTimer(Integer.parseInt(pref.getTimerTime()));
						}

					} else {
						AlertDialog dialog = new AlertDialog(getActivity(), mainJson.optString(SeatLayoutConstant.ERROR_DESC), new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				activity.setNotClickable();
			}

			/*private String getStringfromasset() {
				 StringBuilder returnString = new StringBuilder();
				    InputStream fIn = null;
				    InputStreamReader isr = null;
				    BufferedReader input = null;
				    try {
				        fIn = getActivity().getResources().getAssets()
				                .open("custom.js", Context.MODE_WORLD_READABLE);
				        isr = new InputStreamReader(fIn);
				        input = new BufferedReader(isr);
				        String line = "";
				        while ((line = input.readLine()) != null) {
				            returnString.append(line);
				        }
				    } catch (Exception e) {
				        e.getMessage();
				    } finally {
				        try {
				            if (isr != null)
				                isr.close();
				            if (fIn != null)
				                fIn.close();
				            if (input != null)
				                input.close();
				        } catch (Exception e2) {
				            e2.getMessage();
				        }
				    }
				    return returnString.toString();
			}*/

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try {

			// schedule.GetSeatLayoutAsync(scheduleId, getActivity());

			if (pref.getMaxxCardLoginResponse().trim().length() > 10) {

				System.out.println("Seat Layout Parameter: " + scheduleId + " : " + getMaxxCardLoyaltyID() + " : " + true + " : " + getMaxxCardMemberID());
				schedule.GetSeatLayoutAsync(scheduleId, getMaxxCardLoyaltyID(), true, getMaxxCardMemberID(), getActivity());

			} else {
				System.out.println("...........................//" + scheduleId + "//" + 01 + "//" + true + "//" + "0");
				schedule.GetSeatLayoutAsync(scheduleId, 0l, true, "0", getActivity());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	NumberofseatInterface numberinterface = new NumberofseatInterface() {

		@Override
		public void onCompleted(String value) {

			numberofseats=value;

		}
	};

	protected void generateNoteOnSD(SeatLayoutFragment seatLayoutFragment,
			String string, String string2) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date now = new Date();
		String fileName = formatter.format(now) + ".html";


		try
		{
			File root = new File(Environment.getExternalStorageDirectory()+File.separator+"Cinemax", "Data Collection");


			if (!root.exists()) 
			{
				root.mkdirs();
			}
			File gpxfile = new File(root, fileName);


			FileWriter writer = new FileWriter(gpxfile,true);
			writer.append(string2+"\n\n");
			writer.flush();
			writer.close();

		}
		catch(IOException e)
		{
			e.printStackTrace();

		}
	}

		public String getMaxxCardMemberID() {
		pref = new CinemaxxPref(getActivity());
		if (pref.getMaxxCardLoginResponse().trim().length() > 10) {
			try {

				JSONObject mainObj = new JSONObject(pref.getMaxxCardLoginResponse().trim());
				if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {

					JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
					return memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID);

				}

			} catch (Exception e) {
				return "";
			}
		}
		return "";
	}

	public long getMaxxCardLoyaltyID() {
		pref = new CinemaxxPref(getActivity());
		long returnValue = 0l;
		if (pref.getMaxxCardLoginResponse().trim().length() > 10) {
			try {

				JSONObject mainObj = new JSONObject(pref.getMaxxCardLoginResponse().trim());
				if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {

					JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
					returnValue = Long.valueOf(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));

					return returnValue;

				}

			} catch (Exception e) {
				return returnValue;
			}
		}
		return returnValue;
	}

	public void callServiceContinue() {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				try {
					System.out.println("Responseconfirmbutton: " + Data.toString());
					JSONObject mainJson = new JSONObject(Data.toString());
					seatdetails=mainJson.getString("SeatDetails");
					if(seatdetails.equals("null")){

						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), "Please select Sofa Bed seats separately.", new DialogListener() {

							@Override
							public void onOkClick() {
								Fragment fragment = new HomeFragment();
								FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
								if (fragment != null) {
									FragmentTransaction ft = getFragmentManager().beginTransaction();
									CurrentFragmentSingletone.getInstance().setFragment(fragment);
									ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
									ft.replace(R.id.frame_container, fragment, "fragment");
									// Start the animated transition.
									ft.commit();
								}

								// TODO Auto-generated method stub
								//activity.callBackPress();
								
								
							}
						});
						dialog.show();
					
					}

					OrderTo orderTo = new OrderTo();

					orderTo.setOrderDate(mainJson.optString(OrderConstant.ORDER_DATE));
					orderTo.setOrderId(mainJson.optString(OrderConstant.ORDER_ID));

					JSONObject aObj = mainJson.optJSONObject(OrderConstant.SUMMARY_DET_ARRAY);
					orderTo.setCharges(aObj.optString(OrderConstant.CHARGES));
					orderTo.setOrderAmount(aObj.optString(OrderConstant.ORDER_AMOUNT));
					orderTo.setDiscountAmount(aObj.optString(OrderConstant.DISCOUNT_AMOUNT));
					orderTo.setTotalPrice(aObj.optString(OrderConstant.TOTAL_PRICE));
					orderTo.setQuentity(aObj.optString(OrderConstant.QUENTITY));
					orderTo.setNoOfSeat(String.valueOf(seatList.size()));
					orderTo.setSelectedSeat(seatSelectedTv.getText().toString());
					orderTo.setBookingFee(bookingFeeTv.getText().toString());
				
					
					

					Fragment frag = new UserDetailsFragment(seatTo, orderTo, selectedSeat, formatTv.getText().toString());
					FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
					if (frag != null) {
						if (cTimer != null) {
							cTimer.cancel();
						}
						FragmentTransaction ft = getFragmentManager().beginTransaction();
						CurrentFragmentSingletone.getInstance().setFragment(frag);
						ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
						ft.replace(R.id.frame_container, frag, "fragment");
						// Start the animated transition.
						ft.commit();

						// update selected item and title, then close the drawer

					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				/*
				 * Fragment frag = new UserDetailsFragment(seatTo);
				 * FragmentContainer
				 * .getInstance().setFragment(CurrentFragmentSingletone
				 * .getInstance().getFragment()); if (frag != null) {
				 * FragmentTransaction ft =
				 * getFragmentManager().beginTransaction();
				 * CurrentFragmentSingletone.getInstance().setFragment(frag);
				 * ft.setCustomAnimations(R.anim.enter_from_right,
				 * R.anim.exit_to_left); ft.replace(R.id.frame_container, frag,
				 * "fragment"); // Start the animated transition. ft.commit();
				 * 
				 * // update selected item and title, then close the drawer
				 * 
				 * }
				 */
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				
				
				// TODO Auto-generated method stub

			}
		});
		try {
			System.out.println("Selected seat__________________________:" + selectedSeat);			
			System.out.println("RequestSeatsConfirm : " + pref.getDeviceId() + ":" + seatTo.getRequistId() + ":" + selectedSeat);
			schedule.RequestSeatsConfirmAsync(pref.getDeviceId(), seatTo.getRequistId(), selectedSeat, getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Fragment frag = new UserDetailsFragment(seatTo);
		// FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
		// if (frag != null) {
		// FragmentTransaction ft = getFragmentManager().beginTransaction();
		// CurrentFragmentSingletone.getInstance().setFragment(frag);
		// ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
		// ft.replace(R.id.frame_container, frag, "fragment");
		// // Start the animated transition.
		// ft.commit();
		//
		// // update selected item and title, then close the drawer
		//
		// }
	}

	// @JavascriptInterface
	// public void sendToAndroid(String text) {
	// // this is called from JS with passed value
	// Toast t = Toast.makeText(getActivity(), text, 2000);
	// t.show();
	// }

	@SuppressLint("ShowToast")
	final class IJavascriptHandler {
		IJavascriptHandler() {
		}

		// This annotation is required in Jelly Bean and later:
		@JavascriptInterface
		public void sendToAndroid(String text) {
			// this is called from JS with passed value
			// Toast t = Toast.makeText(getActivity(), text, 2000);
			// t.show();
			selectedSeat = text;

		}
	}

	@SuppressWarnings("static-access")
	public void initListener() {

		webView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				webView.loadUrl(url);
				return super.shouldOverrideUrlLoading(view, url);
			}

			@Override
			public void onLoadResource(WebView view, String url) {
				super.onLoadResource(view, url);

			}

			@Override
			public void onPageFinished(WebView view, String url) {
				view.postDelayed(new Runnable() {
					@SuppressLint("NewApi")
					@Override
					public void run() {
						// float webviewsize = webView.getContentHeight() -
						// webView.getTop();
						// float positionInWV = webviewsize *
						// mProgressToRestore;
						// int positionY = Math.round(mWebView.getTop() +
						// positionInWV);
						// mWebView.scrollTo(0, positionY);
						Display display = getActivity().getWindowManager().getDefaultDisplay();
						Point size = new Point();
						display.getSize(size);
						int width = size.x;

						webView.scrollTo((getWidthOfWebview(Utils.getWidth(layout)) - width) / 2, 0);
						webView.setVisibility(View.VISIBLE);
						timerTv.setVisibility(View.VISIBLE);
						System.out.println(width + " : " + getWidthOfWebview(Utils.getWidth(layout)));
						// System.out.println(width+" : "+ webView.getsc);
					}
					// Delay the scrollTo to make it work
				}, 300);
				super.onPageFinished(view, url);
			}
		});

		webView.setWebChromeClient(new WebChromeClient() {

			@Override
			public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
				// TODO Auto-generated method stub		
				
				if (seatTo.getCinemaNo().equals("JUNIOR")) {				

					if (message.trim().contains("chk_") && !message.trim().contains("dis:")) {
						if (seatList.size() < 10) {
							if(message.trim().contains(",")){
								String[] messageArr = message.split(",");	
								String[] value=messageArr[0].split("_");
								seatList.add(value[1]);
								setat=value[1];
								
								if(setat.contains("A")){
									sofabed.setVisibility(View.VISIBLE);
									sofabed_pricepertickit.setText("Rp. " +seatTo.getClassList().get(3).getPrice());								
									count=count+1;
									sofabedty.setText(String.valueOf(count));
									sofabedsets.setText(String.valueOf(count*2));
									int numberofsets=count*2;
									int total=count*Integer.parseInt(seatTo.getClassList().get(3).getPrice());
									int bookingfees=numberofsets*(Integer.parseInt(seatTo.getClassList().get(3).getBookingFee()));
									sofabedtickittotal.setText("Rp. " +String.valueOf(total));
									sofabedbookingfees.setText("Rp. " +String.valueOf(bookingfees));
									price=price+Integer.parseInt(seatTo.getClassList().get(3).getPrice());
									price=price+2*Integer.parseInt(seatTo.getClassList().get(3).getBookingFee());
									totalPriceTv.setText("Rp. " +String.valueOf(price));
									
									
								}
								priceList.add(Integer.parseInt(seatTo.getPrice()));

								String[] valuex=messageArr[1].split("_");
								seatList.add(valuex[1]);
								priceList.add(Integer.parseInt(seatTo.getPrice()));

							}else{

								String[] messageArr = message.split("_");
								//Log.d("messageArr", "messageArr" +messageArr[1]);
								seatList.add(messageArr[1]);
								setat=messageArr[1];
								if(setat.contains("B") || setat.contains("C") ||setat.contains("D")){
									cinemaseat.setVisibility(View.VISIBLE);
									cinemastcount=cinemastcount+1;
									cinemast_pricepertickit.setText("Rp. " +seatTo.getClassList().get(0).getPrice());
									cinemastsets.setText(String.valueOf(cinemastcount));
									int total=cinemastcount*Integer.parseInt(seatTo.getClassList().get(0).getPrice());
									int bookingfees=cinemastcount*(Integer.parseInt(seatTo.getClassList().get(0).getBookingFee()));
									cinemasttickittotal.setText("Rp. " +String.valueOf(total));
									cinemastbookingfees.setText("Rp. " +String.valueOf(bookingfees));
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getPrice());
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getBookingFee());
									totalPriceTv.setText("Rp. " +String.valueOf(price));
								}else if(setat.contains("E")){
									lounger.setVisibility(View.VISIBLE);
									loungercount=loungercount+1;
									loungere_pricepertickit.setText("Rp. " +seatTo.getClassList().get(1).getPrice());
									loungeresets.setText(String.valueOf(loungercount));
									int total=loungercount*Integer.parseInt(seatTo.getClassList().get(1).getPrice());
									int bookingfees=loungercount*(Integer.parseInt(seatTo.getClassList().get(1).getBookingFee()));
									loungeretickittotal.setText("Rp. " +String.valueOf(total));
									loungerebookingfees.setText("Rp. " +String.valueOf(bookingfees));
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getPrice());
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getBookingFee());
									totalPriceTv.setText("Rp. " +String.valueOf(price));
									
								}else if(setat.contains("F") ||setat.contains("G")){
									beanbag.setVisibility(View.VISIBLE);
									bebangcount=bebangcount+1;
									beabang_pricepertickit.setText("Rp. " +seatTo.getClassList().get(2).getPrice());
									beabangsets.setText(String.valueOf(bebangcount));
									int total=bebangcount*Integer.parseInt(seatTo.getClassList().get(2).getPrice());
									int bookingfees=bebangcount*(Integer.parseInt(seatTo.getClassList().get(2).getBookingFee()));
									beabangtickittotal.setText("Rp. " +String.valueOf(total));
									beabangbookingfees.setText("Rp. " +String.valueOf(bookingfees));
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getPrice());
									price=price+Integer.parseInt(seatTo.getClassList().get(0).getBookingFee());
									totalPriceTv.setText("Rp. " +String.valueOf(price));
								}
								priceList.add(Integer.parseInt(seatTo.getPrice()));
								Log.d("value", "value"+Integer.parseInt(seatTo.getPrice()));
							}


						}
					} else if (message.trim().contains("dis:")) {
						if(message.trim().contains(",")){
							String[] messageArr = message.split(",");
							String[] value=messageArr[0].split("_");							
							String[] valuex=messageArr[1].split("_");	

							for(int i=0;i<seatList.size();i++){
								if(seatList.get(i).equals(value[1])){
									if(seatList.get(i).contains("A")){
										count=count-1;	
										sofabedty.setText(String.valueOf(count));
										sofabedsets.setText(String.valueOf(count*2));
										int numberofsets=count*2;
										int total=count*Integer.parseInt(seatTo.getClassList().get(3).getPrice());
										int bookingfees=numberofsets*(Integer.parseInt(seatTo.getClassList().get(3).getBookingFee()));
										sofabedtickittotal.setText("Rp. " +String.valueOf(total));
										sofabedbookingfees.setText("Rp. " +String.valueOf(bookingfees));
										
										price=price-Integer.parseInt(seatTo.getClassList().get(3).getPrice());
										price=price-2*Integer.parseInt(seatTo.getClassList().get(3).getBookingFee());
										
										
										totalPriceTv.setText("Rp. " +String.valueOf(price));
																		
									}
									if(count==0){
										sofabed.setVisibility(View.GONE);
										
									}
									seatList.remove(i);
									priceList.remove(i);
								}
							}

							for(int i=0;i<seatList.size();i++){
								if(seatList.get(i).equals(valuex[1])){	
									
									seatList.remove(i);
									priceList.remove(i);
								}
							}

						}else{
							String[] messageArr = message.split("_");
							for (int i = 0; i < seatList.size(); i++) {
								
								if (seatList.get(i).equals(messageArr[1])) {
									
									if(seatList.get(i).contains("B")||seatList.get(i).contains("C") ||seatList.get(i).contains("D")){
										cinemastcount=cinemastcount-1;
										cinemast_pricepertickit.setText(seatTo.getClassList().get(0).getPrice());
										cinemastsets.setText(String.valueOf(cinemastcount));
										int total=cinemastcount*Integer.parseInt(seatTo.getClassList().get(0).getPrice());
										int bookingfees=cinemastcount*(Integer.parseInt(seatTo.getClassList().get(0).getBookingFee()));
										cinemasttickittotal.setText("Rp. " +String.valueOf(total));
										cinemastbookingfees.setText("Rp. " +String.valueOf(bookingfees));
										price=price-Integer.parseInt(seatTo.getClassList().get(0).getPrice());
										price=price-Integer.parseInt(seatTo.getClassList().get(0).getBookingFee());
										totalPriceTv.setText("Rp. " +String.valueOf(price));
									}
									if(cinemastcount==0){
										cinemaseat.setVisibility(View.GONE);
									}
									if(seatList.get(i).contains("E")){
										loungercount=loungercount-1;
										loungere_pricepertickit.setText(seatTo.getClassList().get(1).getPrice());
										loungeresets.setText(String.valueOf(loungercount));
										int total=loungercount*Integer.parseInt(seatTo.getClassList().get(1).getPrice());
										int bookingfees=loungercount*(Integer.parseInt(seatTo.getClassList().get(1).getBookingFee()));
										loungeretickittotal.setText("Rp. " +String.valueOf(total));
										loungerebookingfees.setText("Rp. " +String.valueOf(bookingfees));
										price=price-Integer.parseInt(seatTo.getClassList().get(1).getPrice());
										price=price-Integer.parseInt(seatTo.getClassList().get(1).getBookingFee());
										totalPriceTv.setText("Rp. " +String.valueOf(price));
									}
									if(loungercount==0){
										lounger.setVisibility(View.GONE);
									}
									
									if(seatList.get(i).contains("F") || seatList.get(i).contains("G")){
										bebangcount=bebangcount-1;
										beabang_pricepertickit.setText(seatTo.getClassList().get(2).getPrice());
										beabangsets.setText(String.valueOf(bebangcount));
										int total=bebangcount*Integer.parseInt(seatTo.getClassList().get(2).getPrice());
										int bookingfees=bebangcount*(Integer.parseInt(seatTo.getClassList().get(2).getBookingFee()));
										beabangtickittotal.setText("Rp. " +String.valueOf(total));
										beabangbookingfees.setText("Rp. " +String.valueOf(bookingfees));									
										price=price-Integer.parseInt(seatTo.getClassList().get(2).getPrice());
										price=price-Integer.parseInt(seatTo.getClassList().get(2).getBookingFee());
										totalPriceTv.setText("Rp. " +String.valueOf(price));
									}
									if(bebangcount==0){
										beanbag.setVisibility(View.GONE);
									}
									
									seatList.remove(i);
									priceList.remove(i);
								}
							}

						}



					} else if (message.trim().contains(seatTo.getMaxSeat())) {
						// Toast.makeText(getActivity(),
						// "You can't select more than 10 seat.",
						// Toast.LENGTH_LONG).show();
						/*
						 * MainActivity activity = (MainActivity) getActivity();
						 * activity
						 * .showCustomAlert("You can't select \nmore than 10 seat."
						 * );
						 */
						AlertDialog dialog = new AlertDialog(getActivity(), "Cannot Book more than " + seatTo.getMaxSeat() + " seats", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub

							}
						});
						dialog.show();
					}
					
				}else{
					if (message.trim().contains("chk_") && !message.trim().contains("dis:")) {
                        //get item
                        String[] messageList = message.split("\\|");
                        String seatType = messageList[messageList.length-1];
                        SeatLayoutClassListTo item = null;
                        for (int i =0; i<classListeJunior.size(); i++){
                            String x= seatTo.getCinemaNo();
                            if("REGULAR".equals(classListeJunior.get(i).getSeatClass()) && seatType.equals("STANDARD")){
                                item = classListeJunior.get(i);
                                break;
                            }
                            else if(seatType.equals(classListeJunior.get(i).getSeatClass())){
                                item = classListeJunior.get(i);
                                break;
                            }
                        }

                        if(item == null){
                            item = classListeJunior.get(0);
                        }

						if (seatList.size() < 10 && item != null) {
							String[] messageArr = message.split("_");
							seatList.add(messageArr[1]);
                            bookingList.add(Integer.parseInt(item.getBookingFee()));
							priceList.add(Integer.parseInt(item.getPrice()));

                            int bookingPrice = 0;
                            for(Integer x: bookingList){
                                bookingPrice += x;
                            }
                            bookingsfees.setText("Rp. "+ String.valueOf(bookingPrice));
                            int ticketPrice = 0;
                            for(Integer x: priceList){
                                ticketPrice += x;
                            }
                            ticketTotal.setText("Rp. "+ String.valueOf(ticketPrice));
                            totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + ticketPrice));


                            //now check for packages and go on
                            for(SeatLayoutClassListTo __ : classListeJunior){
                                if(__.isIsLoyalty() && __.isIsPackage()){
                                    _package = __;
                                }
                            }

                            if(_package != null && seatList.size() >= Integer.parseInt(_package.getMinNoOfSeats())){
                                int packOf = Integer.parseInt(_package.getMinNoOfSeats());
                                int lastAmount = Integer.parseInt(_package.getPrice());

                                if(seatList.size() > packOf){
                                    for (int i = packOf; i<seatList.size(); i++){
                                        lastAmount += priceList.get(i);
                                    }
                                }

                                ticketTotal.setText("Rp. "+ String.valueOf(lastAmount));
                                totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + lastAmount));
                            }



							/*if(shPreference.get_Loyalt(getActivity()).equals("true")){
								//pricepertickit.setText("Rp. "+Integer.parseInt(classListeJunior.get(1).getPrice()));
                                int bookingPrice = 0;
                                for(Integer x: bookingList){
                                    bookingPrice += x;
                                }
								bookingsfees.setText("Rp. "+ String.valueOf(bookingPrice));
                                int ticketPrice = 0;
                                for(Integer x: priceList){
                                    ticketPrice += x;
                                }
                                ticketTotal.setText("Rp. "+ String.valueOf(ticketPrice));
                                totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + ticketPrice));
								if(seatList.size()>4){
                                    int ticketPrice = 0;
                                    for(Integer x: priceList){
                                        ticketPrice += x;
                                    }
									ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())+((seatList.size() - 4) * Integer.parseInt(item.getPrice()))));
									int totalfees=(Integer.parseInt(item.getPrice()))+((seatList.size() - 4) * Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
									totalPriceTv.setText("Rp. " +totalfees);
								}else{
									ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())));
									int totalfees=(Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
									totalPriceTv.setText("Rp. " +totalfees);
								}
							}else{
								//pricepertickit.setText("Rp. "+Integer.parseInt(classListeJunior.get(0).getPrice()));

                                ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())* seatList.size()));

                                bookingsfees.setText("Rp. "+seatList.size()*Integer.parseInt(item.getBookingFee()));
                                int totalfees=(seatList.size()*Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
                                totalPriceTv.setText("Rp. " +totalfees);
							}*/
							
						}

					} else if (message.trim().contains("dis:")) {

						String[] messageArr = message.split("_");
                        //get item
                        String[] messageList = message.split("\\|");
                        String seatType = messageList[messageList.length-1];
                        SeatLayoutClassListTo item = null;
                        for (int i =0; i<classListeJunior.size(); i++){
                            if(seatType.equals(classListeJunior.get(i).getSeatClass())){
                                item = classListeJunior.get(i);
                                break;
                            }
                        }

						for (int i = 0; i < seatList.size(); i++) {
							if (seatList.get(i).equals(messageArr[1])) {
								seatList.remove(i);
								priceList.remove(i);
                                bookingList.remove(i);

                                int bookingPrice = 0;
                                for(Integer x: bookingList){
                                    bookingPrice += x;
                                }
                                bookingsfees.setText("Rp. "+ String.valueOf(bookingPrice));
                                int ticketPrice = 0;
                                for(Integer x: priceList){
                                    ticketPrice += x;
                                }
                                ticketTotal.setText("Rp. "+ String.valueOf(ticketPrice));
                                totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + ticketPrice));

                                if(_package != null && seatList.size() >= Integer.parseInt(_package.getMinNoOfSeats())){
                                    int packOf = Integer.parseInt(_package.getMinNoOfSeats());
                                    int lastAmount = Integer.parseInt(_package.getPrice());

                                    if(seatList.size() == packOf){
                                        ticketTotal.setText("Rp. "+ String.valueOf(lastAmount));
                                        totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + lastAmount));
                                    }else {
                                        for (int j = packOf; j<seatList.size(); j++){
                                            lastAmount += priceList.get(j);
                                        }
                                        ticketTotal.setText("Rp. "+ String.valueOf(lastAmount));
                                        totalPriceTv.setText("Rp. " +String.valueOf(bookingPrice + lastAmount));
                                    }
                                }


								/*pricepertickit.setText("Rp. "+Integer.parseInt(seatTo.getPrice()));
								ticketTotal.setText("Rp. "+(Integer.parseInt(seatTo.getPrice())* seatList.size()));

								bookingsfees.setText("Rp. "+seatList.size()*Integer.parseInt(seatTo.getBookingFees()));
								int totalfees=(seatList.size()*Integer.parseInt(seatTo.getPrice()))+(seatList.size()*Integer.parseInt(seatTo.getBookingFees()));
								totalPriceTv.setText("Rp. " +totalfees);*/
								/*if(shPreference.get_Loyalt(getActivity()).equals("true")){
									//pricepertickit.setText("Rp. "+Integer.parseInt(classListeJunior.get(1).getPrice()));
									bookingsfees.setText("Rp. "+seatList.size()*Integer.parseInt(item.getBookingFee()));
									
									if(seatList.size()>4){
										ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())+((seatList.size() - 4) * Integer.parseInt(item.getPrice()))));

										int totalfees=(Integer.parseInt(item.getPrice()))+((seatList.size() - 4) * Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
										totalPriceTv.setText("Rp. " +totalfees);

									}else{
										ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())));

										int totalfees=(Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
										totalPriceTv.setText("Rp. " +totalfees);

									}
									
									
								}else{
									//pricepertickit.setText("Rp. "+Integer.parseInt(classListeJunior.get(0).getPrice()));
									ticketTotal.setText("Rp. "+(Integer.parseInt(item.getPrice())* seatList.size()));

									bookingsfees.setText("Rp. "+seatList.size()*Integer.parseInt(item.getBookingFee()));
									int totalfees=(seatList.size()*Integer.parseInt(item.getPrice()))+(seatList.size()*Integer.parseInt(item.getBookingFee()));
									totalPriceTv.setText("Rp. " +totalfees);

								}*/
							}
						}

					} else if (message.trim().contains(seatTo.getMaxSeat())) {
						// Toast.makeText(getActivity(),
						// "You can't select more than 10 seat.",
						// Toast.LENGTH_LONG).show();
						/*
						 * MainActivity activity = (MainActivity) getActivity();
						 * activity
						 * .showCustomAlert("You can't select \nmore than 10 seat."
						 * );
						 */
						AlertDialog dialog = new AlertDialog(getActivity(), "Cannot Book more than " + seatTo.getMaxSeat() + " seats", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub

							}
						});
						dialog.show();
					}
					
				}
			
				
				


				setSeat();
				totalSeatTv.setText(String.valueOf(seatList.size()));				

				result.confirm();
				return true;

			}
		});

		plusBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (infoStatus) {
					infoLayout.setVisibility(View.GONE);
					scrollvie.setVisibility(View.GONE);
					plusBtn.setBackgroundResource(R.drawable.seat_layout_plus_button_back);
					infoStatus = false;
				} else if (!infoStatus) {
					infoLayout.setVisibility(View.VISIBLE);
					scrollvie.setVisibility(View.VISIBLE);
					plusBtn.setBackgroundResource(R.drawable.seat_layout_minus_button_back);
					infoStatus = true;
				}
			}
		});

		continueBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

					if (seatList.size() >= 4 && _package != null) {
						Loyalty loyalty = new Loyalty(new com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {
							
							@Override
							public void Wsdl2CodeStartedRequest() {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void Wsdl2CodeFinishedWithException(Exception ex) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void Wsdl2CodeFinished(String methodName, Object Data) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void Wsdl2CodeEndedRequest() {
								// TODO Auto-generated method stub
								
							}
						});

						
						try {

                            //jaisi karni vaisi bharni
                            String _ = shPreference.get_LoyaltyRequestid(getActivity().getApplicationContext());
                            String __ =  shPreference.get_MaxcardMemberid(getActivity().getApplicationContext());
                            String ___ = _package.getClassIdDP();
                            int ____ = Integer.parseInt(_package.getMinQty());
                            int _____ = Integer.parseInt(_package.getMinNoOfSeats());

                            loyalty.GetLoyaltyTicketTypeQtyAsync(Long.parseLong(_),__, ___, ____, _____, activity);

						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						callServiceContinue();

					}else if (seatList.size() > 0 ) {

                        Loyalty loyalty = new Loyalty(new com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {

                            @Override
                            public void Wsdl2CodeStartedRequest() {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void Wsdl2CodeFinishedWithException(Exception ex) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void Wsdl2CodeFinished(String methodName, Object Data) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void Wsdl2CodeEndedRequest() {
                                // TODO Auto-generated method stub

                            }
                        });


                        try {

                            //jaisi karni vaisi bharni
                            String _ = shPreference.get_LoyaltyRequestid(getActivity().getApplicationContext());
                            String __ =  shPreference.get_MaxcardMemberid(getActivity().getApplicationContext());
                            String ___ = classListeJunior.get(0).getClassIdDP();
                            int ____ = Integer.parseInt(classListeJunior.get(0).getMinQty());
                            int _____ = Integer.parseInt(classListeJunior.get(0).getMinNoOfSeats());

                            loyalty.GetLoyaltyTicketTypeQtyAsync(Long.parseLong(_),__, ___, ____, _____, activity);

                        } catch (NumberFormatException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        callServiceContinue();
					/*if( seatList.size() >= Integer.valueOf(numberofseats)){
						if (cTimer != null) {
							cTimer.cancel();
						}
						System.out.println("Request id _____________________________________ :" + seatTo.getRequistId());
						callServiceContinue();
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "Please Choose minimum "+numberofseats+"Seat(s)", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub

							}
						});
						dialog.show();
					}*/

                    } else {
                        AlertDialog dialog = new AlertDialog(getActivity(), "Please Choose Seat(s)", new DialogListener() {

                            @Override
                            public void onOkClick() {
                                // TODO Auto-generated method stub

                            }
                        });
                        dialog.show();
                    }
			}
		});

	}

	@SuppressLint("NewApi")
	public int getWidthOfWebview(int ctn) {
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;

		int consVal = 0;
		if (width < 500) {
			consVal = 45;
		} else if (width < 800) {
			consVal = 52;
		} else if (width < 1100) {
			consVal = 97;
		} else if (width < 1250) {
			consVal = 108;
		}

		int widthVal = 0;
		if (ctn * consVal > width) {
			widthVal = ctn * consVal;
		} else {
			widthVal = width;
		}

		return widthVal;
	}

	public void setSeat() {

		String seatText = "";
		int price = 0;
		if (seatList.size() > 0 && priceList.size() > 0) {

			Log.d("seatlist", "seatlist" +seatList);

			pricePerTicketTv.setText("RP. " + seatTo.getPrice());

			for (int i = 0; i < seatList.size(); i++) {
				seatText = seatText.replace("|", "-") + "," + seatList.get(i).replace("|", "-");
				price = price + priceList.get(i);
				if (seatText.trim().charAt(0) == ',') {
					// seatText.replaceAll(",", "");
					StringBuilder myName = new StringBuilder(seatText);
					myName.setCharAt(0, '\0');
					seatText = myName.toString();
				}
			}

			int booking = Integer.parseInt(seatTo.getBookingFees()) * priceList.size();
			bookingFeeTv.setText("RP. " + String.valueOf(booking));

			seatSelectedTv.setText(seatText);

			//ticketTotalTv.setText("RP. " + String.valueOf(price));

			price = price + booking;
			totalPriceValue = price;

			//totalPriceTv.setText("RP. " + String.valueOf(price));
		} else {
			pricePerTicketTv.setText("");
			bookingFeeTv.setText("");
			seatSelectedTv.setText("");

			//ticketTotalTv.setText("");
			//totalPriceTv.setText("");
		}

	}

	String secVal = "";
	String minuteVal = "";

	public void setTimer(int tTime) {

		cTimer = new CountDownTimer(tTime, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				int timeVal = (int) (millisUntilFinished / 1000);
				int second = timeVal % 60;
				int minute = timeVal / 60;

				if (second < 10) {
					secVal = "0" + String.valueOf(second);
				} else {
					secVal = String.valueOf(second);
				}
				if (minute < 10) {
					minuteVal = "0" + String.valueOf(minute);
				} else {
					minuteVal = String.valueOf(minute);
				}

				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						timerTv.setText(minuteVal + ":" + secVal);
					}
				});
				pref.setTimerTime(String.valueOf((int) millisUntilFinished));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				System.out.println("Timer finished..............");
				AlertDialog dialog = new AlertDialog(getActivity(), "Session Time Out", new DialogListener() {

					@Override
					public void onOkClick() {
						// TODO Auto-generated method stub
						MainActivity activity = (MainActivity) getActivity();
						activity.reCreateHome();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
		}.start();

	}


}
