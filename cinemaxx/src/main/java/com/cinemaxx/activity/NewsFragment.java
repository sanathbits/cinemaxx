package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.NewsAdapter;
import com.cinemaxx.bean.NewsTo;
import com.cinemaxx.constant.NewsConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class NewsFragment extends Fragment{

	ListView listView;
	TextView locationNameTv;
	ArrayList<NewsTo> dataList;
	NewsAdapter adapter;
	CinemaxxPref pref;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_news, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		listView = (ListView) getView().findViewById(R.id.news_list);
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.news_setlocation_txt);
		locationNameTv.setText("Location: " + pref.getCityName());
		locationNameTv.setTypeface(faceThin);
		dataList = new ArrayList<NewsTo>();
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnNews();
	}
	
	public void initListener(){
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				NewsTo newsTo = (NewsTo) parent.getItemAtPosition(position);
				pref.setNewsId(newsTo.getNewsId());
				
				Fragment fragment = new NewsDetailsFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				
				dataList = new ArrayList<NewsTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(NewsConstant.ERROR_DESC).equals("0")){
						JSONArray newsArray = mainObj.optJSONArray(NewsConstant.NEWS_ARRAY);
						for(int i = 0; i < newsArray.length(); i++){
							
							JSONObject newsObj = newsArray.optJSONObject(i);
							NewsTo to = new NewsTo();
							to.setNewsId(newsObj.optString(NewsConstant.NEWS_ID));
							to.setNewsTitle(newsObj.optString(NewsConstant.NEWS_TITLE));
							to.setNewsImage(newsObj.optString(NewsConstant.NEWS_IMAGE));
							
							dataList.add(to);
						}
						
						initList();
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(),
								"No news is available", new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
						dialog.show();
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			schedule.GetNewsAsync(pref.getDeviceId(), getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initList(){
		
		adapter = new NewsAdapter(getActivity(), dataList, NewsFragment.this);
		listView.setAdapter(adapter);
//		ViewUtility.setListViewHeightBasedOnNewsFragment(listView, getActivity());
		listView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});
		
	}
	
}
