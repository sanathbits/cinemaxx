package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.MaxxCardDescAdapter;
import com.cinemaxx.adapter.MaxxCardHistoryAdapter;
import com.cinemaxx.adapter.MaxxCardSpecialOfferAdapter;
import com.cinemaxx.adapter.MaxxCardStarClassSpecialOfferAdapter;
import com.cinemaxx.adapter.MaxxCardUpcommingAdapter;
import com.cinemaxx.bean.MaxxCardDescTo;
import com.cinemaxx.bean.MaxxCardDetailsTo;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;
import com.cinemaxx.bean.MaxxCardUpCommingTo;
import com.cinemaxx.bean.MemberBalanceTo;
import com.cinemaxx.constant.MaxxCardBalanceConstant;
import com.cinemaxx.constant.MaxxCardDetailsConstant;
import com.cinemaxx.constant.MaxxCardManuConstant;
import com.cinemaxx.constant.MaxxCardWelcomeConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.loyaltyservice.IWsdl2CodeEvents;
import com.cinemaxx.loyaltyservice.Loyalty;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.SharedPreference;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.Schedule;
import com.squareup.picasso.Picasso;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class MaxxCardWelcomeFragment extends Fragment {

	MainActivity activity;
	TextView locationTv;
	LinearLayout maxxCardInfoPresentLayout, maxxCardInfoNotPresentLayout, mainContentLayout;
	TextView maxxCardCardNumberTv, maxxCardStatusTv, maxxCardPointBalanceTv, maxxCardPointEarnedTv, maxxCardExpiryDateTv, welcomeTv;
	ListView upcomingListView, historyListView;
	Button bookNowBtn;
	TextView bookNowTv, aboutTv;
	TextView logoutTv;
	TextView leveNameTv;

	MaxxCardDetailsTo maxxCardDetailsTo;
	ArrayList<MaxxCardUpCommingTo> dataListUpcoming, dataListHistory;
	ArrayList<MemberBalanceTo> memberBalanceList;
	CinemaxxPref pref;
	LinearLayout memberBalancePresentLay;

	SharedPreference shpref;
	List<MaxxCardDescTo> list;
	RelativeLayout relMenu;
	LinearLayout lnrMenuItem, lnrPrivilegesAndBenefits, lnrStarClass, lnrStarClassPrivileges, lnrFaq, lnrTermAndCondition, lnrProfile;
	int chkMenu = 0, chkAboutList = 1, chkPrivateAndBenefitsList = 1, chklistStarClassList = 1;
	TextView starClass, privateAndBenefits, starClassPrivileges, faq, termsAndConditions, txtProfile;
	ListView listDesc, listPrivateAndBenefits, listStarClass;
	private ValueAnimator mAnimator;
	WebView webViewFaq, webViewTnd, webViewContentForPrivilegeandBenefits, webViewContentForStarClassPrivileges, webViewContentClickForAbout;
	ArrayList<MaxxCardManuDetailsTo> newsList, specialOfferList, starClassSpecialOfferList;
	private CustomProgressDialog pDialog;
	private LinearLayout lnrRecentHistory;
	Button btnJoinNow;
	boolean chkBlncDtls = false;
	ProgressBar progressBar;
	double time = 0;
	int progress = 0;
	private ImageView ivMaxCardImage;

	/*
	 * public MaxxCardWelcomeFragment(MaxxCardManuDetailsTo
	 * maxxCardManuDetailsTo) { this.maxxCardManuDetailsTo =
	 * maxxCardManuDetailsTo; }
	 */

	public MaxxCardWelcomeFragment(boolean chkBlncDtls) {
		this.chkBlncDtls = chkBlncDtls;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_maxx_card_welcome, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (MainActivity) getActivity();
		shpref = new SharedPreference();

		
		initData();
		if(!chkBlncDtls){
			UpdateMemberDetails();
		}else{
			initView();
			initListener();
		}
		
//		initListener();
	}

	public void initData() {
		pref = new CinemaxxPref(getActivity());
		System.out.println("Maxx Card Pref Data: " + pref.getMaxxCardLoginResponse().trim());
		if (pref.getMaxxCardLoginResponse().trim().length() > 10) {
			try {

				JSONObject mainObj = new JSONObject(pref.getMaxxCardLoginResponse().trim());
				if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {

					maxxCardDetailsTo = new MaxxCardDetailsTo();
					maxxCardDetailsTo.setLoyaltyRedeemType(mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
					maxxCardDetailsTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
					maxxCardDetailsTo.setStarClassCriteria(mainObj.optString(MaxxCardDetailsConstant.STAR_CLASS_CRITERIA));
					maxxCardDetailsTo.setMaxCardImage(mainObj.optString(MaxxCardDetailsConstant.MAX_CARD_IMAGE));
					shpref.set_MaxxcardImage(getActivity().getApplicationContext(), maxxCardDetailsTo.getMaxCardImage());

					
					JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
					maxxCardDetailsTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
					maxxCardDetailsTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
					maxxCardDetailsTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
					maxxCardDetailsTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
					maxxCardDetailsTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));

					maxxCardDetailsTo.setLevelId(memberObj.optString(MaxxCardDetailsConstant.LEVEL_ID));
					maxxCardDetailsTo.setLevelName(memberObj.optString(MaxxCardDetailsConstant.LEVEL_NAME));

					maxxCardDetailsTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
					maxxCardDetailsTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
					maxxCardDetailsTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
					maxxCardDetailsTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
					maxxCardDetailsTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
					maxxCardDetailsTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
					maxxCardDetailsTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
					maxxCardDetailsTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
					maxxCardDetailsTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
					maxxCardDetailsTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
					maxxCardDetailsTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
					maxxCardDetailsTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
					maxxCardDetailsTo.setWishToReceiveSMS(memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
					maxxCardDetailsTo.setSendNewsLetter(memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
					maxxCardDetailsTo.setMailingFrequency(memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
					maxxCardDetailsTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
					maxxCardDetailsTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
					maxxCardDetailsTo.setMembershipActivated(memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));
					
					JSONArray balanceListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
					for(int k = 0; k < balanceListArray.length(); k++){
					JSONObject balanceListObj = balanceListArray.optJSONObject(k);
					maxxCardDetailsTo.setBalanceTypeId(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
					maxxCardDetailsTo.setBalanceName(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
					maxxCardDetailsTo.setBalanceMessage(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
					maxxCardDetailsTo.setBalanceDisplay(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
					maxxCardDetailsTo.setBalancePointRemaining(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
					maxxCardDetailsTo.setBalanceIsDefault(balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
					}
					
					JSONArray pointsExpiryListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
//					for(int j = 0; j < pointsExpiryListArray.length(); j++){
					JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(0);
					maxxCardDetailsTo.setPointsExpiring(pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
					maxxCardDetailsTo.setExpireOn(pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
					maxxCardDetailsTo.setBalanceTypeIdExp(pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
//					}
					
					
					maxxCardDetailsTo.setIsProfileComplete(memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
					maxxCardDetailsTo.setLoyaltyRequestId(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));

				}else{
                    AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim(), new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							activity.callBackPress();
						}
					});
					dialog.show();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}

	public void initView() {
//		activity = (MainActivity) getActivity();
		activity.refreshAdapter();
		locationTv = (TextView) getView().findViewById(R.id.maxx_card_welcome_location_tv);
//		 CinemaxxPref pref = new CinemaxxPref(getActivity());
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv.setTypeface(faceThin);
		locationTv.setText("Location: " + pref.getCityName());


		leveNameTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_level_name_tv);
		leveNameTv.setText(maxxCardDetailsTo.getLevelName().trim());
		logoutTv = (TextView) getView().findViewById(R.id.maxx_card_welcome_logout);
		maxxCardInfoNotPresentLayout = (LinearLayout) getView().findViewById(R.id.maxx_card_card_info_not_present_lay);
		maxxCardInfoPresentLayout = (LinearLayout) getView().findViewById(R.id.maxx_card_card_info_present_lay);
		mainContentLayout = (LinearLayout) getView().findViewById(R.id.maxx_card_welcome_main_content_layout);
		mainContentLayout.setVisibility(View.GONE);
		/*if (maxxCardDetailsTo.getBalanceTypeId() != null && maxxCardDetailsTo.getBalanceTypeId().trim().length() > 0) {

			maxxCardInfoPresentLayout.setVisibility(View.VISIBLE);
			maxxCardInfoNotPresentLayout.setVisibility(View.GONE);

		} else {

			maxxCardInfoPresentLayout.setVisibility(View.GONE);
			maxxCardInfoNotPresentLayout.setVisibility(View.VISIBLE);

		}*/

		maxxCardCardNumberTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_card_number_tv);
		if(maxxCardDetailsTo.getCardNumber() != null){
			maxxCardCardNumberTv.setText(maxxCardDetailsTo.getCardNumber());
			maxxCardInfoPresentLayout.setVisibility(View.VISIBLE);
			maxxCardInfoNotPresentLayout.setVisibility(View.GONE);
		}else{
			maxxCardInfoPresentLayout.setVisibility(View.GONE);
			maxxCardInfoNotPresentLayout.setVisibility(View.VISIBLE);
		}
		
		maxxCardStatusTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_card_status_tv);
		if (maxxCardDetailsTo.getMembershipActivated().trim().equals("true")) {
			maxxCardStatusTv.setText("ACTIVE");
		} else {
			maxxCardStatusTv.setText("NOT ACTIVE");
		}
		maxxCardPointEarnedTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_card_point_earned_tv);
		if(maxxCardDetailsTo.getBalanceDisplay() != null){
			maxxCardPointEarnedTv.setText(maxxCardDetailsTo.getBalanceDisplay());
		}
		maxxCardPointBalanceTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_card_point_balance_tv);
		if(maxxCardDetailsTo.getBalancePointRemaining() != null){
			maxxCardPointBalanceTv.setText(maxxCardDetailsTo.getBalancePointRemaining());
		}
		maxxCardExpiryDateTv = (TextView) getView().findViewById(R.id.maxx_card_welocme_card_expiry_date_tv);
		try{
			if(Utils.CompareWithcurrentMonth(maxxCardDetailsTo.getExpireOn().trim()).equals("1")){
				maxxCardExpiryDateTv.setText(maxxCardDetailsTo.getPointsExpiring().trim());
			}else{
				maxxCardExpiryDateTv.setText("0");
			}
		}catch(Exception e){
			maxxCardExpiryDateTv.setText("0");
		}
		
		
		
		/***
		 * EDITED BY DIBYENDU on 17 JAN 2017 for image change dynamically
		 */
		ivMaxCardImage = (ImageView) getView().findViewById(R.id.ivMaxCardImage);
		if(shpref.get_MaxxcardImage(getActivity().getApplicationContext())!=null&&!shpref.get_MaxxcardImage(getActivity().getApplicationContext()).equals("")){
			Picasso.with(getActivity()).load(shpref.get_MaxxcardImage(getActivity().getApplicationContext()).trim())
					.placeholder(R.drawable.abc_btn_check_material).error(R.drawable.only_for_maxx_card)
					.into(ivMaxCardImage);
		}
		
		
		welcomeTv = (TextView) getView().findViewById(R.id.maxx_card_welcome_profileName_tv);
		welcomeTv.setText("WELCOME " + maxxCardDetailsTo.getFirstName() + "!");

		upcomingListView = (ListView) getView().findViewById(R.id.maxx_card_welcome_upcoming_booking_lv);
		historyListView = (ListView) getView().findViewById(R.id.maxx_card_welcome_recent_history_lv);

		bookNowBtn = (Button) getView().findViewById(R.id.maxx_card_welcome_book_now_btn);
		bookNowTv = (TextView) getView().findViewById(R.id.maxx_card_welcome_book_now_tv);
		aboutTv = (TextView) getView().findViewById(R.id.maxx_card_welcome_about_tv);
		memberBalancePresentLay = (LinearLayout) getView().findViewById(R.id.maxx_card_welcome_member_balance_present_lay);

		webViewFaq = (WebView) getView().findViewById(R.id.webViewFaq);
		webViewTnd = (WebView) getView().findViewById(R.id.webViewTnd);
//		webViewContentForAbout = (WebView) getView().findViewById(R.id.webViewContentForAbout);
		webViewContentForPrivilegeandBenefits = (WebView) getView().findViewById(R.id.webViewContentForPrivilegeandBenefits);
		webViewContentForStarClassPrivileges = (WebView) getView().findViewById(R.id.webViewContentForStarClassPrivileges);

		relMenu = (RelativeLayout) getView().findViewById(R.id.rel_maxx_card_menu);
		lnrMenuItem = (LinearLayout) getView().findViewById(R.id.lnrMenuItem);
		starClass = (TextView) getView().findViewById(R.id.maxx_card_star_class);
		privateAndBenefits = (TextView) getView().findViewById(R.id.maxx_card_private_and_benefits);
		starClassPrivileges = (TextView) getView().findViewById(R.id.maxx_card_star_class_privileges);
		faq = (TextView) getView().findViewById(R.id.maxx_card_faq);
		termsAndConditions = (TextView) getView().findViewById(R.id.maxx_card_terms_and_conditions);
		lnrPrivilegesAndBenefits = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_privileges_and_benefits);
		lnrStarClass = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_star_class);
		lnrStarClassPrivileges = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_star_class_privileges);
		lnrFaq = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_faq);
		lnrTermAndCondition = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_term_and_condition);
		lnrProfile = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_welcome_profile);
		txtProfile = (TextView) getView().findViewById(R.id.maxx_card_welcome_profile);
		txtProfile.setVisibility(View.GONE);
		lnrRecentHistory = (LinearLayout) getView().findViewById(R.id.maxx_card_welcome_recent_history_tv);
		webViewContentClickForAbout = (WebView) getView().findViewById(R.id.webViewContentClickForAbout);
		btnJoinNow = (Button) getView().findViewById(R.id.btnJoinNow);
		
		progressBar = (ProgressBar) getView().findViewById(R.id.maxx_card_welcome_progress);
		progressBar.setProgress(0);
		progressBar.setMax(Integer.parseInt(maxxCardDetailsTo.getStarClassCriteria()));
		
		if(maxxCardDetailsTo.getBalanceDisplay() != null){
			if((Integer.parseInt(maxxCardDetailsTo.getBalanceDisplay())) >= (Integer.parseInt(maxxCardDetailsTo.getStarClassCriteria()))){
				progressBar.setProgress(Integer.parseInt(maxxCardDetailsTo.getStarClassCriteria()));
			}else{
				progressBar.setProgress(Integer.parseInt(maxxCardDetailsTo.getBalanceDisplay()));
			}
		}
		
//		setProgressSloly();
		
		// aboutMaxxCard();

		// callCallService();

		MainActivity act = (MainActivity) getActivity();
		act.clickOnMaxxCard();

		CallMemberBalanceList();
	}

	/*private void setProgressSloly() {
		Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				
				
			}
		}, 0, 100);
	}*/

	public void loadWebView() {
		webViewFaq.loadData(Utils.getTncWebview(pref.getManuFaq()), "text/html; charset=UTF-8", null);
		webViewTnd.loadData(Utils.getTncWebview(pref.getManuTermsAndConditions()), "text/html; charset=UTF-8", null);
		webViewContentClickForAbout.loadData(Utils.getTncWebview(pref.getClickManuAboutMaxxCard()), "text/html; charset=UTF-8", null);
		webViewContentForPrivilegeandBenefits.loadData(Utils.getTncWebview(pref.getManuPrivilegesAndBenefits()), "text/html; charset=UTF-8", null);
		webViewContentForStarClassPrivileges.loadData(Utils.getTncWebview(pref.getManuStarClassPrivileges()), "text/html; charset=UTF-8", null);
	}

	/*public void showNewsList() {
		if (pref.getManuMaxxCardNews().trim().length() > 1) {
			try {
				JSONObject mainObj = new JSONObject(pref.getManuMaxxCardNews().trim().toString());
				if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
					newsList = new ArrayList<MaxxCardManuDetailsTo>();
					JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDNEWS);
					for (int i = 0; i < webContentsArry.length(); i++) {
						MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
						JSONObject itemObj = webContentsArry.optJSONObject(i);
						maxxCardManuDetailsTo.setMaxxCardNewsId(itemObj.optLong(MaxxCardManuConstant.MAXCARDNEWSID));
						maxxCardManuDetailsTo.setMaxxCardNewsTitle(itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSTITLE).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsMedia(itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSMEDIA).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsDescription(itemObj.optString(MaxxCardManuConstant.MAXXCARDNEWSDESCRIPTION).trim());
						maxxCardManuDetailsTo.setMaxxCardNewPublishDate(itemObj.optString(MaxxCardManuConstant.MAXCARDNEWPUBLICDATE).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsImageURL(itemObj.optString(MaxxCardManuConstant.MAXXCARDNEWSIMAGEURL).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsValidFrom(itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSVALIDFROM).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsValidTo(itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSVALIDTO).trim());
						maxxCardManuDetailsTo.setMaxxCardNewsStatus(itemObj.optString(MaxxCardManuConstant.MAXXCARDSTATUS).trim());
						newsList.add(maxxCardManuDetailsTo);
					}
				} else {

				}
				callNewsListAdapter();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}*/
	
	private void showSpacialOfferList() {
		if (pref.getManuSpcialOffer().length() > 1) {
			try {
				JSONObject mainObj = new JSONObject(pref.getManuSpcialOffer());
				if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
					specialOfferList = new ArrayList<MaxxCardManuDetailsTo>();
					JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDSPECIALOFFER);
					for (int i = 0; i < webContentsArry.length(); i++) {
						MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
						JSONObject itemObj = webContentsArry.optJSONObject(i);
						maxxCardManuDetailsTo.setSpecialOfferId(itemObj.optLong(MaxxCardManuConstant.SPECIALOFFERID));
						maxxCardManuDetailsTo.setSpecialOfferTitle(itemObj.optString(MaxxCardManuConstant.SPECIALOFFERTITLE).trim());
						maxxCardManuDetailsTo.setSpecialOfferImageURL(itemObj.optString(MaxxCardManuConstant.SPECIALOFFERIMAGEURL).trim());
						maxxCardManuDetailsTo.setSpecialOfferDescription(itemObj.optString(MaxxCardManuConstant.SPECIALOFFERDESCRIPTION).trim());

						specialOfferList.add(maxxCardManuDetailsTo);
					}
				} else {

				}
				callListAdapterPrivateAndBenefits();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void showStarClassSpacialOffer() {
		if (pref.getStarClassSpcialOffer().length() > 1) {
			try {
				JSONObject mainObj = new JSONObject(pref.getStarClassSpcialOffer().toString());
				if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
					starClassSpecialOfferList = new ArrayList<MaxxCardManuDetailsTo>();
					JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDSTARCLASSSPECIALOFFER);
					for (int i = 0; i < webContentsArry.length(); i++) {
						MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
						JSONObject itemObj = webContentsArry.optJSONObject(i);
						maxxCardManuDetailsTo.setStarClassSpecialOfferId(itemObj.optLong(MaxxCardManuConstant.STARCLASSSPECIALOFFERID));
						maxxCardManuDetailsTo.setStarClassSpecialOfferTitle(itemObj.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERTITLE).trim());
						maxxCardManuDetailsTo.setStarClassSpecialOfferDescription(itemObj.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERDESCRIPTION).trim());
						maxxCardManuDetailsTo.setStarClassSpecialOfferImageURL(itemObj.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERIMAGEURL).trim());

						starClassSpecialOfferList.add(maxxCardManuDetailsTo);
					}
				} else {

				}
				callListAdapterStarClassPrivileges();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * public void aboutMaxxCard() { TextView txtContent = (TextView) getView()
	 * .findViewById(R.id.txtContent); String text_view_str =
	 * "<b>Bahasa Indonesia: </b> MAXX CARD & MAXX CARD STAR CLASS adalah bentuk apresiasi manajemen Cinemaxx kepada para pengunjung setia yang bertransaksi di Cinemaxx. Sebagai member, Anda bisa memperoleh MAXX POINT dan menukarkan MAXX POINT dengan berbagai hadiah atau penawaran menarik dari Cinemaxx. Untuk memberikan nilai tambah dari setiap transaksi Anda, Cinemaxx juga menghadirkan berbagai keistimewaan eksklusif dari merchant dan business partner yang berpartisipasi untuk Anda nikmati melalui MAXX CARD STAR CLASS.<br><br>Tingkatkan terus transaksi Anda dan kumpulkan MAXX POINT untuk mendapatkan berbagai keuntungan eksklusif hanya di Cinemaxx.<br><br><br><b>English: </b> MAXX CARD & MAXX CARD STAR CLASS are created as a token of appreciation from Cinemaxx Management to all loyal Cinemaxx customers. As a member, you get to collect MAXX POINTS and can redeem your MAXX POINTS with various gifts or exciting oers from Cinemaxx. To add value to your transactions, Cinemaxx also presents a variety of exclusive privileges from participating merchants and business partners you can enjoy with MAXX CARD STAR CLASS.<br><br>Do more transactions and collect more MAXX POINTS to get access to a variety of exclusive benefits only at Cinemaxx."
	 * ; txtContent.setText(Html.fromHtml(text_view_str));
	 * 
	 * list = new ArrayList<MaxxCardDescTo>(); List<Integer> imageList = new
	 * ArrayList<Integer>(); List<String> headerList = new ArrayList<String>();
	 * List<String> descList = new ArrayList<String>();
	 * imageList.addAll(Arrays.asList(R.drawable.demo4, R.drawable.demo1,
	 * R.drawable.demo2, R.drawable.demo3));
	 * headerList.addAll(Arrays.asList("OPTIK TUNGGAL: DISC. 10%*",
	 * "GRAND IMPERIAL LAMIAN: FREE 2 DIMSUM*", "OPTIK TUNGGAL: DISC. 10%*",
	 * "GRAND IMPERIAL LAMIAN: FREE 2 DIMSUM*")); descList.addAll(Arrays
	 * .asList(
	 * "(From Frame & Sun Glasses)\n*Khusus pembelian frame komplit (frame + lensa) kecuali price controlled item (cartier, lensa, accesories) - All Outlets"
	 * ,
	 * "*Minimum spending Rp 250.000 (sebelum tax) All Outlets (Jabodetabek, Medan, Solo, Surabaya)"
	 * ,
	 * "(From Frame & Sun Glasses)\n*Khusus pembelian frame komplit (frame + lensa) kecuali price controlled item (cartier, lensa, accesories) - All Outlets"
	 * ,
	 * "*Minimum spending Rp 250.000 (sebelum tax) All Outlets (Jabodetabek, Medan, Solo, Surabaya)"
	 * ));
	 * 
	 * for (int i = 0; i < 4; i++) { MaxxCardDescTo maxxCardDescTo = new
	 * MaxxCardDescTo(); maxxCardDescTo.setImageLogo(imageList.get(i));
	 * maxxCardDescTo.setHeader(headerList.get(i));
	 * maxxCardDescTo.setDesc(descList.get(i)); list.add(maxxCardDescTo); } }
	 */

	public void initListener() {

		/*
		 * upcomingListView.setOnTouchListener(new OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) { // TODO
		 * Auto-generated method stub
		 * v.getParent().requestDisallowInterceptTouchEvent(true); return false;
		 * } });
		 */

		/*historyListView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});*/

		bookNowBtn.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new NowPlayingFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
			}
		});

		aboutTv.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new AboutMaxxCardFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
			}
		});

		logoutTv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				pref.setMaxxCardLoginResponse("");
				activity.callBackPress();
				activity.refreshAdapter();
			}
		});

		relMenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkMenu == 0) {
					lnrMenuItem.setVisibility(View.VISIBLE);
					mAnimator.start();
					chkMenu = 1;
				} else {
					// lnrMenuItem.setVisibility(View.GONE);
					collapse();
					chkMenu = 0;
				}
			}
		});

		starClass.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.GONE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);
				txtProfile.setVisibility(View.VISIBLE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.VISIBLE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				lnrProfile.setVisibility(View.GONE);
				if(pref.getClickManuAboutMaxxCard().equals("")){
					callClickOnMenuForAboutMaxx();
				}else{
					webViewContentClickForAbout.loadData(Utils.getTncWebview(pref.getClickManuAboutMaxxCard()), "text/html; charset=UTF-8", null);
				}
			}
		});

		privateAndBenefits.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.GONE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);
				txtProfile.setVisibility(View.VISIBLE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.VISIBLE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				lnrProfile.setVisibility(View.GONE);
				// openPrivateAndBenefits();
			}
		});

		starClassPrivileges.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.GONE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);
				txtProfile.setVisibility(View.VISIBLE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.VISIBLE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				lnrProfile.setVisibility(View.GONE);
				// openstarClassPrivileges();
			}
		});

		faq.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.GONE);
				termsAndConditions.setVisibility(View.VISIBLE);
				txtProfile.setVisibility(View.VISIBLE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.VISIBLE);
				lnrTermAndCondition.setVisibility(View.GONE);
				lnrProfile.setVisibility(View.GONE);
				// openFaq();
			}
		});

		termsAndConditions.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.GONE);
				txtProfile.setVisibility(View.VISIBLE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.VISIBLE);
				lnrProfile.setVisibility(View.GONE);
				// openTermAndConditions();
			}
		});

		txtProfile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);
				txtProfile.setVisibility(View.GONE);
				logoutTv.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;

				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				lnrProfile.setVisibility(View.VISIBLE);
			}
		});

		lnrMenuItem.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

			@Override
			public boolean onPreDraw() {
				lnrMenuItem.getViewTreeObserver().removeOnPreDrawListener(this);
				lnrMenuItem.setVisibility(View.GONE);
				final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
				final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
				lnrMenuItem.measure(widthSpec, heightSpec);

				mAnimator = slideAnimator(0, lnrMenuItem.getMeasuredHeight());
				return true;
			}
		});
		
		btnJoinNow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToWebview("http://www.cinemaxxtheater.com/Loyalty/Signup.aspx");
			}
		});

		/*webViewContentForAbout.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

		webViewContentForPrivilegeandBenefits.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				v.getParent().requestDisallowInterceptTouchEvent(true);

				return false;
			}
		});

		webViewContentForStarClassPrivileges.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});*/

	}

	public void collapse() {
		int finalHeight = lnrMenuItem.getHeight();

		ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

		mAnimator.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationEnd(Animator animator) {
				// Height=0, but it set visibility to GONE
				lnrMenuItem.setVisibility(View.GONE);
				// mLinearLayoutHeader.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationStart(Animator animator) {
			}

			@Override
			public void onAnimationCancel(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
			}
		});
		mAnimator.start();
	}

	public ValueAnimator slideAnimator(int start, int end) {

		ValueAnimator animator = ValueAnimator.ofInt(start, end);

		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update Height
				int value = (Integer) valueAnimator.getAnimatedValue();

				ViewGroup.LayoutParams layoutParams = lnrMenuItem.getLayoutParams();
				layoutParams.height = value;
				lnrMenuItem.setLayoutParams(layoutParams);
			}
		});
		return animator;
	}

	/*public void callNewsListAdapter() {
		listDesc = (ListView) getView().findViewById(R.id.listDesc);
		getView().findViewById(R.id.rel_maxx_card_about).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkAboutList == 0) {
					listDesc.setVisibility(View.VISIBLE);
					chkAboutList = 1;
				} else {
					listDesc.setVisibility(View.GONE);
					chkAboutList = 0;
				}
			}
		});

		listDesc.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (newsList.size() * (int) getResources().getDimension(R.dimen.maxx_card_fragment_maxx_card_content_adapter_item_height))));
		MaxxCardDescAdapter maxxCardDescAdapter = new MaxxCardDescAdapter(getActivity(), newsList);
		listDesc.setAdapter(maxxCardDescAdapter);
	}*/

	public void callListAdapterPrivateAndBenefits() {
		listPrivateAndBenefits = (ListView) getView().findViewById(R.id.listDesclist);
		getView().findViewById(R.id.rel_maxx_card_privileges_ben).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkPrivateAndBenefitsList == 0) {
					listPrivateAndBenefits.setVisibility(View.VISIBLE);
					chkPrivateAndBenefitsList = 1;
				} else {
					listPrivateAndBenefits.setVisibility(View.GONE);
					chkPrivateAndBenefitsList = 0;
				}
			}
		});
		listPrivateAndBenefits.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (specialOfferList.size() * (int) getResources().getDimension(R.dimen.maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height))));
		MaxxCardSpecialOfferAdapter maxxCardspecialofferAdapter = new MaxxCardSpecialOfferAdapter(getActivity(), specialOfferList);
		listPrivateAndBenefits.setAdapter(maxxCardspecialofferAdapter);
	}

	/*
	 * public void openTermAndConditions() { TextView tm1 = (TextView)
	 * getView().findViewById(R.id.tv1); TextView tm11 = (TextView)
	 * getView().findViewById(R.id.tv11);
	 * tm1.setText(Html.fromHtml(getString(R.string.tm1)));
	 * tm11.setText(Html.fromHtml(getString(R.string.tm11))); }
	 * 
	 * public void openFaq() { TextView faq1 = (TextView)
	 * getView().findViewById(R.id.tv_faq1); TextView faq2 = (TextView)
	 * getView().findViewById(R.id.tv_faq2);
	 * faq1.setText(Html.fromHtml(getString(R.string.faq1)));
	 * faq2.setText(Html.fromHtml(getString(R.string.faq2))); }
	 */

	/*
	 * public void openstarClassPrivileges() { TextView txtContent = (TextView)
	 * getView().findViewById( R.id.txtContentStarClassPriviles); String
	 * text_view_str =
	 * "- Collect Maxx Points*\n- Get Birthday & Anniversary Treats*\n- Receive Special Oers From Selected Merchants*"
	 * ; txtContent.setText(text_view_str);
	 * 
	 * callListAdapterStarClassPrivileges(); }
	 */

	public void callListAdapterStarClassPrivileges() {
		listStarClass = (ListView) getView().findViewById(R.id.listDescStarClassPreviliges);
		getView().findViewById(R.id.rel_star_class).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chklistStarClassList == 0) {
					listStarClass.setVisibility(View.VISIBLE);
					chklistStarClassList = 1;
				} else {
					listStarClass.setVisibility(View.GONE);
					chklistStarClassList = 0;
				}
			}
		});
		listStarClass.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (starClassSpecialOfferList.size() * (int) getResources().getDimension(R.dimen.maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height_starclass))));
		MaxxCardStarClassSpecialOfferAdapter maxxcardstarclassspecialOfferAdapter = new MaxxCardStarClassSpecialOfferAdapter(getActivity(), starClassSpecialOfferList);
		listStarClass.setAdapter(maxxcardstarclassspecialOfferAdapter);

	}

	/*
	 * public void openPrivateAndBenefits() { TextView txtContent = (TextView)
	 * getView().findViewById( R.id.txtContentpriviles); String text_view_str =
	 * "- Collect Maxx Points*\n- Get Birthday & Anniversary Treats*\n- Receive Special Oers From Selected Merchants*"
	 * ; txtContent.setText(text_view_str);
	 * 
	 * callListAdapterPrivateAndBenefits(); }
	 */

	public void callCallService() {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@SuppressWarnings("static-access")
				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					// TODO Auto-generated method stub
					System.out.println("Maxx card welcome response: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {

							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {

								dataListUpcoming = new ArrayList<MaxxCardUpCommingTo>();
								dataListHistory = new ArrayList<MaxxCardUpCommingTo>();
								JSONArray memberArray = mainObj.optJSONArray(MaxxCardWelcomeConstant.MEMBER_TRANS_LIST_ARRAY);
								for (int i = 0; i < memberArray.length(); i++) {
									MaxxCardUpCommingTo upcomingTo = new MaxxCardUpCommingTo();
									JSONObject memberObj = memberArray.optJSONObject(i);

									upcomingTo.setId(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_ID));
									upcomingTo.setVistaTransId(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_VISTA_TRANS_ID));
									upcomingTo.setTransDateTime(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_TRANSDATE_TIME));
									upcomingTo.setCinemaName(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_CINEMA_NAME));
									upcomingTo.setMovieNAme(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_MOVIE_NAME));
									upcomingTo.setMovieCode(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_MOVIE_CODE));
									upcomingTo.setShowDateTime(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_SHOW_DATE_TIME));
									upcomingTo.setScreen(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_SCREEN));
									upcomingTo.setSeatNo(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_SEAT_NOS));
									upcomingTo.setNoOfSeat(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_NO_OF_SEAT));
									upcomingTo.setTicketPrice(Integer.toString(memberObj.optInt(MaxxCardWelcomeConstant.UPCOMING_TICKET_PRICE)));
									upcomingTo.setTotalPrice(Integer.toString(memberObj.optInt(MaxxCardWelcomeConstant.UPCOMING_TOTAL_PRICE)));
									upcomingTo.setBookingFee(Integer.toString(memberObj.optInt(MaxxCardWelcomeConstant.UPCOMING_BOOKING_FEE)));
									upcomingTo.setDiscount(Integer.toString(memberObj.optInt(MaxxCardWelcomeConstant.UPCOMING_DISCOUNT)));
									upcomingTo.setTotalAmount(Integer.toString(memberObj.optInt(MaxxCardWelcomeConstant.UPCOMING_TOTAL_AMOUNT)));
									upcomingTo.setPointsRedeem(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_POINTS_REDEEM));
									upcomingTo.setPointsEarned(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_POINTS_EARNED));
									upcomingTo.setIsUpcoming(memberObj.optString(MaxxCardWelcomeConstant.UPCOMING_IS_UPCOMING));

									if (upcomingTo.getIsUpcoming().trim().equals("true")) {
										dataListUpcoming.add(upcomingTo);
									} 
									
									dataListHistory.add(upcomingTo);
									
								}

								if (dataListUpcoming.size() > 1) {
									upcomingListView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (dataListUpcoming.size() * ((5 * (int) getResources().getDimension(R.dimen.maxx_card_upcoming_booking_item_lay_height)) + ((int) getResources().getDimension(R.dimen.maxx_card_upcoming_booking_middle_lay_height)) + (2 * (int) getResources().getDimension(R.dimen.maxx_card_upcoming_booking_main_lay_top_margin))))));
								}
								MaxxCardUpcommingAdapter upcomingAdapter = new MaxxCardUpcommingAdapter(getActivity(), dataListUpcoming);
								upcomingListView.setAdapter(upcomingAdapter);

								if (dataListUpcoming.size() > 0) {
									bookNowTv.setText("Would you like to book another ticket?");
								}
								
								if(dataListHistory.size() > 0){
									lnrRecentHistory.setVisibility(View.GONE);
									historyListView.setVisibility(View.VISIBLE);
									MaxxCardHistoryAdapter historyAdapter = new MaxxCardHistoryAdapter(getActivity(), dataListHistory);
									historyListView.setAdapter(historyAdapter);
									historyListView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (dataListHistory.size() * (((int) getResources().getDimension(R.dimen.maxx_card_recent_history_date_layout_height)) + ((int) getResources().getDimension(R.dimen.maxx_card_recent_history_content_layout_height)) + 2))));
								}else{
									
									new AlertDialog(getActivity(), getActivity().getResources().getString(R.string.no_history_alert), null).show();//CODE CHANGED BY DIBYENDU BHATTACHARYYA ON 28-DEC-2016
									
									lnrRecentHistory.setVisibility(View.VISIBLE);
									historyListView.setVisibility(View.GONE);
								}
								
								if(!pref.getManuAboutMaxxCard().equals("")){
									loadWebView();
//									showNewsList();
									showSpacialOfferList();
									showStarClassSpacialOffer();
									mainContentLayout.setVisibility(View.VISIBLE);
								}else{
									callMenuServiceForFaq();
								}
								
							}else{
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
//										activity.callBackPress();
									}
								});
								dialog.show();
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}

					activity.setNotClickable();
//					mainContentLayout.setVisibility(View.VISIBLE);
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				loyalty.GetMemberTransactionHistoryAsync(Long.parseLong(maxxCardDetailsTo.getLoyaltyRequestId()), true, maxxCardDetailsTo.getMemberId(), activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}

	}

	public void CallMemberBalanceList() {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@SuppressWarnings("static-access")
				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					// TODO Auto-generated method stub
					System.out.println("Maxx card member balance list response: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {

							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {

								memberBalanceList = new ArrayList<MemberBalanceTo>();
								JSONObject balanceObj = mainObj.optJSONObject(MaxxCardBalanceConstant.BALANCE_LIST);
								JSONArray memberArray = balanceObj.optJSONArray(MaxxCardBalanceConstant.DISCOUNT_LIST);
								for (int i = 0; i < memberArray.length(); i++) {
									MemberBalanceTo memberBalanceTo = new MemberBalanceTo();
									JSONObject memberObj = memberArray.optJSONObject(i);

									memberBalanceTo.setBalanceTypeId(memberObj.optString(MaxxCardBalanceConstant.BALANCE_TYPE_ID));
									memberBalanceTo.setRecognitionID(memberObj.optString(MaxxCardBalanceConstant.RECOGNITION_ID));
									memberBalanceTo.setName(memberObj.optString(MaxxCardBalanceConstant.NAME));
									memberBalanceTo.setDescription(memberObj.optString(MaxxCardBalanceConstant.DESCRIPTION));
									memberBalanceTo.setMessageText(memberObj.optString(MaxxCardBalanceConstant.MESSAGE_TEXT));
									memberBalanceTo.setDisplay(memberObj.optString(MaxxCardBalanceConstant.DISPLAY));
									memberBalanceTo.setIsManuallySelected(memberObj.optString(MaxxCardBalanceConstant.IS_MANUALLY_SELECTED));
									memberBalanceTo.setQtyAvailable(memberObj.optString(MaxxCardBalanceConstant.QTY_AVAILABLE));
									memberBalanceTo.setQtyEarn(memberObj.optString(MaxxCardBalanceConstant.QTY_EARN));
									memberBalanceTo.setVistaId(memberObj.optString(MaxxCardBalanceConstant.VISTA_ID));
									memberBalanceTo.setExpiryDate(memberObj.optString(MaxxCardBalanceConstant.EXPIRY_DATE));
									
									memberBalanceList.add(memberBalanceTo);
								}
								showBalanceList(memberBalanceList);

								callCallService();
							}else{
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					
					}

					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				loyalty.GetMemberBalanceListAsync(Long.parseLong(maxxCardDetailsTo.getLoyaltyRequestId()), true, maxxCardDetailsTo.getMemberId(), activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}

	}

	protected void showBalanceList(ArrayList<MemberBalanceTo> memberBalanceList) {
		try {
			memberBalancePresentLay.removeAllViews();
			if (memberBalanceList.size() > 0) {
				for (int i = 0; i < memberBalanceList.size(); i++) {
					TextView txtVw = new TextView(activity);
//					TextView txtVwExpDate = new TextView(activity);
					LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					param.setMargins(0, 0, 0, 10);

					txtVw.setCompoundDrawablesWithIntrinsicBounds(R.drawable.maxx_card_star, 0, 0, 0);
					txtVw.setCompoundDrawablePadding(10);
//					txtVw.setText(memberBalanceList.get(i).getName().trim());
					txtVw.setText(memberBalanceList.get(i).getDescription().trim());
					txtVw.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.maxx_card_welcome_fragment_small_text_size));

//					txtVwExpDate.setText(Utils.getMaxxCardUpcomingDate(memberBalanceList.get(i).getExpiryDate()));
//					txtVwExpDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.maxx_card_welcome_fragment_small_text_size));
//					txtVwExpDate.setPadding(45, 0, 0, 0);
//					txtVwExpDate.setLayoutParams(param);

					memberBalancePresentLay.addView(txtVw);
//					memberBalancePresentLay.addView(txtVwExpDate);
				}
			} else {
				TextView txtVw = new TextView(activity);
				txtVw.setText("You don't have any rewards");
				txtVw.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.maxx_card_welcome_fragment_small_text_size));
				memberBalancePresentLay.addView(txtVw);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void callMenuServiceForFaq() {
		if (Utils.isNetworkAvailable(getActivity())) {
			pDialog = new CustomProgressDialog(activity);
        	pDialog.showDialog();
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("MAXX CARD FAQ: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuFaq(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
								}
							} else {

							}
							callMenuServiceForTermsAndConditions();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					
					
					
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("21", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void callMenuServiceForTermsAndConditions() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("Terms & Conditions: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuTermsAndConditions(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
								}
							} else {
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}
							callMenuServiceForPrivilegesAndBenefis();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					
					
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("23", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	/*public void callMenuServiceForAboutMaxx() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("ABOUT MAXX CARD & MAXX CARD STAR CLASS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuAboutMaxxCard(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
								}
							} else {

							}
							callMenuServiceForNews();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("19", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialog dialog = new AlertDialog(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub

				}
			});
			dialog.show();
		}
	}*/
	
	/*public void callMenuServiceForNews() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("GetMaxxCardNewsAsync: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setManuMaxxCardNews(Data.toString());
							callMenuServiceForPrivilegesAndBenefis();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardNewsAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialog dialog = new AlertDialog(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub

				}
			});
			dialog.show();
		}
	}*/
	
	public void callMenuServiceForPrivilegesAndBenefis() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("MAXX CARD PRIVILEGES & BENEFITS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuPrivilegesAndBenefits(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
								}
							} else {
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}
							callMenuServiceForPrivilegesAndBenefisSpecialOffer();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("20", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void callMenuServiceForPrivilegesAndBenefisSpecialOffer() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("callMenuServiceForPrivilegesAndBenefisOfferList: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setManuSpcialOffer(Data.toString().trim());
							callMenuServiceForStarClassPrivilages();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardSpecialOfferAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void callMenuServiceForStarClassPrivilages() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("Terms & Conditions: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuStarClassPrivileges(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
								}
							} else {
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}
							callMenuServiceForStarClassSpecialOffer();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{

						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								activity.callBackPress();
							}
						});
						dialog.show();
					
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("24", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void callMenuServiceForStarClassSpecialOffer() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("callMenuServiceForStarClassSpecialOffer: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setStarClassSpcialOffer(Data.toString().trim());
							loadWebView();
							showSpacialOfferList();
							showStarClassSpacialOffer();
							callClickOnMenuForAboutMaxx();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					activity.setNotClickable();
					mainContentLayout.setVisibility(View.VISIBLE);
					if(pDialog != null){
	            		pDialog.dismissDialog();
	            	}
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardStarClassSpecialOfferAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void callClickOnMenuForAboutMaxx() {
		if (Utils.isNetworkAvailable(getActivity())) {
			if(pDialog == null){
				pDialog = new CustomProgressDialog(activity);
	        	pDialog.showDialog();
			}
			activity.setClickable();
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("ABOUT MAXX CARD & MAXX CARD STAR CLASS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setClickManuAboutMaxxCard(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewContentClickForAbout.loadData(Utils.getTncWebview(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()), "text/html; charset=UTF-8", null);
								
								}
							} else{
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					activity.setNotClickable();
					if(pDialog != null){
	            		pDialog.dismissDialog();
	            		pDialog = null;
	            	}
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("22", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void goToWebview(String str) {
		Uri uri = Uri.parse(str);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}
	
	public void UpdateMemberDetails() {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("UpdateMemberDetails: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {

							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {
								pref.setMaxxCardLoginResponse(Data.toString().trim());

								maxxCardDetailsTo = new MaxxCardDetailsTo();
								maxxCardDetailsTo.setLoyaltyRedeemType(mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
								maxxCardDetailsTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
								maxxCardDetailsTo.setStarClassCriteria(mainObj.optString(MaxxCardDetailsConstant.STAR_CLASS_CRITERIA));

								JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
								maxxCardDetailsTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
								maxxCardDetailsTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
								maxxCardDetailsTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
								maxxCardDetailsTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
								maxxCardDetailsTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));

								maxxCardDetailsTo.setLevelId(memberObj.optString(MaxxCardDetailsConstant.LEVEL_ID));
								maxxCardDetailsTo.setLevelName(memberObj.optString(MaxxCardDetailsConstant.LEVEL_NAME));

								maxxCardDetailsTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
								maxxCardDetailsTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
								maxxCardDetailsTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
								maxxCardDetailsTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
								maxxCardDetailsTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
								maxxCardDetailsTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
								maxxCardDetailsTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
								maxxCardDetailsTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
								maxxCardDetailsTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
								maxxCardDetailsTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
								maxxCardDetailsTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
								maxxCardDetailsTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
								maxxCardDetailsTo.setWishToReceiveSMS(memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
								maxxCardDetailsTo.setSendNewsLetter(memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
								maxxCardDetailsTo.setMailingFrequency(memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
								maxxCardDetailsTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
								maxxCardDetailsTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
								maxxCardDetailsTo.setMembershipActivated(memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));
								
								/***
								 * EDITED BY DIBYENDU on 17 JAN 2017 for image change dynamically
								 */
								maxxCardDetailsTo.setMaxCardImage(memberObj.optString(MaxxCardDetailsConstant.MAX_CARD_IMAGE));//todo edited by Dibyendu
								
								JSONArray balanceListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
								for(int k = 0; k < balanceListArray.length(); k++){
								JSONObject balanceListObj = balanceListArray.optJSONObject(k);
								maxxCardDetailsTo.setBalanceTypeId(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
								maxxCardDetailsTo.setBalanceName(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
								maxxCardDetailsTo.setBalanceMessage(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
								maxxCardDetailsTo.setBalanceDisplay(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
								maxxCardDetailsTo.setBalancePointRemaining(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
								maxxCardDetailsTo.setBalanceIsDefault(balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
							}
							
								JSONArray pointsExpiryListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
//								for(int j = 0; j < pointsExpiryListArray.length(); j++){
								JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(0);
								maxxCardDetailsTo.setPointsExpiring(pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
								maxxCardDetailsTo.setExpireOn(pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
								maxxCardDetailsTo.setBalanceTypeIdExp(pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
//								}
								
								maxxCardDetailsTo.setIsProfileComplete(memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
								maxxCardDetailsTo.setLoyaltyRequestId(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));

								initView();
								initListener();
								shpref.set_MaxcardMemberid(getActivity().getApplicationContext(), maxxCardDetailsTo.getMemberId());
								shpref.set_LoyaltyRequestid(getActivity().getApplicationContext(), maxxCardDetailsTo.getLoyaltyRequestId());
								
								
							}else{
		                        AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim(), new DialogListener() {
									
									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
								dialog.show();
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{

						AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

							@Override
							public void onOkClick() {
								activity.callBackPress();
							}
						});
						dialog.show();
					}

					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				loyalty.GetMemberDetailsAsync(Long.parseLong(maxxCardDetailsTo.getLoyaltyRequestId()), true, maxxCardDetailsTo.getMemberId(), activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}

	}

}
