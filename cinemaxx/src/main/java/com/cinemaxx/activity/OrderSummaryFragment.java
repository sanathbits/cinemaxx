package com.cinemaxx.activity;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.bean.HistoryDescTo;
import com.cinemaxx.bean.OrderTo;
import com.cinemaxx.bean.SeatLayoutTo;
import com.cinemaxx.constant.PaymentConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForOrderSummary;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract.Events;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class OrderSummaryFragment extends Fragment {

	TextView bookingIdTv, bookingNumberTv, movieTitleTv, cinemaTv, showTimeTv,
			formatTv, cinemaNoTv, emailTv, paymentModeTv;
	TextView bookingIdTvCont, bookingNumberTvCont, movieTitleTvCont,
			cinemaTvCont, showTimeTvCont, formatTvCont, cinemaNoTvCont,
			emailTvCont, paymentModeTvCont;
	TextView seatSelectedTv, totalSeatTv, pricePerTicketTv, ticketTotalTv,
			bookingFeeTv, discountTv, totalPriceTv, loyalityPointsTv,
			loyalityAmountTv;
	TextView seatSelectedTvCont, totalSeatTvCont, pricePerTicketTvCont,
			ticketTotalTvCont, bookingFeeTvCont, discountTvCont,
			totalPriceTvCont;
	TextView containerTv, containerTv1, containerTv2, containerTv3;
	TextView locationTv;
	ImageView imageView;
	LinearLayout containerLayout, savetoHistorylnr;
	MainActivity activity;

	String jsonString;
	SeatLayoutTo seatTo;
	OrderTo orderTo;
	String traId;
	CinemaxxPref pref;
	Button savetoHistoryBtn;
	String description, startTime, title;
	int mnth;
	ScrollView scrollView;

	public OrderSummaryFragment(SeatLayoutTo seatTo, OrderTo orderTo,
			String jsonString, String traId) {
		this.seatTo = seatTo;
		this.orderTo = orderTo;
		this.jsonString = jsonString;
		this.traId = traId;
		System.out.println(traId);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_history_details,
				container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initView();
		initListener();
		activity = (MainActivity) getActivity();

		callService();

		activity.hideBackBtn();
		activity.clickOnBuyTickets("ORDER CONFIRMATION");
	}

	public void initView() {

		pref = new CinemaxxPref(getActivity());
		containerLayout = (LinearLayout) getView().findViewById(
				R.id.history_detals_container_layout);
		containerLayout.setVisibility(View.INVISIBLE);
		Typeface faceMedium = Typeface.createFromAsset(getActivity()
				.getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(),
				"Gotham-Medium.ttf");
		savetoHistorylnr = (LinearLayout) getView().findViewById(
				R.id.history_save_to_history_lnr);
		savetoHistoryBtn = (Button) getView().findViewById(
				R.id.history_save_to_history_btn);
		locationTv = (TextView) getView().findViewById(
				R.id.history_details_location_tv);
		locationTv.setTypeface(faceThin);
		locationTv.setText("Location: " + pref.getCityName());

		containerTv = (TextView) getView().findViewById(
				R.id.history_details_content_tv);
		// containerTv.setTypeface(faceMedium);
		containerTv1 = (TextView) getView().findViewById(
				R.id.history_details_content_tv1);
		// containerTv1.setTypeface(faceMedium);
		containerTv2 = (TextView) getView().findViewById(
				R.id.history_details_content_tv2);
		// containerTv2.setTypeface(faceMedium);
		containerTv3 = (TextView) getView().findViewById(
				R.id.history_details_content_tv3);
		// containerTv3.setTypeface(faceMedium);
		imageView = (ImageView) getView().findViewById(R.id.history_iv);

		bookingIdTv = (TextView) getView().findViewById(R.id.history_booked_id);
		// bookingIdTv.setTypeface(faceMedium);
		bookingNumberTv = (TextView) getView().findViewById(
				R.id.history_booking_no);
		// bookingNumberTv.setTypeface(faceMedium);
		movieTitleTv = (TextView) getView().findViewById(
				R.id.history_movie_title);
		// movieTitleTv.setTypeface(faceMedium);
		cinemaTv = (TextView) getView().findViewById(R.id.history_cinema);
		// cinemaTv.setTypeface(faceMedium);
		showTimeTv = (TextView) getView().findViewById(R.id.history_showtime);
		// showTimeTv.setTypeface(faceMedium);
		formatTv = (TextView) getView().findViewById(R.id.history_format);
		// formatTv.setTypeface(faceMedium);
		cinemaNoTv = (TextView) getView().findViewById(R.id.history_cinema_no);
		// cinemaNoTv.setTypeface(faceMedium);
		emailTv = (TextView) getView().findViewById(R.id.history_email);
		// emailTv.setTypeface(faceMedium);
		paymentModeTv = (TextView) getView().findViewById(
				R.id.history_payment_mode);
		// paymentModeTv.setTypeface(faceMedium);

		bookingIdTvCont = (TextView) getView().findViewById(R.id.textView1);
		// bookingIdTvCont.setTypeface(faceMedium);
		bookingNumberTvCont = (TextView) getView().findViewById(R.id.textView2);
		// bookingNumberTvCont.setTypeface(faceMedium);
		movieTitleTvCont = (TextView) getView().findViewById(R.id.textView3);
		// movieTitleTvCont.setTypeface(faceMedium);
		cinemaTvCont = (TextView) getView().findViewById(R.id.textView4);
		// cinemaTvCont.setTypeface(faceMedium);
		showTimeTvCont = (TextView) getView().findViewById(R.id.textView5);
		// showTimeTvCont.setTypeface(faceMedium);
		formatTvCont = (TextView) getView().findViewById(R.id.textView6);
		// formatTvCont.setTypeface(faceMedium);
		cinemaNoTvCont = (TextView) getView().findViewById(R.id.textView7);
		// cinemaNoTvCont.setTypeface(faceMedium);
		emailTvCont = (TextView) getView().findViewById(R.id.textView8);
		// emailTvCont.setTypeface(faceMedium);
		paymentModeTvCont = (TextView) getView().findViewById(R.id.textView9);
		// paymentModeTvCont.setTypeface(faceMedium);

		// ////////////////////
		seatSelectedTv = (TextView) getView().findViewById(
				R.id.history_selected);
		// seatSelectedTv.setTypeface(faceMedium);
		totalSeatTv = (TextView) getView()
				.findViewById(R.id.history_total_seat);
		// totalSeatTv.setTypeface(faceMedium);
		pricePerTicketTv = (TextView) getView().findViewById(
				R.id.history_price_perticket);
		// pricePerTicketTv.setTypeface(faceMedium);
		ticketTotalTv = (TextView) getView().findViewById(
				R.id.history_ticket_total);
		loyalityPointsTv = (TextView) getView().findViewById(
				R.id.history_ticket_loyalty_points);
		loyalityAmountTv = (TextView) getView().findViewById(
				R.id.history_ticket_loyalty_amount);
		// ticketTotalTv.setTypeface(faceMedium);
		bookingFeeTv = (TextView) getView().findViewById(
				R.id.history_booking_fee);
		// bookingFeeTv.setTypeface(faceMedium);
		discountTv = (TextView) getView().findViewById(R.id.history_discount);
		// discountTv.setTypeface(faceMedium);
		totalPriceTv = (TextView) getView().findViewById(
				R.id.history_total_price);
		// totalPriceTv.setTypeface(faceMedium);

		seatSelectedTvCont = (TextView) getView().findViewById(R.id.textView11);
		// seatSelectedTvCont.setTypeface(faceMedium);
		totalSeatTvCont = (TextView) getView().findViewById(R.id.textView12);
		// totalSeatTvCont.setTypeface(faceMedium);
		pricePerTicketTvCont = (TextView) getView().findViewById(
				R.id.textView13);
		// pricePerTicketTvCont.setTypeface(faceMedium);
		ticketTotalTvCont = (TextView) getView().findViewById(R.id.textView14);
		// ticketTotalTvCont.setTypeface(faceMedium);
		bookingFeeTvCont = (TextView) getView().findViewById(R.id.textView16);
		// bookingFeeTvCont.setTypeface(faceMedium);
		discountTvCont = (TextView) getView().findViewById(R.id.textView17);
		// discountTvCont.setTypeface(faceMedium);
		totalPriceTvCont = (TextView) getView().findViewById(R.id.textView18);
		// totalPriceTvCont.setTypeface(faceMedium);
		scrollView = (ScrollView) getView().findViewById(R.id.scrollView);
	}

	private void initListener() {
		// TODO Auto-generated method stub
		savetoHistoryBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveCalendar();

			}
		});
	}

	public void callService() {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				// containerTv.setText(Data.toString());
				HistoryDescTo historyDescTo = null;
				containerLayout.setVisibility(View.VISIBLE);
				System.out.println("Last Response: " + Data.toString());
				try {
					JSONObject mainJson = new JSONObject(Data.toString());

					if (mainJson.optString(PaymentConstant.ERROR_DESC).equals(
							"0")) {
						JSONArray mainArray = mainJson
								.optJSONArray("BookingOrderDetails");
						for (int i = 0; i < mainArray.length(); i++) {

							JSONObject hisObj = mainArray.optJSONObject(i);

							historyDescTo = new HistoryDescTo();

							historyDescTo.setBookingId(hisObj
									.optString(PaymentConstant.BOOKING_ID));
							historyDescTo.setBookingNumber(hisObj
									.optString(PaymentConstant.BOOKING_NUMBER));
							historyDescTo.setOrderId(hisObj
									.optString(PaymentConstant.ORDER_ID));
							historyDescTo.setMovieName(hisObj
									.optString(PaymentConstant.MOVIE_NAME));
							historyDescTo.setCinemaName(hisObj
									.optString(PaymentConstant.CINEMA_NAME));
							historyDescTo.setShowTime(hisObj
									.optString(PaymentConstant.SHOW_TIME));
							historyDescTo.setFormat(hisObj
									.optString(PaymentConstant.FORMAT));
							historyDescTo.setCinemaNo(hisObj
									.optString(PaymentConstant.CINEMA_NUMBER));
							historyDescTo.setEmail(hisObj
									.optString(PaymentConstant.EMAIL));
							historyDescTo.setPaymentMode(hisObj
									.optString(PaymentConstant.PAYMENT_MODE));
							historyDescTo.setSeatNumber(hisObj
									.optString(PaymentConstant.SEAT_NUMBER));
							historyDescTo.setNoofSeats(hisObj
									.optString(PaymentConstant.NO_OF_SEAT));
							historyDescTo.setPricePerTicket(hisObj
									.optString(PaymentConstant.PRICE_PER_TICKET));
							historyDescTo.setTotalTicket(hisObj
									.optString(PaymentConstant.TOTAL_TICKET));
							historyDescTo.setConvenieceFee(hisObj
									.optString(PaymentConstant.CONVINENCE_FEE));
							historyDescTo.setDiscountAmount(hisObj
									.optString(PaymentConstant.DISCOUNT_AMOUNT));
							historyDescTo.setLoyaltyPoints(hisObj
									.optString(PaymentConstant.LOYALTY_POINTS));
							historyDescTo.setLoyaltyAmount(hisObj
									.optString(PaymentConstant.LOYALTY_AMOUNT));

							historyDescTo.setTotalPrice(hisObj
									.optString(PaymentConstant.TOTAL_PRICE));
							historyDescTo.setQrCode(hisObj.optString(
									PaymentConstant.QR_CODE).replace(
									"data:image/gif;base64,", ""));
							historyDescTo.setOrderStatus(hisObj
									.optString(PaymentConstant.STATUS_CODE));

							imageView.setImageBitmap(Utils
									.getBitmap(historyDescTo.getQrCode()));
							
							
							bookingIdTv.setText(historyDescTo.getBookingId());
							bookingNumberTv.setText(historyDescTo
									.getBookingNumber());
							movieTitleTv.setText(historyDescTo.getMovieName());
							cinemaTv.setText(historyDescTo.getCinemaName());
							showTimeTv.setText(historyDescTo.getShowTime());
							formatTv.setText(historyDescTo.getFormat());
							cinemaNoTv.setText(historyDescTo.getCinemaNo());
							emailTv.setText(historyDescTo.getEmail());
							paymentModeTv.setText(historyDescTo
									.getPaymentMode());

							seatSelectedTv.setText(historyDescTo
									.getSeatNumber());
							totalSeatTv.setText(historyDescTo.getNoofSeats());
							pricePerTicketTv.setText(historyDescTo
									.getPricePerTicket());
							ticketTotalTv.setText(historyDescTo
									.getTotalTicket());
							if (!historyDescTo.getLoyaltyPoints().equals("")
									&& historyDescTo.getLoyaltyPoints() != null) {
								loyalityPointsTv.setText(historyDescTo
										.getLoyaltyPoints());
							}
							if (!historyDescTo.getLoyaltyAmount().equals("")
									&& historyDescTo.getLoyaltyAmount() != null) {
								loyalityAmountTv.setText(historyDescTo
										.getLoyaltyAmount());
							}
							bookingFeeTv.setText(historyDescTo
									.getConvenieceFee());
							discountTv.setText(historyDescTo
									.getDiscountAmount());
							totalPriceTv.setText(historyDescTo.getTotalPrice());

							description = "Booking Id : "
									+ historyDescTo.getBookingId() + "\n\n"
									+ "Booking Number : "
									+ historyDescTo.getBookingNumber() + "\n\n"
									+ "Movie Title : "
									+ historyDescTo.getMovieName() + "\n\n"
									+ "Cinema : "
									+ historyDescTo.getCinemaName() + "\n\n"
									+ "Showtime : "
									+ historyDescTo.getShowTime() + "\n\n"
									+ "Format : " + historyDescTo.getFormat()
									+ "\n\n" + "Cinema No : "
									+ historyDescTo.getCinemaNo() + "\n\n"
									+ "Email : " + historyDescTo.getEmail()
									+ "\n\n" + "Payment Mode : "
									+ historyDescTo.getPaymentMode() + "\n\n"
									+ "Seat(s) Selected : "
									+ historyDescTo.getSeatNumber() + "\n\n"
									+ "Total Seat(s) : "
									+ historyDescTo.getNoofSeats() + "\n\n"
									+ "Price Per Ticket : "
									+ historyDescTo.getPricePerTicket()
									+ "\n\n" + "Ticket Total : "
									+ historyDescTo.getTotalTicket() + "\n\n"
									+ "Booking Fee : "
									+ historyDescTo.getConvenieceFee() + "\n\n"
									+ "Discount Amount: "
									+ historyDescTo.getDiscountAmount()
									+ "\n\n"
									+ "Total Price : "
									+ historyDescTo.getTotalPrice();

							startTime = historyDescTo.getShowTime();
							title = "Watching " + historyDescTo.getMovieName() + " at Cinemaxx Theater " + historyDescTo.getCinemaName();
						}
						scrollView.setVisibility(View.VISIBLE);
						savetoHistorylnr.setVisibility(View.VISIBLE);
					} else {
						/*
						 * MainActivity activity = (MainActivity) getActivity();
						 * activity.showCustomAlert("Server is not working.");
						 */
						AlertDialogForOrderSummary dialog = new AlertDialogForOrderSummary(getActivity(),
								mainJson.optString(PaymentConstant.ERROR_DESC),
								new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
						dialog.show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try {

			if (jsonString.trim().equals("loyalty")) {
				schedule.GetOrderSummaryAsync(seatTo.getRequistId(),
						orderTo.getOrderId(), "1", "lc", traId, getActivity());
			} else {
				schedule.GetOrderSummaryAsync(seatTo.getRequistId(),
						orderTo.getOrderId(), "1", "cc", traId, getActivity());
			}
			// schedule.GetHistoryAsync("xxxx", getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressLint("InlinedApi")
	public void saveCalendar() {
		if (!startTime.equals("") || startTime == null) {
			try {
				int am_pm;
				String[] nstartTime = startTime.split(", ");
				String timePM = nstartTime[0];
				String[] timePMs = timePM.split(" ");
				String[] times = timePMs[0].toString().split(":");

				String hour = times[0];
				String minute = times[1];
				String am_pms = timePMs[1];
				if (am_pms.equals("AM")) {
					am_pm = 0;
				} else {
					am_pm = 1;
				}

				String monthDate = nstartTime[1];
				String[] nmonthDate = monthDate.split(" ");
				String month = nmonthDate[0];

				monthSelect(month);
				String date = nmonthDate[1];
				String year = nstartTime[2];

				Calendar beginTime = Calendar.getInstance();

				beginTime.set(Calendar.YEAR, Integer.parseInt(year));
				beginTime.set(Calendar.MONTH, mnth);
				beginTime.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date));
				// beginTime.set(Calendar.DAY_OF_WEEK, 5);
				beginTime.set(Calendar.HOUR, Integer.parseInt(hour));
				beginTime.set(Calendar.MINUTE, Integer.parseInt(minute));
				beginTime.set(Calendar.AM_PM, am_pm);

				long starttime = beginTime.getTimeInMillis();
				long endTime = starttime + 3000 * 60 * 60;

				ContentValues event = new ContentValues();
				event.put(Events.CALENDAR_ID, 1);

				event.put(Events.TITLE, title);
				event.put(Events.DESCRIPTION, description);
				event.put(Events.EVENT_LOCATION, "");

				event.put(Events.DTSTART, starttime);
				event.put(Events.DTEND, endTime);
				event.put(Events.ALL_DAY, 0); // 0 for false, 1 for true
				event.put(Events.HAS_ALARM, 1); // 0 for false, 1 for true
				event.put(Events.STATUS, 1); // sufficient for most entries
												// tentative (0),confirmed (1)
												// or canceled (2):

				String timeZone = TimeZone.getDefault().getID();
				event.put(Events.EVENT_TIMEZONE, timeZone);

				Uri baseUri;
				if (Build.VERSION.SDK_INT >= 8) {
					baseUri = Uri
							.parse("content://com.android.calendar/events");
				} else {
					baseUri = Uri.parse("content://calendar/events");
				}

				baseUri = activity.getContentResolver().insert(baseUri, event);
				// System.out.println("event:::: " + event);

				long eventID = Long.parseLong(baseUri.getLastPathSegment());
				String reminderUriString = "content://com.android.calendar/reminders";

				ContentValues reminderValues = new ContentValues();

				reminderValues.put("event_id", eventID);
				reminderValues.put("minutes", 180); // Default value of the
													// system. Minutes is a
													// integer
				reminderValues.put("method", 1); // Alert Methods: Default(0),
													// Alert(1), Email(2),
													// SMS(3)

				Uri reminderUri = activity.getContentResolver().insert(
						Uri.parse(reminderUriString), reminderValues);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Fragment fragment = new HomeFragment();
		FragmentContainer.getInstance().setFragment(
				CurrentFragmentSingletone.getInstance().getFragment());
		if (fragment != null) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			CurrentFragmentSingletone.getInstance().setFragment(fragment);
			ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
			ft.replace(R.id.frame_container, fragment, "fragment");
			// Start the animated transition.
			ft.commit();

			// update selected item and title, then close the drawer

		}
	}

	private void monthSelect(String month) {
		// update the main content by replacing fragments
		switch (month) {
		case "Jan":
			mnth = 0;
			break;
		case "Feb":
			mnth = 1;
			break;
		case "Mar":
			mnth = 2;
			break;
		case "Apr":
			mnth = 3;
			break;
		case "May":
			mnth = 4;
			break;
		case "Jun":
			mnth = 5;
			break;
		case "Jul":
			mnth = 6;
			break;
		case "Aug":
			mnth = 7;
			break;
		case "Sep":
			mnth = 8;
			break;
		case "Oct":
			mnth = 9;
			break;
		case "Nov":
			mnth = 10;
			break;
		case "Dec":
			mnth = 11;
			break;
		case "Mei":
			mnth = 4;
			break;
		case "Juni":
			mnth = 5;
			break;
		}
	}

}
