package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.Arrays;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cinemaxx.adapter.BuyTicketAdapter;
import com.cinemaxx.adapter.GoldCinemaListAdapter;
import com.cinemaxx.adapter.GoldCinemasAdapter;
import com.cinemaxx.adapter.JuniorCinemaListAdapter;
import com.cinemaxx.adapter.JuniorCinemasAdapter;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.bean.GoldCinemaClassTo;
import com.cinemaxx.bean.GoldCinemaTo;
import com.cinemaxx.bean.GoldMovieTo;
import com.cinemaxx.bean.GoldShowTimeTo;
import com.cinemaxx.constant.CinemaxxGoldConstand;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

public class CinemaxxJunior extends Fragment implements OnWheelChangedListener{

	TextView locationTv, descriptionTxt, cityTxt;
	MainActivity activity;
	CinemaxxPref pref;
	GoldCinemaListAdapter adapter;
	ListView listViewSLocation, todaymovieDetls;
	String [] cinemaList, cityList, cityId;
	String cityCode = "";

	LinearLayout lnrwheelCity;
	BuyTicketAdapter buyTicketAdapter;
	private WheelView wvBuyTicketCity;
	boolean flagt = false, flagtt = false;
	int mvalue = 0, cvalue = 0;
	int cityIndex = 0;
	ScrollView scrollView;
	LinearLayout dateLayout, containerLayout1, containerLayout2, lnrvisible;
	Button nextBtn, prevBytn, timingBtn1, timingBtn2, timingBtn3, timingBtn4, timingBtn5, timingBtn6;
	int showDateIndex = 0;
	ArrayList<String> showingDateList, DateDtlsList, DateList, showDateList;
	ArrayList<GoldCinemaTo> cinemaNameCityList;
	ArrayList<GoldMovieTo> mainMovieList;
	String header, cinema_Name, city_Name, class_Name;
	ArrayList<CinemaTo> list;
	ListView cinemaDtlsList;
	private ScrollView parentscrollvie,childscrollvie;
	
	 
		

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.cinemaxxjunior, container, false);
		

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initView();
		activity = (MainActivity) getActivity();

		initListener();

		if(Utils.isNetworkAvailable(getActivity())){
			callCinemaService();
		}else{


			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();			
		}
	}


	private void initView() {
		// TODO Auto-generated method stub
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv = (TextView) getView().findViewById(R.id.cinemaxx_gold_location_tv);
		pref = new CinemaxxPref(getActivity());
		locationTv.setText("Location: "+pref.getCityName());
		locationTv.setTypeface(faceThin);
		lnrvisible = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_lnr_visible);
		lnrvisible.setVisibility(View.INVISIBLE);
		descriptionTxt = (TextView) getView().findViewById(R.id.cinemaxx_gold_description_txt);
		//		descriptionTxt.setTypeface(faceThin);
		String dtls = "Cinemaxx Junior is the VIP cinema oering of Cinemaxx. At Cinemaxx Gold, guests can view the seasons "
				+ "biggest hits while enjoying first-class dining - from gourmet snacks and entrees to luscious desserts - "
				+ "served by attendants on call. The seating features lavish, twin-motor leather recliners, complete with "
				+ "pillows and blankets, where guests can lay back and lift their feet at the push of a button. While waiting "
				+ "for the movie to begin, guests can also enjoy dining at the exclusive Gold lounge.";


		String description="Cinemaxx Junior is an innovative and fun new concept, combining two things that kids love movies and play.Before the movie starts, kids can enjoy the Cinemaxx Junior Play Area and have"
				+ "fun with:\n- Tube Slides \n - Flying Fox \n - Giant Trampoline \n - Ball Pit \n- Multi-story Climbing Trees \n - Cocoon Spinners \n - Wall-O-lla \n - Sliding Pole \n - Music Spinning Balls \n - Chalk board / Drawing Wall \n - Toddler Area \n "
				+"Once the movie begins, parents and children are invited to relax on various"
				+"family-friendly seat types, including Sofa Beds, Bean Bags, Loungers, and Regular"
				+"Cinema Seats. Films screened at Cinemaxx Junior are specially chosen to be"
				+"exciting and educational for the whole family.While watching the movie, enjoy"
				+"mouth-watering snacks from our special Junior Meals menu, featuring kids"
				+"favorites such as fish & chips and soft-serve ice";

		descriptionTxt.setText(description);
		listViewSLocation = (ListView) getView().findViewById(R.id.cinemaxx_gold_selected_location_list);
		cityTxt = (TextView) getView().findViewById(R.id.cinemaxx_gold_city_txt);
		lnrwheelCity = (LinearLayout) getView().findViewById(R.id.lnrwheelCinema);
		wvBuyTicketCity = (WheelView) getView().findViewById(R.id.wvBuyTicketMovie);
		scrollView = (ScrollView ) getView().findViewById(R.id.cinemaxx_gold_scrollview);
		dateLayout = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_date_layout);
		dateLayout.setVisibility(View.VISIBLE);
		containerLayout1 = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_timing_container1);
		containerLayout2 = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_timing_container2);
		nextBtn = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn_next);
		prevBytn = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn_prev);
		timingBtn1 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn1);
		timingBtn2 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn2);
		timingBtn3 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn3);
		timingBtn4 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn4);
		timingBtn5 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn5);
		timingBtn6 = (Button) getView().findViewById(R.id.cinemaxx_gold_timing_btn6);

		parentscrollvie=(ScrollView)getView().findViewById(R.id.cinemaxx_gold_scrollview);
		//childscrollvie=(ScrollView)getView().findViewById(R.id.childscrollvie);

		containerLayout1.setVisibility(View.VISIBLE);
		containerLayout2.setVisibility(View.GONE);
		timingBtn1.setText("Today");
		timingBtn2.setText("Tomorrow");
		timingBtn3.setText("Next Days");
		timingBtn1.setEnabled(false);
		timingBtn2.setEnabled(false);
		timingBtn3.setEnabled(false);
		//		detailsListView = (ListView) getView().findViewById(R.id.cinemaxx_gold_lv);
		//		txtHeader = (TextView) getView().findViewById(R.id.cinemaxx_gold_txt_header);
		//		lnrinflate = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_lnr_inflate);
		//		lnrblnk = (LinearLayout) getView().findViewById(R.id.cinemaxx_gold_lnr_blnk);
		//		lnrblnk.setVisibility(View.VISIBLE);
		cinemaDtlsList = (ListView) getView().findViewById(R.id.cinemaxx_gold_cinema_list);

		MainActivity act = (MainActivity) getActivity();
		act.clickOnCinemaxxJunior();

	}

	private void initListener() {
		// TODO Auto-generated method stub
		cityTxt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(cityList != null && cityList.length > 0){
					flagt = false;
					flagtt = false;
					lnrwheelCity.setVisibility(View.VISIBLE);
					cityTxt.setEnabled(false);
					initCinemaCityList();
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							scrollView.fullScroll(View.FOCUS_DOWN);
						}
					}, 100);
				}

			}
		});

		nextBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(showingDateList.size() > showDateIndex + 1){
					showDateIndex = showDateIndex + 1;
					timingBtn4.setText(showingDateList.get(showDateIndex));
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);

					if(showingDateList.size() > showDateIndex + 1){
						showDateIndex = showDateIndex + 1;
						timingBtn5.setText(showingDateList.get(showDateIndex));
						timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
						if(showingDateList.size() > showDateIndex + 1){
							showDateIndex = showDateIndex + 1;
							timingBtn6.setText(showingDateList.get(showDateIndex));
							timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
						}else{
							timingBtn6.setVisibility(View.GONE);
						}
					}else{
						timingBtn5.setVisibility(View.GONE);
						timingBtn6.setVisibility(View.GONE);
					}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}
			}
		});
		prevBytn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(showDateIndex > 0){

					if(showDateIndex == 4 || showDateIndex == 3 || showDateIndex == 2){
						containerLayout1.setVisibility(View.VISIBLE);
						containerLayout2.setVisibility(View.GONE);
						showDateIndex = 0;
					}else{
						if(timingBtn6.getVisibility() == View.VISIBLE){
							showDateIndex = showDateIndex - 3;
						}else if(timingBtn5.getVisibility() == View.VISIBLE){
							showDateIndex = showDateIndex - 2;
						}else{
							showDateIndex = showDateIndex - 1;
						}
						timingBtn4.setText(showingDateList.get(showDateIndex - 2));
						timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
						timingBtn4.setVisibility(View.VISIBLE);

						if(showingDateList.size() > showDateIndex - 2){
							timingBtn5.setText(showingDateList.get(showDateIndex - 1));
							timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
							timingBtn5.setVisibility(View.VISIBLE);
							if(showingDateList.size() > showDateIndex - 1){
								timingBtn6.setText(showingDateList.get(showDateIndex));
								timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
								timingBtn6.setVisibility(View.VISIBLE);
							}
						}
					}

				}
			}
		});
		timingBtn3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(showingDateList.size() > showDateIndex + 2){
					containerLayout1.setVisibility(View.GONE);
					containerLayout2.setVisibility(View.VISIBLE);

					showDateIndex = showDateIndex + 2;
					timingBtn4.setText(showingDateList.get(showDateIndex));

					if(showingDateList.size() > showDateIndex + 1){
						showDateIndex = showDateIndex + 1;
						if(showingDateList.get(showDateIndex).trim().length() > 0){
							timingBtn5.setText(showingDateList.get(showDateIndex));
							timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
						}else{
							timingBtn5.setVisibility(View.GONE);
						}

						if(showingDateList.size() > showDateIndex + 1){
							showDateIndex = showDateIndex + 1;
							if(showingDateList.get(showDateIndex).trim().length() > 0){
								timingBtn6.setText(showingDateList.get(showDateIndex));
								timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
							}else{
								timingBtn6.setVisibility(View.GONE);
							}

						}else{
							timingBtn6.setVisibility(View.GONE);
						}
					}else{
						timingBtn5.setVisibility(View.GONE);
						timingBtn6.setVisibility(View.GONE);
					}

				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}
			}
		});
		timingBtn1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String cDate = Utils.getShowDate(timingBtn1.getText().toString().trim());

				Log.d("kartick", "kartick" +timingBtn1.getText().toString().trim());
				//				System.out.println("Date is : " +cDate);
				if(DateList.contains(cDate)){
					pref.setGoldDate(showDateList.get(DateList.indexOf(cDate)));
					//					lnrblnk.setVisibility(View.GONE);
					//					lnrinflate.removeAllViews();
					timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					callTodayCinemaList(cityCode, DateDtlsList.get(DateList.indexOf(cDate)));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}

			}
		});
		timingBtn2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String cDate = Utils.getShowDate(timingBtn2.getText().toString().trim());			

				Log.d("datelist", "datelist" +DateList);
				Log.d("datelist1", "datelist1" +cDate);

				if(DateList.contains(cDate)){
					pref.setGoldDate(showDateList.get(DateList.indexOf(cDate)));
					//					lnrblnk.setVisibility(View.GONE);
					//					lnrinflate.removeAllViews();
					timingBtn2.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					callTodayCinemaList(cityCode, DateDtlsList.get(DateList.indexOf(cDate)));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}

			}
		});

		timingBtn4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String cDate = Utils.getShowDate(timingBtn4.getText()
						.toString().trim());
				//				System.out.println("Date is : " + cDate);
				if(DateList.contains(cDate)){
					pref.setGoldDate(showDateList.get(DateList.indexOf(cDate)));
					//					lnrblnk.setVisibility(View.GONE);
					//					lnrinflate.removeAllViews();
					timingBtn4.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
					callTodayCinemaList(cityCode, DateDtlsList.get(DateList.indexOf(cDate)));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}
			}
		});

		timingBtn5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String cDate = Utils.getShowDate(timingBtn5.getText()
						.toString().trim());
				//				System.out.println("Date is : " + cDate);
				if(DateList.contains(cDate)){
					pref.setGoldDate(showDateList.get(DateList.indexOf(cDate)));
					//					lnrblnk.setVisibility(View.GONE);
					//					lnrinflate.removeAllViews();
					timingBtn5.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn6.setBackgroundResource(R.drawable.city_list_item_back_gold);
					callTodayCinemaList(cityCode, DateDtlsList.get(DateList.indexOf(cDate)));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}
			}
		});

		timingBtn6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String cDate = Utils.getShowDate(timingBtn6.getText()
						.toString().trim());
				//				System.out.println("Date is : " + cDate);
				if(DateList.contains(cDate)){
					pref.setGoldDate(showDateList.get(DateList.indexOf(cDate)));
					//					lnrblnk.setVisibility(View.GONE);
					//					lnrinflate.removeAllViews();
					timingBtn6.setBackgroundResource(R.drawable.buy_tickets_btn_back);
					timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn5.setBackgroundResource(R.drawable.city_list_item_back_gold);
					timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
					callTodayCinemaList(cityCode, DateDtlsList.get(DateList.indexOf(cDate)));
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No shows available for the chosen day", new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
					dialog.show();
				}
			}
		});










		listViewSLocation.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

		listViewSLocation.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CinemaTo cinemaTo = (CinemaTo) parent.getItemAtPosition(position);
				Fragment fragment = new CinemaInfoFragment(cinemaTo);
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
			}
		});

	}

	protected void initCinemaCityList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), cityList, 0);

		wvBuyTicketCity.setViewAdapter(buyTicketAdapter);
		if(cityIndex != 0){
			wvBuyTicketCity.setCurrentItem(cityIndex);
		}else{
			wvBuyTicketCity.setCurrentItem(0);
		}

		wvBuyTicketCity.addChangingListener(this);
		wvBuyTicketCity.addClickingListener(new OnWheelClickedListener() {

			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketCity.setCurrentItem(itemIndex);
				try {
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							if(flagtt == false){
								flagtt = true;
								//									System.out.println("addClickingListener::::::::::::::::::::::::::::::;"+ cityList[wvBuyTicketCity.getCurrentItem()]);
								cityIndex = wvBuyTicketCity.getCurrentItem();
								cityTxt.setText(cityList[wvBuyTicketCity.getCurrentItem()]);
								cityCode = cityId[wvBuyTicketCity.getCurrentItem()];
								callService(cityCode);
								//									if(wvBuyTicketCity.getCurrentItem() != 0){
								//										paymentMethod = cityList[wvBuyTicketCity.getCurrentItem()];
								//									}
								//									scrollView.fullScroll(View.FOCUS_UP);
								cityTxt.setEnabled(true);
								lnrwheelCity.setVisibility(View.GONE);
								flagt = false;
								cvalue = 0;
								wvBuyTicketCity.setViewAdapter(null);
							}
						}

					}, 500);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wvBuyTicketCity.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){

							flagt = true;
							//							System.out.println("setOnTouchListener///////////////////////////////////////"+ cityList[wvBuyTicketCity.getCurrentItem()]);
							cityIndex = wvBuyTicketCity.getCurrentItem();
							cityTxt.setText(cityList[wvBuyTicketCity.getCurrentItem()]);
							cityCode = cityId[wvBuyTicketCity.getCurrentItem()];
							callService(cityCode);
							//							if(wvBuyTicketCity.getCurrentItem() != 0){
							//								paymentMethod = cityList[wvBuyTicketCity.getCurrentItem()];
							//							}
							//							scrollView.fullScroll(View.FOCUS_UP);
							cityTxt.setEnabled(true);
							lnrwheelCity.setVisibility(View.GONE);
							cvalue = 0;
							wvBuyTicketCity.setViewAdapter(null);

						}

					}
				}, 200);

				return false;
			}
		});

		wvBuyTicketCity.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);

			}
		});

	}

	private void callCinemaService() {
		// TODO Auto-generated method stub
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub

				Log.d("hello", "hello" +"hello");
				System.out.println("Cinema List:___________________" +Data.toString());
				list = new ArrayList<CinemaTo>();
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaxxGoldConstand.ERROR_DESC).equals("0")){
						JSONArray cinemaArray = mainObj.optJSONArray(CinemaxxGoldConstand.CINEMAS_ARRAY);
						cinemaList = new String[cinemaArray.length()];
						for(int i = 0; i < cinemaArray.length(); i++){

							JSONObject cinemaObj = cinemaArray.optJSONObject(i);
							CinemaTo cinemaTo = new CinemaTo();
							cinemaTo.setCinemaId(cinemaObj.optString(CinemaxxGoldConstand.CINEMA_ID));
							String [] nameCity = cinemaObj.optString(CinemaxxGoldConstand.CINEMA_NAME).split(", ");
							cinemaTo.setCinemaName(nameCity[0]);
							cinemaTo.setCityName(nameCity[1]);
							cinemaTo.setPhone(cinemaObj.optString(CinemaxxGoldConstand.PHONE));
							cinemaTo.setImage(cinemaObj.optString(CinemaxxGoldConstand.THUMBNAIL));
							cinemaList[i] = cinemaObj.optString(CinemaxxGoldConstand.CINEMA_NAME);

							JSONObject classObj = cinemaObj.optJSONObject(CinemaxxGoldConstand.CINEMA_CLASS_DATA);
							JSONArray cinemaClassArray = classObj.optJSONArray(CinemaxxGoldConstand.CINEMA_CLASS_ARRAY);

							ArrayList<CinemaClassTo> classList = new ArrayList<CinemaClassTo>();
							for(int j = 0; j < cinemaClassArray.length(); j++){

								JSONObject classValObj = cinemaClassArray.optJSONObject(j);
								CinemaClassTo classTo = new CinemaClassTo();

								classTo.setClassId(classValObj.optString(CinemaxxGoldConstand.CINEMA_CLASS_ID));
								classTo.setClassName(classValObj.optString(CinemaxxGoldConstand.CINEMA_CLASS_NAME));

								classList.add(classTo);
							}

							cinemaTo.setClassList(classList);

							list.add(cinemaTo);
							Log.d("list", "list" +list);
							System.out.println("list :  : ::: " + list);
						}
						initCinemaList();
						callCityService();

					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "Cinemas are not available", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
							}
						});
						dialog.show();

					}
				}catch(Exception e){
					e.printStackTrace();
				}

				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try{

			schedule.GetCinemaListForJuniorAsync(getActivity());

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void callCityService() {
		// TODO Auto-generated method stub
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("City List:___________________" +Data.toString());

				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaxxGoldConstand.ERROR_DESC).equals("0")){
						JSONArray cityArray = mainObj.optJSONArray(CinemaxxGoldConstand.CITY_ARRAY);
						cityList = new String[cityArray.length()];
						cityId = new String[cityArray.length()];
						for(int i = 0; i < cityArray.length(); i++){

							JSONObject cityObj = cityArray.optJSONObject(i);
							cityId[i] = cityObj.optString(CinemaxxGoldConstand.CITY_ID);
							cityList[i] = cityObj.optString(CinemaxxGoldConstand.CITY_NAME);
						}

						if(cityList.length > 0 && Arrays.asList(cityList).contains(pref.getCityName())){
							int index = Arrays.asList(cityList).indexOf(pref.getCityName());
							cityCode = cityId[index];
							cityIndex = index;
							cityTxt.setText(cityList[index]);
							callService(cityCode);
						}else{
							cityTxt.setText(cityList[0]);
							cityCode = cityId[0];
							callService(cityCode);
						}

					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "City are not available", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
							}
						});
						dialog.show();

					}
				}catch(Exception e){
					e.printStackTrace();
				}

				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try{

			schedule.GetCityListForJuniorAsync(getActivity());

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void callService(final String cityCode) {
		// TODO Auto-generated method stub
		activity.setClickable();
		//		lnrinflate.removeAllViews();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Date List:___________________" +Data.toString());

				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaxxGoldConstand.ERROR_DESC).equals("0")){
						JSONArray dateArray = mainObj.optJSONArray(CinemaxxGoldConstand.DATE_ARRAY);
						showingDateList = new ArrayList<String>();
						DateDtlsList = new ArrayList<String>();
						DateList = new ArrayList<String>();
						showDateList = new ArrayList<String>();
						for(int i = 0; i < dateArray.length(); i++){

							JSONObject dateObj = dateArray.optJSONObject(i);
							String sTime = dateObj.optString(CinemaxxGoldConstand.SHOW_DATE);
							String showDateDisp = dateObj.optString(CinemaxxGoldConstand.SHOW_DATE_DISP);
							showDateList.add(showDateDisp);
							DateDtlsList.add(sTime);
							showingDateList.add(Utils.getDateFromFormatedString(sTime));

							String returnVal = "";
							String[] valueTime = sTime.split(" ");
							String[] valueA = valueTime[0].split("/");
							DateList.add(valueA[1]);
						}
						System.out.println("DateList::::::: " + DateList.toString());
						timingBtn1.setEnabled(true);
						timingBtn2.setEnabled(true);
						timingBtn3.setEnabled(true);
						showDateIndex = 0;
						containerLayout1.setVisibility(View.VISIBLE);
						containerLayout2.setVisibility(View.GONE);
						if(showingDateList.get(0).trim().equals("Today")){
							//	            			lnrblnk.setVisibility(View.GONE);
							timingBtn1.setBackgroundResource(R.drawable.buy_tickets_btn_back);
							timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
							timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
							pref.setGoldDate(showDateList.get(0));
							callTodayCinemaList(cityCode, DateDtlsList.get(0));
						}else{
							timingBtn4.setBackgroundResource(R.drawable.city_list_item_back_gold);
							timingBtn1.setBackgroundResource(R.drawable.city_list_item_back_gold);
							timingBtn2.setBackgroundResource(R.drawable.city_list_item_back_gold);
							lnrvisible.setVisibility(View.VISIBLE);
							//	            			lnrblnk.setVisibility(View.VISIBLE);
						}
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "Server not Found", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
							}
						});
						dialog.show();

					}
				}catch(Exception e){
					e.printStackTrace();
				}

				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});
		try{

			schedule.GetScheduleShowDateForJuniorAsync(cityCode, getActivity());
			System.out.println("cityCode:::: " + cityCode);

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void callTodayCinemaList(String cityCode, String showDate) {
		// TODO Auto-generated method stub
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("show List:___________________" +Data.toString());

				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CinemaxxGoldConstand.ERROR_DESC).equals("0")){
						JSONArray todayArray = mainObj.optJSONArray(CinemaxxGoldConstand.TODAY_ARRAY);
						Log.d("check", "check" +todayArray.length());
						cinemaNameCityList = new ArrayList<GoldCinemaTo>();
						for(int i = 0; i < todayArray.length(); i++){
							JSONObject todayObj = todayArray.optJSONObject(i);
							GoldCinemaTo goldCinemaTo = new GoldCinemaTo();

							goldCinemaTo.setCinemaId(todayObj.optString(CinemaxxGoldConstand.TODAY_CINEMA_ID));
							goldCinemaTo.setCinemaName(todayObj.optString(CinemaxxGoldConstand.TODAY_CINEMA_NAME));
							goldCinemaTo.setCityName(todayObj.optString(CinemaxxGoldConstand.TODAY_CITY_NAME));

							JSONObject todayMovieListobj = todayObj.optJSONObject(CinemaxxGoldConstand.TODAY_MOVIE_LIST);
							JSONArray todayMovieListArray = todayMovieListobj.optJSONArray(CinemaxxGoldConstand.TODAY_MOVIE_LIST_ARRAY);
							ArrayList<GoldMovieTo> goldMovieList = new ArrayList<GoldMovieTo>();
							for(int j = 0; j < todayMovieListArray.length(); j++){
								Log.d("checka", "checka" +todayMovieListArray.length());

								JSONObject todayMovieListObj = todayMovieListArray.optJSONObject(j);
								GoldMovieTo goldMovieTo = new GoldMovieTo();

								goldMovieTo.setMovieId(todayMovieListObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_ID));
								goldMovieTo.setMovieName(todayMovieListObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_NAME));
								goldMovieTo.setThumbnailImage(todayMovieListObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_IMAGE));
								goldMovieTo.setIsComingSoon(todayMovieListObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_ISCOMING));

								JSONObject todayCinemaClassobj = todayMovieListObj.optJSONObject(CinemaxxGoldConstand.TODAY_CINEMA_CLASS);
								JSONArray todayCinemaClassArray = todayCinemaClassobj.optJSONArray(CinemaxxGoldConstand.TODAY_CINEMA_CLASS_ARRAY);
								ArrayList<GoldCinemaClassTo> goldCinemaClassList = new ArrayList<GoldCinemaClassTo>();
								for(int k = 0; k < todayCinemaClassArray.length(); k++){

									Log.d("checka", "checka" +todayMovieListArray.length());

									JSONObject todayCinemaClassObj = todayCinemaClassArray.optJSONObject(k);
									GoldCinemaClassTo goldCinemaClassTo = new GoldCinemaClassTo();

									goldCinemaClassTo.setCinemaClassId(todayCinemaClassObj.optString(CinemaxxGoldConstand.TODAY_CLASS_ID));
									goldCinemaClassTo.setCinemaClassName(todayCinemaClassObj.optString(CinemaxxGoldConstand.CINEMA_CLASS));

									JSONObject todayShowTimeDataobj = todayCinemaClassObj.optJSONObject(CinemaxxGoldConstand.TODAY_SHOW_TIME_DATA);
									JSONArray todayShowTimeArray = todayShowTimeDataobj.optJSONArray(CinemaxxGoldConstand.TODAY_SHOW_TIME_ARRAY);
									ArrayList<GoldShowTimeTo> goldShowTimeList = new ArrayList<GoldShowTimeTo>();
									for(int l = 0; l < todayShowTimeArray.length(); l++){
										JSONObject todayShowTimeObj = todayShowTimeArray.optJSONObject(l);
										GoldShowTimeTo goldShowTimeTo = new GoldShowTimeTo();

										goldShowTimeTo.setIsActive(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_IS_ACTIVE));
										//			                    		goldShowTimeTo.setMovieFormat(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_FORMAT));
										goldShowTimeTo.setPriceList(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_PRICE_LIST));
										goldShowTimeTo.setScheduleId(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_SCHEDULE_ID));
										goldShowTimeTo.setScreenName(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_SCREEN_NAME));

										String time = todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_SHOW_TIME);
										String format = todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_MOVIE_FORMAT);
										//String showTime = time + " (" + format + ")" ;
										String showTime = time + " (" + format + ")" + todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_PRICE_LIST);
										//			                    		goldShowTimeTo.setShowTime(todayShowTimeObj.optString(CinemaxxGoldConstand.TODAY_SHOW_TIME));
										goldShowTimeTo.setShowTime(showTime);

										goldShowTimeList.add(goldShowTimeTo);

									}
									goldCinemaClassTo.setShowTimeList(goldShowTimeList);

									goldCinemaClassList.add(goldCinemaClassTo);
									class_Name = goldCinemaClassList.get(k).getCinemaClassName();
									pref.setClassName(class_Name);
								}
								goldMovieTo.setCinemaClassList(goldCinemaClassList);

								goldMovieList.add(goldMovieTo);
							}
							goldCinemaTo.setMovieList(goldMovieList);
							mainMovieList = goldMovieList;
							cinemaNameCityList.add(goldCinemaTo);							
							header = cinemaNameCityList.get(i).getCinemaName() + ", " + cinemaNameCityList.get(i).getCityName();
							cinema_Name = cinemaNameCityList.get(i).getCinemaName();
							city_Name = cinemaNameCityList.get(i).getCityName();
							pref.setGoldUltraCity(city_Name);
							pref.setCinemaName(cinema_Name);					

						}

						initListview();

					}else{

						AlertDialog dialog = new AlertDialog(getActivity(), "Cinema details are not available", new DialogListener() {

							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
							}
						});
						dialog.show();

					}
				}catch(Exception e){
					e.printStackTrace();
				}

				activity.setNotClickable();
				lnrvisible.setVisibility(View.VISIBLE);
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}

		});
		try{

			schedule.GetTodayFullScheduleForJuniorAsync(cityCode, showDate, getActivity());
			Log.d("hello", "hello" +cityCode +showDate);
			//			System.out.println("cityCode:::: " + cityCode + " showDate:::: " + showDate);

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void initCinemaList(){
		JuniorCinemaListAdapter	adapter=new JuniorCinemaListAdapter(getActivity(), list, CinemaxxJunior.this);

		listViewSLocation.setAdapter(adapter);
		listViewSLocation.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

	}

	public void initListview(){




		Log.d("cinemanamecity", "cinemanamecity" +cinemaNameCityList.size());


		JuniorCinemasAdapter adapterg = new JuniorCinemasAdapter(getActivity(), cinemaNameCityList, CinemaxxJunior.this);

		cinemaDtlsList.setAdapter(adapterg);
		cinemaDtlsList.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub

	}
}
