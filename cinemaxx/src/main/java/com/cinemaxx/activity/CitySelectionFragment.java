package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.constant.CityConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

@SuppressLint("NewApi")
public class CitySelectionFragment extends Fragment{

	EditText cityEt;
	ListView listView;
	CinemaxxPref pref;
	ArrayList<HashMap<String, String>> cityList;
	ArrayAdapter<String> adapter;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.city_selection_fragment, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
		
		activity.setCityHeader();
		activity.exitStatus = true;
	}
	
	public void initView(){
		
		cityEt = (EditText) getView().findViewById(R.id.city_selection_city_et);
		listView = (ListView) getView().findViewById(R.id.city_selection_lv);
		pref = new CinemaxxPref(getActivity());
		
		
		cityList = new ArrayList<HashMap<String, String>>();
//		HashMap<String, String> map1 = new HashMap<String, String>();
//		map1.put("name", "Jakarta");
//		list.add(map1);
//		HashMap<String, String> map2 = new HashMap<String, String>();
//		map2.put("name", "Manado");
//		list.add(map2);
//		HashMap<String, String> map3 = new HashMap<String, String>();
//		map3.put("name", "Palembang");
//		list.add(map3);
//		HashMap<String, String> map4 = new HashMap<String, String>();
//		map4.put("name", "Ponorogo");
//		list.add(map4);
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("CITY");
//		pref.setCityName("");
		activity.cityStatus = true;
	}
	
	
	public void initListener(){
		
		cityEt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(s);
//				listView.setAdapter(adapter);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("City List:___________________" +Data.toString());
				
				cityList = new ArrayList<HashMap<String, String>>();
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(CityConstant.ERROR_DESC).equals("0")){
						JSONArray cityArray = mainObj.optJSONArray(CityConstant.CITY_ARRAY);

                    	HashMap<String, String> city1 = new HashMap<String, String>();
                    	city1.put("name", "All Cities");
                    	city1.put("id", "0");
                    	cityList.add(city1);

	                    for(int i = 0; i < cityArray.length(); i++){
	                    	
	                    	JSONObject cityObj = cityArray.optJSONObject(i);
	                    	HashMap<String, String> city = new HashMap<String, String>();

	                    	city.put("name", cityObj.optString(CityConstant.CITY_NAME));
	                    	city.put("id", cityObj.optString(CityConstant.CITY_ID));
	                    	
	                    	cityList.add(city);
	                    }
						
	                    initList();
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), "Cities are not available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
							}
						});
						dialog.show();
						
//						MainActivity activity = (MainActivity) getActivity();
//						activity.showCustomAlert("Server is not working.");
					}
                    
				}catch(Exception e){
					e.printStackTrace();
				}
				getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			
			schedule.GetCityListAsync(pref.getDeviceId(), getActivity());

		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void initList(){
		
//		adapter = new SimpleAdapter(getActivity(), cityList, R.layout.city_list_item, new String[]{"name"}, new int[]{R.id.city_list_name_tv});
		String []list = new String[cityList.size()];
		for(int i = 0; i < cityList.size(); i++){
			list[i] = cityList.get(i).get("name");
		}
		adapter = new ArrayAdapter<String>(getActivity(), R.layout.city_list_item, R.id.city_list_name_tv, list);
		listView.setAdapter(adapter);
		
		/*if(pref.getCityName().trim().length() < 1){
			activity.hideDrawer();
		}*/
		
		activity.hideDrawer();
		
        listView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				HashMap<String, String> map = cityList.get(position);
				pref.setCityName(map.get("name"));
				pref.setCityCode(map.get("id"));
				
				Fragment fragment = new HomeFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					/*FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction ft = fragmentManager.beginTransaction();
					ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
					fragmentManager.beginTransaction()
							.replace(R.id.frame_container, fragment).commit();*/
					activity.showDrawer();
					activity.backCityHeader();
					activity.cityStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
	}
	
	
}
