package com.cinemaxx.activity;

import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class TermsAndCondationFragment extends Fragment{

	Button backBtn;
	TextView conTv, lovationTv;
	MainActivity activity;
	CountDownTimer cTimer;
	CinemaxxPref pref;
	WebView webview;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		activity.hideBackBtn();
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
		setTimer(Integer.parseInt(pref.getTimerTime()));
	}
	
	public void initView(){
		
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf"); 
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		
		webview = (WebView) getView().findViewById(R.id.tnc_webview);
		pref = new CinemaxxPref(getActivity());
		lovationTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		lovationTv.setText("Location: "+pref.getCityName());
		lovationTv.setTypeface(faceThin);
		backBtn = (Button) getView().findViewById(R.id.terms_and_condition_back_btn);
//		conTv = (TextView) getView().findViewById(R.id.terms_and_con_tv);
//		conTv.setTypeface(face);
		
	}
	
	
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(cTimer != null){
			cTimer.cancel();
		}
	}

	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("TNC Res: "+Data.toString());
				if(Data.toString().trim().length() > 0 && Data.toString() != null){
//					conTv.setText(Html.fromHtml(Data.toString().trim()));
					webview.loadData(Utils.getTncWebview(Data.toString().trim()), "text/html; charset=UTF-8", null);
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetTNCAsync(getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initListener(){
		
		backBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(cTimer != null){
					cTimer.cancel();
				}
				MainActivity activity = (MainActivity) getActivity();
				activity.callBackPress();
			}
		});
		
	}
	
    public void setTimer(int tTime){
		
		cTimer = new CountDownTimer(tTime, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				int timeVal = (int) (millisUntilFinished / 1000);
				int second = timeVal % 60;
				int minute = timeVal / 60;
				
				final String secVal = String.valueOf(second);
				final String minuteVal = String.valueOf(minute);
				
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
//						timerTv.setText(minuteVal +":"+secVal);
					}
				});
				pref.setTimerTime(String.valueOf((int)millisUntilFinished));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				System.out.println("Timer finished..............");
				AlertDialog dialog = new AlertDialog(getActivity(), "Session Time Out", new DialogListener() {
					
					@Override
					public void onOkClick() {
						// TODO Auto-generated method stub
						MainActivity activity = (MainActivity) getActivity();
						activity.reCreateHome();
					}
				});
				dialog.show();
			}
		}.start();
		
	}
	
}
