package com.cinemaxx.activity;

import java.util.ArrayList;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.activity.PaymentFragment.VeritransTask;
import com.cinemaxx.adapter.BuyTicketAdapter;
import com.cinemaxx.bean.MaxxCardDetailsTo;
import com.cinemaxx.bean.OrderTo;
import com.cinemaxx.bean.SeatLayoutTo;
import com.cinemaxx.constant.MaxxCardDetailsConstant;
import com.cinemaxx.constant.MovieInfoConstant;
import com.cinemaxx.constant.UserDetailsConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.dialog.AlertForEmailDialog;
import com.cinemaxx.dialog.YesNoDialogBig;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.loyaltyservice.Loyalty;
import com.cinemaxx.loyaltyservice.WS_Enums;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomScrollView;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class UserDetailsFragment extends Fragment implements OnWheelChangedListener{

	SeatLayoutTo seatTo;
	OrderTo orderTo;
	CinemaxxPref pref;
	TextView cinemaTv, showTimeTv, cinemaNoTv, formatTv, seatSelectedTv, noOfSeatTv, tickedtTotalTv, bookingFeeTv, discountTv, totalPriceTv, timerTv, loyaltyPointsTv; 
	TextView termsAndConTv, movieTitleTv;
	TextView selectPaymentSpinner;
	TextView locationTv;
	EditText nameTv, emailTv, mobileTv;
	Button continueBtn;
	CheckBox rememberMeCb, tncCb;
	ImageView movieImageIv;
	ImageLoader imageLoader;
	RadioButton guestRadio, cardRadio;
	LinearLayout guestLayout, cardLayout, paymentMethodLayout, loyaltyPointsLayout, userDetailsDiscountLayout;
	
	String selectedSeat, format;
	String[] paymentTypeArr;
	ArrayAdapter paymentTypeAdapter;
	String paymentMethod = "SELECT PAYMENT";
	MainActivity activity;
	CountDownTimer cTimer;
	
	LinearLayout lnrwheelCity;
	BuyTicketAdapter buyTicketAdapter;
	private WheelView wvBuyTicketCity;
	boolean flagt = false, flagtt = false;
	int mvalue = 0, cvalue = 0;
	int cityIndex = 0;
	ScrollView scrollView;
	
	
	Button loginBtn, submitBtn;
	EditText emailEt, passwordEt;
	LinearLayout infoLnrLayout, radioLnrLayout;
//	ArrayList<MaxxCardDetailsTo> list;
	TextView maxxNameTv, numberTv, statusTv, pointBalanceTv, expiryDateTv, paymentBalanceTv, maxxCardForgotPasswordTv;
	int total = 0;
	CheckBox redeemPointChk, maxxCardRememberMe;
	MaxxCardDetailsTo maxxCardTo;
//	TextView signupTv;
	boolean submitButtonStatus = false;
	TextView userNameTvn;
	
	
	
	public UserDetailsFragment(SeatLayoutTo seatTo, OrderTo orderTo, String selectedSeat, String format){
		this.seatTo = seatTo;
		this.orderTo = orderTo;
		this.selectedSeat = selectedSeat;
		this.format = format;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_user_details, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		
		initView();
		activity = (MainActivity) getActivity();
		initListener();
		activity.showBackBtn();
		
		
		if(Utils.isNetworkAvailable(getActivity())){
			callServiceForMovieDetails();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
		
		activity.clickOnBuyTickets("ORDER INFORMATION");
		
		
		if(pref.getMaxxCardLoginResponse().trim().length() > 10){
//			guestLayout.setVisibility(View.GONE);
//			radioLnrLayout.setVisibility(View.GONE);
			initData();
		}
	}
	
	public void initData(){
		pref = new CinemaxxPref(getActivity());
		if(pref.getMaxxCardLoginResponse().trim().length() > 10){
			try{
				
				JSONObject mainObj = new JSONObject(pref.getMaxxCardLoginResponse().trim());
				if(mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")){
					
					maxxCardTo = new MaxxCardDetailsTo();
					maxxCardTo.setLoyaltyRedeemType(mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
					maxxCardTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
					
					JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
					maxxCardTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
					maxxCardTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
					maxxCardTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
					maxxCardTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
					maxxCardTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));
					
					maxxCardTo.setLevelId(memberObj.optString(MaxxCardDetailsConstant.LEVEL_ID));
					maxxCardTo.setLevelName(memberObj.optString(MaxxCardDetailsConstant.LEVEL_NAME));
					
					maxxCardTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
					maxxCardTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
					maxxCardTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
					maxxCardTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
					maxxCardTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
					maxxCardTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
					maxxCardTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
					maxxCardTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
					maxxCardTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
					maxxCardTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
					maxxCardTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
					maxxCardTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
					maxxCardTo.setWishToReceiveSMS(memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
					maxxCardTo.setSendNewsLetter(memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
					maxxCardTo.setMailingFrequency(memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
					maxxCardTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
					maxxCardTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
					maxxCardTo.setMembershipActivated(memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));
					
					JSONArray balanceListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
					for(int k = 0; k < balanceListArray.length(); k++){
					JSONObject balanceListObj = balanceListArray.optJSONObject(k);
					maxxCardTo.setBalanceTypeId(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
					maxxCardTo.setBalanceName(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
					maxxCardTo.setBalanceMessage(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
					maxxCardTo.setBalanceDisplay(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
					maxxCardTo.setBalancePointRemaining(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
					maxxCardTo.setBalanceIsDefault(balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
					}
					
					JSONArray pointsExpiryListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
					for(int j = 0; j < pointsExpiryListArray.length(); j++){
					JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(j);
					maxxCardTo.setPointsExpiring(pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
					maxxCardTo.setExpireOn(pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
					maxxCardTo.setBalanceTypeIdExp(pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
					}
					maxxCardTo.setIsProfileComplete(memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
					maxxCardTo.setLoyaltyRequestId(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));
					
//					userDetailsMaxxCard(maxxCardTo);
//					callServiceForMaxxCard(maxxCardTo.getFirstName(), maxxCardTo.getEmail(), maxxCardTo.getMobilePhone());
					guestLayout.setVisibility(View.GONE);
					UpdateMemberDetails();
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public void initView(){
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		
		loginBtn = (Button) getView().findViewById(R.id.user_details_maxx_card_login_btn);
		emailEt = (EditText) getView().findViewById(R.id.user_details_maxx_card_email_et);
		passwordEt = (EditText) getView().findViewById(R.id.user_details_maxx_card_password_et);
		infoLnrLayout = (LinearLayout) getView().findViewById(R.id.user_details_card_info_lay);
		radioLnrLayout = (LinearLayout) getView().findViewById(R.id.user_details_radio_layout);
		numberTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_number_tv);
		maxxNameTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_name_tv);
		statusTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_status_tv);
//		signupTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_signup_tv);
		pointBalanceTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_point_balance_tv);
		expiryDateTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_expiry_date_tv);
//		noOfPointRedeemEt = (EditText) getView().findViewById(R.id.user_details_no_of_point_redeem_et);
		paymentBalanceTv = (TextView) getView().findViewById(R.id.user_details_card_info_payment_balance_tv);
		paymentBalanceTv.setTypeface(faceMedium);
		submitBtn = (Button) getView().findViewById(R.id.user_details_card_info_submit_btn);
		redeemPointChk = (CheckBox) getView().findViewById(R.id.user_details_redeem_point_chk);
		maxxCardRememberMe = (CheckBox) getView().findViewById(R.id.user_details_maxx_card_remember_me_chk);
		maxxCardForgotPasswordTv = (TextView) getView().findViewById(R.id.user_details_maxx_card_forgot_tv);
		
		guestRadio = (RadioButton) getView().findViewById(R.id.user_details_guest_radio);
		cardRadio = (RadioButton) getView().findViewById(R.id.user_card_radio);
		guestLayout = (LinearLayout) getView().findViewById(R.id.user_details_user_info_lay_guest);
		cardLayout = (LinearLayout) getView().findViewById(R.id.user_details_user_info_lay_maxx);
		paymentMethodLayout = (LinearLayout) getView().findViewById(R.id.user_details_payment_method_lay);
		
		pref = new CinemaxxPref(getActivity());
		
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv = (TextView) getView().findViewById(R.id.movie_info_location_tv);
		locationTv.setText("Location: "+pref.getCityName());
		locationTv.setTypeface(faceThin);
		lnrwheelCity = (LinearLayout) getView().findViewById(R.id.lnrwheelCinema);
		loyaltyPointsLayout = (LinearLayout) getView().findViewById(R.id.user_details_loyalty_point_lay);
		userDetailsDiscountLayout = (LinearLayout) getView().findViewById(R.id.user_details_discount_layout);
//		lnrwheelCity.setVisibility(View.VISIBLE);
		wvBuyTicketCity = (WheelView) getView().findViewById(R.id.wvBuyTicketMovie);
		scrollView = (ScrollView ) getView().findViewById(R.id.buy_ticket_scroll);
		
		movieImageIv = (ImageView) getView().findViewById(R.id.movie_info_movie_iv);
		imageLoader = new ImageLoader(getActivity());
		
		termsAndConTv = (TextView) getView().findViewById(R.id.user_details_terms_and_con_tv);
		selectPaymentSpinner = (TextView) getView().findViewById(R.id.user_details_select_payment_spinner);
		paymentTypeArr = getResources().getStringArray(R.array.payment_type_array);
		selectPaymentSpinner.setText(paymentTypeArr[0]);
//		initMonthSpinner();
		
		nameTv = (EditText) getView().findViewById(R.id.movie_info_name_et);
		nameTv.setText(pref.getUserName());
		emailTv = (EditText) getView().findViewById(R.id.movie_info_email_et);
		emailTv.setText(pref.getUserEmaile());
		mobileTv = (EditText) getView().findViewById(R.id.movie_info_mobile_et2);
		mobileTv.setText(pref.getUserPhone());
		continueBtn = (Button) getView().findViewById(R.id.user_details_continue_btn);
		
		rememberMeCb = (CheckBox) getView().findViewById(R.id.user_details_remember_me_chk);
		tncCb = (CheckBox) getView().findViewById(R.id.user_details_terms_and_con_chk);
		if(pref.getUserEmaile().trim().length() > 0){
			rememberMeCb.setChecked(true);
		}
		
		
		timerTv = (TextView) getView().findViewById(R.id.user_details_movie_time_tv);
		
		movieTitleTv = (TextView) getView().findViewById(R.id.user_details_movie_name_tv);
		
		movieTitleTv.setText(seatTo.getMovieName());
		
		cinemaTv = (TextView) getView().findViewById(R.id.user_details_cinema_tv);
		TextView cinemaTvN = (TextView) getView().findViewById(R.id.user_details_cinema_tv_n);
		cinemaTv.setTypeface(faceMedium);
		cinemaTvN.setTypeface(faceMedium);
		
		showTimeTv = (TextView) getView().findViewById(R.id.user_details_show_time_tv);
		TextView showTimeTvN = (TextView) getView().findViewById(R.id.user_details_show_time_tv_n);
		showTimeTv.setTypeface(faceMedium);
		showTimeTvN.setTypeface(faceMedium);
		
		cinemaNoTv = (TextView) getView().findViewById(R.id.user_details_cinema_no_tv);
		TextView cinemaNoTvN = (TextView) getView().findViewById(R.id.user_details_cinema_no_tv_n);
		cinemaNoTv.setTypeface(faceMedium);
		cinemaNoTvN.setTypeface(faceMedium);
		
		seatSelectedTv = (TextView) getView().findViewById(R.id.user_details_seat_selected_tv);
		TextView seatSelectedTvN = (TextView) getView().findViewById(R.id.user_details_seat_selected_tv_n);
		seatSelectedTv.setTypeface(faceMedium);
		seatSelectedTvN.setTypeface(faceMedium);
		
		noOfSeatTv = (TextView) getView().findViewById(R.id.user_details_no_of_seat_tv);
		TextView noOfSeatTvN = (TextView) getView().findViewById(R.id.user_details_no_of_seat_tv_n);
		noOfSeatTv.setTypeface(faceMedium);
		noOfSeatTvN.setTypeface(faceMedium);
		
		formatTv = (TextView) getView().findViewById(R.id.user_details_format_tv);
		TextView formatNoTv = (TextView) getView().findViewById(R.id.user_details_format_tv_n);
		formatTv.setTypeface(faceMedium);
		formatNoTv.setTypeface(faceMedium);
		
		tickedtTotalTv = (TextView) getView().findViewById(R.id.user_details_ticket_total_tv1);
		TextView tickedtTotalTvN = (TextView) getView().findViewById(R.id.user_details_ticket_total_tv1_n);
		tickedtTotalTv.setTypeface(faceMedium);
		tickedtTotalTvN.setTypeface(faceMedium);
		
		bookingFeeTv = (TextView) getView().findViewById(R.id.user_details_booking_fee_tv2);
		TextView bookingFeeTvN = (TextView) getView().findViewById(R.id.user_details_booking_fee_tv2_n);
		bookingFeeTv.setTypeface(faceMedium);
		bookingFeeTvN.setTypeface(faceMedium);
		
		discountTv = (TextView) getView().findViewById(R.id.user_details_discount_fee_tv2);
		TextView discountTvN = (TextView) getView().findViewById(R.id.user_details_discount_fee_tv2_n);
		discountTv.setTypeface(faceMedium);
		discountTvN.setTypeface(faceMedium);
		
		totalPriceTv = (TextView) getView().findViewById(R.id.user_details_total_price_tv3);
		TextView totalPriceTvN = (TextView) getView().findViewById(R.id.user_details_total_price_tv3_n);
		totalPriceTv.setTypeface(faceMedium);
		totalPriceTvN.setTypeface(faceMedium);
		
		loyaltyPointsTv = (TextView) getView().findViewById(R.id.user_details_loyalty_point_tv1);
		TextView loyaltyPointsTvN = (TextView) getView().findViewById(R.id.user_details_loyalty_point_tv1_n);
		loyaltyPointsTv.setTypeface(faceMedium);
		loyaltyPointsTvN.setTypeface(faceMedium);
		
		userNameTvn = (TextView) getView().findViewById(R.id.user_details_maxx_card_user_name_tvn);
		
		termsAndConTv.setText(Html.fromHtml("<u>Terms & Condition</u>"));
		cinemaTv.setText(seatTo.getCinemaName());
		showTimeTv.setText(seatTo.getShowTime());
		cinemaNoTv.setText(seatTo.getCinemaNo());
		seatSelectedTv.setText(orderTo.getSelectedSeat());
		noOfSeatTv.setText(orderTo.getNoOfSeat());
		discountTv.setText("RP. "+orderTo.getDiscountAmount());
		tickedtTotalTv.setText("RP. "+orderTo.getTotalPrice());
		bookingFeeTv.setText(orderTo.getBookingFee());
		
		formatTv.setText(format.trim());
		
		try{
			total = Integer.parseInt(orderTo.getTotalPrice()) + Integer.parseInt(orderTo.getBookingFee()) - Integer.parseInt(orderTo.getDiscountAmount());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(total != 0){
			totalPriceTv.setText("RP. "+String.valueOf(total));
		}else{
			totalPriceTv.setText("RP. "+orderTo.getOrderAmount());
			total = Integer.parseInt(orderTo.getOrderAmount());
		}
		
		setTimer(Integer.parseInt(pref.getTimerTime()));
		
		if(pref.getMaxxCardEmail().trim().length() > 1){
			emailEt.setText(pref.getMaxxCardEmail().trim());
			maxxCardRememberMe.setChecked(true);
		}
//		if(pref.getMaxxCardPassword().trim().length() > 1){
//			passwordEt.setText(pref.getMaxxCardPassword().trim());
//		}
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(cTimer != null){
			cTimer.cancel();
			System.out.println("Debugggggggggggggggggggggggggggggggggggggggggg");
		}
	}
	
	public void initListener(){
		
		termsAndConTv.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment frag = new TermsAndCondationFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (frag != null) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(frag);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, frag, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		
//		selectPaymentTv.setOnClickListener(new View.OnClickListener() {
//			
//			@SuppressWarnings("static-access")
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Fragment frag = new PaymentFragment();
//				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
//				if (frag != null) {
//					FragmentTransaction ft = getFragmentManager().beginTransaction();
//					CurrentFragmentSingletone.getInstance().setFragment(frag);
//					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//					ft.replace(R.id.frame_container, frag, "fragment");
//					// Start the animated transition.
//					ft.commit();
//
//					// update selected item and title, then close the drawer
//				
//				}
//			}
//		});
		/*selectPaymentSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				if(position != 0){
					paymentMethod = (String) parent.getItemAtPosition(position);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});*/
		selectPaymentSpinner.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				flagt = false;
				flagtt = false;
				lnrwheelCity.setVisibility(View.VISIBLE);
				selectPaymentSpinner.setEnabled(false);
				initCinemaCityList();
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						scrollView.fullScroll(View.FOCUS_DOWN);
					}
				}, 100);
			}
		});
		
		continueBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainActivity activity = (MainActivity) getActivity();

//				if(selectPaymentSpinner.getText().toString().trim().equals("Points")){
//					
////					if(noOfPointRedeemEt.getText().toString().trim().length() > 0){
//						callServicePayement();
////					}else{
////						showDialog("Enter the number of points to redeem");
////					}
//					
//				}else 
					if(selectPaymentSpinner.getText().toString().trim().equals("Credit Card") && guestLayout.getVisibility() == View.VISIBLE){
					
					if (nameTv.getText().toString().length() > 0) {
						if (emailTv.getText().toString().length() > 0) {
							if(Utils.isValidMail(emailTv.getText().toString().trim())){
								if (mobileTv.getText().toString().length() > 0) {
									if (mobileTv.getText().toString().length() > 9 && mobileTv.getText().toString().length() < 15){
										if (tncCb.isChecked()) {
											if (!selectPaymentSpinner.getText().toString().trim().equals(
													"SELECT PAYMENT")) {
												
												if(rememberMeCb.isChecked()){
													saveOnPref(nameTv.getText().toString().trim(), emailTv.getText().toString().trim(), mobileTv.getText().toString().trim());
												}
												
												callService(nameTv.getText().toString().trim(), emailTv.getText().toString().trim(), mobileTv.getText().toString().trim());

												
												
											} else {
												showDialog("Select payment method");
											}
										} else {
											showDialog("Please agree Terms & Conditions");
										}
									}else{
										showDialog("Mobile Number must be 10 to 14");
									}
									
								} else {
									showDialog("Please Enter Phone Number");
								}
							}else{
								showDialogForEmail("Please Enter a Valid Email");
							}
							
						} else {
							showDialogForEmail("Please enter email");
						}
					} else {
						showDialog("Please enter name");
					}				
				}else if(selectPaymentSpinner.getText().toString().trim().equals("Credit Card") && guestLayout.getVisibility() == View.GONE){
					callServiceByLoyalty();
				}else if(!selectPaymentSpinner.getText().toString().trim().equals("Credit Card") && guestLayout.getVisibility() == View.GONE){
					showDialog("Select payment method");
				}
				

			}
		});
		
		
		mobileTv.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String text = mobileTv.getText().toString().trim();
				if(mobileTv.getText().toString().length() > 15){
					InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(mobileTv.getWindowToken(), 0);
					
					text =  text.substring(0, text.length()-1);
					mobileTv.setText(text);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		guestRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					guestLayout.setVisibility(View.VISIBLE);
					cardLayout.setVisibility(View.GONE);
					
					paymentMethodLayout.setVisibility(View.VISIBLE);
					
					radioLnrLayout.setVisibility(View.VISIBLE);
					infoLnrLayout.setVisibility(View.GONE);
				}
			}
		});
		
        cardRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
                if(isChecked){
                	guestLayout.setVisibility(View.GONE);
					cardLayout.setVisibility(View.VISIBLE);
					
					paymentMethodLayout.setVisibility(View.GONE);
					radioLnrLayout.setVisibility(View.VISIBLE);
					infoLnrLayout.setVisibility(View.GONE);
				}
			}
		});
        
        loginBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(emailEt.getText().toString().trim().length() > 0){
					if(Utils.isValidMail(emailEt.getText().toString().trim())){
						if(passwordEt.getText().toString().trim().length() > 0){
							
							if(maxxCardRememberMe.isChecked()){
								pref.setMaxxCardEmail(emailEt.getText().toString().trim());
//								pref.setMaxxCardPassword(passwordEt.getText().toString().trim());
							}
							
							callServiceForLogin();
						}else{
							showDialog("Please Enter Your Password");
						}
					}else{
						showDialog("Please Enter a Valid Email");
					}
				}else{
					showDialog("Please Enter Your Email");
				}
			}
		});
        
        submitBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (submitButtonStatus) {
					
					Fragment frag = new OrderSummaryFragment(seatTo, orderTo,
							"loyalty", maxxCardTo.getLoyaltyRequestId());
					FragmentContainer.getInstance().setFragment(
							CurrentFragmentSingletone.getInstance()
									.getFragment());
					if (frag != null) {
						if (cTimer != null) {
							cTimer.cancel();
						}
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						CurrentFragmentSingletone.getInstance().setFragment(
								frag);
						ft.setCustomAnimations(R.anim.enter_from_right,
								R.anim.exit_to_left);
						ft.replace(R.id.frame_container, frag, "fragment");
						// Start the animated transition.
						ft.commit();

						// update selected item and title, then close the drawer

					}
					
				} else {
					if (redeemPointChk.isChecked()) {
						callServicePayement();
					} else {
						AlertDialog dialog = new AlertDialog(getActivity(),
								"PLease check redeem points",
								new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub

									}
								});
						dialog.show();
					}
				}
				
				
				/*if(redeemPointChk.isChecked()){
					callServicePayement();
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"PLease check redeem points", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									
								}
							});
					dialog.show();
				}*/
				
				
				
				
				/*if(Double.parseDouble(pointBalanceTv.getText().toString().trim()) >= total){
					if(Double.parseDouble(pointBalanceTv.getText().toString().trim()) >= Double.parseDouble(noOfPointRedeemEt.getText().toString().trim())){
						checkBlance();
					}else{
						showDialog("Insufficient Point");
					}
				}else{
					showDialog("You have to enter more points...Please check");
				}*/
				
				/*if(noOfPointRedeemEt.getText().toString().trim().length() > 0){
					if(Double.parseDouble(pointBalanceTv.getText().toString().trim()) >= total){
						if(Double.parseDouble(noOfPointRedeemEt.getText().toString().trim()) == total){
							checkBlance();
						}else{
							showDialog("You have entered more points...Please check");
						}
					}else{
						showDialog("You have insufficient points to redeem");
					}
				}else{
					showDialog("Enter the number of points to redeem");
				}*/
			}
		});
        
        redeemPointChk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
//					noOfPointRedeemEt.setText(String.valueOf(total));
//					selectPaymentSpinner.setText("Points");
					paymentMethodLayout.setVisibility(View.GONE);
//					selectPaymentSpinner.setEnabled(false);
				}else{
//					noOfPointRedeemEt.setText("0");
//					selectPaymentSpinner.setText("SELECT PAYMENT");
					paymentMethodLayout.setVisibility(View.VISIBLE);
				}
			}
		});
        
//        noOfPointRedeemEt.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				if(!redeemPointChk.isChecked()){
//					redeemPointChk.setChecked(true);
//					selectPaymentSpinner.setText("Points");
//				}
//			}
//		});
        
//        signupTv.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent signIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://124.81.97.102/CinemaxxWeb/Loyalty/Signup.aspx"));
//				startActivity(signIntent);
//			}
//		});
        
        maxxCardForgotPasswordTv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Utils.isValidMail(emailEt.getText().toString().trim())){
					if(emailEt.getText().toString().trim().length() > 0){
						callForgotPassword(emailEt.getText().toString().trim());
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(),
								"PLease enter your email.", new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										
									}
								});
						dialog.show();
					}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"PLease enter a valid email.", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									
								}
							});
					dialog.show();
				}
			}
		});
		
	}
	
	/*public void checkBlance() {
		int afterSubmit = total - Integer.parseInt(noOfPointRedeemEt.getText().toString().trim());
		paymentBalanceTv.setText(String.valueOf(afterSubmit));
//		selectPaymentSpinner.setText("Points");
//		selectPaymentSpinner.setEnabled(false);
	}*/

	protected void initCinemaCityList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), paymentTypeArr, 0);
		
		wvBuyTicketCity.setViewAdapter(buyTicketAdapter);
//		if(citytv.getText().toString().contains("Select")){
//			cityIndex = 0;
//		}else{
//			wvBuyTicketCity.setCurrentItem(cityIndex);
//		}
			if(cityIndex != 0){
				wvBuyTicketCity.setCurrentItem(cityIndex);
			}else{
				wvBuyTicketCity.setCurrentItem(0);
			}
		
		wvBuyTicketCity.addChangingListener(this);
		wvBuyTicketCity.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketCity.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if(flagtt == false){
									flagtt = true;
									System.out.println("addClickingListener::::::::::::::::::::::::::::::;"+ paymentTypeArr[wvBuyTicketCity.getCurrentItem()]);
									cityIndex = wvBuyTicketCity.getCurrentItem();
									selectPaymentSpinner.setText(paymentTypeArr[wvBuyTicketCity.getCurrentItem()]);
									
									if(wvBuyTicketCity.getCurrentItem() != 0){
										paymentMethod = paymentTypeArr[wvBuyTicketCity.getCurrentItem()];
									}
									scrollView.fullScroll(View.FOCUS_UP);
									selectPaymentSpinner.setEnabled(true);
									lnrwheelCity.setVisibility(View.GONE);
									flagt = false;
									cvalue = 0;
									wvBuyTicketCity.setViewAdapter(null);
								}
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCity.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){
							
							flagt = true;
							System.out.println("setOnTouchListener///////////////////////////////////////"+ paymentTypeArr[wvBuyTicketCity.getCurrentItem()]);
							cityIndex = wvBuyTicketCity.getCurrentItem();
							selectPaymentSpinner.setText(paymentTypeArr[wvBuyTicketCity.getCurrentItem()]);
//						
							if(wvBuyTicketCity.getCurrentItem() != 0){
								paymentMethod = paymentTypeArr[wvBuyTicketCity.getCurrentItem()];
							}
							scrollView.fullScroll(View.FOCUS_UP);
							selectPaymentSpinner.setEnabled(true);
							lnrwheelCity.setVisibility(View.GONE);
							cvalue = 0;
							wvBuyTicketCity.setViewAdapter(null);
							
						}
						
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCity.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);
				
			}
		});
		
	}
	
	
	
	
	public void saveOnPref(String name, String email, String phone){
		pref.setUserName(name);
		pref.setUserEmail(email);
		pref.setUserPhone(phone);
	}
	
	public void showDialog(String message){
		AlertDialog dialog = new AlertDialog(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
				
			}
		});
		dialog.show();
	}
	
	public void showDialogForEmail(String message){
		AlertForEmailDialog dialog = new AlertForEmailDialog(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
				
			}
		});
		dialog.show();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
//	protected void initMonthSpinner() {
//		// TODO Auto-generated method stub
//		paymentTypeAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTypeArr);
//		paymentTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		selectPaymentSpinner.setAdapter(paymentTypeAdapter);
//	}
	
	
	String secVal = "";
	String minuteVal = "";
    public void setTimer(int tTime){
		
		cTimer = new CountDownTimer(tTime, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				int timeVal = (int) (millisUntilFinished / 1000);
				int second = timeVal % 60;
				int minute = timeVal / 60;
				
				if(second < 10){
					secVal = "0"+String.valueOf(second);
				}else{
					secVal = String.valueOf(second);
				}
				if(minute < 10){
					minuteVal = "0"+String.valueOf(minute);
				}else{
					minuteVal = String.valueOf(minute);
				}
				
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						timerTv.setText(minuteVal +":"+secVal);
					}
				});
				pref.setTimerTime(String.valueOf((int)millisUntilFinished));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				System.out.println("Timer finished..............");
				if(lnrwheelCity.getVisibility() == View.VISIBLE){
					lnrwheelCity.setVisibility(View.GONE);
				}
				AlertDialog dialog = new AlertDialog(getActivity(), "Session Time Out", new DialogListener() {
					
					@Override
					public void onOkClick() {
						// TODO Auto-generated method stub
						MainActivity activity = (MainActivity) getActivity();
						activity.reCreateHome();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
		}.start();
		
	}
    
    
    public void callServiceForMaxxCard(String name, String email, String mobileNo){
    	
    	Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out
						.println("After user update:_______________________________:"
								+ Data.toString());
				
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString("ErrDesc").trim().equals("0")){
						orderTo.setOrderId(mainObj.optString("OrderId"));
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(), mainObj.optString("ErrDesc").trim(), new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				if (Data.toString().trim().length() > 0) {
					/*Fragment frag = new PaymentFragment(seatTo, orderTo,
							selectedSeat, nameTv.getText().toString(), emailTv
									.getText().toString(), mobileTv.getText()
									.toString());
					FragmentContainer.getInstance().setFragment(
							CurrentFragmentSingletone.getInstance()
									.getFragment());
					if (frag != null) {
						if(cTimer != null){
							cTimer.cancel();
						}
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						CurrentFragmentSingletone.getInstance().setFragment(
								frag);
						ft.setCustomAnimations(R.anim.enter_from_right,
								R.anim.exit_to_left);
						ft.replace(R.id.frame_container, frag, "fragment");
						// Start the animated transition.
						ft.commit();

						// update selected item and title, then
						// close the drawer

					}*/
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No Data Found", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							activity.callBackPress();
						}
					});
					dialog.show();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
    	try{
    		System.out.println("UpdateCustomerInfoAsync : "+pref.getDeviceId()+":"+seatTo.getRequistId()+":"+name+":"+email+":"+mobileNo+":"+"True"+":"+0l+":"+false+":"+"0");
    		schedule.UpdateCustomerInfoAsync(pref.getDeviceId(), seatTo.getRequistId(), name, email, mobileNo, "True", Long.parseLong(maxxCardTo.getLoyaltyRequestId()), false, maxxCardTo.getMemberId(), activity);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
    public void callService(String name, String email, String mobileNo){
    	
    	Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out
						.println("After user update:_______________________________:"
								+ Data.toString());
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					orderTo.setOrderId(mainObj.optString("OrderId"));
				}catch(Exception e){
					e.printStackTrace();
				}
//				orderTo.setOrderId(Data.toString().trim());
				if (Data.toString().trim().length() > 0) {
					Fragment frag = new PaymentFragment(seatTo, orderTo,
							selectedSeat, nameTv.getText().toString(), emailTv
									.getText().toString(), mobileTv.getText()
									.toString());
					FragmentContainer.getInstance().setFragment(
							CurrentFragmentSingletone.getInstance()
									.getFragment());
					if (frag != null) {
						if(cTimer != null){
							cTimer.cancel();
						}
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						CurrentFragmentSingletone.getInstance().setFragment(
								frag);
						ft.setCustomAnimations(R.anim.enter_from_right,
								R.anim.exit_to_left);
						ft.replace(R.id.frame_container, frag, "fragment");
						// Start the animated transition.
						ft.commit();

						// update selected item and title, then
						// close the drawer

					}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No Data Found", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							activity.callBackPress();
						}
					});
					dialog.show();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
    	try{
    		schedule.UpdateCustomerInfoAsync(pref.getDeviceId(), seatTo.getRequistId(), name, email, mobileNo, "True", 0l, false, "0", activity);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
    public void callServiceByLoyalty(){
    	
    	Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out
						.println("After user update:_______________________________:"
								+ Data.toString());
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
						orderTo.setOrderId(mainObj.optString("OrderId"));
				}catch(Exception e){
					e.printStackTrace();
				}
				if (Data.toString().trim().length() > 0) {
					Fragment frag = new PaymentFragment(seatTo, orderTo,
							selectedSeat, nameTv.getText().toString(), emailTv
									.getText().toString(), mobileTv.getText()
									.toString());
					FragmentContainer.getInstance().setFragment(
							CurrentFragmentSingletone.getInstance()
									.getFragment());
					if (frag != null) {
						if(cTimer != null){
							cTimer.cancel();
						}
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						CurrentFragmentSingletone.getInstance().setFragment(
								frag);
						ft.setCustomAnimations(R.anim.enter_from_right,
								R.anim.exit_to_left);
						ft.replace(R.id.frame_container, frag, "fragment");
						// Start the animated transition.
						ft.commit();

						// update selected item and title, then
						// close the drawer

					}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(), "No Data Found", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							activity.callBackPress();
						}
					});
					dialog.show();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
    	try{
//    		schedule.UpdateCustomerInfoAsync(pref.getDeviceId(), seatTo.getRequistId(), "", "", "", "True", Long.parseLong(maxxCardTo.getLoyaltyRequestId()), true, maxxCardTo.getMemberId(), activity);
    		schedule.UpdateCustomerInfoAsync(pref.getDeviceId(), seatTo.getRequistId(), maxxCardTo.getFirstName(), maxxCardTo.getEmail(), maxxCardTo.getMobilePhone(), "True", Long.parseLong(maxxCardTo.getLoyaltyRequestId()), true, maxxCardTo.getMemberId(), activity);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
    public void callServiceForMovieDetails(){
    	
    	Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Movie Details::::::::::::::::::::::"+Data.toString());
				try{
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(MovieInfoConstant.ERROR_DESC).equals("0")){
						JSONArray movieArray = mainObj.optJSONArray(MovieInfoConstant.MOVIE_ARRAY);
						for(int i = 0; i < movieArray.length(); i++){
							
							JSONObject movieObj = movieArray.optJSONObject(i);
							
							imageLoader.DisplayImage(movieObj.optString(MovieInfoConstant.MOVIE_IMAGE).replace(" ", "%20").trim(), movieImageIv);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
    	try{
    		schedule.GetMovieDetailsAsync(pref.getDeviceId(), seatTo.getMovieId(), "0", getActivity());
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    
    public void callServiceForLogin(){
    	
    	Loyalty loyalty = new Loyalty(new com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				System.out.println("callServiceForLogin:::::::" + Data.toString());
				try {
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")){
						pref.setMaxxCardLoginResponse(Data.toString().trim());
						maxxCardTo = new MaxxCardDetailsTo();
						maxxCardTo.setLoyaltyRedeemType(mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
						maxxCardTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
						
						JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
						maxxCardTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
						maxxCardTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
						maxxCardTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
						maxxCardTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
						maxxCardTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));
						maxxCardTo.setLevelId(memberObj.optString(MaxxCardDetailsConstant.LEVEL_ID));
						maxxCardTo.setLevelName(memberObj.optString(MaxxCardDetailsConstant.LEVEL_NAME));
						maxxCardTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
						maxxCardTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
						maxxCardTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
						maxxCardTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
						maxxCardTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
						maxxCardTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
						maxxCardTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
						maxxCardTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
						maxxCardTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
						maxxCardTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
						maxxCardTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
						maxxCardTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
						maxxCardTo.setWishToReceiveSMS(memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
						maxxCardTo.setSendNewsLetter(memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
						maxxCardTo.setMailingFrequency(memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
						maxxCardTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
						maxxCardTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
						maxxCardTo.setMembershipActivated(memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));
						
						JSONArray balanceListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
						for(int k = 0; k < balanceListArray.length(); k++){
						JSONObject balanceListObj = balanceListArray.optJSONObject(k);
						maxxCardTo.setBalanceTypeId(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
						maxxCardTo.setBalanceName(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
						maxxCardTo.setBalanceMessage(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
						maxxCardTo.setBalanceDisplay(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
						maxxCardTo.setBalancePointRemaining(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
						maxxCardTo.setBalanceIsDefault(balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
						}
						
						JSONArray pointsExpiryListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
						for(int j = 0; j < pointsExpiryListArray.length(); j++){
						JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(j);
						maxxCardTo.setPointsExpiring(pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
						maxxCardTo.setExpireOn(pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
						maxxCardTo.setBalanceTypeIdExp(pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
						}
						maxxCardTo.setIsProfileComplete(memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
						maxxCardTo.setLoyaltyRequestId(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));
						
//						list.add(maxxCardTo);
						userDetailsMaxxCard(maxxCardTo);
						
						callServiceForMaxxCard(maxxCardTo.getFirstName(), maxxCardTo.getEmail(), maxxCardTo.getMobilePhone());
						activity.refreshAdapter();
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				
			}
		});
    	
    	try {
    		System.out.println("Login Response: "+Long.parseLong(seatTo.getRequistId())+" : "+true+" : "+emailEt.getText().toString().trim()+" : "+passwordEt.getText().toString().trim());
//			loyalty.ClubLoginAsync("2", emailEt.getText().toString().trim(), passwordEt.getText().toString().trim(), getActivity());
    		loyalty.LoginWithInitAsync(Long.parseLong(seatTo.getRequistId()), true, emailEt.getText().toString().trim(), passwordEt.getText().toString().trim(), getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	public void userDetailsMaxxCard(MaxxCardDetailsTo maxxCardTo) {
		
		userNameTvn.setText(maxxCardTo.getLevelName().trim());
		if(maxxCardTo.getCardNumber().trim().length() > 0){
			maxxNameTv.setText("Hi "+maxxCardTo.getFirstName().toUpperCase()+"!");
		}
		numberTv.setText(maxxCardTo.getCardNumber());
		if(maxxCardTo.getMembershipActivated().trim().equals("true")){
			statusTv.setText("ACTIVE");
		}else{
			statusTv.setText("NOT ACTIVE");
		}
		if(maxxCardTo.getBalancePointRemaining() != null){
			pointBalanceTv.setText(maxxCardTo.getBalancePointRemaining());
		}else{
			pointBalanceTv.setText("0");
		}
		
//		pointBalanceTv.setText("180000");
//		expiryDateTv.setText(Utils.getMaxxCardExpiryDate(maxxCardTo.getExpiryDate()));
		try{
			if(Utils.CompareWithcurrentMonth(maxxCardTo.getExpireOn().trim()).equals("1")){
				expiryDateTv.setText(maxxCardTo.getPointsExpiring().trim());
			}else{
				expiryDateTv.setText("0");
			}
			
//			expiryDateTv.setText(maxxCardTo.getPointsExpiring().trim() + " Points expiring on " + Utils
//					.getMaxxCardExpiryDate(maxxCardTo.getExpireOn().trim()));
		}catch(Exception e){
			expiryDateTv.setText("0");
		}
		
		if(total != 0){
			paymentBalanceTv.setText("RP. "+String.valueOf(total));
		}else{
			paymentBalanceTv.setText("RP. "+orderTo.getOrderAmount());
		}
		
		cardLayout.setVisibility(View.GONE);
		radioLnrLayout.setVisibility(View.GONE);
		infoLnrLayout.setVisibility(View.VISIBLE);
		paymentMethodLayout.setVisibility(View.VISIBLE);
	}
	
	
	
	public void callServicePayement(){
		
		Loyalty loyalty = new Loyalty(new com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("callServicePayementRespnse: " +Data.toString());
				String tra_id;
				String errorDesc = "";
				String lOrderId = "";
				String loyaltyPoints = "", redeemAmount = "", totalPrice = "", totalCharge = "", orderAmount = "";
				
				try{
					JSONObject mainJson = new JSONObject(Data.toString());
					
					/*if(mainJson.optString(UserDetailsConstant.STATUS_CODE).equals("200") || mainJson.optString(UserDetailsConstant.STATUS_CODE).equals("201")){
						
						
						
					}else {
                        AlertDialog dialog = new AlertDialog(getActivity(), "No Data Found", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}*/
					
					
					tra_id = maxxCardTo.getLoyaltyRequestId();
					errorDesc = mainJson.optString("ErrDesc");
					lOrderId = mainJson.optString("OrderId");
					loyaltyPoints = mainJson.optString("RedeemPoints");
					redeemAmount = mainJson.optString("RedeemAmt");
					totalPrice = mainJson.optString("TotalPrice");
					totalCharge = mainJson.optString("TotalCharges");
					orderAmount = mainJson.optString("OrderAmt");
					
					if(errorDesc.trim().equals("0")){
						
						redeemPointChk.setVisibility(View.GONE);
						submitButtonStatus = true;
						submitBtn.setText("Continue");
						
						loyaltyPointsLayout.setVisibility(View.VISIBLE);
						loyaltyPointsTv.setText(loyaltyPoints);
						tickedtTotalTv.setText("RP. "+totalPrice);
						bookingFeeTv.setText("RP. "+totalCharge);
						totalPriceTv.setText("RP. "+orderAmount);
						paymentBalanceTv.setText("RP. "+orderAmount);
						userDetailsDiscountLayout.setVisibility(View.GONE);
						
						orderTo.setOrderId(lOrderId);
						/*Fragment frag = new OrderSummaryFragment(seatTo, orderTo, "loyalty", tra_id);
						FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
						if (frag != null) {
							if(cTimer != null){
								cTimer.cancel();
							}
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							CurrentFragmentSingletone.getInstance().setFragment(frag);
							ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
							ft.replace(R.id.frame_container, frag, "fragment");
							// Start the animated transition.
							ft.commit();

							// update selected item and title, then close the drawer
						
						}*/
					}else{
//						showMessage("Transaction Unsuccessfull");
						YesNoDialogBig dialog = new YesNoDialogBig(getActivity(), errorDesc, new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
//								if(cTimer != null){
//									cTimer.cancel();
//								}
//								activity.reCreateHome();
							}
						});
						dialog.show();
					}

					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{

			System.out.println("SetOrderLoyaltyPointsAsync :::: "+ Long.valueOf(maxxCardTo.getLoyaltyRequestId())+":"+ true+":"+ maxxCardTo.getMemberId()+":"+ Long.valueOf(orderTo.getOrderId())+":"+  true+":"+  "0" +":"+  true+":"+  Double.parseDouble(maxxCardTo.getBalancePointRemaining())+":"+  true);
//			loyalty.SetOrderLoyaltyPointsAsync(10649, true, "MRPTPVDGWW08", 30236, true, 0, true, 2018.0, true, getActivity());

			loyalty.SetOrderLoyaltyPointsAsync(Long.valueOf(maxxCardTo.getLoyaltyRequestId()), true, maxxCardTo.getMemberId(), Long.valueOf(orderTo.getOrderId()), true, 0, true, Double.parseDouble(maxxCardTo.getBalancePointRemaining()), true, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
    public void callForgotPassword(String email){
		
        if(Utils.isNetworkAvailable(getActivity())){
        	activity.setClickable();
    		Loyalty loyalty = new Loyalty(new  com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {
    			
    			@Override
    			public void Wsdl2CodeStartedRequest() {
    				// TODO Auto-generated method stub
    				
    			}
    			
    			@Override
    			public void Wsdl2CodeFinishedWithException(Exception ex) {
    				// TODO Auto-generated method stub
    				
    			}
    			
    			@Override
    			public void Wsdl2CodeFinished(String methodName, Object Data) {
    				// TODO Auto-generated method stub
    				System.out.println("Maxx forgot password response: "+Data.toString());
    				
    				if(Data.toString().trim().length() > 1){
    					try{
    						
    						if(Data.toString().trim().equals("0")){
    							showDialog("We just sent to your email \nfor the link you can use to \nreset your password.Thank you.");
    						}
    						
    					}catch(Exception e){
    						e.printStackTrace();
    					}
    				}
    	
    				
    				
    				activity.setNotClickable();
    			}
    			
    			@Override
    			public void Wsdl2CodeEndedRequest() {
    				// TODO Auto-generated method stub
    				
    			}
    		});
    		
            try{
    			
            	loyalty.ForgotPasswordAsync(email, getActivity());
//            	loyalty.SetOrderLoyaltyPointsAsync(42, true, memberId, orderId, orderIdSpecified, loyaltyRedeemType, loyaltyRedeemTypeSpecified, redeemPoints, redeemPointsSpecified, headers);
    			
    		}catch(Exception e){
    			e.printStackTrace();
    		}
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							
						}
					});
			dialog.show();
		}
        
		
	}
	
    public void showMessage(String message){
		
		YesNoDialogBig dialog = new YesNoDialogBig(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
				if(cTimer != null){
					cTimer.cancel();
				}
				activity.callBackPress();
			}
		});
		dialog.show();
		
	}
	
	
	

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		
	}
	
	public void UpdateMemberDetails() {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new com.cinemaxx.loyaltyservice.IWsdl2CodeEvents() {
				
				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("UpdateMemberDetails: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {
							
							JSONObject mainObj = new JSONObject(pref.getMaxxCardLoginResponse().trim());
							if(mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")){
								
								maxxCardTo = new MaxxCardDetailsTo();
								maxxCardTo.setLoyaltyRedeemType(mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
								maxxCardTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
								
								JSONObject memberObj = mainObj.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
								maxxCardTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
								maxxCardTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
								maxxCardTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
								maxxCardTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
								maxxCardTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));
								
								maxxCardTo.setLevelId(memberObj.optString(MaxxCardDetailsConstant.LEVEL_ID));
								maxxCardTo.setLevelName(memberObj.optString(MaxxCardDetailsConstant.LEVEL_NAME));
								
								maxxCardTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
								maxxCardTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
								maxxCardTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
								maxxCardTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
								maxxCardTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
								maxxCardTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
								maxxCardTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
								maxxCardTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
								maxxCardTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
								maxxCardTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
								maxxCardTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
								maxxCardTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
								maxxCardTo.setWishToReceiveSMS(memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
								maxxCardTo.setSendNewsLetter(memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
								maxxCardTo.setMailingFrequency(memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
								maxxCardTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
								maxxCardTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
								maxxCardTo.setMembershipActivated(memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));
								
								JSONArray balanceListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
								for(int k = 0; k < balanceListArray.length(); k++){
								JSONObject balanceListObj = balanceListArray.optJSONObject(k);
								maxxCardTo.setBalanceTypeId(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
								maxxCardTo.setBalanceName(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
								maxxCardTo.setBalanceMessage(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
								maxxCardTo.setBalanceDisplay(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
								maxxCardTo.setBalancePointRemaining(balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
								maxxCardTo.setBalanceIsDefault(balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
								}
								
								JSONArray pointsExpiryListArray = memberObj.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
								for(int j = 0; j < pointsExpiryListArray.length(); j++){
								JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(0);
								maxxCardTo.setPointsExpiring(pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
								maxxCardTo.setExpireOn(pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
								maxxCardTo.setBalanceTypeIdExp(pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
								}
								maxxCardTo.setIsProfileComplete(memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
								maxxCardTo.setLoyaltyRequestId(memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));
								
								userDetailsMaxxCard(maxxCardTo);
								callServiceForMaxxCard(maxxCardTo.getFirstName(), maxxCardTo.getEmail(), maxxCardTo.getMobilePhone());
//								guestLayout.setVisibility(View.GONE);

							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					activity.setNotClickable();
				}
				
				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub
					
				}
			});

			try {

				loyalty.GetMemberDetailsAsync(Long.parseLong(maxxCardTo.getLoyaltyRequestId()), true, maxxCardTo.getMemberId(), activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {

				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub

				}
			});
			dialog.show();
		}

	}

}
