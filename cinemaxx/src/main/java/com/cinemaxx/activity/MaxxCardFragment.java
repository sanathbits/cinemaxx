package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.MaxxCardDescAdapter;
import com.cinemaxx.adapter.MaxxCardSpecialOfferAdapter;
import com.cinemaxx.adapter.MaxxCardStarClassSpecialOfferAdapter;
import com.cinemaxx.bean.MaxxCardDescTo;
import com.cinemaxx.bean.MaxxCardDetailsTo;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;
import com.cinemaxx.constant.MaxxCardDetailsConstant;
import com.cinemaxx.constant.MaxxCardManuConstant;
import com.cinemaxx.constant.NewsDescriptionConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.loyaltyservice.IWsdl2CodeEvents;
import com.cinemaxx.loyaltyservice.Loyalty;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.SharedPreference;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.Schedule;
import com.cinemaxx.bean.MaxxCardManuDetailsTo;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MaxxCardFragment extends Fragment {

	TextView locationTv, forgotPasswordTv, signupTv;
	MainActivity activity;
	Button loginBtn, btnJoinNow;
	EditText emailEt, passwordEt;
	CheckBox rememberMeChk;

	CinemaxxPref pref;
	SharedPreference shpref;
	RelativeLayout relMenu;
	LinearLayout lnrMenuItem, lnrPrivilegesAndBenefits, lnrStarClass, lnrStarClassPrivileges, lnrFaq,
			lnrTermAndCondition, lnrClickAboutClass;
	int chkMenu = 0, chkAboutList = 1, chkPrivateAndBenefitsList = 1, chklistStarClassList = 1;
	TextView starClass, privateAndBenefits, starClassPrivileges, faq, termsAndConditions;
	ListView listDesc, listPrivateAndBenefits, listStarClass;
	private ValueAnimator mAnimator;
	ImageView imgActiveNow, imgJoinNow;
	WebView webViewFaq, webViewTnd, webViewContentForAbout, webViewContentClickForAbout,
			webViewContentForPrivilegeandBenefits, webViewContentForStarClassPrivileges;
	ArrayList<MaxxCardManuDetailsTo> newsList, specialOfferList, starClassSpecialOfferList;
	CustomProgressDialog pDialog = null;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// View rootView = inflater.inflate(R.layout.fragment_max_card_login,
		// container, false);
		View rootView = inflater.inflate(R.layout.fragment_maxx_card_login_new_design, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initView();
		initLIstener();
		shpref = new SharedPreference();

		callMenuServiceForFaq();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		hideSoftKeyboard();
		activity.chkMaxxCardLogin = false;
	}

	public void initView() {
		activity = (MainActivity) getActivity();
		activity.chkMaxxCardLogin = true;
		activity.showBackBtn();
		activity.exitStatus = false;
		locationTv = (TextView) getView().findViewById(R.id.maxx_card_location_tv);
		pref = new CinemaxxPref(getActivity());
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv.setTypeface(faceThin);
		locationTv.setText("Location: " + pref.getCityName());

		loginBtn = (Button) getView().findViewById(R.id.maxx_card_login_btn);
		emailEt = (EditText) getView().findViewById(R.id.maxx_card_email_et);
		passwordEt = (EditText) getView().findViewById(R.id.maxx_card_password_et);
		rememberMeChk = (CheckBox) getView().findViewById(R.id.maxx_card_remember_me_chk);
		forgotPasswordTv = (TextView) getView().findViewById(R.id.maxx_card_forgot_tv);
		signupTv = (TextView) getView().findViewById(R.id.maxx_card_login_signup_tv);

		if (pref.getMaxxCardEmail().trim().length() > 1) {
			emailEt.setText(pref.getMaxxCardEmail().trim());
			rememberMeChk.setChecked(true);
		}
		// if (pref.getMaxxCardPassword().trim().length() > 1) {
		// passwordEt.setText(pref.getMaxxCardPassword().trim());
		// }

		webViewFaq = (WebView) getView().findViewById(R.id.webViewFaq);
		webViewTnd = (WebView) getView().findViewById(R.id.webViewTnd);
		webViewContentForAbout = (WebView) getView().findViewById(R.id.webViewContentForAbout);
		webViewContentClickForAbout = (WebView) getView().findViewById(R.id.webViewContentClickForAbout);
		webViewContentForPrivilegeandBenefits = (WebView) getView()
				.findViewById(R.id.webViewContentForPrivilegeandBenefits);
		webViewContentForStarClassPrivileges = (WebView) getView()
				.findViewById(R.id.webViewContentForStarClassPrivileges);

		relMenu = (RelativeLayout) getView().findViewById(R.id.rel_maxx_card_menu);
		lnrMenuItem = (LinearLayout) getView().findViewById(R.id.lnrMenuItem);
		starClass = (TextView) getView().findViewById(R.id.maxx_card_star_class);
		privateAndBenefits = (TextView) getView().findViewById(R.id.maxx_card_private_and_benefits);
		starClassPrivileges = (TextView) getView().findViewById(R.id.maxx_card_star_class_privileges);
		faq = (TextView) getView().findViewById(R.id.maxx_card_faq);
		termsAndConditions = (TextView) getView().findViewById(R.id.maxx_card_terms_and_conditions);
		lnrPrivilegesAndBenefits = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_privileges_and_benefits);
		lnrStarClass = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_star_class);
		lnrStarClassPrivileges = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_star_class_privileges);
		lnrFaq = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_faq);
		lnrTermAndCondition = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_term_and_condition);
		lnrClickAboutClass = (LinearLayout) getView().findViewById(R.id.lnr_maxx_card_click_on_about);
		imgActiveNow = (ImageView) getView().findViewById(R.id.maxx_card_active_now);
		imgJoinNow = (ImageView) getView().findViewById(R.id.maxx_card_join_now);
		btnJoinNow = (Button) getView().findViewById(R.id.btnJoinNow);

		starClass.setVisibility(View.VISIBLE);
		// aboutMaxxCard();

		MainActivity act = (MainActivity) getActivity();
		act.clickOnMaxxCard();
	}

	/** ABOUT MAXX CARD & MAXX CARD STAR CLASS */
	/*
	 * public void aboutMaxxCard() { TextView txtContent = (TextView) getView()
	 * .findViewById(R.id.txtContent); String text_view_str =
	 * "<b>Bahasa Indonesia: </b> MAXX CARD & MAXX CARD STAR CLASS adalah bentuk apresiasi manajemen Cinemaxx kepada para pengunjung setia yang bertransaksi di Cinemaxx. Sebagai member, Anda bisa memperoleh MAXX POINT dan menukarkan MAXX POINT dengan berbagai hadiah atau penawaran menarik dari Cinemaxx. Untuk memberikan nilai tambah dari setiap transaksi Anda, Cinemaxx juga menghadirkan berbagai keistimewaan eksklusif dari merchant dan business partner yang berpartisipasi untuk Anda nikmati melalui MAXX CARD STAR CLASS.<br><br>Tingkatkan terus transaksi Anda dan kumpulkan MAXX POINT untuk mendapatkan berbagai keuntungan eksklusif hanya di Cinemaxx.<br><br><br><b>English: </b> MAXX CARD & MAXX CARD STAR CLASS are created as a token of appreciation from Cinemaxx Management to all loyal Cinemaxx customers. As a member, you get to collect MAXX POINTS and can redeem your MAXX POINTS with various gifts or exciting oers from Cinemaxx. To add value to your transactions, Cinemaxx also presents a variety of exclusive privileges from participating merchants and business partners you can enjoy with MAXX CARD STAR CLASS.<br><br>Do more transactions and collect more MAXX POINTS to get access to a variety of exclusive benefits only at Cinemaxx."
	 * ; txtContent.setText(Html.fromHtml(text_view_str));
	 * 
	 * list = new ArrayList<MaxxCardDescTo>(); List<Integer> imageList = new
	 * ArrayList<Integer>(); List<String> headerList = new ArrayList<String>();
	 * List<String> descList = new ArrayList<String>();
	 * imageList.addAll(Arrays.asList(R.drawable.demo4, R.drawable.demo1,
	 * R.drawable.demo2, R.drawable.demo3)); headerList.addAll(Arrays.asList(
	 * "OPTIK TUNGGAL: DISC. 10%*", "GRAND IMPERIAL LAMIAN: FREE 2 DIMSUM*",
	 * "OPTIK TUNGGAL: DISC. 10%*", "GRAND IMPERIAL LAMIAN: FREE 2 DIMSUM*"));
	 * descList.addAll(Arrays .asList(
	 * "(From Frame & Sun Glasses)\n*Khusus pembelian frame komplit (frame + lensa) kecuali price controlled item (cartier, lensa, accesories) - All Outlets"
	 * ,
	 * "*Minimum spending Rp 250.000 (sebelum tax) All Outlets (Jabodetabek, Medan, Solo, Surabaya)"
	 * ,
	 * "(From Frame & Sun Glasses)\n*Khusus pembelian frame komplit (frame + lensa) kecuali price controlled item (cartier, lensa, accesories) - All Outlets"
	 * ,
	 * "*Minimum spending Rp 250.000 (sebelum tax) All Outlets (Jabodetabek, Medan, Solo, Surabaya)"
	 * ));
	 * 
	 * for (int i = 0; i < 4; i++) { MaxxCardDescTo maxxCardDescTo = new
	 * MaxxCardDescTo(); maxxCardDescTo.setImageLogo(imageList.get(i));
	 * maxxCardDescTo.setHeader(headerList.get(i));
	 * maxxCardDescTo.setDesc(descList.get(i)); list.add(maxxCardDescTo); }
	 * callListAdapter(); }
	 */

	public void callNewsListAdapter() {
		listDesc = (ListView) getView().findViewById(R.id.listDesc);
		getView().findViewById(R.id.rel_maxx_card_about).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkAboutList == 0) {
					listDesc.setVisibility(View.VISIBLE);
					chkAboutList = 1;
				} else {
					listDesc.setVisibility(View.GONE);
					chkAboutList = 0;
				}
			}
		});
		/***
		 * DIBYENDU BHATTACHARYYA TEST CODE CHANGED ON 28-DEC-2016 FOR HANDLING
		 * NULL POINTER EXCEPTION WHILE GETTING BLANK VALUE FROM NEWS API
		 * SERVICE AND GETTING "newsList=NULL"
		 * 
		 * ORIGINAL CODE----------- listDesc.setLayoutParams(new
		 * LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (newsList.size()
		 * * (int) getResources() .getDimension(R.dimen.
		 * maxx_card_fragment_maxx_card_content_adapter_item_height))));
		 * MaxxCardDescAdapter maxxCardDescAdapter = new
		 * MaxxCardDescAdapter(getActivity(), newsList);
		 * listDesc.setAdapter(maxxCardDescAdapter);
		 * ------------------------ORIGINAL CODE
		 */
		if (newsList != null) {
			listDesc.setLayoutParams(
					new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (newsList.size() * (int) getResources()
							.getDimension(R.dimen.maxx_card_fragment_maxx_card_content_adapter_item_height))));
			MaxxCardDescAdapter maxxCardDescAdapter = new MaxxCardDescAdapter(getActivity(), newsList);
			listDesc.setAdapter(maxxCardDescAdapter);
		} /*else {
			new AlertDialog(getActivity(), getActivity().getString(R.string.no_news_alert), null).show();
		}*/
		
		/***
		 * the above else part is blocked on 03-jan-2017
		 */
	}

	public void callListAdapterPrivateAndBenefitsSpecialOffer() {
		listPrivateAndBenefits = (ListView) getView().findViewById(R.id.listDesclist);
		getView().findViewById(R.id.rel_maxx_card_privileges_ben).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkPrivateAndBenefitsList == 0) {
					listPrivateAndBenefits.setVisibility(View.VISIBLE);
					chkPrivateAndBenefitsList = 1;
				} else {
					listPrivateAndBenefits.setVisibility(View.GONE);
					chkPrivateAndBenefitsList = 0;
				}
			}
		});
		/***
		 * DIBYENDU BHATTACHARYYA TEST CODE CHANGED ON 28-DEC-2016 FOR HANDLING
		 * NULL POINTER EXCEPTION WHILE GETTING BLANK VALUE FROM NEWS API
		 * SERVICE AND GETTING "specialOfferList=NULL"
		 * 
		 * ORIGINAL CODE----------- listDesc.setLayoutParams(new
		 * listPrivateAndBenefits.setLayoutParams(new
		 * LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
		 * (specialOfferList.size() * (int) getResources().getDimension(
		 * R.dimen.
		 * maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height
		 * )))); MaxxCardSpecialOfferAdapter maxxCardspecialofferAdapter = new
		 * MaxxCardSpecialOfferAdapter(getActivity(), specialOfferList);
		 * listPrivateAndBenefits.setAdapter(maxxCardspecialofferAdapter);
		 * ------------------------ORIGINAL CODE
		 */
		if (specialOfferList != null) {
			listPrivateAndBenefits.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
					(specialOfferList.size() * (int) getResources().getDimension(
							R.dimen.maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height))));
			MaxxCardSpecialOfferAdapter maxxCardspecialofferAdapter = new MaxxCardSpecialOfferAdapter(getActivity(),
					specialOfferList);
			listPrivateAndBenefits.setAdapter(maxxCardspecialofferAdapter);
		}
	}

	public void initLIstener() {

		loginBtn.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (Utils.isValidMail(emailEt.getText().toString().trim())) {
					if (emailEt.getText().toString().trim().length() > 0) {

						if (passwordEt.getText().toString().trim().length() > 0) {

							if (rememberMeChk.isChecked()) {
								pref.setMaxxCardEmail(emailEt.getText().toString().trim());
								// pref.setMaxxCardPassword(passwordEt.getText().toString().trim());
							}

							callLoginService(emailEt.getText().toString().trim(),
									passwordEt.getText().toString().trim());
						} else {
							AlertDialog dialog = new AlertDialog(getActivity(), "PLease enter your password",
									new DialogListener() {

										@Override
										public void onOkClick() {
											// TODO Auto-generated method stub

										}
									});
							dialog.show();
						}

					} else {
						AlertDialog dialog = new AlertDialog(getActivity(), "PLease enter your email.",
								new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub

									}
								});
						dialog.show();
					}
				} else {
					AlertDialog dialog = new AlertDialog(getActivity(), "PLease enter a valid email.",
							new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub

								}
							});
					dialog.show();
				}

			}
		});

		forgotPasswordTv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utils.isValidMail(emailEt.getText().toString().trim())) {
					if (emailEt.getText().toString().trim().length() > 0) {
						// callForgotPassword(emailEt.getText().toString().trim());
						goToWebview("http://www.cinemaxxtheater.com/Loyalty/ForgotPassword.aspx");
					} else {
						AlertDialog dialog = new AlertDialog(getActivity(), "PLease enter your email.",
								new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub

									}
								});
						dialog.show();
					}
				} else {
					AlertDialog dialog = new AlertDialog(getActivity(), "PLease enter a valid email.",
							new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub

								}
							});
					dialog.show();
				}

			}
		});

		relMenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chkMenu == 0) {
					lnrMenuItem.setVisibility(View.VISIBLE);
					mAnimator.start();
					chkMenu = 1;
				} else {
					collapse();
					// lnrMenuItem.setVisibility(View.GONE);
					chkMenu = 0;
				}
			}
		});

		starClass.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.GONE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;
				lnrClickAboutClass.setVisibility(View.VISIBLE);
				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				if (pref.getClickManuAboutMaxxCard().equals("")) {
					callClickOnMenuForAboutMaxx();
				} else {
					webViewContentClickForAbout.loadData(Utils.getTncWebview(pref.getClickManuAboutMaxxCard()),
							"text/html; charset=UTF-8", null);
				}
			}
		});

		privateAndBenefits.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.GONE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;
				lnrClickAboutClass.setVisibility(View.GONE);
				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.VISIBLE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				// openPrivateAndBenefits();
			}
		});

		starClassPrivileges.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.GONE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;
				lnrClickAboutClass.setVisibility(View.GONE);
				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.VISIBLE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.GONE);
				// openstarClassPrivileges();
			}
		});

		faq.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.GONE);
				termsAndConditions.setVisibility(View.VISIBLE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;
				lnrClickAboutClass.setVisibility(View.GONE);
				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.VISIBLE);
				lnrTermAndCondition.setVisibility(View.GONE);
				// openFaq();
			}
		});

		termsAndConditions.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				starClass.setVisibility(View.VISIBLE);
				privateAndBenefits.setVisibility(View.VISIBLE);
				starClassPrivileges.setVisibility(View.VISIBLE);
				faq.setVisibility(View.VISIBLE);
				termsAndConditions.setVisibility(View.GONE);

				// lnrMenuItem.setVisibility(View.GONE);
				collapse();
				chkMenu = 0;
				lnrClickAboutClass.setVisibility(View.GONE);
				lnrStarClass.setVisibility(View.GONE);
				lnrPrivilegesAndBenefits.setVisibility(View.GONE);
				lnrStarClassPrivileges.setVisibility(View.GONE);
				lnrFaq.setVisibility(View.GONE);
				lnrTermAndCondition.setVisibility(View.VISIBLE);
				// openTermAndConditions();
			}
		});

		lnrMenuItem.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

			@Override
			public boolean onPreDraw() {
				lnrMenuItem.getViewTreeObserver().removeOnPreDrawListener(this);
				lnrMenuItem.setVisibility(View.GONE);
				final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
				final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
				lnrMenuItem.measure(widthSpec, heightSpec);

				mAnimator = slideAnimator(0, lnrMenuItem.getMeasuredHeight());
				return true;
			}
		});

		signupTv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToWebview("http://www.cinemaxxtheater.com/Loyalty/Signup.aspx");
			}
		});

		imgActiveNow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToWebview("http://www.cinemaxxtheater.com/Loyalty/ActivateCard.aspx");
			}
		});

		imgJoinNow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToWebview("http://www.cinemaxxtheater.com/Loyalty/Signup.aspx");
			}
		});

		btnJoinNow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToWebview("http://www.cinemaxxtheater.com/Loyalty/Signup.aspx");
			}
		});

		/*
		 * webViewContentForAbout.setOnTouchListener(new View.OnTouchListener()
		 * {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) {
		 * v.getParent().requestDisallowInterceptTouchEvent(true); return false;
		 * } });
		 * 
		 * webViewContentForPrivilegeandBenefits.setOnTouchListener(new
		 * View.OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) {
		 * 
		 * v.getParent().requestDisallowInterceptTouchEvent(true);
		 * 
		 * return false; } });
		 * 
		 * webViewContentForStarClassPrivileges.setOnTouchListener(new
		 * View.OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View v, MotionEvent event) {
		 * v.getParent().requestDisallowInterceptTouchEvent(true); return false;
		 * } });
		 */
	}

	public void collapse() {
		int finalHeight = lnrMenuItem.getHeight();

		ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

		mAnimator.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationEnd(Animator animator) {
				// Height=0, but it set visibility to GONE
				lnrMenuItem.setVisibility(View.GONE);
				// mLinearLayoutHeader.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationStart(Animator animator) {
			}

			@Override
			public void onAnimationCancel(Animator animator) {
			}

			@Override
			public void onAnimationRepeat(Animator animator) {
			}
		});
		mAnimator.start();
	}

	public ValueAnimator slideAnimator(int start, int end) {

		ValueAnimator animator = ValueAnimator.ofInt(start, end);

		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				// Update Height
				int value = (Integer) valueAnimator.getAnimatedValue();

				ViewGroup.LayoutParams layoutParams = lnrMenuItem.getLayoutParams();
				layoutParams.height = value;
				lnrMenuItem.setLayoutParams(layoutParams);
			}
		});
		return animator;
	}

	/*
	 * public void openTermAndConditions() { TextView tm1 = (TextView)
	 * getView().findViewById(R.id.tv1); TextView tm11 = (TextView)
	 * getView().findViewById(R.id.tv11);
	 * tm1.setText(Html.fromHtml(getString(R.string.tm1)));
	 * tm11.setText(Html.fromHtml(getString(R.string.tm11))); }
	 * 
	 * public void openFaq() { TextView faq1 = (TextView)
	 * getView().findViewById(R.id.tv_faq1); TextView faq2 = (TextView)
	 * getView().findViewById(R.id.tv_faq2);
	 * faq1.setText(Html.fromHtml(getString(R.string.faq1)));
	 * faq2.setText(Html.fromHtml(getString(R.string.faq2))); }
	 */

	/*
	 * public void openstarClassPrivileges() { TextView txtContent = (TextView)
	 * getView().findViewById( R.id.txtContentStarClassPriviles); String
	 * text_view_str =
	 * "- Collect Maxx Points*\n- Get Birthday & Anniversary Treats*\n- Receive Special Oers From Selected Merchants*"
	 * ; txtContent.setText(text_view_str);
	 * 
	 * callListAdapterStarClassPrivileges(); }
	 */

	public void callListAdapterStarClassSpecialOfferPrivileges() {
		listStarClass = (ListView) getView().findViewById(R.id.listDescStarClassPreviliges);
		getView().findViewById(R.id.rel_star_class).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (chklistStarClassList == 0) {
					// callListAdapterStarClassSpecialOfferPrivileges();
					listStarClass.setVisibility(View.VISIBLE);
					chklistStarClassList = 1;
				} else {
					listStarClass.setVisibility(View.GONE);
					chklistStarClassList = 0;
				}
			}
		});

		/***
		 * DIBYENDU BHATTACHARYYA TEST CODE CHANGED ON 28-DEC-2016 FOR HANDLING
		 * NULL POINTER EXCEPTION WHILE GETTING BLANK VALUE FROM NEWS API
		 * SERVICE AND GETTING "listStarClass=NULL"
		 * 
		 * ORIGINAL CODE----------- 
		 * LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
		 * (starClassSpecialOfferList.size() * (int)
		 * getResources().getDimension( R.dimen.
		 * maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height_starclass
		 * )))); MaxxCardStarClassSpecialOfferAdapter
		 * maxxcardstarclassspecialOfferAdapter = new
		 * MaxxCardStarClassSpecialOfferAdapter( getActivity(),
		 * starClassSpecialOfferList);
		 * listStarClass.setAdapter(maxxcardstarclassspecialOfferAdapter);
		 * ------------------------ORIGINAL CODE
		 */

		if (starClassSpecialOfferList != null) {
			listStarClass.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
					(starClassSpecialOfferList.size() * (int) getResources().getDimension(
							R.dimen.maxx_card_fragment_maxx_card_content_adapter_specialoffer_item_height_starclass))));
			MaxxCardStarClassSpecialOfferAdapter maxxcardstarclassspecialOfferAdapter = new MaxxCardStarClassSpecialOfferAdapter(
					getActivity(), starClassSpecialOfferList);
			listStarClass.setAdapter(maxxcardstarclassspecialOfferAdapter);
		}

	}

	/*
	 * public void openPrivateAndBenefits() { TextView txtContent = (TextView)
	 * getView().findViewById( R.id.txtContentpriviles); String text_view_str =
	 * "- Collect Maxx Points*\n- Get Birthday & Anniversary Treats*\n- Receive Special Oers From Selected Merchants*"
	 * ; txtContent.setText(text_view_str);
	 * 
	 * callListAdapterPrivateAndBenefits(); }
	 */

	public void callForgotPassword(String email) {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@SuppressWarnings("static-access")
				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					// TODO Auto-generated method stub
					System.out.println("Maxx forgot password response: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {

							if (Data.toString().trim().equals("0")) {
								showDialog(
										"We just sent to your email \nfor the link you can use to \nreset your password.Thank you.");
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				loyalty.ForgotPasswordAsync(email, getActivity());
				// loyalty.SetOrderLoyaltyPointsAsync(42, true, memberId,
				// orderId, orderIdSpecified, loyaltyRedeemType,
				// loyaltyRedeemTypeSpecified, redeemPoints,
				// redeemPointsSpecified, headers);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}

	}

	public void showDialog(String message) {
		AlertDialog dialog = new AlertDialog(getActivity(), message, new DialogListener() {

			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub

			}
		});
		dialog.show();
	}

	public void callLoginService(String emailId, String password) {

		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Loyalty loyalty = new Loyalty(new IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@SuppressWarnings("static-access")
				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					// TODO Auto-generated method stub
					System.out.println("Maxx card Login response: " + Data.toString());

					if (Data.toString().trim().length() > 1) {
						try {

							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim().equals("0")) {
								pref.setMaxxCardLoginResponse(Data.toString().trim());
								MaxxCardDetailsTo maxxCardTo = new MaxxCardDetailsTo();
								maxxCardTo.setLoyaltyRedeemType(
										mainObj.optString(MaxxCardDetailsConstant.LOYALTY_REDEEM_TYPE));
								maxxCardTo.setOrderId(mainObj.optString(MaxxCardDetailsConstant.ORDER_ID));
								maxxCardTo.setMaxCardImage(mainObj.optString(MaxxCardDetailsConstant.MAX_CARD_IMAGE));

								JSONObject memberObj = mainObj
										.optJSONObject(MaxxCardDetailsConstant.MEMBER_DETAILS_OBJ);
								maxxCardTo.setMemberId(memberObj.optString(MaxxCardDetailsConstant.MEMBER_ID));
								maxxCardTo.setUserName(memberObj.optString(MaxxCardDetailsConstant.USER_NAME));
								maxxCardTo.setPassword(memberObj.optString(MaxxCardDetailsConstant.PASSWORD));
								maxxCardTo.setClubId(memberObj.optString(MaxxCardDetailsConstant.CLUB_ID));
								maxxCardTo.setClubName(memberObj.optString(MaxxCardDetailsConstant.CLUB_NAME));
								maxxCardTo.setCardNumber(memberObj.optString(MaxxCardDetailsConstant.CARD_NUMBER));
								maxxCardTo.setFirstName(memberObj.optString(MaxxCardDetailsConstant.FIRST_NAME));
								maxxCardTo.setLastName(memberObj.optString(MaxxCardDetailsConstant.LAST_NAME));
								maxxCardTo.setGender(memberObj.optString(MaxxCardDetailsConstant.GENDER));
								maxxCardTo.setMeritalStatus(memberObj.optString(MaxxCardDetailsConstant.MERITAL_STATUS));
								maxxCardTo.setDob(memberObj.optString(MaxxCardDetailsConstant.DOB));
								maxxCardTo.setEmail(memberObj.optString(MaxxCardDetailsConstant.EMAIL));
								maxxCardTo.setHomePhone(memberObj.optString(MaxxCardDetailsConstant.HOME_PHONE));
								maxxCardTo.setMobilePhone(memberObj.optString(MaxxCardDetailsConstant.MOBILE_PHONE));
								maxxCardTo.setAddress(memberObj.optString(MaxxCardDetailsConstant.ADDRESS));
								maxxCardTo.setInHouseId(memberObj.optString(MaxxCardDetailsConstant.PERSONS_INHOUSEHOLD));
								maxxCardTo.setHouseHoldIncome(memberObj.optString(MaxxCardDetailsConstant.HOUSE_HOLD_INCOME));
								maxxCardTo.setWishToReceiveSMS(
										memberObj.optString(MaxxCardDetailsConstant.WISH_TO_RECEIVE_SMS));
								maxxCardTo.setSendNewsLetter(
										memberObj.optString(MaxxCardDetailsConstant.SEND_NEWS_LETTER));
								maxxCardTo.setMailingFrequency(
										memberObj.optString(MaxxCardDetailsConstant.MAILING_FREQUENCY));
								maxxCardTo.setExpiryDate(memberObj.optString(MaxxCardDetailsConstant.EXPIRY_DATE));
								maxxCardTo.setStatus(memberObj.optString(MaxxCardDetailsConstant.STATUS));
								maxxCardTo.setMembershipActivated(
										memberObj.optString(MaxxCardDetailsConstant.MEMBERSHIP_ACTIVATED));

								JSONArray balanceListArray = memberObj
										.optJSONArray(MaxxCardDetailsConstant.BALANCE_LIST_ARRAY);
								for (int k = 0; k < balanceListArray.length(); k++) {
									JSONObject balanceListObj = balanceListArray.optJSONObject(k);
									maxxCardTo.setBalanceTypeId(
											balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPE_ID));
									maxxCardTo.setBalanceName(
											balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_NAME));
									maxxCardTo.setBalanceMessage(
											balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_MESSAGE));
									maxxCardTo.setBalanceDisplay(
											balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_DISPLAY));
									maxxCardTo.setBalancePointRemaining(
											balanceListObj.optString(MaxxCardDetailsConstant.BALANCE_REMAINING_POINT));
									maxxCardTo.setBalanceIsDefault(
											balanceListObj.optBoolean(MaxxCardDetailsConstant.BALANCE_IS_DEFAULT));
								}

								JSONArray pointsExpiryListArray = memberObj
										.optJSONArray(MaxxCardDetailsConstant.POINTS_EXPIRY_LIST_ARRAY);
								for (int j = 0; j < pointsExpiryListArray.length(); j++) {
									JSONObject pointsExpiryListObj = pointsExpiryListArray.optJSONObject(j);
									maxxCardTo.setPointsExpiring(
											pointsExpiryListObj.optString(MaxxCardDetailsConstant.POINTS_EXPIRIRING));
									maxxCardTo.setExpireOn(
											pointsExpiryListObj.optString(MaxxCardDetailsConstant.EXPIRE_ON));
									maxxCardTo.setBalanceTypeIdExp(
											pointsExpiryListObj.optString(MaxxCardDetailsConstant.BALANCE_TYPEID));
								}

								maxxCardTo.setIsProfileComplete(
										memberObj.optString(MaxxCardDetailsConstant.IS_PROFILE_COMPLETE));
								maxxCardTo.setLoyaltyRequestId(
										memberObj.optString(MaxxCardDetailsConstant.LOYALTY_REQUEST_ID));
								shpref.set_MaxxcardImage(getActivity().getApplicationContext(), maxxCardTo.getMaxCardImage());

								shpref.set_MaxcardMemberid(getActivity().getApplicationContext(), maxxCardTo.getMemberId());
								shpref.set_LoyaltyRequestid(getActivity().getApplicationContext(), maxxCardTo.getLoyaltyRequestId());

								Fragment fragment = new MaxxCardWelcomeFragment(true);
								// FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
								if (fragment != null) {

									FragmentTransaction ft = getFragmentManager().beginTransaction();
									CurrentFragmentSingletone.getInstance().setFragment(fragment);
									ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
									ft.replace(R.id.frame_container, fragment, "fragment");
									// Start the animated transition.
									ft.commit();

									// update selected item and title, then
									// close the drawer
								}

							} else {
								AlertDialog dialog = new AlertDialog(getActivity(),
										mainObj.optString(MaxxCardDetailsConstant.ERROR_DESC).trim(),
										new DialogListener() {
											@Override
											public void onOkClick() {
											}
										});
								dialog.show();
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				// loyalty.ClubLoginAsync("2", "rushikesh3011@gmail.com",
				// "123456", getActivity());
				loyalty.ClubLoginAsync("2", emailId, password, getActivity());
				// loyalty.LoginAsync(emailEt.getText().toString().trim(),
				// passwordEt.getText().toString().trim(), getActivity());
				// loyalty.LoginWithInitAsync(532434l, true,
				// emailEt.getText().toString().trim(),
				// passwordEt.getText().toString().trim(), getActivity());
				// loyalty.GetMemberTransactionHistoryAsync(42, true,
				// "MRPTPVDGWW08", activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}

	}

	public void callMenuServiceForAboutMaxx() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("ABOUT MAXX CARD & MAXX CARD STAR CLASS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuAboutMaxxCard(
											itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewContentForAbout.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							} else {

							}
							callMenuServiceForNews();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("19", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForNews() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("GetMaxxCardNewsAsync: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setManuMaxxCardNews(Data.toString());
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								newsList = new ArrayList<MaxxCardManuDetailsTo>();
								JSONArray webContentsArry = mainObj
										.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDNEWS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									maxxCardManuDetailsTo
											.setMaxxCardNewsId(itemObj.optLong(MaxxCardManuConstant.MAXCARDNEWSID));
									maxxCardManuDetailsTo.setMaxxCardNewsTitle(
											itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSTITLE).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsMedia(
											itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSMEDIA).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsDescription(
											itemObj.optString(MaxxCardManuConstant.MAXXCARDNEWSDESCRIPTION).trim());
									maxxCardManuDetailsTo.setMaxxCardNewPublishDate(
											itemObj.optString(MaxxCardManuConstant.MAXCARDNEWPUBLICDATE).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsImageURL(
											itemObj.optString(MaxxCardManuConstant.MAXXCARDNEWSIMAGEURL).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsValidFrom(
											itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSVALIDFROM).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsValidTo(
											itemObj.optString(MaxxCardManuConstant.MAXCARDNEWSVALIDTO).trim());
									maxxCardManuDetailsTo.setMaxxCardNewsStatus(
											itemObj.optString(MaxxCardManuConstant.MAXXCARDSTATUS).trim());
									newsList.add(maxxCardManuDetailsTo);
								}
							} else {

							}
							callNewsListAdapter();
							callMenuServiceForPrivilegesAndBenefis();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardNewsAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForPrivilegesAndBenefis() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("MAXX CARD PRIVILEGES & BENEFITS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuPrivilegesAndBenefits(
											itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewContentForPrivilegeandBenefits.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							} else {

							}
							callMenuServiceForPrivilegesAndBenefisSpecialOffer();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("20", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForPrivilegesAndBenefisSpecialOffer() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("callMenuServiceForPrivilegesAndBenefisOfferList: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setManuSpcialOffer(Data.toString().trim());
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								specialOfferList = new ArrayList<MaxxCardManuDetailsTo>();
								JSONArray webContentsArry = mainObj
										.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDSPECIALOFFER);
								for (int i = 0; i < webContentsArry.length(); i++) {
									MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									maxxCardManuDetailsTo
											.setSpecialOfferId(itemObj.optLong(MaxxCardManuConstant.SPECIALOFFERID));
									maxxCardManuDetailsTo.setSpecialOfferTitle(
											itemObj.optString(MaxxCardManuConstant.SPECIALOFFERTITLE).trim());
									maxxCardManuDetailsTo.setSpecialOfferImageURL(
											itemObj.optString(MaxxCardManuConstant.SPECIALOFFERIMAGEURL).trim());
									maxxCardManuDetailsTo.setSpecialOfferDescription(
											itemObj.optString(MaxxCardManuConstant.SPECIALOFFERDESCRIPTION).trim());

									specialOfferList.add(maxxCardManuDetailsTo);
								}
							} else {

							}
							callListAdapterPrivateAndBenefitsSpecialOffer();
							callMenuServiceForStarClassPrivilages();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardSpecialOfferAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForStarClassPrivilages() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("Terms & Conditions: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuStarClassPrivileges(
											itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewContentForStarClassPrivileges.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							} else {

							}
							callMenuServiceForStarClassSpecialOffer();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("24", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForStarClassSpecialOffer() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub

				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("callMenuServiceForStarClassSpecialOffer: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							pref.setStarClassSpcialOffer(Data.toString().trim());
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								starClassSpecialOfferList = new ArrayList<MaxxCardManuDetailsTo>();
								JSONArray webContentsArry = mainObj
										.optJSONArray(MaxxCardManuConstant.LATESTMAXXCARDSTARCLASSSPECIALOFFER);
								for (int i = 0; i < webContentsArry.length(); i++) {
									MaxxCardManuDetailsTo maxxCardManuDetailsTo = new MaxxCardManuDetailsTo();
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									maxxCardManuDetailsTo.setStarClassSpecialOfferId(
											itemObj.optLong(MaxxCardManuConstant.STARCLASSSPECIALOFFERID));
									maxxCardManuDetailsTo.setStarClassSpecialOfferTitle(
											itemObj.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERTITLE).trim());
									maxxCardManuDetailsTo.setStarClassSpecialOfferDescription(itemObj
											.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERDESCRIPTION).trim());
									maxxCardManuDetailsTo.setStarClassSpecialOfferImageURL(itemObj
											.optString(MaxxCardManuConstant.STARCLASSSPECIALOFFERIMAGEURL).trim());

									starClassSpecialOfferList.add(maxxCardManuDetailsTo);
								}
							} else {

							}
							callListAdapterStarClassSpecialOfferPrivileges();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
					if (pDialog != null) {
						pDialog.dismissDialog();
						pDialog = null;
					}
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetMaxxCardStarClassSpecialOfferAsync(pref.getDeviceId(), getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForFaq() {
		if (Utils.isNetworkAvailable(getActivity())) {
			if (pDialog == null) {
				pDialog = new CustomProgressDialog(activity);
				pDialog.showDialog();
			}
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("MAXX CARD FAQ: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuFaq(itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewFaq.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							} else {

							}
							callMenuServiceForTermsAndConditions();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("21", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void callMenuServiceForTermsAndConditions() {
		if (Utils.isNetworkAvailable(getActivity())) {
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("Terms & Conditions: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setManuTermsAndConditions(
											itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewTnd.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							} else {

							}
							callMenuServiceForAboutMaxx();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("23", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

	public void goToWebview(String str) {
		Uri uri = Uri.parse(str);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}

	public void hideSoftKeyboard() {
		try {

			InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
					.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

			getActivity().getCurrentFocus().clearFocus();

		} catch (Exception e) {
		}
	}

	public void callClickOnMenuForAboutMaxx() {
		if (Utils.isNetworkAvailable(getActivity())) {
			if (pDialog == null) {
				pDialog = new CustomProgressDialog(activity);
				pDialog.showDialog();
			}
			activity.setClickable();
			activity.setClickable();
			Schedule schedule = new Schedule(new com.cinemaxx.webservice.IWsdl2CodeEvents() {

				@Override
				public void Wsdl2CodeStartedRequest() {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinishedWithException(Exception ex) {
					// TODO Auto-generated method stub
					System.out.println("failed");
				}

				@Override
				public void Wsdl2CodeFinished(String methodName, Object Data) {
					System.out.println("ABOUT MAXX CARD & MAXX CARD STAR CLASS: " + Data.toString());
					if (Data.toString().trim().length() > 1) {
						try {
							JSONObject mainObj = new JSONObject(Data.toString());
							if (mainObj.optString(MaxxCardManuConstant.ERROR_DESC).trim().equals("0")) {
								JSONArray webContentsArry = mainObj.optJSONArray(MaxxCardManuConstant.WEBCONTENTS);
								for (int i = 0; i < webContentsArry.length(); i++) {
									JSONObject itemObj = webContentsArry.optJSONObject(i);
									pref.setClickManuAboutMaxxCard(
											itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim());
									webViewContentClickForAbout.loadData(
											Utils.getTncWebview(
													itemObj.optString(MaxxCardManuConstant.PAGECONTENT).trim()),
											"text/html; charset=UTF-8", null);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					activity.setNotClickable();
					if (pDialog != null) {
						pDialog.dismissDialog();
						pDialog = null;
					}
				}

				@Override
				public void Wsdl2CodeEndedRequest() {
					// TODO Auto-generated method stub

				}
			});

			try {

				schedule.GetWebPageContentAsync("22", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(),
					SplashActivity.internetMsg, new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
	}

}
