package com.cinemaxx.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cinemaxx.adapter.BuyTicketAdapter;
import com.cinemaxx.adapter.MySpinnerAdapter;
import com.cinemaxx.bean.BuyTicketsTo;
import com.cinemaxx.constant.BuyTicketConstant;
import com.cinemaxx.constant.NowPlayingConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomScrollView;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

public class BuyTicketsFragment extends Fragment implements OnClickListener, OnWheelChangedListener{

	CinemaxxPref pref;
	TextView locationNameTv, continueTv;
	TextView movietv, citytv, cinematv, cinemaTypetv, datetv, timetv;
	LinearLayout movieSpnrContainer, citySpnrContainer, cinemaSpnrContainer, cinemaTypeSpnrContainer, dateSpnrContainer, timeSpnrContainer;
	Button continueBtn;
	LinearLayout loderLnr;
	private String movieList[];
	private String cityNameList[], cinemaNameList[], cinemaTypeNameList[], showDateList[], TimeList[];
	ArrayList<String> movieId;
	ArrayList<String> cityIdList;
	ArrayList<String> cinemaIdList;
	ArrayList<String> cinemaTypeIdList;
	ArrayList<String> scheduleIdList, movieFormatList, timeScheduleIdList;
	MySpinnerAdapter movieAdapter, cityAdapter, cinemaAdapter, cinemaTypeAdapter, dateAdapter, timeAdapter, MovieFormatAdapter;
	String movieSelectId, citySelectId, cinemaSelectId, cinemaTypeSelectId, dateSelectId, timeSelectId;
	String movieSelectName, citySelectName, cinemaSelectName, cinemaTypeSelectName, dateSelect, timeSelect;
	String scheduleId, timeScheduleId, movieIdValue;
	MainActivity activity;
	String movieName = "", status = "", movieIdV = "";
	String cityName = "", cinemaNameV = "",cinemaTypeV = "", showDateV = "", timeV = "", cinemaNameVV = "", cinemaType = "", showDate = "", timeValuee = "";
	private WheelView wvBuyTicketMovie, wvBuyTicketCity, wvBuyTicketCinema, wvBuyTicketCinemaType, wvBuyTicketDate, wvBuyTicketTime;
	private BuyTicketAdapter buyTicketAdapter;
	LinearLayout lnrwheelMovie, lnrwheelCity,lnrwheelCinema, lnrwheelCinemaType, buy_ticket_lnr_scroll, lnrwheelDate, lnrwheelTime;
	CustomScrollView scrollView;
	boolean flagt = false, flagcnema = false, flagcnemaType = false, flagdate = false, flagtime = false;
	int mvalue = 0, cvalue = 0, cinemavalue = 0, cinemaTypevalue = 0, datevalue = 0, timevalue = 0;
	int mdilgck = 0, cityDchk = 0, cinemaDchk = 0, cinemaTypeDchk = 0, dateDchk = 0;
	int timeIndex = 0, dateIndex = 0, cinemaTypeIndex = 0, cinemaIndex = 0, cityIndex = 0, movieIndex = 0;
	int chkloading = 0;
	
	public BuyTicketsFragment(String movieName, String movieId, String cityName, String cinemaName, String cinemaType, String showDate, String status, String timeValuee){
		this.movieName = movieName;
		this.status = status;
		this.cityName = cityName;
		this.cinemaNameV = cinemaName;
		this.cinemaType = cinemaType;
		this.showDate = showDate;
		if(timeValuee.equals("")){
			this.timeValuee = timeValuee;
		}else{
			this.timeValuee = timeValuee.substring(0, 13);
		}
		
		movieIdV = movieId;
		
		
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_buy_ticket, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		locationNameTv.setText("Location: "+pref.getCityName());
		
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callForMovieList();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(
					getActivity(), SplashActivity.internetMsg,
					new DialogListener() {

						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub

						}
					});
			dialog.show();
		}
		
		activity.clickOnBuyTickets("SELECT SHOW");
		
		pref.setTimerTime("600000");
		activity.stopTimer();
	}
	
	
	public void initView(){
		loderLnr = (LinearLayout) getView().findViewById(R.id.buy_ticket_fragment_loder_lnr);
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf"); 
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		pref = new CinemaxxPref(getActivity());
		continueBtn = (Button) getView().findViewById(R.id.buy_ticket_continue_btn);
		locationNameTv = (TextView) getView().findViewById(R.id.buy_ticket_setlocation_txt);
		locationNameTv.setTypeface(faceThin);
		loderLnr = (LinearLayout) getView().findViewById(R.id.buy_ticket_fragment_loder_lnr);
		loderLnr.setVisibility(View.INVISIBLE);
		continueTv = (TextView) getView().findViewById(R.id.buy_ticket_continue_txt);
		continueTv.setTypeface(face);
		movietv = (TextView) getView().findViewById(R.id.buy_ticket_movie_tv);

		citytv = (TextView) getView().findViewById(R.id.buy_ticket_city_tv);
		cinematv = (TextView) getView().findViewById(R.id.buy_ticket_cinema_tv);
		cinemaTypetv = (TextView) getView().findViewById(R.id.buy_ticket_cinema_type_tv);
		datetv = (TextView) getView().findViewById(R.id.buy_ticket_date_tv);
		timetv = (TextView) getView().findViewById(R.id.buy_ticket_time_tv);
		wvBuyTicketMovie = (WheelView) getView().findViewById(R.id.wvBuyTicketMovie);
		wvBuyTicketCity = (WheelView) getView().findViewById(R.id.wvBuyTicketCity);
		lnrwheelMovie = (LinearLayout) getView().findViewById(R.id.lnrwheelMovie);
		lnrwheelCity = (LinearLayout) getView().findViewById(R.id.lnrwheelCity);
		
		System.out.println(movieName+"______________"+movieIdV);
		
		if(!movieName.equals("") && movieIdV != null){
			movietv.setText(movieName);
			movieSelectName = movieName;
			movieSelectId  = movieIdV;
			
			if(status.equals("movie") && !pref.getCityName().equals("All Cities")){
				citytv.setText(pref.getCityName());
				citySelectId = pref.getCityCode();
				cityDchk = 1;
			}else if(!cityName.equals("") && !cityName.equals("All Cities")){
				citySelectName = cityName;
				citytv.setText(cityName);
				cityDchk = 1;
			}
		}
		
		if(cinemaNameVV != null && !cinemaNameVV.equals("")){
			cinemaSelectName = cinemaNameVV;
			cinematv.setText(cinemaNameVV);
		}
		
		if(!cinemaTypeV.equals("") && cinemaTypeV != null){
			cinemaTypetv.setText(cinemaTypeV);
		}
		if(!showDateV.equals("") && showDateV != null){
			datetv.setText(showDateV);
		}
		if(!timeV.equals("") && timeV != null){
			
			Log.d("timecheck", "timecheck" +timeV);
			timetv.setText(timeV);
		}
		
		
		lnrwheelCinema = (LinearLayout) getView().findViewById(R.id.lnrwheelCinema);
		wvBuyTicketCinema = (WheelView) getView().findViewById(R.id.wvBuyTicketCinema);
		lnrwheelCinemaType = (LinearLayout) getView().findViewById(R.id.lnrwheelCinemaType);
		wvBuyTicketCinemaType = (WheelView) getView().findViewById(R.id.wvBuyTicketCinemaType);
		
		lnrwheelDate = (LinearLayout) getView().findViewById(R.id.lnrwheelDate);
		wvBuyTicketDate = (WheelView) getView().findViewById(R.id.wvBuyTicketDate);
		lnrwheelTime = (LinearLayout) getView().findViewById(R.id.lnrwheelTime);
		wvBuyTicketTime = (WheelView) getView().findViewById(R.id.wvBuyTicketTime);

        //wvBuyTicketCinema(getResources().getColor(R.color.list_item_title));
//        wvBuyTicketCinemaType.setWheelForeground(getResources().getColor(R.color.list_item_title));
//        wvBuyTicketDate.setWheelForeground(getResources().getColor(R.color.list_item_title));
//        wvBuyTicketTime.setWheelForeground(getResources().getColor(R.color.list_item_title));


		
		scrollView = (CustomScrollView ) getView().findViewById(R.id.buy_ticket_scroll);
		
		movieSpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_movie_spinner_container);
		citySpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_city_spinner_container);
		cinemaSpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_cinema_spinner_container);
		cinemaTypeSpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_cinema_type_spinner_container);
		dateSpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_date_spinner_container);
		timeSpnrContainer = (LinearLayout) getView().findViewById(R.id.buy_ticket_time_spinner_container);
		buy_ticket_lnr_scroll = (LinearLayout) getView().findViewById(R.id.buy_ticket_lnr_scroll);
		
	}
	
	
	public void callForMovieList(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				movieId = new ArrayList<String>();
				try{
					
					if(Data.toString().trim().length() > 1){
						JSONObject mainJson = new JSONObject(Data.toString());
						if(mainJson.optString(BuyTicketConstant.ERROR_DESC).equals("0")){
							System.out.println("BuyTicket Movie List: " +mainJson.toString());
							
							JSONArray movieArray = mainJson.optJSONArray(BuyTicketConstant.MOVIE_ARRAY);
							movieList = new String[movieArray.length()];
							for(int i = 0; i < movieArray.length(); i++){
								
								JSONObject movieObj = movieArray.optJSONObject(i);
								String movieid = movieObj.optString(BuyTicketConstant.MOVIE_ID);
								String movieName = movieObj.optString(BuyTicketConstant.MOVIE_NAME);
								String movieRating = movieObj.optString(BuyTicketConstant.MOVIE_RATING);
								
								String moviItem = movieName;
								movieList[i] = moviItem;
								movieId.add(movieid);
								
							}
							
							
							if(!movieName.equals("") && movieName != null && Arrays.asList(movieList).contains(movieName)){
								for(int i = 0; i < movieList.length; i++){
									if(movieList[i].contains(movieName)){
//										wvBuyTicketMovie.setCurrentItem(i);
										movieIndex = i;
										mdilgck = 1;
										movieSelectId = movieId.get(i);
										movietv.setText(movieName);
										
										if(!cityName.equals("") && cityName != null){
											chkloading = 1;
											callForCityListDefault(pref.getDeviceId(), movieSelectId);
										}
										break;
									}

								}
							}else if(!movieName.equals("") && movieName != null && !movieSelectId.equals("") && movieSelectId != null) {
								if(!cityName.equals("") && cityName != null){
									mdilgck = 1;
									chkloading = 1;
									callForCityListDefault(pref.getDeviceId(), movieSelectId);
								}
							}
						}else{
//							MainActivity activity = (MainActivity) getActivity();
//							activity.showCustomAlert("No movies are available");
							showDialog("No movies are available");
						}
					}else{
						AlertDialog dialog = new AlertDialog(getActivity(),
								"No data available", new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
						dialog.show();
					}
					
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
				if(chkloading == 0){
					loderLnr.setVisibility(View.VISIBLE);
					chkloading = 0;
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
        try{
			
			schedule.GetNowPlayingMovieListAsync(pref.getDeviceId(), pref.getCityCode(), getActivity());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void callForCityList(String deviceId, String movieId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cityIdList = new ArrayList<String>();
				
				if(Data.toString().trim().length() > 1){
					try{
						JSONObject mainJson = new JSONObject(Data.toString());
						System.out.println("BuyTicket City List: " +mainJson.toString());
						
						JSONArray cityArray = mainJson.optJSONArray(BuyTicketConstant.CITY_ARRAY);
						cityNameList = new String[cityArray.length()];
						for(int i = 0; i < cityArray.length(); i++){
							
							JSONObject cityObj = cityArray.optJSONObject(i);
							String cityName = cityObj.optString(BuyTicketConstant.CITY_NAME);
							String cityId = cityObj.optString(BuyTicketConstant.CITY_ID);
							
							cityNameList[i] = cityName;
							cityIdList.add(cityId);
						}
						if(cityArray.length() > 0){
							lnrwheelCity.setVisibility(View.VISIBLE);
							initCityList();
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									scrollView.fullScroll(View.FOCUS_DOWN);
								}
							}, 100);
						}else{
							showDialog("No cities are available");
							citytv.setEnabled(true);
						}
						
					}catch(Exception e){
						
					}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetCityListByMovieAsync(deviceId, movieId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void callForCinemaList(String deviceId, String movieId, String cityCode) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cinemaIdList = new ArrayList<String>();
				
                if(Data.toString().trim().length() > 1){
                	try {
    					JSONObject mainJson = new JSONObject(Data.toString());
    					System.out.println("BuyTicket Cinema List: " +mainJson.toString());
    					
    					JSONArray cinemaArray = mainJson.optJSONArray(BuyTicketConstant.CINEMA_ARRAY);
    					cinemaNameList = new String[cinemaArray.length()];
    					for(int i = 0; i < cinemaArray.length(); i++){
    						
    						JSONObject cinemaObj = cinemaArray.optJSONObject(i);
    						String cinemaName = cinemaObj.optString(BuyTicketConstant.CINEMA_NAME);
    						String cinemaId = cinemaObj.optString(BuyTicketConstant.CINEMA_ID);
    						
    						cinemaNameList[i] = cinemaName;
    						cinemaIdList.add(cinemaId);
    					}
    					
    					if(cinemaArray.length() > 0){
    						lnrwheelCinema.setVisibility(View.VISIBLE);
    						initCinemaList();
    						new Handler().postDelayed(new Runnable() {
    							@Override
    							public void run() {
    								scrollView.fullScroll(View.FOCUS_DOWN);
    							}
    						}, 100);
    					}else{
    						showDialog("No cinemas are available");
    						cinematv.setEnabled(true);
    					}
    				} catch (Exception e) {

    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetCinemaListByMovieCityAsync(deviceId, movieId, cityCode, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void callForCinemaTypeList(String cinemaId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cinemaTypeIdList = new ArrayList<String>();
				System.out.println("Response: "+Data.toString());
                if(Data.toString().trim().length() > 1){
                	try {
    					JSONObject mainJson = new JSONObject(Data.toString());
    					System.out.println("BuyTicket CinemaType List: " +mainJson.toString());
    					JSONArray cinemaTypeArray = mainJson.optJSONArray(BuyTicketConstant.CINEMA_TYPE_ARRAY);
    					cinemaTypeNameList = new String[cinemaTypeArray.length()];
    					for(int i = 0; i < cinemaTypeArray.length(); i++){
    						
    						JSONObject cinemaTypeObj = cinemaTypeArray.optJSONObject(i);
    						String cinemaTypeName = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_NAME);
    						String cinemaTypeIcon = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_ICON);
    						String cinemaTypeId = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_ID);
    						
    						cinemaTypeNameList[i] = cinemaTypeName;
    						cinemaTypeIdList.add(cinemaTypeId);
    					}
    					
    					if(cinemaTypeArray.length() > 0){
    						lnrwheelCinemaType.setVisibility(View.VISIBLE);
    						initCinemaTypeList();
    						new Handler().postDelayed(new Runnable() {
    							@Override
    							public void run() {
    								scrollView.fullScroll(View.FOCUS_DOWN);
    							}
    						}, 100);
    					}else{
    						showDialog("No cinema types are available");
    						cinemaTypetv.setEnabled(true);
    					}
    				} catch (Exception e) {

    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {
			

			schedule.GetCinemaTypeAsync(cinemaId, movieSelectId, citySelectId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void callForDateListList(String movieId, String cityCode, String cinemaId, String cinemaClassIdField) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				scheduleIdList = new ArrayList<String>();
                if(Data.toString().trim().length() > 1){
                	try {
    					JSONObject mainJson = new JSONObject(Data.toString());
    					System.out.println("BuyTicket Date List: " +mainJson.toString());
    					JSONArray dateArray = mainJson.optJSONArray(BuyTicketConstant.DATE_ARRAY);
    					showDateList = new String[dateArray.length()];
    					for(int i = 0; i < dateArray.length(); i++){
    						
    						JSONObject dateObj = dateArray.optJSONObject(i);
    						String showDate = dateObj.optString(BuyTicketConstant.SHOW_DATE);
    						String scheduleId = dateObj.optString(BuyTicketConstant.SCHEDULE_ID);
    						
    						showDateList[i] = showDate;
    						scheduleIdList.add(scheduleId);
    					}
    					
    					if(dateArray.length() > 0){
    						lnrwheelDate.setVisibility(View.VISIBLE);
    						initDateList();
    						new Handler().postDelayed(new Runnable() {
    							@Override
    							public void run() {
    								scrollView.fullScroll(View.FOCUS_DOWN);
    							}
    						}, 100);
    					}else{
    						showDialog("No show dates are available");
    						datetv.setEnabled(true);
    					}
    				} catch (Exception e) {

    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {
			
			Log.d("cinemaId", "cinemaId" +cinemaId);
			Log.d("movieSelectId", "movieSelectId" +movieSelectId);
			Log.d("citySelectId", "citySelectId" +citySelectId);

			schedule.GetShowDateAsync(movieId, cityCode, cinemaId, cinemaClassIdField, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void callForTimeList(String movieId, String cityCode, String cinemaId, String cinemaClassIdField, String scheduleId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				timeScheduleIdList = new ArrayList<String>();
                if(Data.toString().trim().length() > 1){
                	try {
    					JSONObject mainJson = new JSONObject(Data.toString());
    					System.out.println("BuyTicket Time List: " +mainJson.toString());
    					JSONArray timeArray = mainJson.optJSONArray(BuyTicketConstant.TIME_ARRAY);
    					TimeList = new String[timeArray.length()];
    					for(int i = 0; i < timeArray.length(); i++){
    						
    						JSONObject timeObj = timeArray.optJSONObject(i);
    						String TimeValue = timeObj.optString(BuyTicketConstant.TIME_VALUE);
    						String Time = timeObj.optString(BuyTicketConstant.TIME);
    						Log.d("timekar", "timekar" +Time);
//    						if(Time.contains(";")){
//    							String[] time = Time.split(";");
//    							TimeList[i] = time[0] + ")";
//    						}else{
//    							TimeList[i] = Time;
//    						}
    						TimeList[i] = Time;
    						
    						timeScheduleIdList.add(TimeValue);
    					}
    					
    					if(timeArray.length() > 0){
    						lnrwheelTime.setVisibility(View.VISIBLE);
    						initTimeList();
    						new Handler().postDelayed(new Runnable() {
    							@Override
    							public void run() {
    								scrollView.fullScroll(View.FOCUS_DOWN);
    							}
    						}, 100);
    					}else{
    						showDialog("No show times are available");
    						timetv.setEnabled(true);
    					}
    				} catch (Exception e) {

    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetShowTimeAsync(movieId, cityCode, cinemaId, cinemaClassIdField, scheduleId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public void showDialog(String message){
		AlertDialog dialog = new AlertDialog(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
			}
		});
		dialog.show();
	}
	
	private void initListener() {
		movietv.setOnClickListener(this);
		citytv.setOnClickListener(this);
		cinematv.setOnClickListener(this);
		cinemaTypetv.setOnClickListener(this);
		datetv.setOnClickListener(this);
		timetv.setOnClickListener(this);
		continueBtn.setOnClickListener(this);
	}

	
	protected void initMovieList() {
		// TODO Auto-generated method stub
		
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), movieList, 0);
		
		wvBuyTicketMovie.setViewAdapter(buyTicketAdapter);
		if(movietv.getText().toString().contains("Select")){
			wvBuyTicketMovie.setCurrentItem(0);
			movieIndex = 0;
		}else{
			wvBuyTicketMovie.setCurrentItem(movieIndex);
		}
		
		wvBuyTicketMovie.addChangingListener(this);
		movietv.setText(movieList[wvBuyTicketMovie.getCurrentItem()]);
		movieSelectName = movieList[wvBuyTicketMovie.getCurrentItem()];
		movieSelectId = movieId.get(wvBuyTicketMovie.getCurrentItem());
		wvBuyTicketMovie.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketMovie.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								movieIndex = wvBuyTicketMovie.getCurrentItem();
								movietv.setText(movieList[wvBuyTicketMovie.getCurrentItem()]);
								movieSelectName = movieList[wvBuyTicketMovie.getCurrentItem()];
								movieSelectId = movieId.get(wvBuyTicketMovie.getCurrentItem());
								movietv.setEnabled(true);
								lnrwheelMovie.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagt = false;
								mvalue = 0;
								wvBuyTicketMovie.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketMovie.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){
							movieIndex = wvBuyTicketMovie.getCurrentItem();
							movietv.setText(movieList[wvBuyTicketMovie.getCurrentItem()]);
							movieSelectName = movieList[wvBuyTicketMovie.getCurrentItem()];
							movieSelectId = movieId.get(wvBuyTicketMovie.getCurrentItem());
							movietv.setEnabled(true);
							lnrwheelMovie.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							mvalue = 0;
							wvBuyTicketMovie.setViewAdapter(null);
						}
						
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketMovie.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);
				
			}
		});
		
	}
	
	protected void initCityList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), cityNameList, 0);
		
		wvBuyTicketCity.setViewAdapter(buyTicketAdapter);
		if(citytv.getText().toString().contains("Select")){
			wvBuyTicketCity.setCurrentItem(0);
			cityIndex = 0;
		}else{
			wvBuyTicketCity.setCurrentItem(cityIndex);
		}
		
		wvBuyTicketCity.addChangingListener(this);
		citytv.setText(cityNameList[wvBuyTicketCity.getCurrentItem()]);
		citySelectName = cityNameList[wvBuyTicketCity.getCurrentItem()];
		citySelectId = cityIdList.get(wvBuyTicketCity.getCurrentItem());
		wvBuyTicketCity.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketCity.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								cityIndex = wvBuyTicketCity.getCurrentItem();
								citytv.setText(cityNameList[wvBuyTicketCity.getCurrentItem()]);
								citySelectName = cityNameList[wvBuyTicketCity.getCurrentItem()];
								citySelectId = cityIdList.get(wvBuyTicketCity.getCurrentItem());
								citytv.setEnabled(true);
								lnrwheelCity.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagt = false;
								cvalue = 0;
								wvBuyTicketCity.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCity.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){
							cityIndex = wvBuyTicketCity.getCurrentItem();
							citytv.setText(cityNameList[wvBuyTicketCity.getCurrentItem()]);
							citySelectName = cityNameList[wvBuyTicketCity.getCurrentItem()];
							citySelectId = cityIdList.get(wvBuyTicketCity.getCurrentItem());
							citytv.setEnabled(true);
							lnrwheelCity.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							cvalue = 0;
							wvBuyTicketCity.setViewAdapter(null);
						}
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCity.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);
				
			}
		});
		
	}
	
	protected void initCinemaList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), cinemaNameList, 0);
		
		wvBuyTicketCinema.setViewAdapter(buyTicketAdapter);
		if(cinematv.getText().toString().contains("Select")){
			wvBuyTicketCinema.setCurrentItem(0);
			cinemaIndex = 0;
		}else{
			wvBuyTicketCinema.setCurrentItem(cinemaIndex);
		}
		
		wvBuyTicketCinema.addChangingListener(this);
		cinematv.setText(cinemaNameList[wvBuyTicketCinema.getCurrentItem()]);
		cinemaSelectName = cinemaNameList[wvBuyTicketCinema.getCurrentItem()];
		cinemaSelectId = cinemaIdList.get(wvBuyTicketCinema.getCurrentItem());
		wvBuyTicketCinema.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagcnema = true;
				wvBuyTicketCinema.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								cinemaIndex = wvBuyTicketCinema.getCurrentItem();
								cinematv.setText(cinemaNameList[wvBuyTicketCinema.getCurrentItem()]);
								cinemaSelectName = cinemaNameList[wvBuyTicketCinema.getCurrentItem()];
								cinemaSelectId = cinemaIdList.get(wvBuyTicketCinema.getCurrentItem());
								cinematv.setEnabled(true);
								lnrwheelCinema.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagcnema = false;
								cinemavalue = 0;
								wvBuyTicketCinema.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCinema.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagcnema == false){
							cinemaIndex = wvBuyTicketCinema.getCurrentItem();
							cinematv.setText(cinemaNameList[wvBuyTicketCinema.getCurrentItem()]);
							cinemaSelectName = cinemaNameList[wvBuyTicketCinema.getCurrentItem()];
							cinemaSelectId = cinemaIdList.get(wvBuyTicketCinema.getCurrentItem());
							cinematv.setEnabled(true);
							lnrwheelCinema.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							cinemavalue = 0;
							wvBuyTicketCinema.setViewAdapter(null);
						}
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCinema.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagcnema = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagcnema = false;
					}
				}, 100);
			}
		});
		
	}
	
	protected void initCinemaTypeList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), cinemaTypeNameList, 0);
		
		wvBuyTicketCinemaType.setViewAdapter(buyTicketAdapter);
		if(cinemaTypetv.getText().toString().contains("Select")){
			wvBuyTicketCinemaType.setCurrentItem(0);
			cinemaTypeIndex = 0;
		}else{
			wvBuyTicketCinemaType.setCurrentItem(cinemaTypeIndex);
		}
		
		wvBuyTicketCinemaType.addChangingListener(this);
		cinemaTypetv.setText(cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()]);
		cinemaTypeSelectName = cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()];
		cinemaTypeSelectId = cinemaTypeIdList.get(wvBuyTicketCinemaType.getCurrentItem());
		wvBuyTicketCinemaType.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagcnemaType = true;
				wvBuyTicketCinemaType.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								cinemaTypeIndex = wvBuyTicketCinemaType.getCurrentItem();
								cinemaTypetv.setText(cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()]);
								cinemaTypeSelectName = cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()];
								cinemaTypeSelectId = cinemaTypeIdList.get(wvBuyTicketCinemaType.getCurrentItem());
								cinemaTypetv.setEnabled(true);
								lnrwheelCinemaType.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagcnemaType = false;
								cinemaTypevalue = 0;
								wvBuyTicketCinemaType.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCinemaType.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagcnemaType == false){
							cinemaTypeIndex = wvBuyTicketCinemaType.getCurrentItem();
							cinemaTypetv.setText(cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()]);
							cinemaTypeSelectName = cinemaTypeNameList[wvBuyTicketCinemaType.getCurrentItem()];
							cinemaTypeSelectId = cinemaTypeIdList.get(wvBuyTicketCinemaType.getCurrentItem());
							cinemaTypetv.setEnabled(true);
							lnrwheelCinemaType.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							cinemaTypevalue = 0;
							wvBuyTicketCinemaType.setViewAdapter(null);
						}
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCinemaType.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagcnemaType = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagcnemaType = false;
					}
				}, 100);
			}
		});
		
	}
	
	protected void initDateList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), showDateList, 0);
		
		wvBuyTicketDate.setViewAdapter(buyTicketAdapter);
		if(datetv.getText().toString().contains("Select")){
			wvBuyTicketDate.setCurrentItem(0);
			dateIndex = 0;
		}else{
			wvBuyTicketDate.setCurrentItem(dateIndex);
		}
		
		wvBuyTicketDate.addChangingListener(this);
		datetv.setText(showDateList[wvBuyTicketDate.getCurrentItem()]);
		dateSelect = showDateList[wvBuyTicketDate.getCurrentItem()];
		dateSelectId = scheduleIdList.get(wvBuyTicketDate.getCurrentItem());
		wvBuyTicketDate.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagdate = true;
				wvBuyTicketDate.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								dateIndex = wvBuyTicketDate.getCurrentItem();
								datetv.setText(showDateList[wvBuyTicketDate.getCurrentItem()]);
								dateSelect = showDateList[wvBuyTicketDate.getCurrentItem()];
								dateSelectId = scheduleIdList.get(wvBuyTicketDate.getCurrentItem());
								datetv.setEnabled(true);
								lnrwheelDate.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagdate = false;
								datevalue = 0;
								wvBuyTicketDate.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketDate.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagdate == false){
							dateIndex = wvBuyTicketDate.getCurrentItem();
							datetv.setText(showDateList[wvBuyTicketDate.getCurrentItem()]);
							dateSelect = showDateList[wvBuyTicketDate.getCurrentItem()];
							dateSelectId = scheduleIdList.get(wvBuyTicketDate.getCurrentItem());
							datetv.setEnabled(true);
							lnrwheelDate.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							datevalue = 0;
							wvBuyTicketDate.setViewAdapter(null);
						}
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketDate.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagdate = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagdate = false;
					}
				}, 100);
			}
		});
		
	}
	
	protected void initTimeList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), TimeList, 0);
		
		wvBuyTicketTime.setViewAdapter(buyTicketAdapter);
		if(timetv.getText().toString().contains("Select")){
			wvBuyTicketTime.setCurrentItem(0);
			timeIndex = 0;
		}else{
			wvBuyTicketTime.setCurrentItem(timeIndex);
		}
		wvBuyTicketTime.addChangingListener(this);
		
		Log.d("kartick", "kartick" +TimeList[wvBuyTicketTime.getCurrentItem()]);
		timetv.setText(TimeList[wvBuyTicketTime.getCurrentItem()]);
		timeSelect = TimeList[wvBuyTicketTime.getCurrentItem()];
		timeSelectId = timeScheduleIdList.get(wvBuyTicketTime.getCurrentItem());
		timeScheduleId = timeSelectId;
		wvBuyTicketTime.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagtime = true;
				wvBuyTicketTime.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								timeIndex = wvBuyTicketTime.getCurrentItem();
								timetv.setText(TimeList[wvBuyTicketTime.getCurrentItem()]);
								timeSelect = TimeList[wvBuyTicketTime.getCurrentItem()];
								timeSelectId = timeScheduleIdList.get(wvBuyTicketTime.getCurrentItem());
								
								timeScheduleId = timeScheduleIdList.get(wvBuyTicketTime.getCurrentItem());
								
								timetv.setEnabled(true);
								lnrwheelTime.setVisibility(View.GONE);
								scrollView.fullScroll(View.FOCUS_UP);
								flagtime = false;
								timevalue = 0;
								wvBuyTicketTime.setViewAdapter(null);
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		});
		
		wvBuyTicketTime.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagtime == false){
							timeIndex = wvBuyTicketTime.getCurrentItem();
							timetv.setText(TimeList[wvBuyTicketTime.getCurrentItem()]);
							timeSelect = TimeList[wvBuyTicketTime.getCurrentItem()];
							timeSelectId = timeScheduleIdList.get(wvBuyTicketTime.getCurrentItem());
							
							timeScheduleId = timeScheduleIdList.get(wvBuyTicketTime.getCurrentItem());
							
							timetv.setEnabled(true);
							lnrwheelTime.setVisibility(View.GONE);
							scrollView.fullScroll(View.FOCUS_UP);
							timevalue = 0;
							wvBuyTicketTime.setViewAdapter(null);
						}
					}
				}, 200);
				return false;
			}
		});
		
		wvBuyTicketTime.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagtime = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagtime = false;
					}
				}, 100);
			}
		});
		
	}

	@SuppressWarnings("static-access")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buy_ticket_movie_tv:
			citytv.setEnabled(true);
			cinematv.setEnabled(true);
			cinemaTypetv.setEnabled(true);
			datetv.setEnabled(true);
			timetv.setEnabled(true);
			lnrwheelCity.setVisibility(View.GONE);
			lnrwheelCinema.setVisibility(View.GONE);
			lnrwheelCinemaType.setVisibility(View.GONE);
			lnrwheelDate.setVisibility(View.GONE);
			lnrwheelTime.setVisibility(View.GONE);
			mvalue = 1;
			mdilgck = 1;
			cityDchk = 0;
			cinemaDchk = 0;
			cinemaTypeDchk = 0;
			dateDchk = 0;
			lnrwheelMovie.setVisibility(View.VISIBLE);
			movietv.setEnabled(false);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					scrollView.fullScroll(View.FOCUS_DOWN);
				}
			}, 100);
			initMovieList();
			citytv.setText("Select City");
			cinematv.setText("Select Cinema");
			cinemaTypetv.setText("Select Type");
			datetv.setText("Select Date");
			timetv.setText("Select Time");
			break;
		case R.id.buy_ticket_city_tv:
			if(mdilgck == 0){
				showDialog("Please Select a Movie");
			}else{
				movietv.setEnabled(true);
				cinematv.setEnabled(true);
				cinemaTypetv.setEnabled(true);
				datetv.setEnabled(true);
				timetv.setEnabled(true);
				lnrwheelCinema.setVisibility(View.GONE);
				lnrwheelCinemaType.setVisibility(View.GONE);
				lnrwheelDate.setVisibility(View.GONE);
				lnrwheelTime.setVisibility(View.GONE);
				wvBuyTicketMovie.setViewAdapter(null);
				cvalue = 1;
				cityDchk = 1;
				cinemaDchk = 0;
				cinemaTypeDchk = 0;
				dateDchk = 0;
				lnrwheelMovie.setVisibility(View.GONE);
				citytv.setEnabled(false);
				callForCityList(pref.getDeviceId(), movieSelectId);
				System.out.println("callForCityList::://" +pref.getDeviceId()+ "//"+movieSelectId);
				cinematv.setText("Select Cinema");
				cinemaTypetv.setText("Select Type");
				datetv.setText("Select Date");
				timetv.setText("Select Time");
			}
			break;
		case R.id.buy_ticket_cinema_tv:
			if(cityDchk == 0){
				showDialog("Please Select a City");
			}else{
				citytv.setEnabled(true);
				cinemaTypetv.setEnabled(true);
				datetv.setEnabled(true);
				timetv.setEnabled(true);
				lnrwheelCinemaType.setVisibility(View.GONE);
				lnrwheelDate.setVisibility(View.GONE);
				lnrwheelTime.setVisibility(View.GONE);
				wvBuyTicketCity.setViewAdapter(null);
				cinemavalue = 1;
				cinemaDchk = 1;
				cinemaTypeDchk = 0;
				dateDchk = 0;
				lnrwheelCity.setVisibility(View.GONE);
				cinematv.setEnabled(false);
				callForCinemaList(pref.getDeviceId(), movieSelectId, citySelectId);
				System.out.println("callForCinemaList::://" +pref.getDeviceId()+ "//"+movieSelectId+"//"+citySelectId);
				cinemaTypetv.setText("Select Type");
				datetv.setText("Select Date");
				timetv.setText("Select Time");
			}
			break;
		case R.id.buy_ticket_cinema_type_tv:
			if(cinemaDchk == 0){
				showDialog("Please Select a Cinema");
			}else{
				cinematv.setEnabled(true);
				datetv.setEnabled(true);
				timetv.setEnabled(true);
				lnrwheelDate.setVisibility(View.GONE);
				lnrwheelTime.setVisibility(View.GONE);
				wvBuyTicketCinema.setViewAdapter(null);
				cinemaTypevalue = 1;
				cinemaTypeDchk = 1;
				dateDchk = 0;
				lnrwheelCinema.setVisibility(View.GONE);
				cinemaTypetv.setEnabled(false);
				callForCinemaTypeList(cinemaSelectId);
				System.out.println("callForCinemaTypeList::://" +cinemaSelectId);
				datetv.setText("Select Date");
				timetv.setText("Select Time");
			}
			break;
		case R.id.buy_ticket_date_tv:
			if(cinemaTypeDchk == 0){
				showDialog("Please Select a Cinema Type");
			}else{
				cinemaTypetv.setEnabled(true);
				timetv.setEnabled(true);
				lnrwheelTime.setVisibility(View.GONE);
				wvBuyTicketCinemaType.setViewAdapter(null);
				datevalue = 1;
				dateDchk = 1;
				lnrwheelCinemaType.setVisibility(View.GONE);
				datetv.setEnabled(false);
				callForDateListList(movieSelectId, citySelectId, cinemaSelectId, cinemaTypeSelectId);
				System.out.println("callForDateListList::://" +movieSelectId+"//"+citySelectId+"//"+cinemaSelectId+"//"+cinemaTypeSelectId);
				
				timetv.setText("Select Time");
			}
			break;
		case R.id.buy_ticket_time_tv:
			if(dateDchk == 0){
				showDialog("Please Select a Date");
			}else{
				datetv.setEnabled(true);
				wvBuyTicketDate.setViewAdapter(null);
				timevalue = 1;
				lnrwheelDate.setVisibility(View.GONE);
				timetv.setEnabled(false);
				callForTimeList(movieSelectId, citySelectId, cinemaSelectId, cinemaTypeSelectId, dateSelectId);
				System.out.println("callForTimeList::://" +movieSelectId+"//"+citySelectId+"//"+cinemaSelectId+"//"+cinemaTypeSelectId+"//"+dateSelectId);
			}
			break;
		case R.id.buy_ticket_continue_btn:
			if(timeSelect != null && !timetv.getText().toString().trim().equals("Select Time")){
				BuyTicketsTo buyTo = new BuyTicketsTo();
				buyTo.setMovieName(movieSelectName);
				buyTo.setCityName(citySelectName);
				buyTo.setCinemaName(cinemaSelectName);
				buyTo.setCinemaTypeName(cinemaTypeSelectName);
				buyTo.setDate(dateSelect);
				buyTo.setTime(timeSelect);
				movieName = movieSelectName;
				movieIdV = movieSelectId;
				cityName = citySelectName;
				cinemaNameVV = cinemaSelectName;
				cinemaNameV = cinemaSelectName;
				cinemaType = cinemaTypeSelectName;
				cinemaTypeV = cinemaTypeSelectName;
				showDateV = dateSelect;
				timeV = timeSelect;
				System.out.println(movieSelectName+":"+citySelectName+":"+cinemaSelectName+":"+cinemaTypeSelectName+":"+dateSelect+":"+timeSelect);
				Fragment frag = new SeatLayoutFragment(timeScheduleId, buyTo, movieSelectId);
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (frag != null) {
					activity.stopTimer();
					pref.setTimerTime("600000");
					activity.setTimer(Integer.parseInt(pref.getTimerTime()));
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(frag);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, frag, "fragment");
					ft.commit();
				}
			}else{
				showDialog("Please Select a Time");
			}
			
			break;
		
		}
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		System.out.println("oldValue:::" +oldValue+"newValue::::"+ newValue);
		if(mvalue == 1 && newValue < movieList.length){
			movieIndex = newValue;
			movietv.setText(movieList[newValue]);
			movieSelectName = movieList[newValue];
			movieSelectId = movieId.get(newValue);
			mvalue = 0;
			
		}else if (cvalue == 1 && newValue < cityNameList.length) {
			cityIndex = newValue;
			citytv.setText(cityNameList[newValue]);
			citySelectName = cityNameList[newValue];
			citySelectId = cityIdList.get(newValue);
			cvalue = 0;
		}else if(cinemavalue == 1 && newValue < cinemaNameList.length){
			cinemaIndex = newValue;
			cinematv.setText(cinemaNameList[newValue]);
			cinemaSelectName = cinemaNameList[newValue];
			cinemaSelectId = cinemaIdList.get(newValue);
			cinemavalue = 0;
		}else if(cinemaTypevalue == 1 && newValue < cinemaTypeNameList.length){
			cinemaTypeIndex = newValue;
			cinemaTypetv.setText(cinemaTypeNameList[newValue]);
			cinemaTypeSelectName = cinemaTypeNameList[newValue];
			cinemaTypeSelectId = cinemaTypeIdList.get(newValue);
			cinemaTypevalue = 0;
		}else if(datevalue == 1 && newValue < showDateList.length){
			dateIndex = newValue;
			datetv.setText(showDateList[newValue]);
			dateSelect = showDateList[newValue];
			dateSelectId = scheduleIdList.get(newValue);
			datevalue = 0;
		}else if(timevalue == 1 && newValue < TimeList.length){
			timeIndex = newValue;
			timetv.setText(TimeList[newValue]);
			timeSelect = TimeList[newValue];
			timeSelectId = timeScheduleIdList.get(newValue);
			timeScheduleId = timeSelectId;
			timevalue = 0;
		}
		
	}
	
	
	public void callForCityListDefault(String deviceId, String movieId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cityIdList = new ArrayList<String>();
				
				try{
					JSONObject mainJson = new JSONObject(Data.toString());
					System.out.println("BuyTicket City List: " +mainJson.toString());
					
					JSONArray cityArray = mainJson.optJSONArray(BuyTicketConstant.CITY_ARRAY);
					cityNameList = new String[cityArray.length()];
					for(int i = 0; i < cityArray.length(); i++){
						
						JSONObject cityObj = cityArray.optJSONObject(i);
						String cityName = cityObj.optString(BuyTicketConstant.CITY_NAME);
						String cityId = cityObj.optString(BuyTicketConstant.CITY_ID);
						
						cityNameList[i] = cityName;
						cityIdList.add(cityId);
					}
					if(!cityName.equals("") && cityName != null){
						for(int i = 0; i < cityNameList.length; i++){
							if(cityNameList[i].contains(cityName)){
//								wvBuyTicketMovie.setCurrentItem(i);
								cityIndex = i;
								cityDchk = 1;
								citySelectId = cityIdList.get(i);
								citytv.setText(cityName);
								
								if(!cinemaNameV.equals("") && cinemaNameV != null){
									chkloading = 0;
									callForCinemaListDefault(pref.getDeviceId(), movieSelectId, citySelectId);
								}
								break;
							}

						}
					}
					
					/*if(cityArray.length() > 0){
						lnrwheelCity.setVisibility(View.VISIBLE);
						initCityList();
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								scrollView.fullScroll(View.FOCUS_DOWN);
							}
						}, 100);
					}else{
						showDialog("No Data Found");
						citytv.setEnabled(true);
					}*/
					
				}catch(Exception e){
					
				}
				activity.setNotClickable();
				if(chkloading == 1){
					loderLnr.setVisibility(View.VISIBLE);
					chkloading = 0;
				}
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetCityListByMovieAsync(deviceId, movieId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

	public void callForCinemaListDefault(String deviceId, String movieId, String cityCode) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cinemaIdList = new ArrayList<String>();
				
				try {
					JSONObject mainJson = new JSONObject(Data.toString());
					System.out.println("BuyTicket Cinema List: " +mainJson.toString());
					
					JSONArray cinemaArray = mainJson.optJSONArray(BuyTicketConstant.CINEMA_ARRAY);
					cinemaNameList = new String[cinemaArray.length()];
					for(int i = 0; i < cinemaArray.length(); i++){
						
						JSONObject cinemaObj = cinemaArray.optJSONObject(i);
						String cinemaName = cinemaObj.optString(BuyTicketConstant.CINEMA_NAME);
						String cinemaId = cinemaObj.optString(BuyTicketConstant.CINEMA_ID);
						
						cinemaNameList[i] = cinemaName;
						cinemaIdList.add(cinemaId);
					}
					
					if(!cinemaNameV.equals("") && cinemaNameV != null){
						for(int i = 0; i < cinemaNameList.length; i++){
							if(cinemaNameList[i].contains(cinemaNameV)){
//								wvBuyTicketMovie.setCurrentItem(i);
								cinemaIndex = i;
								cinemaDchk = 1;
								cinemaSelectId = cinemaIdList.get(i);
								cinematv.setText(cinemaNameV);
								cinemaNameVV = cinemaNameV;
								cinemaSelectName = cinemaNameV;
								if(!cinemaType.equals("") && cinemaType != null){
									chkloading = 1;
									callForCinemaTypeListDefalt(cinemaSelectId);
								}
								break;
							}

						}
					}
					
					/*if(cinemaArray.length() > 0){
						lnrwheelCinema.setVisibility(View.VISIBLE);
						initCinemaList();
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								scrollView.fullScroll(View.FOCUS_DOWN);
							}
						}, 100);
					}else{
						showDialog("No Data Found");
						cinematv.setEnabled(true);
					}*/
				} catch (Exception e) {

				}
				activity.setNotClickable();
				if(chkloading == 0){
					loderLnr.setVisibility(View.VISIBLE);
					chkloading = 0;
				}
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetCinemaListByMovieCityAsync(deviceId, movieId, cityCode, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void callForCinemaTypeListDefalt(String cinemaId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				cinemaTypeIdList = new ArrayList<String>();
				System.out.println("Response: "+Data.toString());
				try {
					JSONObject mainJson = new JSONObject(Data.toString());
					System.out.println("BuyTicket CinemaType List: " +mainJson.toString());
					JSONArray cinemaTypeArray = mainJson.optJSONArray(BuyTicketConstant.CINEMA_TYPE_ARRAY);
					cinemaTypeNameList = new String[cinemaTypeArray.length()];
					for(int i = 0; i < cinemaTypeArray.length(); i++){
						
						JSONObject cinemaTypeObj = cinemaTypeArray.optJSONObject(i);
						String cinemaTypeName = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_NAME);
						String cinemaTypeIcon = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_ICON);
						String cinemaTypeId = cinemaTypeObj.optString(BuyTicketConstant.CINEMA_TYPE_ID);
						
						cinemaTypeNameList[i] = cinemaTypeName;
						cinemaTypeIdList.add(cinemaTypeId);
					}
					
					if(!cinemaType.equals("") && cinemaType != null){
						for(int i = 0; i < cinemaTypeNameList.length; i++){
							if(cinemaTypeNameList[i].contains(cinemaType)){
//								wvBuyTicketMovie.setCurrentItem(i);
								cinemaTypeIndex = i;
								cinemaTypeDchk = 1;
								cinemaTypeSelectId = cinemaTypeIdList.get(i);
								cinemaTypetv.setText(cinemaType);
								cinemaTypeV = cinemaType;
								cinemaTypeSelectName = cinemaType;
								if(!showDate.equals("") && showDate != null){
									chkloading = 0;
									callForDateListDefalt(movieSelectId, citySelectId, cinemaSelectId, cinemaTypeSelectId);
								}
								break;
							}

						}
					}else{
						showDialog("No cinema types are available");
						cinemaTypetv.setEnabled(true);
					}
				} catch (Exception e) {

				}
				activity.setNotClickable();
				if(chkloading == 1){
					loderLnr.setVisibility(View.VISIBLE);
					chkloading = 0;
				}
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetCinemaTypeAsync(cinemaId, movieSelectId, citySelectId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	public void callForDateListDefalt(String movieId, String cityCode, String cinemaId, String cinemaClassIdField) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				scheduleIdList = new ArrayList<String>();
				
				try {
					JSONObject mainJson = new JSONObject(Data.toString());
					System.out.println("BuyTicket Date List: " +mainJson.toString());
					JSONArray dateArray = mainJson.optJSONArray(BuyTicketConstant.DATE_ARRAY);
					showDateList = new String[dateArray.length()];
                    String __copyShowDate = showDate;
					for(int i = 0; i < dateArray.length(); i++){
						JSONObject dateObj = dateArray.optJSONObject(i);
						String showDate = dateObj.optString(BuyTicketConstant.SHOW_DATE);
						String scheduleId = dateObj.optString(BuyTicketConstant.SCHEDULE_ID);
						showDateList[i] = showDate;
						scheduleIdList.add(scheduleId);
					}



                    //String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

					if(!showDate.equals("") && showDate != null){
						for(int i = 0; i < showDateList.length; i++){
							if(showDateList[i].contains(showDate)){
								dateIndex = i;
								dateDchk = 1;
								dateSelectId = scheduleIdList.get(i);
								datetv.setText(showDate);
								showDateV = showDate;
								dateSelect = showDate;
								
								if(timeValuee.trim().length() > 0){
//									timetv.setText(timeValuee);
									callForTimeListDefault(movieSelectId, citySelectId, cinemaSelectId, cinemaTypeSelectId, dateSelectId);
								}
								break;
							}

						}
					}
					
				} catch (Exception e) {

				}
				activity.setNotClickable();
				loderLnr.setVisibility(View.VISIBLE);
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetShowDateAsync(movieId, cityCode, cinemaId, cinemaClassIdField, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void callForTimeListDefault(String movieId, String cityCode, String cinemaId, String cinemaClassIdField, String scheduleId) {
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {

			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				timeScheduleIdList = new ArrayList<String>();
                if(Data.toString().trim().length() > 1){
                	try {
    					JSONObject mainJson = new JSONObject(Data.toString());
    					System.out.println("BuyTicket Time List: " +mainJson.toString());
    					JSONArray timeArray = mainJson.optJSONArray(BuyTicketConstant.TIME_ARRAY);
    					TimeList = new String[timeArray.length()];
    					for(int i = 0; i < timeArray.length(); i++){
    						
    						JSONObject timeObj = timeArray.optJSONObject(i);
    						String TimeValue = timeObj.optString(BuyTicketConstant.TIME_VALUE);
    						String Time = timeObj.optString(BuyTicketConstant.TIME);
    						if(Time.contains(";")){
    							String[] time = Time.split(";");
    							TimeList[i] = time[0] + ")";
    						}else{
    							TimeList[i] = Time;
    						}
    						
    						timeScheduleIdList.add(TimeValue);
    						
    					}
    					
    					if(!timeValuee.equals("") && timeValuee != null){
    						for(int i = 0; i < TimeList.length; i++){
    							System.out.println(timeValuee.substring(0, 5)+":::::::"+TimeList[i].substring(0, 5));
    							if(TimeList[i].trim().substring(0, 5).equals(timeValuee.trim().substring(0, 5))){
    								
    								
    								Log.d("timevalue", "timevalue" +timevalue);
    								timetv.setText(timeValuee);
    								timeScheduleId = timeScheduleIdList.get(i);
    								timeSelect = timeValuee;
    								break;
    							}

    						}
    					}
    					
    				} catch (Exception e) {

    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}

			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub

			}
		});

		try {

			schedule.GetShowTimeAsync(movieId, cityCode, cinemaId, cinemaClassIdField, scheduleId, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
