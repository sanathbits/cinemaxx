package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.FavCinemasAdapter;
import com.cinemaxx.bean.CinemaClassTo;
import com.cinemaxx.bean.CinemaTo;
import com.cinemaxx.constant.CinemaConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FavCinemasFragment extends Fragment{
	
	TextView locationNameTv;
	ListView listView;
	CinemaxxPref pref;
	ArrayList<CinemaTo> dataList;
	FavCinemasAdapter adapter;
	MainActivity activity;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_fav_cinemas, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		
		locationNameTv.setText("Location: "+pref.getCityName());
		activity = (MainActivity) getActivity();

		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceLight = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.fav_cinemas_location_tv);
		locationNameTv.setTypeface(faceThin);
		TextView textTv = (TextView) getView().findViewById(R.id.fav_cinemas_text_tv); 
		textTv.setTypeface(faceLight);
		listView = (ListView) getView().findViewById(R.id.fav_cinemas_lv);
		pref = new CinemaxxPref(getActivity());
		locationNameTv.setText("Location: "+pref.getCityName());
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("CINEMAS");
	}
	
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub

				System.out.println("Fav Cinema: " +Data.toString());
				dataList = new ArrayList<CinemaTo>();
                if(Data.toString().trim().length() > 1){
                	try {

    					JSONObject mainObj = new JSONObject(Data.toString());
    					if(mainObj.optString(CinemaConstant.ERROR_DESC).equals("0")){
    						JSONArray mainArray = mainObj
    								.optJSONArray(CinemaConstant.CINEMA_ARRAY);
    						for (int i = 0; i < mainArray.length(); i++) {

    							JSONObject cinemaObj = mainArray.optJSONObject(i);
    							CinemaTo cinemaTo = new CinemaTo();
    							cinemaTo.setCinemaId(cinemaObj
    									.optString(CinemaConstant.CINEMA_ID));
    							cinemaTo.setCinemaName(cinemaObj
    									.optString(CinemaConstant.CINEMA_NAME));
    							cinemaTo.setMultiplexId(cinemaObj
    									.optString(CinemaConstant.MULTIPLEX_ID));
    							cinemaTo.setAddress(cinemaObj
    									.optString(CinemaConstant.ADDRESS));
    							cinemaTo.setCityId(cinemaObj
    									.optString(CinemaConstant.CITY_ID));
    							cinemaTo.setLocation(cinemaObj
    									.optString(CinemaConstant.LOCATION));
    							cinemaTo.setLatitude(cinemaObj
    									.optString(CinemaConstant.LATITUDE));
    							cinemaTo.setLongitude(cinemaObj
    									.optString(CinemaConstant.LONGITUDE));
    							cinemaTo.setFacs(cinemaObj
    									.optString(CinemaConstant.FACS));
    							cinemaTo.setImage(cinemaObj
    									.optString(CinemaConstant.THUMBNAIL));
    							cinemaTo.setCityName(cinemaObj
    									.optString(CinemaConstant.CITY_NAME));
    							cinemaTo.setPhone(cinemaObj
    									.optString(CinemaConstant.PHONE));
    							cinemaTo.setEmail(cinemaObj
    									.optString(CinemaConstant.EMAIL));
    							cinemaTo.setDistance(cinemaObj
    									.optString(CinemaConstant.DISTANCE));

    							JSONObject classObj = cinemaObj
    									.optJSONObject(CinemaConstant.CINEMA_CLASS_DATA);
    							JSONArray cinemaClassArray = classObj
    									.optJSONArray(CinemaConstant.CINEMA_CLASS_ARRAY);

    							ArrayList<CinemaClassTo> classList = new ArrayList<CinemaClassTo>();
    							for (int j = 0; j < cinemaClassArray.length(); j++) {

    								JSONObject classValObj = cinemaClassArray
    										.optJSONObject(j);
    								CinemaClassTo classTo = new CinemaClassTo();

    								classTo.setClassId(classValObj
    										.optString(CinemaConstant.CINEMA_CLASS_ID));
    								classTo.setClassName(classValObj
    										.optString(CinemaConstant.CINEMA_CLASS_NAME));

    								classList.add(classTo);
    							}
    							cinemaTo.setClassList(classList);

    							dataList.add(cinemaTo);

    						}

    						initListview();
    					}else{
    						/*MainActivity activity = (MainActivity) getActivity();
    						activity.showCustomAlert("Server is not working.");*/
    						AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any cinemas yet.", new DialogListener() {
    							
    							@Override
    							public void onOkClick() {
    								// TODO Auto-generated method stub
    								activity.callBackPress();
    							}
    						});
    						dialog.setCancelable(false);
    						dialog.show();
    					}
    					

    				} catch (Exception e) {
    					e.printStackTrace();
    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetFavouriteListAsync(pref.getDeviceId(), "2", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void callRemoveFavorite(String cinemaId, final int position){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response for removed: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
					dataList.remove(position);
					initListview();
//					Toast.makeText(getActivity(), "Successfully removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully removed \nfrom favoritelist.");*/
				}else{
//					Toast.makeText(getActivity(), "Not removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not removed from \nfavoritelist.");*/
					AlertDialog dialog = new AlertDialog(getActivity(),
							"Not removed from from \nyour favoritelist", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
//									activity.callBackPress();
								}
							});
					dialog.show();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.RemoveFromFavoritesAsync(pref.getDeviceId(), cinemaId, "2", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initListview(){
		
		adapter = new FavCinemasAdapter(getActivity(), dataList, FavCinemasFragment.this);
		listView.setAdapter(adapter);
		
		if(dataList.size() == 0){
			AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any cinemas yet.", new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
		/*ViewUtility.setListViewHeightBasedOnFavCinemaFragment(listView, getActivity());
		listView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	

}
