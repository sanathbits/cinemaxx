package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.FavMoviesAdapter;
import com.cinemaxx.bean.MovieTo;
import com.cinemaxx.constant.FavMoviesConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class FavMoviesFragment extends Fragment{

	TextView locationNameTv;
	GridView gridView;
	ArrayList<MovieTo> dataList;
	FavMoviesAdapter adapter;
	CinemaxxPref pref;
	MainActivity activity;
	ArrayList<String> movieIdList;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_fav_movies, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();

		locationNameTv.setText("Location: "+pref.getCityName());

		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}	
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceLight = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.fav_movies_location_tv);
		locationNameTv.setTypeface(faceThin);
		TextView textTv = (TextView) getView().findViewById(R.id.fav_movies_text_tv); 
		textTv.setTypeface(faceLight);
		gridView = (GridView) getView().findViewById(R.id.fav_movies_gv);
		dataList = new ArrayList<MovieTo>();
		pref = new CinemaxxPref(getActivity());
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("MOVIES");
		
	}
	
	public void initListener(){
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Fav Movies: "+Data.toString());
				dataList = new ArrayList<MovieTo>();
                if(Data.toString().trim().length() > 1){
                	try{
    					
    					JSONObject mainObj = new JSONObject(Data.toString());
    					if(mainObj.optString(FavMoviesConstant.ERROR_DESC).equals("0")){
    						JSONArray movieArray = mainObj.optJSONArray(FavMoviesConstant.MOVIES_ARRAY);
    						movieIdList = new ArrayList<String>();
    						for(int i = 0; i < movieArray.length(); i++){
    							
    							MovieTo movieTo = new MovieTo();
    							JSONObject movieObj = movieArray.optJSONObject(i);
    							
    							movieTo.setMovieId(movieObj.optString(FavMoviesConstant.MOVIE_ID));
    							movieTo.setMovieName(movieObj.optString(FavMoviesConstant.MOVIE_NAME));
    							movieTo.setMovieImage(movieObj.optString(FavMoviesConstant.MOVIE_IMAGE));
    							movieTo.setMovieRating(movieObj.optString(FavMoviesConstant.MOVIE_RATING));
    							movieTo.setMovieIsComing(movieObj.optString(FavMoviesConstant.MOVIE_IS_COMING));
    							
    							dataList.add(movieTo);
    							movieIdList.add(movieObj.optString(FavMoviesConstant.MOVIE_ID));
    						}
    						initGrid();
    					}else{
    						/*MainActivity activity = (MainActivity) getActivity();
    						activity.showCustomAlert("Server is not working.");*/
                            AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any movies yet.", new DialogListener() {
    							
    							@Override
    							public void onOkClick() {
    								// TODO Auto-generated method stub
    								activity.callBackPress();
    							}
    						});
                            dialog.setCancelable(false);
    						dialog.show();
    					}
    					
    				}catch(Exception e){
    					e.printStackTrace();
    				}
				}else{
					AlertDialog dialog = new AlertDialog(getActivity(),
							"No data available", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
									activity.callBackPress();
								}
							});
					dialog.show();
				}
				
				
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetFavouriteListAsync(pref.getDeviceId(), "1", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void callRemoveFavorite(String movieId, final int position, String status){
		
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response for removed: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
					dataList.remove(position);
					initGrid();
//					Toast.makeText(getActivity(), "Successfully removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Successfully removed \nfrom favoritelist.");*/
				}else{
//					Toast.makeText(getActivity(), "Not removed from favoritelist.", Toast.LENGTH_LONG).show();
					/*MainActivity activity = (MainActivity) getActivity();
					activity.showCustomAlert("Not removed from \nfavoritelist.");*/
					AlertDialog dialog = new AlertDialog(getActivity(),
							"Not removed from from \nyour favoritelist", new DialogListener() {

								@Override
								public void onOkClick() {
									// TODO Auto-generated method stub
//									activity.callBackPress();
								}
							});
					dialog.show();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.MoviesRemoveFromFavoritesAsync(pref.getDeviceId(), movieId, "1", status, getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initGrid(){
		
		adapter = new FavMoviesAdapter(getActivity(), dataList, movieIdList, FavMoviesFragment.this);
		gridView.setAdapter(adapter);
		
		if(dataList.size() == 0){
			AlertDialog dialog = new AlertDialog(getActivity(), "You haven't added any movies yet.", new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}

		/*ViewUtility.setListViewHeightBasedOnFavMovieFragment(gridView, getActivity());
		gridView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	
}
