package com.cinemaxx.activity;

import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

//@SuppressLint("NewApi")
public class HomeFragment extends Fragment{

	Button moviesBtn, cinemasBtn, promosBtn, newsBtn, cinemaxxGoldBtn, ultraXdBtn, favoritesBtn, historyBtn, maxxCardBtn,juniorbutton;
	TextView locationNameTv;
	CinemaxxPref pref;
	MainActivity activity;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        
        return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
		initView();
		initClickListener();
		
		
		locationNameTv.setText("Location: "+pref.getCityName());

		activity = (MainActivity) getActivity();

		CinemaxxPref pref = new CinemaxxPref(getActivity());

		String deviceId = Secure.getString(getActivity().getContentResolver(),
                Secure.ANDROID_ID);
		pref.setDeviceId(deviceId);
		activity.hideBackBtn();
		activity.exitStatus = true;
	}
	
	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		
		Typeface faceMedium = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface faceThin=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationNameTv = (TextView) getView().findViewById(R.id.home_location_tv);
		locationNameTv.setTypeface(faceThin);
		locationNameTv.setText("Location: "+pref.getCityName());
		moviesBtn = (Button) getView().findViewById(R.id.home_movies_btn);
		cinemasBtn = (Button) getView().findViewById(R.id.home_cinemas_btn);
		promosBtn = (Button) getView().findViewById(R.id.home_promos_btn);
		newsBtn = (Button) getView().findViewById(R.id.home_news_btn);
		cinemaxxGoldBtn = (Button) getView().findViewById(R.id.home_cinemaxx_gold_btn);
		ultraXdBtn = (Button) getView().findViewById(R.id.home_ultra_xd_btn);
		favoritesBtn = (Button) getView().findViewById(R.id.home_favorites_btn);
		historyBtn = (Button) getView().findViewById(R.id.home_history_btn);
		maxxCardBtn = (Button) getView().findViewById(R.id.home_max_card_btn);
		juniorbutton=(Button)getView().findViewById(R.id.home_junior_btn);
		
		MainActivity act = (MainActivity) getActivity();
		act.clickOnHome();
	}
	
	public void initClickListener(){
		
		moviesBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new MoviesFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		cinemasBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new CinemasFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		promosBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new PromoFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		newsBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new NewsFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		cinemaxxGoldBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new CinemaxxGoldFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		ultraXdBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new UltraXDFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		favoritesBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new FavoritesFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		historyBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new HistoryFragment();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		maxxCardBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = null;
				if(pref.getMaxxCardLoginResponse().trim().length() > 10){
					fragment = new MaxxCardWelcomeFragment(false);
				}else{
					fragment = new MaxxCardFragment();
				}
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer
				
				}
			}
		});
		juniorbutton.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fragment = new CinemaxxJunior();
				FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					activity.showBackBtn();
					activity.exitStatus = false;
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(fragment);
					ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");					
					ft.commit();					
				
				}
			}
		});

		
	}
	

}
