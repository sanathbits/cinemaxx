package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.bean.PromoDetailsTo;
import com.cinemaxx.constant.CinemaDetailsConstant;
import com.cinemaxx.constant.PromotionDetailsConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.ImageLoader;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PromoDetailsFragment extends Fragment{

	TextView locationNameTv, promoTitleTv, promoDescTv;
	ImageView promoImageIv;
	LinearLayout conainerLayout;
	CinemaxxPref pref;
	ArrayList<PromoDetailsTo> dataList;
	ImageLoader imageLoader;
	Button addToFavLay;
	String promoId = "";
	MainActivity activity;
	String isFavourite = "";
	WebView webView;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_promos_details, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		activity = (MainActivity) getActivity();
		
		locationNameTv.setText("Location: "+pref.getCityName());
		initListener();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}

	public void initView(){
		
		pref = new CinemaxxPref(getActivity());
		imageLoader = new ImageLoader(getActivity());
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf"); 
		locationNameTv = (TextView) getView().findViewById(R.id.promos_details_setlocation_txt);
		locationNameTv.setTypeface(faceThin);
		locationNameTv.setText("Location: "+pref.getCityName());
		promoTitleTv = (TextView) getView().findViewById(R.id.promos_details_name_txt);
		promoImageIv = (ImageView) getView().findViewById(R.id.promos_details_promo_img);
//		promoDescTv = (TextView) getView().findViewById(R.id.promos_details_description_txt);
//		promoDescTv.setTypeface(face);
		webView = (WebView) getView().findViewById(R.id.promo_details_webview);
		
		conainerLayout = (LinearLayout) getView().findViewById(R.id.promo_fragment_containr_layout);
		addToFavLay = (Button) getView().findViewById(R.id.promos_details_addToFaver_rel);
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("PROMO");
	}
	
	public void initListener(){
		
		addToFavLay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFavourite.equals("") && isFavourite.equals("False")){
					callAddFavorite(promoId);
					
					
				}else{
					callRemoveFavorite(promoId);
					
				}
				
			}
		});
		
	}
	
	
	public void callRemoveFavorite(String promotionId){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
					AlertDialog dialog = new AlertDialog(getActivity(), "Successfully removed from your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "False";
					addToFavLay.setText("Add To Favorites");
//					Toast.makeText(getActivity(), "Successfully added to favorite list.", Toast.LENGTH_LONG).show();
				}else{
                     AlertDialog dialog = new AlertDialog(getActivity(), "Successfully removed from your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
//					Toast.makeText(getActivity(), "Not added to favorite list.", Toast.LENGTH_LONG).show();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.RemoveFromFavoritesAsync(pref.getDeviceId(), promotionId, "3", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void callAddFavorite(String promotionId){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Response: " +Data.toString());
				if(Data.toString().trim().equals("Success")){
					AlertDialog dialog = new AlertDialog(getActivity(), "Successfully added to your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
					isFavourite = "True";
					addToFavLay.setText("Remove From Favorites");
//					Toast.makeText(getActivity(), "Successfully added to favorite list.", Toast.LENGTH_LONG).show();
				}else{
                     AlertDialog dialog = new AlertDialog(getActivity(), "Not added to your favorite", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
//							activity.callBackPress();
						}
					});
					dialog.show();
//					Toast.makeText(getActivity(), "Not added to favorite list.", Toast.LENGTH_LONG).show();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.AddToFavoritesAsync(pref.getDeviceId(), promotionId, "3", getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				
				System.out.println("Promo Details: "+Data.toString());
				conainerLayout.setVisibility(View.VISIBLE);
				dataList = new ArrayList<PromoDetailsTo>();
				try{
					
					JSONObject mainObj = new JSONObject(Data.toString());
					if(mainObj.optString(PromotionDetailsConstant.ERROR_DESC).equals("0")){
						JSONArray promoArray = mainObj.optJSONArray(PromotionDetailsConstant.PROMO_ARRAY);
						for(int i = 0; i < promoArray.length(); i++){
							
							PromoDetailsTo promoTo = new PromoDetailsTo();
							JSONObject promoObj = promoArray.optJSONObject(i);
							
							promoId = promoObj.optString(PromotionDetailsConstant.PROMO_ID);
							promoTitleTv.setText(promoObj.optString(PromotionDetailsConstant.PROMO_TITLE));
//							promoDescTv.setText(Html.fromHtml(promoObj.optString(PromotionDetailsConstant.PROMO_DESC)));
							webView.loadData(Utils.getTncWebview(promoObj.optString(PromotionDetailsConstant.PROMO_DESC).trim()), "text/html; charset=UTF-8", null);
							imageLoader.DisplayImage(promoObj.optString(PromotionDetailsConstant.PROMO_IMAGE).replace(" ", "%20").trim(), promoImageIv);
							
							isFavourite = promoObj.optString(PromotionDetailsConstant.IS_VALID);
							if(!isFavourite.equals("") && isFavourite.equals("False")){
								addToFavLay.setText("Add To Favorites");
							}else{
								addToFavLay.setText("Remove From Favorites");
							}
							
						}
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
                        AlertDialog dialog = new AlertDialog(getActivity(), "Promo details are not available", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}
					
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
			schedule.GetPromoDetailAsync(pref.getDeviceId() ,pref.getPromoId(), getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}
