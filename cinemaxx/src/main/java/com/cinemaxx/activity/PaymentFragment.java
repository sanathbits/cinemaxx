package com.cinemaxx.activity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kobjects.base64.Base64;

import com.cinemaxx.adapter.BuyTicketAdapter;
import com.cinemaxx.bean.HistoryDescTo;
import com.cinemaxx.bean.OrderTo;
import com.cinemaxx.bean.SeatLayoutTo;
import com.cinemaxx.constant.PaymentConstant;
import com.cinemaxx.constant.UserDetailsConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.YesNoDialogBig;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.Card;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class PaymentFragment extends Fragment implements OnWheelChangedListener{

	EditText cardNoEt, nameEt, cvvNoEt;
	Button continueBtn;
	TextView monthSpinner, yearSpinner, locationTv;
	String[] monthArray, monthArrayVal, yearArray;
	@SuppressWarnings("rawtypes")
	ArrayAdapter monthAdapter, yearAdapter;
	String monthVal = "", yearVal = "";
	CinemaxxPref pref;
	MainActivity activity;
	
	SeatLayoutTo seatTo;
	String pricePerTicket = "";
	OrderTo orderTo;
	String selectedSeat;
	String name, email, mobile;
	public final static String EXTRA_MESSAGE = "Ini extra message.";
	public final static String CLIENT_KEY = "0e02521f-8325-4833-943b-12dd25475b5b";
//	public final static String CLIENT_KEY = "1b866626-fa91-4b09-b196-d0fcfc5a2794";
	CountDownTimer cTimer;
	
	LinearLayout lnrwheelCity, lnrwheelCityYear;
	BuyTicketAdapter buyTicketAdapter;
	private WheelView wvBuyTicketCity, wvBuyTicketCityYear;
	boolean flagt = false, flagtt = false;
	int mvalue = 0, cvalue = 0;
	int cityIndex = 0;
	boolean flagtY = false, flagttY = false;
	int mvalueY = 0, cvalueY = 0;
	int cityIndexY = 0;
	
	
	public PaymentFragment(SeatLayoutTo seatTo, OrderTo orderTo, String selectedSeat, String name, String email, String mobile){
		this.seatTo = seatTo;
		this.orderTo = orderTo;
		this.selectedSeat = selectedSeat;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initView();
		activity = (MainActivity) getActivity();
		clickListener();
		
		
		
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setTimer(Integer.parseInt(pref.getTimerTime()));
		
		activity.clickOnBuyTickets("PAYMENT PAGE");
		
		
		int itemPrice = Integer.parseInt(orderTo.getOrderAmount()) / Integer.parseInt(orderTo.getNoOfSeat());
	    pricePerTicket = String.valueOf(itemPrice);
	    
	    System.out.println(orderTo.getOrderAmount()+"?????????????????????????????????"+pricePerTicket);
		
	}

	public void initView(){
		
		lnrwheelCity = (LinearLayout) getView().findViewById(R.id.lnrwheelCinema);
		lnrwheelCityYear = (LinearLayout) getView().findViewById(R.id.lnrwheelCinemaYear);
//		lnrwheelCity.setVisibility(View.VISIBLE);
		wvBuyTicketCity = (WheelView) getView().findViewById(R.id.wvBuyTicketMovie);
		wvBuyTicketCityYear = (WheelView) getView().findViewById(R.id.wvBuyTicketMovieYear);
		
		cardNoEt = (EditText) getView().findViewById(R.id.payment_card_no_et);
		nameEt = (EditText) getView().findViewById(R.id.payment_name_et);
		cvvNoEt = (EditText) getView().findViewById(R.id.payment_cvv_no_et);
		continueBtn = (Button) getView().findViewById(R.id.payment_continue_btn);
		monthSpinner = (TextView) getView().findViewById(R.id.payment_month_spinner);
		yearSpinner = (TextView) getView().findViewById(R.id.payment_year_spinner);
		locationTv = (TextView) getView().findViewById(R.id.payment_location_tv);
		pref = new CinemaxxPref(getActivity());
		
		locationTv.setText(pref.getCityName());
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		locationTv.setText("Location: "+pref.getCityName());
		locationTv.setTypeface(faceThin);
		monthArray = getResources().getStringArray(R.array.month_array);
		monthArrayVal = getResources().getStringArray(R.array.month_array_val);
		yearArray = getResources().getStringArray(R.array.year_array);
		monthSpinner.setText(monthArray[0]);
		yearSpinner.setText(yearArray[0]);

//		initMonthSpinner();
//		initYearSpinner();
	}
	
	
	public void clickListener(){
		
		continueBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainActivity activity = (MainActivity) getActivity();

				if(cardNoEt.getText().toString().length() > 0){
					if(cardNoEt.getText().toString().length() > 15){
						if(nameEt.getText().toString().length() > 0){
							if(monthVal.trim().length() > 0 && !monthVal.equals("MM")){
								if(yearVal.trim().length() > 0 && !yearVal.equals("YYYY")){
									if(cvvNoEt.getText().toString().length() > 0){
										new VeritransTask().execute(cardNoEt.getText().toString(), monthVal, yearVal, cvvNoEt.getText().toString());
									}else{
										showDialog("Please enter your \n CVV number");
									}
								}else{
									showDialog("Please select year");
								}
							}else{
								showDialog("Please select month");
							}							
						}else{
							showDialog("Please enter name \n on card");
						}
					}else{
						showDialog("Card Number must be 16");
					}
					
				}else{
					showDialog("Please enter card \nnumber.");
				}
			}
		});
		
		monthSpinner.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(cardNoEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(nameEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(cvvNoEt.getWindowToken(), 0);
				flagt = false;
				flagtt = false;
				cvalue = 1;
				lnrwheelCity.setVisibility(View.VISIBLE);
				lnrwheelCityYear.setVisibility(View.GONE);
				monthSpinner.setEnabled(false);
				yearSpinner.setEnabled(true);
				initCinemaMonthList();
			}
		});
		
		yearSpinner.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(cardNoEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(nameEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(cvvNoEt.getWindowToken(), 0);
				flagtY = false;
				flagttY = false;
				cvalueY = 1;
				lnrwheelCityYear.setVisibility(View.VISIBLE);
				lnrwheelCity.setVisibility(View.GONE);
				yearSpinner.setEnabled(false);
				monthSpinner.setEnabled(true);
				initCinemaYearList();
			}
		});
		
		/*monthSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				if(position != 0){
					monthVal = monthArrayVal[position];
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		yearSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				if(position != 0){
					yearVal = (String) parent.getItemAtPosition(position);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
		cardNoEt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String text = cardNoEt.getText().toString().trim();
				if(text.length() > 16){
					InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(cardNoEt.getWindowToken(), 0);
					
					text =  text.substring(0, text.length()-1);
					cardNoEt.setText(text);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		cvvNoEt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				String text = cvvNoEt.getText().toString().trim();
				if(text.length() > 3){
					InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					mgr.hideSoftInputFromWindow(cvvNoEt.getWindowToken(), 0);
					
					text =  text.substring(0, text.length()-1);
					cvvNoEt.setText(text);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	protected void initCinemaYearList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), yearArray, 0);
		
		wvBuyTicketCityYear.setViewAdapter(buyTicketAdapter);
//		if(citytv.getText().toString().contains("Select")){
//			cityIndex = 0;
//		}else{
//			wvBuyTicketCity.setCurrentItem(cityIndex);
//		}
			if(cityIndexY != 0){
				wvBuyTicketCityYear.setCurrentItem(cityIndexY);
			}else{
				wvBuyTicketCityYear.setCurrentItem(0);
			}
		
			wvBuyTicketCityYear.addChangingListener(this);
			wvBuyTicketCityYear.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagtY = true;
				wvBuyTicketCityYear.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if(flagttY == false){
									flagttY = true;
									System.out.println("addClickingListener::::::::::::::::::::::::::::::;"+ yearArray[wvBuyTicketCityYear.getCurrentItem()]);
									cityIndexY = wvBuyTicketCityYear.getCurrentItem();
									yearSpinner.setText(yearArray[wvBuyTicketCityYear.getCurrentItem()]);
									
									if(wvBuyTicketCityYear.getCurrentItem() != 0){
										yearVal = yearArray[wvBuyTicketCityYear.getCurrentItem()];
									}
									
									yearSpinner.setEnabled(true);
									lnrwheelCityYear.setVisibility(View.GONE);
									flagtY = false;
									cvalueY = 0;
									wvBuyTicketCityYear.setViewAdapter(null);
								}
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
			wvBuyTicketCityYear.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagtY == false){
							
							flagtY = true;
							System.out.println("setOnTouchListener///////////////////////////////////////"+ yearArray[wvBuyTicketCityYear.getCurrentItem()]);
							cityIndexY = wvBuyTicketCityYear.getCurrentItem();
							yearSpinner.setText(yearArray[wvBuyTicketCityYear.getCurrentItem()]);
//						
							if(wvBuyTicketCityYear.getCurrentItem() != 0){
								yearVal = yearArray[wvBuyTicketCityYear.getCurrentItem()];
							}
							
							yearSpinner.setEnabled(true);
							lnrwheelCityYear.setVisibility(View.GONE);
							cvalueY = 0;
							wvBuyTicketCityYear.setViewAdapter(null);
							
						}
						
					}
				}, 200);
				
				return false;
			}
		});
		
			wvBuyTicketCityYear.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagtY = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagtY = false;
					}
				}, 100);
				
			}
		});
		
	}
	
	protected void initCinemaMonthList() {
		// TODO Auto-generated method stub
		buyTicketAdapter = new BuyTicketAdapter(getActivity(), monthArray, 0);
		
		wvBuyTicketCity.setViewAdapter(buyTicketAdapter);
//		if(citytv.getText().toString().contains("Select")){
//			cityIndex = 0;
//		}else{
//			wvBuyTicketCity.setCurrentItem(cityIndex);
//		}
			if(cityIndex != 0){
				wvBuyTicketCity.setCurrentItem(cityIndex);
			}else{
				wvBuyTicketCity.setCurrentItem(0);
			}
		
		wvBuyTicketCity.addChangingListener(this);
		wvBuyTicketCity.addClickingListener(new OnWheelClickedListener() {
			
			@Override
			public void onItemClicked(WheelView wheel, int itemIndex) {
				// TODO Auto-generated method stub
				flagt = true;
				wvBuyTicketCity.setCurrentItem(itemIndex);
					try {
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								if(flagtt == false){
									flagtt = true;
									System.out.println("addClickingListener::::::::::::::::::::::::::::::;"+ monthArray[wvBuyTicketCity.getCurrentItem()]);
									cityIndex = wvBuyTicketCity.getCurrentItem();
									monthSpinner.setText(monthArray[wvBuyTicketCity.getCurrentItem()]);
									
									if(wvBuyTicketCity.getCurrentItem() != 0){
										monthVal = monthArrayVal[wvBuyTicketCity.getCurrentItem()];
									}
									
									monthSpinner.setEnabled(true);
									lnrwheelCity.setVisibility(View.GONE);
									flagt = false;
									cvalue = 0;
									wvBuyTicketCity.setViewAdapter(null);
								}
							}
						}, 500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
			}
		});
		
		wvBuyTicketCity.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if(flagt == false){
							
							flagt = true;
							System.out.println("setOnTouchListener///////////////////////////////////////"+ monthArray[wvBuyTicketCity.getCurrentItem()]);
							cityIndex = wvBuyTicketCity.getCurrentItem();
							monthSpinner.setText(monthArray[wvBuyTicketCity.getCurrentItem()]);
//						
							if(wvBuyTicketCity.getCurrentItem() != 0){
								monthVal = monthArrayVal[wvBuyTicketCity.getCurrentItem()];
							}
							
							monthSpinner.setEnabled(true);
							lnrwheelCity.setVisibility(View.GONE);
							cvalue = 0;
							wvBuyTicketCity.setViewAdapter(null);
							
						}
						
					}
				}, 200);
				
				return false;
			}
		});
		
		wvBuyTicketCity.addScrollingListener(new OnWheelScrollListener() {
			
			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub
				flagt = true;
			}
			
			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						flagt = false;
					}
				}, 100);
				
			}
		});
		
	}
	
	
	
	
	public void showDialog(String message){
		AlertDialog dialog = new AlertDialog(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
				
			}
		});
		dialog.show();
	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@SuppressWarnings("static-access")
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				System.out.println("Respnse: " +Data.toString());
				String tra_id;
				String statusCode;
				
				try{
					JSONObject mainJson = new JSONObject(Data.toString());
					
					/*if(mainJson.optString(UserDetailsConstant.STATUS_CODE).equals("200") || mainJson.optString(UserDetailsConstant.STATUS_CODE).equals("201")){
						
						
						
					}else {
                        AlertDialog dialog = new AlertDialog(getActivity(), "No Data Found", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								activity.callBackPress();
							}
						});
						dialog.show();
					}*/
					
					
					tra_id = mainJson.optString("transaction_id");
					statusCode = mainJson.optString("status_code");
					
					if(statusCode.equals("200") || statusCode.equals("201")){
						
						Fragment frag = new OrderSummaryFragment(seatTo, orderTo, Data.toString(), tra_id);
						FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
						if (frag != null) {
							if(cTimer != null){
								cTimer.cancel();
							}
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							CurrentFragmentSingletone.getInstance().setFragment(frag);
							ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
							ft.replace(R.id.frame_container, frag, "fragment");
							// Start the animated transition.
							ft.commit();

							// update selected item and title, then close the drawer
						
						}
					}else if(statusCode.equals("202")){
						showMessage("Your transaction is Denied by the processing Bank. Pls try again with another credit card.");
					}else if(statusCode.equals("300")){
						showMessage("Move Permanently, this and all future request should be directed to the new URL.");
					}else if(statusCode.equals("400")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("401")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again ");
					}else if(statusCode.equals("402")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("403")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("404")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("405")){
						showMessage("The payment could not be processed because of wrong method.. Pls try again after sometime");
					}else if(statusCode.equals("406")){
						showMessage("It seems your credit card is already charged for this order. Pls check with your bank and send us the your feedback to feedback@cinemaxxtheater.com");
					}else if(statusCode.equals("407")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("408")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("409")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("410")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("411")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("412")){
						showMessage("There is an internal error occurred while processing your payment.. Pls try again");
					}else if(statusCode.equals("413")){
						showMessage("Your payment canot be processed due to syntax error..Pls try again.");
					}else if(statusCode.equals("500")){
						showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later");
					}else if(statusCode.equals("502")){
						showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later");
					}else if(statusCode.equals("503")){
						showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later");
					}else if(statusCode.equals("504")){
						showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later");
					}else{
						showMessage("Transaction Unsuccessfull");
						YesNoDialogBig dialog = new YesNoDialogBig(getActivity(), "Transaction Unsuccessfull", new DialogListener() {
							
							@Override
							public void onOkClick() {
								// TODO Auto-generated method stub
								if(cTimer != null){
									cTimer.cancel();
								}
								activity.reCreateHome();
							}
						});
						dialog.show();
					}

					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		try{
//			schedule.RequestSeatsConfirmAsync(pref.getDeviceId(), seatTo.getRequistId(), selectedSeat, getActivity());
			
//			System.out.println("order amount: "+orderTo.getOrderAmount());
//			System.out.println("price: "+seatTo.getPrice());
//			System.out.println("request id: "+seatTo.getRequistId());
			
			System.out.println("pref.getTokenId():::"+ pref.getTokenId() + "//orderTo.getOrderId():::" + orderTo.getOrderId() + "//orderTo.getOrderAmount():::" + orderTo.getOrderAmount() + "//seatTo.getMovieId():::" + seatTo.getMovieId() + "//pricePerTicket:::" + pricePerTicket + "//orderTo.getNoOfSeat():::" + orderTo.getNoOfSeat() + "//seatTo.getMovieName():::" + seatTo.getMovieName() + "//name:::" + name + "//email:::" + email + "//mobile:::" + mobile + "//CardNo:::" + cardNoEt.getText().toString());
			schedule.PaymentCheckoutProcessAsync(pref.getTokenId(), orderTo.getOrderId(), orderTo.getOrderAmount(), seatTo.getMovieId(), 
					pricePerTicket, orderTo.getNoOfSeat(), seatTo.getMovieName(), name, email, mobile, cardNoEt.getText().toString(),getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	public void showMessage(String message){
		
		YesNoDialogBig dialog = new YesNoDialogBig(getActivity(), message, new DialogListener() {
			
			@Override
			public void onOkClick() {
				// TODO Auto-generated method stub
				if(cTimer != null){
					cTimer.cancel();
				}
				activity.callBackPress();
			}
		});
		dialog.show();
		
	}

	public String getTotal(String price, String no){
		
		return String.valueOf(Integer.parseInt(price) * Integer.parseInt(no));
	}
	
	/*@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initMonthSpinner() {
		// TODO Auto-generated method stub
		monthAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, monthArray);
		monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		monthSpinner.setAdapter(monthAdapter);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void initYearSpinner() {
		// TODO Auto-generated method stub
		yearAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, yearArray);
		yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearSpinner.setAdapter(yearAdapter);
	}*/
	
	
	public class VeritransTask extends AsyncTask<String, Void, String>{

        CustomProgressDialog pDialog;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new CustomProgressDialog(getActivity());
			pDialog.showDialog();
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String output = "";
			Card card = new Card();
			card.number = params[0];
			card.expiredMonth = params[1];
			card.expiredYear = params[2];
			card.cvv = params[3];
			// Get Token

			JSONObject json = getToken(card);
			output = json.toString();
			System.out.println("JSON: " + json.toString());

			return output;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(pDialog != null){
				pDialog.dismissDialog();
			}
			
		   activity = (MainActivity) getActivity();
			String tokenId;
			try {
				JSONObject json = new JSONObject(result);
				String code = json.optString("status_code");
//				String status = json.getString("status");
				String m = json.optString("status_message");
				tokenId = json.optString("token_id");
				System.out.println("Token Id: "+tokenId);
				  
				if (code.equals("200")) {
//					JSONObject x = json.getJSONObject("data");

//					Toast.makeText(getActivity(), "Token Id: "+tokenId, Toast.LENGTH_LONG).show();
					pref.setTokenId(tokenId);
					callService();
//					activity.callBackPress();
					// Send Data to Merchant
//				    response = sendDataToMerchantServer(getSampleOrderDetailData(tokenId));
				} else if(code.equals("202")){
					showMessage("Your transaction is Denied by the processing Bank. Pls try again with another credit card.");
				}else if(code.equals("300")){
					showMessage("Move Permanently, this and all future request should be directed to the new URL.");
				}else if(code.equals("400")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("401")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again �");
				}else if(code.equals("402")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("403")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("404")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("405")){
					showMessage("The payment could not be processed because of wrong method.. Pls try again after sometime�");
				}else if(code.equals("406")){
					showMessage("It seems your credit card is already charged for this order. Pls check with your bank and send us the your feedback to feedback@cinemaxxtheater.com");
				}else if(code.equals("407")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("408")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("409")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("410")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("411")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("412")){
					showMessage("There is an internal error occurred while processing your payment.. Pls try again�");
				}else if(code.equals("413")){
					showMessage("Your payment canot be processed due to syntax error..Pls try again.");
				}else if(code.equals("500")){
					showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later�");
				}else if(code.equals("502")){
					showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later�");
				}else if(code.equals("503")){
					showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later�");
				}else if(code.equals("504")){
					showMessage("Sorry, we are unable to process your request as we are experiencing some connection issues with the bank server. We will resolve the issue as soon as possible. Pls try again later�");
				}else{
					showMessage("Transaction Unsuccessfull");
					YesNoDialogBig dialog = new YesNoDialogBig(getActivity(), "Transaction Unsuccessfull", new DialogListener() {
						
						@Override
						public void onOkClick() {
							// TODO Auto-generated method stub
							if(cTimer != null){
								cTimer.cancel();
							}
							activity.reCreateHome();
						}
					});
					dialog.show();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Web service Response: "+result);
		}
		
		
		
	}
	
	private JSONObject getToken(Card card) {
		StringBuilder builder = new StringBuilder(); 
		builder.append("https://api.veritrans.co.id/v2/token?"); 
		builder.append("card_number=").append(card.number).append("&"); 
		builder.append("card_exp_month=").append(card.expiredMonth).append("&"); 
		builder.append("card_exp_year=").append(card.expiredYear).append("&"); 
		builder.append("card_cvv=").append(card.cvv).append("&"); 
		builder.append("client_key=").append(CLIENT_KEY);
//		builder.append("secure").append("false");
//		builder.append("bank").append("bni");
//		builder.append("gross_amount").append("10000");
		
		HttpGet get = new HttpGet(builder.toString()); 
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		
		//String tokenId = null;

		try {
			response = httpClient.execute(get);
			
			//Convert Inputstream to String
			StringWriter writer = new StringWriter();
			IOUtils.copy(response.getEntity().getContent(), writer, "UTF-8");
			String bodyContent = writer.toString();
			 System.out.println("JSON: "+bodyContent);
			JSONObject json = new JSONObject(bodyContent);
			return json;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(cTimer != null){
			cTimer.cancel();
		}
	}
	
    public void setTimer(int tTime){
		
		cTimer = new CountDownTimer(tTime, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				int timeVal = (int) (millisUntilFinished / 1000);
				int second = timeVal % 60;
				int minute = timeVal / 60;
				
				final String secVal = String.valueOf(second);
				final String minuteVal = String.valueOf(minute);
				
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
//						timerTv.setText(minuteVal +":"+secVal);
					}
				});
				pref.setTimerTime(String.valueOf((int)millisUntilFinished));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				System.out.println("Timer finished..............");
				if(lnrwheelCity.getVisibility() == View.VISIBLE){
					lnrwheelCity.setVisibility(View.GONE);
				}
				if(lnrwheelCityYear.getVisibility() == View.VISIBLE){
					lnrwheelCityYear.setVisibility(View.GONE);
				}
				InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				mgr.hideSoftInputFromWindow(cardNoEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(nameEt.getWindowToken(), 0);
				mgr.hideSoftInputFromWindow(cvvNoEt.getWindowToken(), 0);
				
				AlertDialog dialog = new AlertDialog(getActivity(), "Session Time Out", new DialogListener() {
					
					@Override
					public void onOkClick() {
						// TODO Auto-generated method stub
						MainActivity activity = (MainActivity) getActivity();
						activity.reCreateHome();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
		}.start();
		
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		System.out.println("oldValue:::" +oldValue+"newValue::::"+ newValue);
		if(cvalue == 1 && newValue < monthArray.length){
			cityIndex = newValue;
			monthSpinner.setText(monthArray[newValue]);
//			monthVal = monthArray[newValue];
			monthVal = monthArrayVal[newValue];
			cvalue = 0;
			
		}else if (cvalueY == 1 && newValue < yearArray.length) {
			cityIndexY = newValue;
			yearSpinner.setText(yearArray[newValue]);
			yearVal = yearArray[newValue];
//			citySelectId = cityIdList.get(newValue);
			cvalueY = 0;
		}
	}
	
}
