package com.cinemaxx.activity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.bean.ComingSoonTo;
import com.cinemaxx.constant.ComingSoonConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.dialog.BannerDialog;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.Utils;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class SplashActivity extends Activity{

	public static String version;
	private Thread splashThread;
	TextView versionCodeTv;
	SplashActivity activity;
	CinemaxxPref pref;
	Timer t;
	int time = 0;
	public static String internetMsg = "We are unable to detect any internet connectivity on your phone. Pls connect your phone to internet and try again.";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_splash);
		
		initView();
		
		activity = this;
		
		
		navigateBanner();
		/*t = new Timer();
        //Set the schedule function and rate
		
        t.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
//				sleep(2000); // 2000 ms = 2 seconds.
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						
                        time += 10;
                        if(time == 100){
                        	navigateActivity();
                        }
					}
					
					
				});
			}
        	
        }, 0, 100);*/
		
		/*BannerDialog dialog = new BannerDialog(SplashActivity.this, "Hello");
		dialog.setCancelable(false);
		dialog.show();*/
		
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(t != null){
			t.cancel();
		}
	}

	
	public void navigateBanner(){
		if(Utils.isNetworkAvailable(SplashActivity.this)){

			callService();
			
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(SplashActivity.this, SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					finish();
				}
			});
			dialog.show();
		}
	}

	
	public void initView(){
		versionCodeTv = (TextView) findViewById(R.id.splash_version_code_tv);
		pref = new CinemaxxPref(getApplicationContext());
        
		if(pref.getDeviceId().equals("") || pref.getDeviceId() == null){
	        String android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	        pref.setDeviceId(android_id);
		}
        
		PackageManager manager = this.getPackageManager();
		PackageInfo info;
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			version = info.versionName;
//			versionCodeTv.setText("Version: "+version);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void navigateActivity(){
		
		/*if(t != null){
			t.cancel();
		}*/
		Intent nextIntent = new Intent(SplashActivity.this, MainActivity.class);
		startActivity(nextIntent);
		finish();
		
	}
	
	public void callService(){
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				
				System.out.println("Splash Respnse: "+Data.toString());
				try{
					
					if(Data.toString().trim().length() > 1){
						JSONObject mainObj = new JSONObject(Data.toString());
						if(mainObj.optString("ErrorDesc").equals("0")){
							String message = mainObj.optString("FlashMessage");
							String imageLink = mainObj.optString("FlashImage");
							if(message.trim().length() > 0 || imageLink.trim().length() > 5){
								BannerDialog dialog = new BannerDialog(SplashActivity.this, message, imageLink);
								dialog.setCancelable(false);
								dialog.show();
							}else{
								navigateActivity();
							}
						}else{
							navigateActivity();
						}
					}else{
						navigateActivity();
					}
					
				}catch(Exception e){
					e.printStackTrace();
					navigateActivity();
				}
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			schedule.GetFlashScreenDataAsync();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	

}
