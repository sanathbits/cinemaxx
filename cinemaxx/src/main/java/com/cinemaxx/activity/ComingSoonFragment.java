package com.cinemaxx.activity;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cinemaxx.adapter.ComingSoonAdapter;
import com.cinemaxx.bean.ComingSoonTo;
import com.cinemaxx.constant.ComingSoonConstant;
import com.cinemaxx.constant.NowPlayingConstant;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.AlertDialogForInternetChecking;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.CustomProgressDialog;
import com.cinemaxx.util.Utils;
import com.cinemaxx.util.ViewUtility;
import com.cinemaxx.webservice.IWsdl2CodeEvents;
import com.cinemaxx.webservice.Schedule;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ComingSoonFragment extends Fragment{

	
	GridView gridView;
	ComingSoonAdapter comingSoonAdapter;
	TextView locationNameTv;
	CinemaxxPref pref;
	ArrayList<ComingSoonTo> dataList;
	MainActivity activity;
	ArrayList<String> movieIdList;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_coming_soon, container, false);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		initView();
		initClickListener();
		activity = (MainActivity) getActivity();

		
		locationNameTv.setText("Location: "+pref.getCityName());
		
//		activity.clickOnHome();
		
		if(Utils.isNetworkAvailable(getActivity())){
			callService();
		}else{
			AlertDialogForInternetChecking dialog = new AlertDialogForInternetChecking(getActivity(), SplashActivity.internetMsg, new DialogListener() {
				
				@Override
				public void onOkClick() {
					// TODO Auto-generated method stub
					activity.callBackPress();
				}
			});
			dialog.show();
		}
	}	
	
	public void initView(){
		
		Typeface faceThin = Typeface.createFromAsset(getActivity().getAssets(), "Gotham-Medium.ttf");
		gridView = (GridView) getView().findViewById(R.id.coming_soon_gv);
		locationNameTv = (TextView) getView().findViewById(R.id.coming_soon_location_tv);
		locationNameTv.setTypeface(faceThin);
		TextView textTv = (TextView) getView().findViewById(R.id.coming_soon_text_tv);
		textTv.setTypeface(faceThin);
		
		pref = new CinemaxxPref(getActivity());
		
		dataList = new ArrayList<ComingSoonTo>();
		
		MainActivity act = (MainActivity) getActivity();
		act.setHeaderText("MOVIES");
	}
	
	public void initClickListener() {

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ComingSoonTo comingSoonTo = (ComingSoonTo) parent.getItemAtPosition(position);
				pref.setMovieId(comingSoonTo.getMovieId());
				pref.setMovieStatus("1");
				
				Fragment fragment = new MovieInfoFragment("coming", movieIdList, 0);
				FragmentContainer.getInstance().setFragment(
						CurrentFragmentSingletone.getInstance().getFragment());
				if (fragment != null) {
					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					CurrentFragmentSingletone.getInstance().setFragment(
							fragment);
					ft.setCustomAnimations(R.anim.enter_from_right,
							R.anim.exit_to_left);
					ft.replace(R.id.frame_container, fragment, "fragment");
					// Start the animated transition.
					ft.commit();

					// update selected item and title, then close the drawer

				}
			}
		});

	}
	
	public void callService(){
		activity.setClickable();
		Schedule schedule = new Schedule(new IWsdl2CodeEvents() {
			
			@Override
			public void Wsdl2CodeStartedRequest() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinishedWithException(Exception ex) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void Wsdl2CodeFinished(String methodName, Object Data) {
				// TODO Auto-generated method stub
				CustomProgressDialog pDialog;

				pDialog = new CustomProgressDialog(getActivity());
				pDialog.showDialog();
				dataList = new ArrayList<ComingSoonTo>();
				
				try{
					
					if(Data.toString().trim().length() > 1){
						JSONObject mainObj = new JSONObject(Data.toString());
						if(mainObj.optString(ComingSoonConstant.ERROR_DESC).equals("0")){
							JSONArray movieArray = mainObj.optJSONArray(ComingSoonConstant.MOVIES_ARRAY);
							movieIdList = new ArrayList<String>();
							for(int i = 0; i < movieArray.length(); i ++){
								
								JSONObject movieObj = movieArray.optJSONObject(i);
								ComingSoonTo cTo = new ComingSoonTo();
								
								cTo.setMovieId(movieObj.optString(ComingSoonConstant.MOVIE_ID));
								cTo.setMovieImage(movieObj.optString(ComingSoonConstant.MOVIE_IMAGE));
								cTo.setMovieName(movieObj.optString(ComingSoonConstant.MOVIE_NAME));
								cTo.setMovieRating(movieObj.optString(ComingSoonConstant.MOVIE_RATING));
								
								dataList.add(cTo);
								movieIdList.add(movieObj.optString(ComingSoonConstant.MOVIE_ID));
							}
							
							initGrid();
						}else{
							/*MainActivity activity = (MainActivity) getActivity();
							activity.showCustomAlert("Server is not working.");*/
							AlertDialog dialog = new AlertDialog(getActivity(),
									"No movies are available", new DialogListener() {

										@Override
										public void onOkClick() {
											// TODO Auto-generated method stub
											activity.callBackPress();
										}
									});
							dialog.show();
						}
					}else{
						/*MainActivity activity = (MainActivity) getActivity();
						activity.showCustomAlert("Server is not working.");*/
						AlertDialog dialog = new AlertDialog(getActivity(),
								"No movies are available", new DialogListener() {

									@Override
									public void onOkClick() {
										// TODO Auto-generated method stub
										activity.callBackPress();
									}
								});
						dialog.show();
					}
					
					if(pDialog != null){
						pDialog.dismissDialog();
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				activity.setNotClickable();
			}
			
			@Override
			public void Wsdl2CodeEndedRequest() {
				// TODO Auto-generated method stub
				
			}
		});
		
		try{
			schedule.GetComingSoonMovieListAsync(pref.getDeviceId(), pref.getCityCode(), getActivity());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void initGrid(){
		
		comingSoonAdapter = new ComingSoonAdapter(getActivity(), dataList, ComingSoonFragment.this);
		gridView.setAdapter(comingSoonAdapter);
		/*ViewUtility.setListViewHeightBasedOnComingSoonFragment(gridView, getActivity());
		gridView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
//		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }

		});*/
		
	}
	
}
