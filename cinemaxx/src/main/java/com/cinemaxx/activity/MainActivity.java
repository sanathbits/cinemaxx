package com.cinemaxx.activity;


import java.util.ArrayList;

import com.cinemaxx.activity.R;
import com.cinemaxx.adapter.NavDrawerListAdapter;
import com.cinemaxx.bean.NavDrawerItem;
import com.cinemaxx.dialog.AlertDialog;
import com.cinemaxx.dialog.YesNoDialog;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.listener.YesNoDialogListener;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.CinemaxxPref;
import com.cinemaxx.util.Utils;

import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;

@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	CinemaxxPref pref;
	CountDownTimer cTimer;
	
	Button menuBtn, homeBtn, movieBtn, cinemaBtn, backBtn;
	TextView headerTv;
	private int drawerStatus = 0, homeStatus = 0, movieStatus = 0, cinemaStatus = 0;
	LinearLayout menuLayout;
	RelativeLayout headerLayout;
	public static boolean exitStatus = false;
	public static boolean cityStatus = false;
	public static boolean chkMaxxCardLogin = false;
	ImageView logoIv, nameLogoIv;

    private int fragOpenStatus = 0;
    private boolean selectedFromMenu = false;

/*	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean result = false;
		switch (event.getKeyCode()) {

		case KeyEvent.KEYCODE_MENU:
			result = true;
			break;

		}

		return result;
	}*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		setContentView(R.layout.activity_main);
		initView();
		
        menuBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(pref.getCityName().trim().length() > 0 && !cityStatus){
					if(drawerStatus == 1){
						mDrawerLayout.closeDrawer(mDrawerList);
						drawerStatus = 0;
						menuBtn.setBackgroundResource(R.drawable.menu_button);

                        if(fragOpenStatus == 1 && !selectedFromMenu){
                            homeBtn.setBackgroundResource(R.drawable.home_button_selected);
                            headerTv.setText("HOME");
                            homeStatus = 1;
                        }
                        else if(fragOpenStatus == 2 && !selectedFromMenu){
                            cinemaBtn.setBackgroundResource(R.drawable.cinemas_button_selected);
                            headerTv.setText("SCHEDULE");
                            cinemaStatus = 1;
                        }
                        else if(fragOpenStatus == 3 && !selectedFromMenu){
                            movieBtn.setBackgroundResource(R.drawable.movies_button_selected);
                            headerTv.setText("MOVIES");
                            movieStatus = 1;
                        }
					}else if(drawerStatus == 0){
						mDrawerLayout.openDrawer(mDrawerList);
						drawerStatus = 1;
						menuBtn.setBackgroundResource(R.drawable.menu_button_selected);
						
						homeBtn.setBackgroundResource(R.drawable.home_button);
						movieBtn.setBackgroundResource(R.drawable.movies_button);
						cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);

                        if(cinemaStatus == 1 && !selectedFromMenu) fragOpenStatus = 2;
                        else if(homeStatus == 1 && !selectedFromMenu) fragOpenStatus = 1;
                        else if(movieStatus == 1 && !selectedFromMenu) fragOpenStatus = 3;

						cinemaStatus = 0;
						homeStatus = 0;
						movieStatus = 0;

                        //prevTitle = String.valueOf(headerTv.getText());
						//headerTv.setText("MENU");
					}
				}

				selectedFromMenu = false;
				//setTitle("CINEMAXX");
				
			}
		});
        
		mDrawerLayout.setDrawerListener(new DrawerListener() {
			
			@Override
			public void onDrawerStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onDrawerSlide(View arg0, float arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onDrawerOpened(View arg0) {
				// TODO Auto-generated method stub
				drawerStatus = 1;
				menuBtn.setBackgroundResource(R.drawable.menu_button_selected);
				
				homeBtn.setBackgroundResource(R.drawable.home_button);
				movieBtn.setBackgroundResource(R.drawable.movies_button);
				cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);

                if(cinemaStatus == 1 && !selectedFromMenu) fragOpenStatus = 2;
                else if(homeStatus == 1 && !selectedFromMenu) fragOpenStatus = 1;
                else if(movieStatus == 1 && !selectedFromMenu) fragOpenStatus = 3;


                cinemaStatus = 0;
				homeStatus = 0;
				movieStatus = 0;

                //headerTv.setText("MENU");
			}
			
			@Override
			public void onDrawerClosed(View arg0) {
				// TODO Auto-generated method stub
				drawerStatus = 0;
				menuBtn.setBackgroundResource(R.drawable.menu_button);
                if(fragOpenStatus == 1 && !selectedFromMenu){
                    homeBtn.setBackgroundResource(R.drawable.home_button_selected);
                    headerTv.setText("HOME");
                    homeStatus = 1;
                }
                else if(fragOpenStatus == 2 && !selectedFromMenu){
                    cinemaBtn.setBackgroundResource(R.drawable.cinemas_button_selected);
                    headerTv.setText("SCHEDULE");
                    cinemaStatus = 1;
                }
                else if(fragOpenStatus == 3 && !selectedFromMenu){
                    movieBtn.setBackgroundResource(R.drawable.movies_button_selected);
                    headerTv.setText("MOVIES");
                    movieStatus = 1;
                }
                //headerTv.setText(prevTitle);
			}
		});
		
		homeBtn.setOnClickListener(new View.OnClickListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pref.getCityName().trim().length() > 0 && !cityStatus) {
					if(homeStatus == 1){
//						homeStatus = 0;
//						homeBtn.setBackgroundResource(R.drawable.home_button);
					}else if(homeStatus == 0){
						exitStatus = true;
						Fragment fragment = new HomeFragment();
						FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
						if (fragment != null) {
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							CurrentFragmentSingletone.getInstance().setFragment(fragment);
							ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
							ft.replace(R.id.frame_container, fragment, "fragment");
							// Start the animated transition.
							ft.commit();

							// update selected item and title, then close the drawer
						
						}
						homeStatus = 1;
						homeBtn.setBackgroundResource(R.drawable.home_button_selected);
						
						menuBtn.setBackgroundResource(R.drawable.menu_button);
						movieBtn.setBackgroundResource(R.drawable.movies_button);
						cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);
						cinemaStatus = 0;
						//drawerStatus = 0;
						movieStatus = 0;
						if(drawerStatus == 1){
							mDrawerLayout.closeDrawer(mDrawerList);
							drawerStatus = 0;
							menuBtn.setBackgroundResource(R.drawable.menu_button);
						}
						
					}
					//prevTitle = "HOME";
					headerTv.setText("HOME");
				}

                selectedFromMenu = false;
                //setTitle("CINEMAXX");
				
			}
		});
		
		movieBtn.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pref.getCityName().trim().length() > 0 && !cityStatus) {
					if(movieStatus == 1){
//						movieStatus = 0;
//						movieBtn.setBackgroundResource(R.drawable.movies_button);
					}else if(movieStatus == 0){
						
						Fragment fragment = new MoviesFragment();
						FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
						if (fragment != null) {
							exitStatus = false;
							showBackBtn();
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							CurrentFragmentSingletone.getInstance().setFragment(fragment);
							ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
							ft.replace(R.id.frame_container, fragment, "fragment");
							// Start the animated transition.
							ft.commit();

							// update selected item and title, then close the drawer
						
						}
						movieStatus = 1;
						movieBtn.setBackgroundResource(R.drawable.movies_button_selected);
						
						homeBtn.setBackgroundResource(R.drawable.home_button);
						menuBtn.setBackgroundResource(R.drawable.menu_button);
						cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);
						cinemaStatus = 0;
						homeStatus = 0;
						
						if(drawerStatus == 1){
							mDrawerLayout.closeDrawer(mDrawerList);
							drawerStatus = 0;
							menuBtn.setBackgroundResource(R.drawable.menu_button);
						}
					}
                    //prevTitle = "MOVIES";
					headerTv.setText("MOVIES");
				}

                selectedFromMenu = false;
                //setTitle("CINEMAXX");
				
			}
		});

		cinemaBtn.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pref.getCityName().trim().length() > 0 && !cityStatus) {
					if(cinemaStatus == 1){
//						cinemaStatus = 0;
//						cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);
					}else if(cinemaStatus == 0){
						
						Fragment fragment = new CinemasFragment();
						FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
						if (fragment != null) {
							exitStatus = false;
							showBackBtn();
							FragmentTransaction ft = getFragmentManager().beginTransaction();
							CurrentFragmentSingletone.getInstance().setFragment(fragment);
							ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
							ft.replace(R.id.frame_container, fragment, "fragment");
							// Start the animated transition.
							ft.commit();

							// update selected item and title, then close the drawer
						
						}
						cinemaStatus = 1;
						cinemaBtn.setBackgroundResource(R.drawable.cinemas_button_selected);
						
						homeBtn.setBackgroundResource(R.drawable.home_button);
						movieBtn.setBackgroundResource(R.drawable.movies_button);
						menuBtn.setBackgroundResource(R.drawable.menu_button);
						movieStatus = 0;
						homeStatus = 0;
						
						if(drawerStatus == 1){
							mDrawerLayout.closeDrawer(mDrawerList);
							drawerStatus = 0;
							menuBtn.setBackgroundResource(R.drawable.menu_button);
						}
					}
                    //prevTitle = "CINEMAS";
					headerTv.setText("SCHEDULE");
				}
                selectedFromMenu = false;
                //setTitle("CINEMAXX");
				
			}
		});
		
		backBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                //setTitle("CINEMAXX");
                callBackPress();
			}
		});
		

	}
	
	/*@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
	    if (true)
	        menu.getItem(1).setEnabled(false);
	    return true;
	}*/

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		
		callBackPress();
		
	}
	
	public void hideBackBtn(){
		backBtn.setVisibility(View.INVISIBLE);
	}
	public void showBackBtn(){
		backBtn.setVisibility(View.VISIBLE);
	}
	
	@SuppressWarnings("static-access")
	public void callBackPress(){
		
		if(exitStatus == true){
			YesNoDialog yDialog = new YesNoDialog(MainActivity.this, "Do You want to exit", new YesNoDialogListener() {
				
				@Override
				public void onYesClick() {
					// TODO Auto-generated method stub
					if(cTimer != null){
						cTimer.cancel();
					}
					showDrawer();
					backCityHeader();
					cityStatus = false;
					finish();
				}
				
				@Override
				public void onNoClick() {
					// TODO Auto-generated method stub
					
				}
			});
			yDialog.show();
		}else{
			Fragment fragment = FragmentContainer.getInstance().getFragment();
			if (fragment != null) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				CurrentFragmentSingletone.getInstance().setFragment(fragment);
				ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
				ft.replace(R.id.frame_container, fragment, "fragment");
				// Start the animated transition.
				ft.commit();

				// update selected item and title, then close the drawer
			
			}else{
				if(cTimer != null){
					cTimer.cancel();
				}
				finish();
			}
		}
		
		
	}

	@SuppressWarnings("static-access")
	public void initView(){
		
		Utils.getDeviceId(MainActivity.this);
		
		menuLayout = (LinearLayout) findViewById(R.id.main_act_menu_layout);
		headerLayout = (RelativeLayout) findViewById(R.id.main_act_header_layout);
//		Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Gotham-Black.otf");
//		Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Black.ttf");
		
		logoIv = (ImageView) findViewById(R.id.cinemaxx_logo_iv);
		nameLogoIv = (ImageView) findViewById(R.id.cinemaxx_name_logo_iv);
		
		headerTv = (TextView) findViewById(R.id.main_act_header_text_tv);
		Typeface face=Typeface.createFromAsset(getAssets(), "Gotham-Black.ttf"); 
		headerTv.setTypeface(face); 
		menuBtn = (Button) findViewById(R.id.main_activity_menu_btn);
		homeBtn = (Button) findViewById(R.id.main_activity_home_btn);
		movieBtn = (Button) findViewById(R.id.main_activity_movie_btn);
		cinemaBtn = (Button) findViewById(R.id.main_activity_cinema_btn);
		backBtn = (Button) findViewById(R.id.main_act_back_btn);
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		pref = new CinemaxxPref(MainActivity.this);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1), true, "22"));
		// Pages
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		// What's hot, We will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
				.getResourceId(6, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons
				.getResourceId(7, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons
				.getResourceId(8, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons
				.getResourceId(9, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons
				.getResourceId(10, -1), true, "50+"));
		
		
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
		
		if(pref.getCityName().equals("") || pref.getCityName() == null){
			Fragment fragment = new CitySelectionFragment();
			if (fragment != null) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
                CurrentFragmentSingletone.getInstance().setFragment(fragment);
				ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
				ft.replace(R.id.frame_container, fragment, "fragment");
				// Start the animated transition.
				ft.commit();

				// update selected item and title, then close the drawer
			
			}
		}else{
			Fragment fragment = new HomeFragment();
			if (fragment != null) {
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				CurrentFragmentSingletone.getInstance().setFragment(fragment);
				ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
				ft.replace(R.id.frame_container, fragment, "fragment");
				// Start the animated transition.
				ft.commit();
				// update selected item and title, then close the drawer
			
			}
		}
	}
	
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}
	
	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	@SuppressWarnings("static-access")
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new PromoFragment();
			System.out.println("Device Id:" + pref.getDeviceId());
			break;
		case 1:
			fragment = new NewsFragment();
			break;
		case 2:
			fragment = new CinemaxxGoldFragment();
			break;
		case 3:
			fragment = new UltraXDFragment();
			break;
		case 4:
			fragment=new CinemaxxJunior();
			break;
		case 5:
			fragment = new FavoritesFragment();
			break;
		case 6:
			fragment = new HistoryFragment();
			break;
		case 7:
			if(pref.getMaxxCardLoginResponse().trim().length() > 10){
				fragment = new MaxxCardWelcomeFragment(false);
			}else{
				fragment = new MaxxCardFragment();
			}
			break;
		case 8:
			fragment = new ContactFragment();
			break;
		case 9:
			fragment = new CitySelectionFragment();
			break;
		case 10:
			if(pref.getMaxxCardLoginResponse().trim().length() > 10){
				pref.setMaxxCardLoginResponse("");
				mDrawerLayout.closeDrawer(mDrawerList);
				refreshAdapter();
				if(exitStatus == false){
					fragment = new HomeFragment();
					if(cTimer != null){
						cTimer.cancel();
					}
				}
			}else if(chkMaxxCardLogin == false){
				fragment = new MaxxCardFragment();
			}else{
				mDrawerLayout.closeDrawer(mDrawerList);
			}
			break;	

		default:
			break;
		}

		if (fragment != null) {
/*			FragmentManager fragmentManager = getFragmentManager();
//			FragmentTransaction ft = fragmentManager.beginTransaction();
//			ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
			fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment).commit();
*/
			FragmentContainer.getInstance().setFragment(CurrentFragmentSingletone.getInstance().getFragment());
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			CurrentFragmentSingletone.getInstance().setFragment(fragment);

			ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
			ft.replace(R.id.frame_container, fragment, "fragment");
			// Start the animated transition.
			ft.commit();
			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			//setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);

            fragOpenStatus = 0;
            selectedFromMenu = true;

		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}
	

	public void setHeaderText(String text){
		
		headerTv.setText(text);
		
	}
	
	public void showCustomAlert(String message){
         
        Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();
          
        // Call toast.xml file for toast layout 
        View toastRoot = inflater.inflate(R.layout.view_toast, null);
        
        TextView msgView = (TextView) toastRoot.findViewById(R.id.toast_view_message_tv);
        msgView.setText(message);
        Toast toast = new Toast(context);
         
        // Set layout to toast 
        toast.setView(toastRoot);

        toast.setDuration(Toast.LENGTH_SHORT);
       // toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
              //  0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
         
    }
	
	@SuppressWarnings("static-access")
	public void reCreateHome(){
		
		FragmentContainer.getInstance().deleteList();
		
		if(cTimer != null){
			cTimer.cancel();
		}
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}
	
	public void stopTimer(){
		if(cTimer != null){
			cTimer.cancel();
		}
	}
	
    public void setTimer(int tTime){
		
    	cTimer = new CountDownTimer(tTime, 1000) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
				/*int timeVal = (int) (millisUntilFinished / 1000);
				int second = timeVal % 60;
				int minute = timeVal / 60;
				
				final String secVal = String.valueOf(second);
				final String minuteVal = String.valueOf(minute);
				
				getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						timerTv.setText(minuteVal +":"+secVal);
					}
				});*/
				pref.setTimerTime(String.valueOf((int)millisUntilFinished));
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				System.out.println("Timer finished..............");
				/*AlertDialog dialog = new AlertDialog(getApplicationContext(), "Session Time Out", new DialogListener() {
					
					@Override
					public void onOkClick() {
						// TODO Auto-generated method stub
						reCreateHome();
					}
				});
				dialog.show();*/
//				reCreateHome();
			}
		}.start();
		
	}
    
    public void setCityHeader(){
    	headerTv.setVisibility(View.INVISIBLE);
    	logoIv.setVisibility(View.INVISIBLE);
    	nameLogoIv.setVisibility(View.VISIBLE);
    	hideBackBtn();
    }
    public void backCityHeader(){
    	headerTv.setVisibility(View.VISIBLE);
    	logoIv.setVisibility(View.VISIBLE);
    	nameLogoIv.setVisibility(View.INVISIBLE);
    	showBackBtn();
    }
    
    
	public void clickOnHome() {
		homeStatus = 1;
        fragOpenStatus = 1;
		homeBtn.setBackgroundResource(R.drawable.home_button_selected);

		menuBtn.setBackgroundResource(R.drawable.menu_button);
		movieBtn.setBackgroundResource(R.drawable.movies_button);
		cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);
		cinemaStatus = 0;
		drawerStatus = 0;
		movieStatus = 0;

		headerTv.setText("HOME");
	}

	public void clickOnMovie() {
		movieStatus = 1;
        fragOpenStatus = 3;
		movieBtn.setBackgroundResource(R.drawable.movies_button_selected);

		homeBtn.setBackgroundResource(R.drawable.home_button);
		menuBtn.setBackgroundResource(R.drawable.menu_button);
		cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);
		cinemaStatus = 0;
		drawerStatus = 0;
		homeStatus = 0;

		headerTv.setText("MOVIES");
	}

	public void clickOnCinema() {
		cinemaStatus = 1;
		fragOpenStatus = 2;
		cinemaBtn.setBackgroundResource(R.drawable.cinemas_button_selected);

		homeBtn.setBackgroundResource(R.drawable.home_button);
		movieBtn.setBackgroundResource(R.drawable.movies_button);
		menuBtn.setBackgroundResource(R.drawable.menu_button);
		movieStatus = 0;
		drawerStatus = 0;
		homeStatus = 0;

		headerTv.setText("SCHEDULE");
	}

    public void clickOnPromo() {
        clearAll();
        headerTv.setText("PROMO");
    }

    public void clickOnNews(){
        clearAll();
        headerTv.setText("NEWS");
    }

    public void clickOnCinemaxxGold(){
        clearAll();
        headerTv.setText("CINEMAXX GOLD");
    }

    public void clickOnUltraXD(){
        clearAll();
        headerTv.setText("ULTRA XD");
    }

    public void clickOnFavorites(){
        clearAll();
        headerTv.setText("FAVORITES");
    }

    public void clickOnHistory(){
        clearAll();
        headerTv.setText("HISTORY");
    }

    public void clickOnMaxxCard(){
        clearAll();
        headerTv.setText("MAXX CARD");
    }

    public void clickOnCinemaxxJunior(){
        clearAll();
        headerTv.setText("CINEMAXX JUNIOR");
    }

	private void clearAll(){
		homeBtn.setBackgroundResource(R.drawable.home_button);
		movieBtn.setBackgroundResource(R.drawable.movies_button);
		menuBtn.setBackgroundResource(R.drawable.menu_button);
		cinemaBtn.setBackgroundResource(R.drawable.cinemas_button);

		movieStatus = 0;
		drawerStatus = 0;
		homeStatus = 0;
		cinemaStatus = 0;
		fragOpenStatus = 0;
	}

	public void clickOnBuyTickets(String val) {
		headerTv.setText(val);
	}
	
    public void clickOnSeatSelection() {
		headerTv.setText("SELECT SEAT(S)");
	}
    
    public void setClickable(){
    	menuLayout.setClickable(true);
    	headerLayout.setClickable(true);
    }
    
    public void setNotClickable(){
    	menuLayout.setClickable(false);
    	headerLayout.setClickable(false);
    }
    
    public void hideDrawer(){
    	mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void showDrawer(){
    	mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
    
    public void refreshAdapter(){
    	adapter.notifyDataSetChanged();
    }

}
