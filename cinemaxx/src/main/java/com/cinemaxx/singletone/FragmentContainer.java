package com.cinemaxx.singletone;

import java.util.Stack;

import android.app.Fragment;

public class FragmentContainer {
	
	private static Stack<Fragment> stackList = new Stack<Fragment>();
	
	private static FragmentContainer container = new FragmentContainer();
	
	public static FragmentContainer getInstance(){
		return container;
	}
	
	public static Fragment getFragment(){
		
		if(stackList.size() > 0){
//			stackList.pop();
			return stackList.pop();
		}
		return null;
		
	}
	
	public static void setFragment(Fragment frag){
		stackList.push(frag);
	}
	
	public static void deleteList(){
		stackList.clear();
	}
	
}
