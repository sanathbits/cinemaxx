package com.cinemaxx.singletone;

import android.app.Fragment;

public class CurrentFragmentSingletone {

	private static CurrentFragmentSingletone singletone = new CurrentFragmentSingletone();
	private static Fragment fragment;
	
	
	public static CurrentFragmentSingletone getInstance(){
		return singletone;
	}
	
	public static Fragment getFragment(){
		return fragment;
	}
	
	public static void setFragment(Fragment frag){
		fragment = frag;
	}
	
}
