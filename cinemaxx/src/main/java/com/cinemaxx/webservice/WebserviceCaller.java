package com.cinemaxx.webservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import com.cinemaxx.listener.WebServiceListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

public class WebserviceCaller {

	String link;
	Activity activity;
	String message;
	WebServiceListener listener;
	List<NameValuePair> list;

	public WebserviceCaller(String link, Activity activity, String message, List<NameValuePair> list,
			WebServiceListener listener) {
		
		this.link = link;
		this.activity = activity;
		this.message = message;
		this.listener = listener;
		this.list = list;

		new CallerTask().execute("");
	}
	
	class CallerTask extends AsyncTask<String, Void, String>{

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(activity);
			pDialog.setCancelable(false);
			pDialog.setMessage(message);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			String output = null;
			
			try{
				
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(link);
				
				httpPost.setEntity(new UrlEncodedFormEntity(list));
				
				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				
				StringBuilder stringBuilder = new StringBuilder();
				BufferedReader buferedReader = new BufferedReader(new InputStreamReader(httpEntity.getContent()), 65234);
				
				String line = null;
				if((line = buferedReader.readLine()) != null){
					stringBuilder.append(line);
				}
				
				output = stringBuilder.toString();
				
				return output;
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if(pDialog != null || pDialog.isShowing()){
				pDialog.dismiss();
			}
			
		}
		
		
		
	}

}
