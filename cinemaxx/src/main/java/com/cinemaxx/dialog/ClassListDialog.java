package com.cinemaxx.dialog;

import java.util.ArrayList;

import com.cinemaxx.activity.HomeFragment;
import com.cinemaxx.activity.NowPlayingFragment;
import com.cinemaxx.activity.R;
import com.cinemaxx.adapter.ClassListAdapter;
import com.cinemaxx.bean.SeatLayoutClassListTo;
import com.cinemaxx.listener.DialogListener;
import com.cinemaxx.listener.NumberofseatInterface;
import com.cinemaxx.singletone.CurrentFragmentSingletone;
import com.cinemaxx.singletone.FragmentContainer;
import com.cinemaxx.util.SharedPreference;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ClassListDialog {

	Activity activity;
	private Dialog classDialog;
	private ArrayList<SeatLayoutClassListTo> classList,demo;
	private String minimumseat="";
	ClassListAdapter classadapter=null;
	public NumberofseatInterface numberinterface;
	private Button skip;
	public static int flag =0;
	private SharedPreference shpref;

	public ClassListDialog(Activity activity, ArrayList<SeatLayoutClassListTo> classList,NumberofseatInterface numberinterface){
		this.activity = activity;
		this.classList = classList;
		this.numberinterface=numberinterface;
		showClassListDialog();
		shpref = new SharedPreference();
	}

	private void showClassListDialog(){
		classDialog = new Dialog(activity);
		classDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		classDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		classDialog.setContentView(R.layout.dialog_seatlayout_classlist);
		classDialog.setCancelable(false);

		ListView _classList = (ListView) classDialog.findViewById(R.id.classList);
		skip=(Button)classDialog.findViewById(R.id.buttonSkip);	

		
		/*demo=classList;
		for(int i=0;i<classList.size();i++){		

			if(!classList.get(i).isIsLoyalty()){
				demo.remove(i);
			}
		}
		classList=demo;
*/


		if(classList.size()>0){
			classadapter = new ClassListAdapter(activity, classList,classDialog);
			_classList.setAdapter(classadapter);
		
			classDialog.show();
		}


		skip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				flag =0;
				shpref.set_Loyalty(activity.getApplicationContext(), "false");

				classDialog.dismiss();

			}
		});

		_classList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				minimumseat=classList.get(position).getMinNoOfSeats();				
				numberinterface.onCompleted(minimumseat);
				classDialog.dismiss();



			}
		});

	}



}
