package com.cinemaxx.dialog;

import com.cinemaxx.activity.R;
import com.cinemaxx.listener.YesNoDialogListener;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class YesNoDialog extends Dialog{

	Context context;
	String message;
	YesNoDialogListener listener;
	Button yesBtn, noBtn;
	TextView messageTv;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_yes_no);
		
		yesBtn = (Button) findViewById(R.id.alert_dialog_yes_btn);
		noBtn = (Button) findViewById(R.id.alert_dialog_no_btn);
		messageTv = (TextView) findViewById(R.id.toast_view_YNmessage_tv);
		messageTv.setText(message);
		
		yesBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listener.onYesClick();
				dismiss();
			}
		});
		noBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listener.onNoClick();
				dismiss();
			}
		});
		
	}



	public YesNoDialog(Context context, String message, YesNoDialogListener listener) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.message = message;
		this.listener = listener;
	}

}
