package com.cinemaxx.dialog;

import com.cinemaxx.activity.R;
import com.cinemaxx.activity.SplashActivity;
import com.cinemaxx.util.ImageLoader;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BannerDialog extends Dialog{
	
	Button crossBtn;
	TextView textTv;
	Context context;
	String textValue, imageLink;
	ImageView imageView;
	ImageLoader imageLoader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_banner);
		initView();
		
		crossBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SplashActivity activity = (SplashActivity) context;
				activity.navigateActivity();
				dismiss();
			}
		});
		
	}
	
	public void initView(){
		crossBtn = (Button) findViewById(R.id.banner_activity_cross_btn);
		textTv = (TextView) findViewById(R.id.banner_activity_tv);
		imageLoader = new ImageLoader(context);
		imageView = (ImageView) findViewById(R.id.banner_activity_iv);
		Typeface face=Typeface.createFromAsset(context.getAssets(), "Gotham-Medium.ttf"); 
		textTv.setTypeface(face);
		
		if(imageLink.trim().length() > 5){
//			imageLoader.DisplayImage(imageLink, imageView);
			imageView.setImageBitmap(imageLoader.getBitmap(imageLink));
		}else{
			textTv.setText(textValue);
		}
		
	}

	public BannerDialog(Context context, String textValue, String imageLink) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.textValue = textValue;
		this.imageLink = imageLink;
	}

}
