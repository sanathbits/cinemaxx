package com.cinemaxx.dialog;

import com.cinemaxx.activity.R;
import com.cinemaxx.listener.DialogListener;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class AlertDialogForInternetChecking extends Dialog{
	
	Context context;
	DialogListener listener;
	TextView messageTv;
	Button okBtn;
	String message;
	
	public AlertDialogForInternetChecking(Context context, String message, DialogListener listener) {
		super(context);
		this.context = context;
		this.listener = listener;
		this.message = message;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_for_internet_checking);
		
		messageTv = (TextView) findViewById(R.id.toast_view_message_tv);
		messageTv.setText(message);
		okBtn = (Button) findViewById(R.id.alert_dialog_ok_btn);
		okBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listener.onOkClick();
				dismiss();
			}
		});
	}

}
