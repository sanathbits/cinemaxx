package com.cinemaxx.listener;

public interface YesNoDialogListener {

	public void onYesClick();
	public void onNoClick();
	
}
