package com.cinemaxx.constant;

public class MaxxCardWelcomeConstant {
	
	public static final String MEMBER_TRANS_LIST_ARRAY = "MemberTransList";
	public static final String UPCOMING_ID = "Id";
	public static final String UPCOMING_VISTA_TRANS_ID = "VistaTransId";
	public static final String UPCOMING_TRANSDATE_TIME = "TransDateTime";
	public static final String UPCOMING_CINEMA_NAME = "CinemaName";
	public static final String UPCOMING_MOVIE_NAME = "MovieName";
	public static final String UPCOMING_MOVIE_CODE = "MovieCode";
	public static final String UPCOMING_SHOW_DATE_TIME = "ShowDateTime";
	public static final String UPCOMING_SCREEN = "Screen";
	public static final String UPCOMING_SEAT_NOS = "SeatNos";
	public static final String UPCOMING_NO_OF_SEAT = "NoOfSeats";
	public static final String UPCOMING_TICKET_PRICE = "TicketPrice";
	public static final String UPCOMING_TOTAL_PRICE = "TotalPrice";
	public static final String UPCOMING_BOOKING_FEE = "BookingFee";
	public static final String UPCOMING_DISCOUNT = "Discount";
	public static final String UPCOMING_TOTAL_AMOUNT = "TotalAmt";
	public static final String UPCOMING_POINTS_REDEEM = "PointsRedeemed";
	public static final String UPCOMING_POINTS_EARNED = "PointsEarned";
	public static final String UPCOMING_IS_UPCOMING = "IsUpcomming";

}
