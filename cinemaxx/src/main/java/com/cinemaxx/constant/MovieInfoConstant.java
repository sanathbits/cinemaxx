package com.cinemaxx.constant;

public class MovieInfoConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String MOVIE_ARRAY = "Movies";
	public static final String MOVIE_ID = "MovieId";
	public static final String MOVIE_NAME = "MovieName";
	public static final String MOVIE_RATING = "MovieRatings";
	public static final String MOVIE_IMAGE = "ThumbnailImage";
	public static final String RELEASE_DATE = "ReleaseDate";
	public static final String LANGUAGE = "Language";
	public static final String CAST_N_CREW = "CastNCrew";
	public static final String DIRECTOR = "Director";
	public static final String GENER = "Gener";
	public static final String MOVIE_LENGTH = "MovieLength";
	public static final String MUSIC = "Music";
	public static final String SYNOPSYS = "Synopsys";
	public static final String TRAILER = "Trailer";
	public static final String WRITER = "Writer";
	public static final String IS_SCHEDULE_AVAILABLE = "IsScheduleAvail";
	public static final String IS_FAVOURITE = "IsFavourite";

}
