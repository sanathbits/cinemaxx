package com.cinemaxx.constant;

public class NewsDescriptionConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String LATEST_NEWS_ARRAY = "LatestNews";
	public static final String NEWS_ID = "NewsId";
	public static final String NEWS_TITLE = "NewsTitle";
	public static final String NEWS_MEDIA = "NewsMedia";
	public static final String PUBLISHED_DATE = "PublishDate";
	public static final String NEWS_DESCRIPTION = "NewsDescription";
	public static final String NEWS_IMAGE = "NewsImageURL";
	public static final String NEWS_VALID_FROM = "NewsValidFrom";
	public static final String NEWS_VALID_TO = "NewsValidTo";
	public static final String NEWS_STATUS = "NewsStatus";

}
