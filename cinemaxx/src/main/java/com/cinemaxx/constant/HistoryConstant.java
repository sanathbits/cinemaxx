package com.cinemaxx.constant;

public class HistoryConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String BOOKING_HISTORY_ARRAY = "BookingHistory";
	public static final String BOOKING_ID = "BookingId";
	public static final String BOOKING_NUMBER = "BookingNumber";
	public static final String ORDER_ID = "OrderId";
	public static final String SHOW_TIME = "ShowTime";
	public static final String MOVIE_NAME = "MovieName";
	public static final String TOTAL_PRICE = "TotalPrice";

}
