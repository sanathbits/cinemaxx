package com.cinemaxx.constant;

public class PaymentConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String BOOKING_ID = "BookingId";
	public static final String BOOKING_NUMBER= "BookingNumber";
	public static final String ORDER_ID = "OrderId";
	public static final String MOVIE_NAME = "MovieName";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String SHOW_TIME = "ShowTime";
	public static final String FORMAT = "Format";
	public static final String CINEMA_NUMBER = "CinemaNo";
	public static final String EMAIL = "Email";
	public static final String PAYMENT_MODE = "PaymentMode";
	public static final String SEAT_NUMBER = "SeatNumber";
	public static final String NO_OF_SEAT = "NoofSeats";
	public static final String PRICE_PER_TICKET = "PricePerTicket";
	public static final String TOTAL_TICKET = "TotalTicket";
	public static final String CONVINENCE_FEE = "ConvenieceFee";
	public static final String DISCOUNT_AMOUNT = "DiscountAmount";
	public static final String LOYALTY_POINTS = "LoyaltyPoints";
	public static final String LOYALTY_AMOUNT = "LoyaltyAmount";
	public static final String TOTAL_PRICE = "TotalPrice";
	public static final String QR_CODE = "QrCode";
	public static final String STATUS_CODE = "OrderStatus";

}
