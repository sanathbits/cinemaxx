package com.cinemaxx.constant;

public class FavMoviesConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String MOVIES_ARRAY = "Movies";
	public static final String MOVIE_ID = "MovieId";
	public static final String MOVIE_NAME = "MovieName";
	public static final String MOVIE_RATING = "MovieRatings";
	public static final String MOVIE_IMAGE = "ThumbnailImage";
	public static final String MOVIE_IS_COMING = "IsComingSoon";

}
