package com.cinemaxx.constant;

public class PromoConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String PROMO_ARRAY = "Promotions";
	public static final String PROMO_ID = "PromotionId";
	public static final String PROMO_TITLE = "PromotionTitle";
	public static final String PROMO_IMAGE = "PromotionImageURL";

}
