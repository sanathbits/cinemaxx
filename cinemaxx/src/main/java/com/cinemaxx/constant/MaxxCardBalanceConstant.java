package com.cinemaxx.constant;

public class MaxxCardBalanceConstant {

	public static final String BALANCE_LIST = "BalanceList";
	public static final String TICKET_TYPE_LIST = "TicketTypeList";
	public static final String DISCOUNT_LIST = "DiscountList";
	public static final String BALANCE_TYPE_ID = "BalanceTypeId";
	public static final String RECOGNITION_ID = "RecognitionID";
	public static final String NAME = "Name";
	public static final String DESCRIPTION = "Description";
	public static final String MESSAGE_TEXT = "MessageText";
	public static final String DISPLAY = "Display";
	public static final String IS_MANUALLY_SELECTED = "IsManuallySelected";
	public static final String QTY_AVAILABLE = "QtyAvailable";
	public static final String QTY_EARN = "QtyEarn";
	public static final String VISTA_ID = "VistaId";
	public static final String EXPIRY_DATE = "ExpiryDate";
	
}
