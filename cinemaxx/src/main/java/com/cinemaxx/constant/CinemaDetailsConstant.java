package com.cinemaxx.constant;

public class CinemaDetailsConstant {
	
	public static final String ERROR_CODE = "ErrorDesc";
	public static final String CINEMA_MAIN_ARRAY = "Cinemas";
	public static final String CINEMA_ID = "CinemaId";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String CINEMA_ADDRESS = "CinemaAddress";
	public static final String CINEMA_LOCATION = "CinemaLocation";
	public static final String CITY_CODE = "CityCode";
	public static final String CITY_NAME = "CityName";
	public static final String IS_FAVOURITE = "IsFavourite";
	
	public static final String CINEMA_SHOW_DATE_OBJ = "CinemaShowDate";
	public static final String CINEMA_SHOW_DATE_ARRAY = "CinemaShowDate";
	
	public static final String CINEMA_MOVIE_LIST_SHOW_DATE = "ShowDate";
	public static final String CINEMA_MOVIE_LIST_SHOW_DATE_DISP = "ShowDateDisp";
	public static final String CINEMA_MOVIE_LIST_OBJ = "CinemaMovieList";
	public static final String CINEMA_MOVIE_LIST_ARRAY = "CinemaMovieList";
	
	public static final String CINEMA_MOVIE_ID = "MovieId";
	public static final String CINEMA_MOVIE_NAME = "MovieName";
	public static final String CINEMA_MOVIE_FORMAT = "MovieFormat";
	public static final String CINEMA_MOVIE_THUMBNAIL = "ThumbnailImage";
	
	public static final String CINEMA_DETAILS_CLASS_OBJ = "CinemaDetailClass";
	public static final String CINEMA_DETAILS_CLASS_ARRAY = "CinemaDetailClass";
	public static final String CINEMA_DETAILS_CLASS_ID = "CinemaClassId";
	public static final String CINEMA_DETAILS_CLASS_NAME = "CinemaClass";
	
	public static final String CINEMA_DETAILS_SHOW_TIME_OBJ = "CinemaShowTime";
	public static final String CINEMA_DETAILS_SHOW_TIME_ARRAY = "CinemaShowTime";
	
	public static final String CINEMA_DETAILS_SHOW_TIME_SCHEDULE_ID = "ScheduleId";
	public static final String CINEMA_DETAILS_SHOW_TIME_SCHEDULE_TIME = "ShowTime";
	public static final String CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_FORMAT = "MovieFormat";
	public static final String CINEMA_DETAILS_SHOW_TIME_SCHEDULE_MOVIE_PRICE = "Price";
	

}
