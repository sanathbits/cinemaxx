package com.cinemaxx.constant;

public class OrderConstant {
	
	public static final String ORDER_DATE = "OrderDate";
	public static final String ORDER_ID = "OrderId";
	public static final String ORDER_NO = "OrderNo";
	public static final String SUMMARY_DET_ARRAY = "SummaryDet";
	public static final String CHARGES = "Charges";
	public static final String ORDER_AMOUNT = "OrderAmt";
	public static final String QUENTITY = "Qty";
	public static final String DISCOUNT_AMOUNT = "DiscountAmt";
	public static final String TOTAL_PRICE = "TotalPrice";

}
