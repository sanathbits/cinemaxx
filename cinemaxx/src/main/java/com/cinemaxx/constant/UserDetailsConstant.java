package com.cinemaxx.constant;

public class UserDetailsConstant {
	
	public static final String STATUS_CODE = "status_code";
	public static final String STATUS_MESSAGE = "status_message";
	public static final String TRANSACTION_ID = "transaction_id";
	public static final String ORDER_ID = "order_id";
	public static final String PAYMENT_TYPE = "payment_type";
	public static final String TRANSACTION_TIME = "transaction_time";
	public static final String TRANSACTION_STATUS = "transaction_status";
	public static final String FRAUD_STATUS = "fraud_status";
	public static final String MASKED_CARD = "masked_card";
	public static final String APPORVED_CODE = "approval_code";
	public static final String GROSS_AMOUNT = "gross_amount";
	

}
