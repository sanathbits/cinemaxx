package com.cinemaxx.constant;

public class CinemaConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String CINEMA_ARRAY = "Cinemas";
	public static final String CINEMA_CLASS_DATA = "CinemaClassData";
	public static final String CINEMA_CLASS_ARRAY = "CinemaClasses";
	public static final String CINEMA_CLASS_ID = "CinemaClassId";
	public static final String CINEMA_CLASS_NAME = "CinemaClassName";
	
	public static final String CINEMA_ID = "CinemaId";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String MULTIPLEX_ID = "MultiplexId";
	public static final String ADDRESS = "Address";
	public static final String CITY_ID = "CityId";
	public static final String LOCATION = "Location";
	public static final String LATITUDE = "Lat";
	public static final String LONGITUDE = "Long";
	public static final String FACS = "Facs";
	public static final String THUMBNAIL = "Thumbnail";
	public static final String CITY_NAME = "CityName";
	public static final String PHONE = "Phone";
	public static final String EMAIL = "Email";
	public static final String DISTANCE = "Distance";

}
