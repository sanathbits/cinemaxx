package com.cinemaxx.constant;

public class CinemaJunior {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String CINEMAS_ARRAY = "Cinemas";
	public static final String CINEMA_ID = "CinemaId";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String THUMBNAIL = "Thumbnail";
	public static final String PHONE = "Phone";
	public static final String CINEMA_CLASS_DATA = "CinemaClassData";
	public static final String CINEMA_CLASS_ARRAY = "CinemaClasses";
	public static final String CINEMA_CLASS_ID = "CinemaClassId";
	public static final String CINEMA_CLASS_NAME = "CinemaClassName";
	public static final String CINEMA_CLASS_ICON = "CinemaClassIcon";
	
	
	public static final String CITY_ARRAY = "Cities";
	public static final String CITY_ID = "CityId";
	public static final String CITY_NAME = "CityName";
	
	public static final String DATE_ARRAY = "ScheduleShowDate";
	public static final String SHOW_DATE_DISP = "ShowDateDisp";
	public static final String SHOW_DATE = "ShowDate";
	
	public static final String TODAY_ARRAY = "TodaySchedule";
	public static final String TODAY_CINEMA_ID = "CinemaId";
	public static final String TODAY_CINEMA_NAME = "CinemaName";
	public static final String TODAY_CITY_NAME = "CityName";
	public static final String TODAY_MOVIE_LIST = "TodayMovieList";
	public static final String TODAY_MOVIE_LIST_ARRAY = "TodayMovieList";
	public static final String TODAY_MOVIE_ID = "MovieId";
	public static final String TODAY_MOVIE_NAME = "MovieName";
	public static final String TODAY_MOVIE_IMAGE = "ThumbnailImage";
	public static final String TODAY_MOVIE_ISCOMING = "IsComingSoon";
	public static final String TODAY_CINEMA_CLASS = "TodayCinemaClass";
	public static final String TODAY_CINEMA_CLASS_ARRAY = "TodayCinemaClass";
	public static final String TODAY_CLASS_ID = "CinemaClassId";
	public static final String CINEMA_CLASS = "CinemaClass";
	public static final String TODAY_SHOW_TIME_DATA = "TodayShowTimeData";
	public static final String TODAY_SHOW_TIME_ARRAY = "TodayShowTime";
	public static final String TODAY_SHOW_TIME = "ShowTime";
	public static final String TODAY_SCHEDULE_ID = "ScheduleId";
	public static final String TODAY_MOVIE_FORMAT = "MovieFormat";
	public static final String TODAY_SCREEN_NAME = "ScreenName";
	public static final String TODAY_PRICE_LIST = "PriceList";
	public static final String TODAY_IS_ACTIVE = "IsActive";

}
