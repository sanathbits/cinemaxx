package com.cinemaxx.constant;

public class MaxxCardDetailsConstant {
	
	
	public static final String LOYALTY_REDEEM_TYPE = "LoyaltyRedeemType";
	public static final String ORDER_ID = "OrderId";
	public static final String ERROR_DESC = "ErrDesc";
	public static final String MEMBER_DETAILS_OBJ = "MemberDetails";
	public static final String MEMBER_ID = "MemberId";
	public static final String USER_NAME = "UserName";
	public static final String PASSWORD = "Password";
	public static final String CLUB_ID = "ClubId";
	public static final String CLUB_NAME = "ClubName";
	public static final String LEVEL_ID = "LevelId";
	public static final String LEVEL_NAME = "LevelName";
	public static final String CARD_NUMBER = "CardNumber";
	public static final String FIRST_NAME = "FirstName";
	public static final String LAST_NAME = "LastName";
	public static final String GENDER = "Gender";
	public static final String MERITAL_STATUS = "MaritalStatus";
	public static final String DOB = "DateOfBirth";
	public static final String EMAIL = "EMail";
	public static final String HOME_PHONE = "HomePhone";
	public static final String MOBILE_PHONE = "MobilePhone";
	public static final String ADDRESS = "Address";
	public static final String PERSONS_INHOUSEHOLD = "PersonsInHousehold";
	public static final String HOUSE_HOLD_INCOME = "HouseholdIncome";
	public static final String WISH_TO_RECEIVE_SMS = "WishToReceiveSMS";
	public static final String SEND_NEWS_LETTER = "SendNewsletter";
	public static final String MAILING_FREQUENCY = "MailingFrequency";
	public static final String EXPIRY_DATE = "ExpiryDate";
	public static final String PREFERENCE_LIST_ARRAY = "PreferenceList";
	public static final String PREFERENCE_GENRE_ARRAY = "PreferredGenres";
	public static final String STATUS = "Status";
	public static final String MEMBERSHIP_ACTIVATED = "MembershipActivated";
	public static final String BALANCE_LIST_ARRAY = "BalanceList";
	public static final String BALANCE_TYPE_ID = "TypeId";
	public static final String BALANCE_NAME = "Name";
	public static final String BALANCE_MESSAGE = "Message";
	public static final String BALANCE_DISPLAY = "LifetimePointsBalanceDisplay";
	public static final String BALANCE_REMAINING_POINT = "PointsRemaining";
	public static final String BALANCE_IS_DEFAULT = "IsDefault";
	public static final String IS_PROFILE_COMPLETE = "IsProfileComplete";
	public static final String LOYALTY_REQUEST_ID = "LoyaltyRequestId";
	
	public static final String POINTS_EXPIRY_LIST_ARRAY = "PointsExpiryList";
	public static final String POINTS_EXPIRIRING = "PointsExpiring";
	public static final String EXPIRE_ON = "ExpireOn";
	public static final String BALANCE_TYPEID = "BalanceTypeId";
	public static final String STAR_CLASS_CRITERIA = "StarClassCriteria";
	public static final String MAX_CARD_IMAGE = "MaxxCardImage";
	
	
}
