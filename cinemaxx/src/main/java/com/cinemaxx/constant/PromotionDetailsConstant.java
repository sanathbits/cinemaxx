package com.cinemaxx.constant;

public class PromotionDetailsConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String PROMO_ARRAY = "Promotions";
	public static final String PROMO_ID = "PromotionId";
	public static final String PROMO_TITLE = "PromotionTitle";
	public static final String PROMO_DESC = "PromotionDescription";
	public static final String PROMO_IMAGE = "PromotionImageURL";
	public static final String PROMO_VALID_FROM = "PromotionValidFrom";
	public static final String PROMO_VALID_TO = "PromotionValidTo";
	public static final String PROMO_STATUS = "PromotionStatus";
	public static final String IS_VALID = "IsFavourite";

}
