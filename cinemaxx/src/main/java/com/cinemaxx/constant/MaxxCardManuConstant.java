package com.cinemaxx.constant;

public class MaxxCardManuConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String WEBCONTENTS = "WebContents";
	public static final String PAGECONTENT = "PageContent";
	
	public static final String LATESTMAXXCARDNEWS = "LatestMaxxCardNews";
	public static final String MAXCARDNEWSID = "MaxxCardNewsId";

	public static final String MAXCARDNEWSTITLE = "MaxxCardNewsTitle";
	public static final String MAXCARDNEWSMEDIA = "MaxxCardNewsMedia";
	public static final String MAXXCARDNEWSDESCRIPTION = "MaxxCardNewsDescription";
	public static final String MAXCARDNEWPUBLICDATE = "MaxxCardNewPublishDate";
	public static final String MAXXCARDNEWSIMAGEURL = "MaxxCardNewsImageURL";
	public static final String MAXCARDNEWSVALIDFROM = "MaxxCardNewsValidFrom";
	public static final String MAXCARDNEWSVALIDTO = "MaxxCardNewsValidTo";
	public static final String MAXXCARDSTATUS = "MaxxCardNewsStatus";

	public static final String LATESTMAXXCARDSPECIALOFFER = "LatestMaxxCardSpecialOffer";

	public static final String SPECIALOFFERID = "SpecialOfferId";
	public static final String SPECIALOFFERTITLE = "SpecialOfferTitle";
	public static final String SPECIALOFFERDESCRIPTION = "SpecialOfferDescription";
	public static final String SPECIALOFFERIMAGEURL = "SpecialOfferImageURL";
	
	public static final String LATESTMAXXCARDSTARCLASSSPECIALOFFER = "LatestMaxxCardStarClassSpecialOffer";

	public static final String STARCLASSSPECIALOFFERID = "StarClassSpecialOfferId";
	public static final String STARCLASSSPECIALOFFERTITLE = "StarClassSpecialOfferTitle";
	public static final String STARCLASSSPECIALOFFERDESCRIPTION = "StarClassSpecialOfferDescription";
	public static final String STARCLASSSPECIALOFFERIMAGEURL = "StarClassSpecialOfferImageURL";
}
