package com.cinemaxx.constant;

public class SeatLayoutConstant {
	
	public static final String ERROR_DESC = "ErrDesc";
	public static final String REQUEST_ID = "RequestId";
	public static final String CINEMA_CLASS_ID = "CinemaClassId";
	public static final String CINEMA_CLASS_NAME = "CinemaClass";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String CINEMA_NO = "CinemaNo";
	public static final String CLASS_LIST = "ClassList";
	public static final String BOOKING_FEE = "BookingFee";
	public static final String PRICE = "Price";
	public static final String MAX_SEAT = "MaxSeats";
	public static final String MOVIE_FORMAT = "MovieFormat";
	public static final String MOVIE_NAME = "MovieName";
	public static final String MOVIE_RATING = "MovieRatings";
	public static final String SHOW_DATE = "ShowDate";
	public static final String SHOW_TIME = "ShowTime";
	public static final String LAYOUT = "Layout";
	public static final String FORMAT = "Format";
	public static final String SCREEN = "ScreenName";
	
	public static final String AREA_CODE = "AreaCode";
	public static final String AVAILABLE_SEATS = "AvailableSeats";
	public static final String QTY = "Qty";
	public static final String SCHEDULE_CLASS_ID = "ScheduleClassId";
	public static final String SEAT_CLASS = "SeatClass";
	public static final String IS_LOYALTY = "IsLoyalty";
	public static final String IS_PACKAGE = "IsPackage";
	public static final String MAXQTY = "maxQty";
	public static final String MIN_NO_OF_SEATS = "minNoOfSeats";
	public static final String MINQTY = "minQty";
	public static final String PACKAGE_NAME = "PackageName";

}
