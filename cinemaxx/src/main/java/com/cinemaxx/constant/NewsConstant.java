package com.cinemaxx.constant;

public class NewsConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String NEWS_ARRAY = "LatestNews";
	public static final String NEWS_ID = "NewsId";
	public static final String NEWS_TITLE = "NewsTitle";
	public static final String NEWS_IMAGE = "NewsImageURL";

}
