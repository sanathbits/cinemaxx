package com.cinemaxx.constant;

public class CityConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String CITY_ARRAY = "Cities";
	public static final String CITY_ID = "CityId";
	public static final String CITY_NAME = "CityName";

}
