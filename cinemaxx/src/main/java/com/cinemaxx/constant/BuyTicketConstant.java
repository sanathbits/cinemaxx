package com.cinemaxx.constant;

public class BuyTicketConstant {

	public static final String ERROR_DESC = "ErrorDesc";
	public static final String MOVIE_ARRAY = "Movies";
	public static final String MOVIE_ID = "MovieId";
	public static final String MOVIE_NAME = "MovieName";
	public static final String MOVIE_RATING = "MovieRatings";
	
	public static final String CITY_ARRAY = "Cities";
	public static final String CITY_NAME = "CityName";
	public static final String CITY_ID = "CityId";
	
	public static final String CINEMA_ARRAY = "Cinemas";
	public static final String CINEMA_NAME = "CinemaName";
	public static final String CINEMA_ID = "CinemaId";
	
	public static final String CINEMA_TYPE_ARRAY = "CinemaClasses";
	public static final String CINEMA_TYPE_NAME = "CinemaClassName";
	public static final String CINEMA_TYPE_ICON = "CinemaClassIcon";
	public static final String CINEMA_TYPE_ID = "CinemaClassId";
	
	public static final String DATE_ARRAY = "ShowDates";
	public static final String SHOW_DATE = "ShowDate";
	public static final String SCHEDULE_ID = "ScheduleId";
	
	public static final String TIME_ARRAY = "ShowTimeList";
	public static final String TIME_VALUE = "TimeValue";
	public static final String TIME = "Time";
	
}
