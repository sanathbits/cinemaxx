package com.cinemaxx.constant;

public class NowPlayingConstant {
	
	public static final String ERROR_DESC = "ErrorDesc";
	public static final String MOVIE_ARRAY = "Movies";
	public static final String MOVIE_ID = "MovieId";
	public static final String MOVIE_NAME = "MovieName";
	public static final String MOVIE_RATING = "MovieRatings";
	public static final String MOVIE_IMAGE = "ThumbnailImage";

}
